# Description
<!-- Describe the desired feature as precisely as possible. -->


# Proposal for implementation
<!-- Propose any suggestions or details on how this feature could be implemented. -->


# Links and references
<!-- Provide any external sources related to the feature or the proposed implementation details. -->

/label ~"suggestion:feature"
