# Story
<!-- Shortly describe what the aim of the story is (max. 3 sentences). -->
<!-- Follow the schema: As <Role> I want  <Function> to achieve <Benefit>. -->

### Priority
<!-- Rate it with a priority from 1 to 10 -->

### Weight
<!-- Give it a weight on Fibonacci scale (1, 2, 3, 5, 8, 13) that estimates the amount of work for this story -->

# Details
<!-- Here you can give more detailed information about the story. -->

# Proposal and suggestions
<!-- If possible, propose ideas on how to fulfill the story. -->

# Acceptance criteria
<!-- Define a number of criteria that serve as a measure as to when the story is fulfilled/finished. -->


# How does success look like and how can we measure it?
<!-- What are the benefits of the story? -->

/label ~"type:story"
