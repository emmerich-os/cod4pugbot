# Description
<!-- Shortly describe what has to be done (max. 3 sentences). -->

# Further details
<!-- Give further details about the task. -->

# Solution suggestions
<!-- Give suggestions on how the task may be fulfilled. -->

# Risks and tests
<!-- Are there any risks and how can the results of the tasks be tested? -->

# DoD
<!-- Give a set of criteria that define when the task is finished. -->

- The implemented code is tested (public methods required, private methods mandatory).

/label ~"type:task"

