# COD4PUGBot
[![pipeline status](https://gitlab.com/emmerich-os/cod4pugbot/badges/master/pipeline.svg)](https://gitlab.com/emmerich-os/cod4pugbot/-/commits/master)
[![coverage report](https://gitlab.com/emmerich-os/cod4pugbot/badges/master/coverage.svg)](https://gitlab.com/emmerich-os/cod4pugbot/-/commits/master)
[![](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)

COD4PUGBot is a Discord bot designed for different tasks in a Discord server.
But most importantly it was designed to manage CoD4 PUGs of different kinds.
These can be enabled or disabled as the user desires.

The features include:

* different PUG modes (BO1, BO2, BO3 and/or BO5) and sizes (1v1, 2v2, 3v3 and/or 5v5):
    - add/remove player
    - player picking
    - map picking or random map choice
    - adding server information
        + see server information
        + scoreboard
        + killfeed
        + stats tracking
* user actions:
    - register
    - kick
    - ban (global and PUG)
* news posting
* moderation:
    - use of English
    - censoring of explicit language
* report system:
    - abusive behavior, cheating etc.
    - bot errors
