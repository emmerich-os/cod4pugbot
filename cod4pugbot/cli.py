import sys

import click

from .cod4pugbot import COD4PUGBot
from .config import read_config
from .domain.cogs import COGS
from .logger import setup_logger


@click.command()
@click.option(
    "path",
    "--config",
    "-c",
    type=click.Path(exists=True),
    default="../config.yml",
    help="path to bot YAML config",
    show_default=True,
)
def run_bot(path):
    """Run the bot."""
    config = read_config(path=path)
    bot = COD4PUGBot(config=config)
    setup_logger()

    for cog, init in bot.activated_cogs.items():
        if init:
            bot.add_cog(COGS[cog](bot))

    bot.run(bot.token, bot=True)


if __name__ == "__main__":
    run_bot(sys.argv[1])
