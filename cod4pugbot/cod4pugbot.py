import discord
import pytz
from discord.ext import commands

from cod4pugbot import db
from cod4pugbot import domain
from cod4pugbot.service_layer import handlers
from cod4pugbot.service_layer import messagebus
from cod4pugbot.service_layer import unit_of_work


class COD4PUGBot(commands.Bot):
    """COD4PUGBot.

    Attributes:
        statuses : list
            Bot statuses to cycle.
        features : dict
            Contains information which features should be activated.
        activated_cogs : dict
            Contains information which Cogs should be activated.
        category_ids : dict
            Category IDs of the categories the bot uses.
        channel_ids : dict
            Channel IDs of all channels the bot uses.
        role_ids : dict
            Role IDs of the roles the bot requires.
        messages : dict
            Messages the bot sends.
        pugs : dict
            PUG details.
        token : str
            Bot token.
        db_info : dict
            Database information.
            Stored values are:
              * database
              * host
              * port
              * user
              * password
        timezone : `pytz.timezone`
            Time zone the bot uses.
        timestring : str
            Date and time format the bot uses for `datetime` dates.

    """

    def __init__(
        self,
        config: dict,
        simple_uow: unit_of_work.AbstractUnitOfWork = unit_of_work.UnitOfWork,
        database_uow: unit_of_work.AbstractUnitOfWork = unit_of_work.DatabaseUnitOfWork,
    ) -> None:
        """Set main properties of bot.

        Parameters:
            config : dict
                Bot config.

        """
        self.simple_uow = simple_uow
        self.database_uow = database_uow
        intents = discord.Intents.none()
        intents.guilds = True
        intents.members = True
        intents.bans = True
        intents.messages = True
        intents.presences = True
        intents.guild_reactions = True
        super().__init__(command_prefix=config["command_prefix"], intents=intents)
        self.statuses = config["bot_statuses"]
        self.features = {
            "bot_status": config["features"]["bot_status"],
            "contact_admin": config["features"]["contact_admin"],
            "pugs": config["features"]["pugs"],
            "welcome_message": config["features"]["private_welcome_message"],
            "register": config["features"]["register"],
            "report": {
                "report_errors": config["features"]["report_errors"],
                "report_members": config["features"]["report_members"],
            },
        }
        self.activated_cogs = {
            "administration": True,
            "connect": True,
            "contact_admin": config["features"]["contact_admin"],
            "errors": True,
            "initializer": True,
            "join": config["features"]["private_welcome_message"],
            "loader": True,
            "pugs": config["features"]["pugs"],
            "register": config["features"]["register"],
            "report": config["features"]["report_members"],
            "updater": True,
        }
        self.category_ids = config["category_ids"]
        self.channel_ids = config["channel_ids"]
        self.role_ids = config["role_ids"]
        self.messages = config["messages"]
        self.pugs = config["pugs"]
        self.token = config["token"]
        self.db_info = config["dbinfo"]
        # tz = time.strftime("%Z", time.localtime())
        # if tz == "CEST":
        #     tz = "CET"
        self.timezone = pytz.timezone("UTC")
        self.tzinfos = {"CEST": "UTC+2"}
        self.timestring = "%d/%m/%Y %H:%M:%S %Z"
        self.roles_new = {
            "head_admin": {
                "name": "Head admin",
                "id": None,
                "color": discord.Color.dark_red(),
                "permissions": discord.Permissions(
                    administrator=True,
                    ban_members=True,
                    kick_members=True,
                    read_message_history=True,
                    send_messages=True,
                    add_reactions=True,
                ),
                "hoist": True,
                "mentionable": True,
            },
            "admin": {
                "name": "Admin",
                "id": None,
                "color": discord.Color.red(),
                "permissions": discord.Permissions(
                    administrator=True,
                    ban_members=True,
                    kick_members=True,
                    read_message_history=True,
                    send_messages=True,
                    add_reactions=True,
                ),
                "hoist": True,
                "mentionable": True,
            },
            "pug_captain": {
                "name": "PUG Captain",
                "id": None,
                "color": discord.Color.gold(),
                "permissions": None,
                "hoist": False,
                "mentionable": False,
            },
            "registered": {
                "name": "Registered",
                "id": None,
                "color": discord.Color.green(),
                "permissions": None,
                "hoist": True,
                "mentionable": True,
            },
            "unregistered": {
                "name": "Member",
                "id": None,
                "color": discord.Color.greyple(),
                "permissions": None,
                "hoist": True,
                "mentionable": True,
            },
            "global_banned": {
                "name": "GLOBAL banned",
                "id": None,
                "color": discord.Color.from_rgb(0, 0, 0),
                "permissions": discord.Permissions(
                    send_messages=False, add_reactions=False
                ),
                "hoist": False,
                "mentionable": False,
            },
            "pug_banned": {
                "name": "PUG banned",
                "id": None,
                "color": 0x324047,
                "permissions": discord.Permissions(),
                "hoist": False,
                "mentionable": False,
            },
        }
        self.categories_new = {
            "admin_section": {
                "name": "ADMIN SECTION",
                "id": None,
                "channels": {
                    "register": {"name": "\U0001F513-register-requests", "id": None},
                    "report": {"name": "\u26a0-reports", "id": None},
                    "administration": {"name": "\u26a1-administration", "id": None},
                    "errors": {"name": "\U0001F916-errors", "id": None},
                },
            },
            "support": {
                "name": "SUPPORT",
                "id": None,
                "channels": {
                    "register": {"name": "\U0001F5C3-register", "id": None},
                    "report": {"name": "\U0001F6A8-report", "id": None},
                    "bans": {"name": "\U0001F6AB-bans", "id": None},
                },
            },
            "pugs": {},
        }

    async def handle_command_with_db_access(
        self, command: domain.commands.Command, called_by_bot: bool = False
    ) -> None:
        """Handle a command with a message bus that has a DB connection.

        Parameters:
            command : commands.Command
                COD4PUGBot domain command to handle.
            called_by_bot : bool, default False
                Whether following commands are intended to be called with bot rights.
                E.g. only the bot is allowed to force delete members.

        """
        message_bus = self._instantiate_sql_message_bus(called_by_bot=called_by_bot)
        await message_bus.handle(command)

    async def handle_command_without_db_access(
        self, command: domain.commands.Command
    ) -> None:
        """Handle a given command with simple message bus."""
        message_bus = self._instantiate_simple_message_bus()
        await message_bus.handle(command)

    async def handle_event(self, event: domain.events.Event) -> None:
        """Handle given event with simple message bus."""
        message_bus = self._instantiate_simple_message_bus()
        await message_bus.handle(event)

    def _instantiate_sql_message_bus(
        self, called_by_bot: bool = False
    ) -> messagebus.MessageBus:
        """Instantiate a message bus with an SQL unit of work.

        Parameters:
            called_by_bot : bool, default False
                Whether following commands are intended to be called with bot rights.
                E.g. only the bot is allowed to force delete members.

        """
        database_client = db.SQLClient(**self.db_info)
        uow = self.database_uow(
            client=database_client,
            called_by_bot=called_by_bot,
        )
        return messagebus.MessageBus(
            uow=uow,
            command_handlers=handlers.COMMAND_HANDLERS,
            event_handlers=handlers.EVENT_HANDLERS,
        )

    def _instantiate_simple_message_bus(self) -> messagebus.MessageBus:
        """Instantiate a message bus."""
        uow = self.simple_uow()
        return messagebus.MessageBus(
            uow=uow,
            command_handlers=handlers.COMMAND_HANDLERS,
            event_handlers=handlers.EVENT_HANDLERS,
        )
