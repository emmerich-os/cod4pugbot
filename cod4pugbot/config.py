import json
import logging
import os
import re

import yaml

logger = logging.getLogger(__name__)


def build_config(path: str = "./../config.yml") -> None:
    """Build the default bot config.

    Parameters:
        path : str
            File name and path where to save the config.

    """
    config = CONFIG_TEMPLATE

    with open(path, "w+") as outfile:
        yaml.safe_dump(config, outfile, default_flow_style=False)


def _get_env_vars(config: dict) -> None:
    """Get environment variables for each entry in config.

    Parameters:
        config : dict
            Config.

    """
    for key, value in config.items():
        if isinstance(value, str) and (match := re.match(r"\$\{(.*)\}", value)):
            if group := match.group(1):
                var = os.getenv(group)
                try:
                    config[key] = int(var)
                except ValueError:
                    config[key] = var
        elif isinstance(value, dict):
            _get_env_vars(config=value)


def read_config(path: str) -> dict:
    """Read the given YAML config.

    Parameters:
        path : str
            Path to the YAML config
    Returns
        config : dict
            Contains the config.

    """
    with open(path) as configfile:
        config = yaml.safe_load(configfile)
        _get_env_vars(config)
    return config


def save_to_config(keys: tuple, value, path: str = "./../config.yml") -> None:
    """Save any detail to the bot config.

    Parameters:
        keys : tuple
            Keys that represent the `dict` tree in which to save a value.
        value
            Value to save at the given tree position.
        path : str, optional
            Path to config.
            Defaults to `"./../config.yml"`.

    """
    config = read_config(path=path)
    set_value(d=config, keys=keys, value=value)
    logger.debug("%s", json.dumps(config, indent=4, default=str))
    with open(path, "w+") as outfile:
        yaml.safe_dump(config, outfile, default_flow_style=False)


def set_value(d, keys: tuple, value) -> None:
    """Set a value in a dict tree.

    Parameters:
        d : dict
            `dict` in which the value has to be set.
        keys : tuple
            Keys that represent the `dict` tree in which to save a value.
        value
            Value to save at the given tree position.

    """
    for key in keys[:-1]:
        d = d.setdefault(key, {})
    d[keys[-1]] = value


CONFIG_TEMPLATE = {
    "bot_statuses": ["Active", "Welcome to my Discord server!"],
    "category_ids": {
        "admin_section": None,
        "support": None,
        "bo1": None,
        "bo3": None,
        "running_pugs": None,
    },
    "dbinfo": {
        "database": "${DB_NAME}",
        "user": "${DB_USER}",
        "password": "${DB_USER_PASSWORD}",
        "host": "${DB_HOST}",
        "port": "${DB_PORT}",
    },
    "channel_ids": {
        "admin_section": {
            "register": None,
            "report": None,
            "errors": None,
            "administration": None,
        },
        "support": {"register": None, "bans": None, "report": None},
        "news": None,
        "pugs": {
            "sub": None,
            "bo1": {"1v1": None, "2v2": None, "3v3": None, "5v5": None},
            "bo2": {"1v1": None, "2v2": None, "3v3": None, "5v5": None},
            "bo3": {"1v1": None, "2v2": None, "3v3": None, "5v5": None},
            "bo5": {"1v1": None, "2v2": None, "3v3": None, "5v5": None},
        },
    },
    "command_prefix": "!",
    "features": {
        "bot_status": True,
        "private_welcome_message": True,
        "register": True,
        "contact_admin": True,
        "pugs": True,
        "report_members": True,
        "report_errors": True,
    },
    "messages": {
        "welcome_message": "Hi {self.member.name}, welcome to the "
        "{ctx.Guild.name} Discord!",
    },
    "pugs": {
        "modes": {
            "bo1": {"1v1": True, "2v2": True, "3v3": True, "5v5": True},
            "bo3": {"1v1": True, "2v2": True, "3v3": True, "5v5": True},
        },
        "maps_5v5": ["Backlot", "Citystreets", "Crash", "Crossfire", "Strike"],
        "maps_3v3": [
            "Backlot",
            "Citystreets",
            "Crash",
            "Crossfire",
            "Strike",
            "Vacant",
        ],
        "maps_2v2": [
            "Backlot",
            "Citystreets",
            "Crash",
            "Crossfire",
            "Strike",
            "Vacant",
        ],
        "maps_1v1": [
            "Backlot",
            "Citystreets",
            "Crash",
            "Crossfire",
            "Strike",
            "Vacant",
        ],
        "map_elimination": True,
        "ready_up_phase_length": 120,
    },
    "role_ids": {
        "head_admin": None,
        "admin": None,
        "unregistered": None,
        "registered": None,
        "pug_captain": None,
        "global_banned": None,
        "pug_banned": None,
    },
    "token": "${TOKEN}",
}

if __name__ == "__main__":
    build_config()
