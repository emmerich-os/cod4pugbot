import abc
import configparser
import inspect
import logging
from typing import Any
from typing import Union

import psycopg2.extensions
import psycopg2.extras
from psycopg2 import sql

from . import _query_builder as query_builder

logger = logging.getLogger(__name__)


class Client(abc.ABC):
    """A client for database interaction."""

    @classmethod
    @abc.abstractmethod
    def from_config(cls, path: str) -> object:
        """Create instance from Unix shell-like config."""

    def __enter__(self):
        """Return instance when entering context manager."""
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        """Close database connection on exit."""
        self.__del__()

    @abc.abstractmethod
    def __del__(self) -> None:
        """Close the connection to the database and delete any other resources."""

    @abc.abstractmethod
    def insert(self, table: str, called_by_bot: bool = False, **kwargs) -> None:
        """Insert values into table."""

    @abc.abstractmethod
    def insert_and_return_row_id(
        self, table: str, called_by_bot: bool = False, **kwargs
    ) -> int:
        """Insert values into table and return the inserted row's ID."""

    @abc.abstractmethod
    def select(
        self,
        view: str,
        subtable: str = None,
        **kwargs,
    ) -> list[dict[str, Any]]:
        """Select columns from table."""

    @abc.abstractmethod
    def update(
        self, table: str, where: dict[str, Any], called_by_bot: bool = False, **kwargs
    ) -> None:
        """Update entry."""

    @abc.abstractmethod
    def delete(
        self, table: str, force: bool = False, called_by_bot: bool = False, **kwargs
    ) -> None:
        """Delete entry."""


class SQLClient(Client):
    """SQL Client to connect do the database and read from/write to it."""

    def __init__(self, user: str, password: str, host: str, port: int, database: str):
        """Initialize SQL Client.

        Parameters:
            user : str
                Database user name.
            password : str
                Password for given user.
            host : str
                IP address or hostname of database host.
            port : int
                Port at which the database accepts connections.
            database : str
                Name of the database.

        """
        self.user = user
        self.password = password
        self.host = host
        self.port = port
        self.database = database
        self._connection = None

    @classmethod
    def from_config(cls, path: str) -> object:
        """Create instance from Unix shell-like config.

        Parameters:
            path : str
                Path to config file.

        """
        config = configparser.ConfigParser()
        config.read(path)
        if "db_info" not in config.sections():
            raise ValueError("Config missing 'db_info' section")
        for key in inspect.signature(cls).parameters:
            if key not in config["db_info"]:
                raise ValueError("Section 'db_info' is missing key %s", key)
        return cls(**config["db_info"])

    def __enter__(self):
        """Return instance when entering context manager."""
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        """Close database connection on exit."""
        self.__del__()

    def __del__(self):
        """Close database connection on deletion."""
        if self._connection is not None:
            self._connection.close()
            self._connection = None

    @property
    def credentials(self) -> dict[str, Union[str, int]]:
        """Return username, password, host, port, and dbname as dict."""
        return {
            "user": self.user,
            "password": self.password,
            "host": self.host,
            "port": self.port,
            "database": self.database,
        }

    @property
    def connection(self) -> psycopg2.extensions.connection:
        """Create SQL database connection.

        Returns
        -------
        psycopg2.extensions.connection

        """
        if self._connection is None:
            self._connection = psycopg2.connect(**self.credentials)
            logger.debug(
                "Connected to database %s as user %s", self.database, self.user
            )
            self.connection.autocommit = True
        return self._connection

    def _set_active_status(
        self,
        table: str = None,
        called_by_bot: bool = False,
        insert: bool = False,
        delete: bool = False,
        **kwargs,
    ) -> bool:
        """Set a member in the members table to active or inactive.

        Parameters:
            table : str, optional
                Defaults to `None`.
            called_by_bot : bool, optional
                Whether function is called by the bot.
                Defaults to `False`.
            insert : bool, optional
                Whether insert is performed.
                Defaults to `False`.
            delete : bool, optional
                Whether delete is performed.
                Defaults to `False`.

        """
        exists = True if self.select(view=table, **kwargs) else False
        if exists and delete:
            self.update(
                table=table,
                where=kwargs,
                called_by_bot=called_by_bot,
                **{**kwargs, "active": False},
            )
            return True
        elif exists and insert:
            self.update(
                table=table,
                where=kwargs,
                called_by_bot=called_by_bot,
                **{**kwargs, "active": True},
            )
            return True
        return False

    def insert(self, table: str, called_by_bot: bool = False, **kwargs) -> None:
        """Insert values into table.

        Parameters:
            table : str
                Table name.
            called_by_bot : bool, default False
                If `True`, insert is performed by the bot.

        """
        with self.connection.cursor() as cursor:
            if table == "members" and self._set_active_status(
                table=table, insert=True, called_by_bot=called_by_bot, **kwargs
            ):
                return
            query = query_builder.create_insert_query(table=table, **kwargs)
            logger.debug("Writing to database: %s", query.as_string(self.connection))
            cursor.execute(query)
        return

    def insert_and_return_row_id(
        self, table: str, called_by_bot: bool = False, **kwargs
    ) -> int:
        """Insert values into table and return the inserted row's ID.

        Parameters:
            table : str
                Table name.
            called_by_bot : bool, optional
                If `True`, insert is performed by the bot.
                Only the bot is allowed to change name and/or Discord ID.
                Defaults to `False`.

        Returns:
            int
                ID of inserted row.

        """
        with self.connection.cursor() as cursor:
            if table == "members" and self._set_active_status(
                table=table, insert=True, called_by_bot=called_by_bot, **kwargs
            ):
                return
            query = query_builder.create_insert_query(table=table, **kwargs)
            logger.debug("Writing to database: %s", query.as_string(self.connection))
            cursor.execute(query)
            return cursor.lastrowid

    def select(
        self,
        view: str,
        subtable: str = None,
        **kwargs,
    ) -> list[dict[str, Any]]:
        """Select columns from table.

        Parameters:
            view : str
                View name.
            subtable : str, optional
                Specification of the table.
                Meant to be used for stats tables.
                Possible values: 5v5, 4v4, 3v3 and 1v1.

        Returns:
            list
                Contains ``dict``s representing the rows.

        """
        with self.connection.cursor(
            cursor_factory=psycopg2.extras.RealDictCursor
        ) as cursor:
            if subtable is None:
                query = query_builder.create_select_query(table=view)
            else:
                query = query_builder.create_select_subtable_query(
                    table=view, subtable=subtable
                )
            if kwargs:
                query += query_builder.create_where_clause(**kwargs)
            query += sql.SQL(";")
            logger.debug("Reading from database: %s", query.as_string(self.connection))
            cursor.execute(query)
            return [dict(row) for row in cursor.fetchall()]

    def update(
        self, table: str, where: dict[str, Any], called_by_bot: bool = False, **kwargs
    ) -> None:
        """Update entry.

        Parameters:
            table : str
                Table name.
            where : dict
                Current details.
                Used to find current entry in table.
            called_by_bot : bool, optional
                If `True`, updating is performed by the bot.
                Only the bot is allowed to change name and/or Discord ID.
                Defaults to `False`.

        """
        with self.connection.cursor() as cursor:
            if not called_by_bot:
                kwargs = _filter_disallowed_columns_to_update(**kwargs)
            query = query_builder.create_update_query(table=table, **kwargs)
            query += query_builder.create_where_clause(**where)
            query += sql.SQL(";")
            logger.debug("Updating in database: %s", query.as_string(self.connection))
            cursor.execute(query)

    def delete(
        self, table: str, force: bool = False, called_by_bot: bool = False, **kwargs
    ) -> None:
        """Delete entry.

        Parameters:
            table : str
                Table from which to delete the entry.
            force : bool, optional
                If `True` force cascade of all entries referencing the entry.
                Defaults to `False`.
            called_by_bot : bool, optional
                If `True`, delete is performed by the bot.
                Only the bot is allowed to change name and/or Discord ID.
                Defaults to `False`.

        """
        with self.connection.cursor() as cursor:
            if table == "members" and not force:
                self._set_active_status(
                    table=table, delete=True, called_by_bot=called_by_bot, **kwargs
                )
            query = query_builder.create_delete_query(table=table)
            query += query_builder.create_where_clause(**kwargs)
            query += sql.SQL(";")
            logger.debug("Deleting from database: %s", query.as_string(self.connection))
            cursor.execute(query)


def _filter_disallowed_columns_to_update(**kwargs):
    return {
        key: value
        for key, value in kwargs.items()
        if key not in ["discord_id", "name", "discriminator"]
    }
