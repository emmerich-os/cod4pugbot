import abc
import logging
from typing import Any

from .client import Client
from cod4pugbot.domain import model

logger = logging.getLogger(__name__)


class NotFoundInDatabaseError(Exception):
    """Raised when a requested entry was not found in the database."""


class AbstractRepository(abc.ABC):
    """Represents an abstract database table.

    Parameters:
        client : SQLClient
        called_by_bot : bool, default False
            Whether repository is used by the bot.

    """

    _table: str = ""
    _subtable: str = None
    database_model: model.AbstractModel = model.AbstractModel

    def __init__(self, client: Client, called_by_bot: bool = False):
        """Initialize the repository."""
        self._client = client
        self.called_by_bot = called_by_bot
        self.added: list[model.AbstractModel] = []
        self.fetched: list[model.AbstractModel] = []
        self.updated: list[model.AbstractModel] = []
        self.deleted: list[model.AbstractModel] = []

    @property
    def table(self) -> str:
        """Construct table name."""
        return (
            self._table if self._subtable is None else f"{self._table}_{self._subtable}"
        )

    @property
    def entries(self) -> list[database_model]:
        """Get all entries from the repository."""
        return self.get_all()

    def add(self, entity: database_model) -> None:
        """Add row to database."""
        logger.debug("Adding entity %s", entity)
        self._add(entity)
        self.added.append(entity)

    def add_and_return_row_id(self, entity: database_model) -> int:
        """Add row to database."""
        logger.debug("Adding entity %s", entity)
        inserted_row_id = self._add_and_return_row_id(entity)
        self.added.append(entity)
        logger.debug(
            "Inserted entity %s and received row ID %s", entity, inserted_row_id
        )
        return inserted_row_id

    def get(self, identifier: dict[str, Any]) -> database_model:
        """Get row as dataclass."""
        logger.debug("Trying to get entity by identifier %s", identifier)
        entity = self.database_model(**self._get(identifier))
        logger.debug("Found matching entity %s for identifier %s", entity, identifier)
        self.fetched.append(entity)
        return entity

    def get_all(self) -> list[database_model]:
        """Get all rows as dataclasses."""
        logger.debug("Getting all entities in %s", self.table)
        entities = [self.database_model(**kwargs) for kwargs in self._get_all()]
        self.fetched.extend(entities)
        logger.debug("Entities are: %s", entities)
        return entities

    def update(self, entity: database_model) -> None:
        """Update row."""
        logger.debug("Updating entity to %s", entity)
        self._update(entity, called_by_bot=self.called_by_bot)
        self.updated.append(entity)

    def delete(self, entity: database_model, force: bool = False) -> None:
        """Delete row(s).

        If ``force=True``, a cascade delete will be performed.

        """
        logger.debug("Deleting entity %s", entity)
        self._delete(entity, force=force)
        self.deleted.append(entity)

    def _add(self, entity: database_model) -> None:
        """Insert row."""
        self._client.insert(
            table=self.table,
            called_by_bot=self.called_by_bot,
            **entity.to_dict(),
        )

    def _add_and_return_row_id(self, entity: database_model) -> int:
        """Insert row."""
        return self._client.insert_and_return_row_id(
            table=self.table,
            called_by_bot=self.called_by_bot,
            **entity.to_dict(),
        )

    def _get(self, identifier: dict[str, Any]) -> dict[str, Any]:
        """Select first matching row.

        Raises:
            NotFoundInDatabaseError
                If the respective identifier was not found.

        """
        try:
            [match] = self._client.select(
                view=self._table, subtable=self._subtable, **identifier
            )
        except IndexError:
            raise NotFoundInDatabaseError(
                "No entry in table %s with identifier(s) %s",
                self.table,
                identifier,
            )
        else:
            return match

    def _get_all(self) -> list[dict[str, Any]]:
        """Select all rows."""
        return self._client.select(view=self._table, subtable=self._subtable)

    def _update(self, entity: model.AbstractModel, called_by_bot: bool) -> None:
        """Update row."""
        self._client.update(
            table=self.table,
            where=entity.identifier,
            called_by_bot=called_by_bot,
            **entity.to_dict(),
        )

    def _delete(self, entity: model.AbstractModel, force: bool) -> None:
        """Delete row."""
        self._client.delete(
            table=self.table,
            force=force,
            called_by_bot=self.called_by_bot,
            **entity.to_dict(),
        )


class MembersRepository(AbstractRepository):
    """Represents the members table."""

    _table: str = "members"
    database_model = model.Member


class StatsRepository(AbstractRepository):
    """Represents the stats table."""

    _table: str = "stats"
    database_model = model.Stats

    def __init__(self, client: Client, subtable: str, called_by_bot: bool = False):
        """Set subtable."""
        super().__init__(client=client, called_by_bot=called_by_bot)
        self._subtable = subtable


class BansRepository(AbstractRepository):
    """Represents the bans table."""

    _table: str = "bans"
    database_model = model.Ban

    @property
    def expired(self) -> list[model.Ban]:
        """Get all expired bans."""
        logger.debug("Getting list of expired bans")
        expired_bans = self._client.select("expired_bans")
        logger.debug("Expired bans: %s", expired_bans)
        return [self.database_model(**kwargs) for kwargs in expired_bans]


class PUGsRepository(AbstractRepository):
    """Represents the pugs table."""

    _table: str = "pugs"
    database_model = model.PUGInfo
