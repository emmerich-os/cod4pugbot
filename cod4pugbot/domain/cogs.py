from .features.administration.cog import Administration
from .features.connect.cog import Connect
from .features.contact_admins.cog import ContactAdmin
from .features.errors.cog import Errors
from .features.initializer.cog import Initializer
from .features.join.cog import Join
from .features.loader.cog import Loader
from .features.pugs.cog import PUGs
from .features.register.cog import Register
from .features.report.cog import ReportSystem
from .features.updater.cog import Updater

COGS = {
    "administration": Administration,
    "connect": Connect,
    "contact_admin": ContactAdmin,
    "errors": Errors,
    "initializer": Initializer,
    "join": Join,
    "loader": Loader,
    "pugs": PUGs,
    "register": Register,
    "report": ReportSystem,
    "updater": Updater,
}
