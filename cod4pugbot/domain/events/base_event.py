import dataclasses

import discord


@dataclasses.dataclass(frozen=True)
class Event:
    pass


@dataclasses.dataclass(frozen=True)
class CommandEvent(Event):
    """Invoking a command.

    Parameters:
        channel : discord.TextChannel
            Channel where the command was invoked.
        member : discord.Member
            Member who invoked the command.
        message : str, default None
            Message to the member.
        embed : discord.Embed, default None
            Embed to send with the message.
        file : discord.File, default None
            File to send with the message.
        files : list, default None
            Files to send with the message

    """

    channel: discord.TextChannel
    member: discord.Member
    message: str = None
    embed: discord.Embed = None
    file: discord.File = None
    files: list[discord.File] = None


@dataclasses.dataclass(frozen=True)
class CommandSuccessful(CommandEvent):
    """Invoking a command was successful."""


@dataclasses.dataclass(frozen=True)
class CommandFailed(CommandEvent):
    """Invoking a command failed."""
