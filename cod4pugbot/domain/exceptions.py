class InvalidUserInputException(Exception):
    """User provided incorrect input."""
