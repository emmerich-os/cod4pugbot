import logging
from typing import Optional
from typing import Union

import discord.ext.commands

from . import commands
from cod4pugbot.cod4pugbot import COD4PUGBot
from cod4pugbot.domain import events
from cod4pugbot.domain import utils

logger = logging.getLogger(__name__)

COMMAND_PREFIX: str = None
VALID_MEMBER_INFO = ["name", "nick", "discord_id"]


class Administration(discord.ext.commands.Cog, name="Administration"):
    """Perform different actions on users like warning, kicking and banning.

    Listeners:
        on_ready() : coroutine func
            Start all tasks.
        on_member_ban(guild, user) : coroutine func
            When a member gets ban, send embed to bans channel.
        on_member_unban(guild, user) : coroutine func
            When a member gets unbanned, modify embed in bans channel.

    Tasks:
        _lift_expired_bans() : local coroutine func
            Lift expired bans.

    Commands:
        kick_member(ctx, user, *, reason) : coroutine func
            Kick member from guild.
        permanently_ban_member(ctx, user, *, content) : coroutine func
            Permanently globally ban a member.
        temporarily_ban_member(ctx, user, *, content) : coroutine func
            Temporarily globally ban a member.
        pug_ban_member(ctx, user, *, content) : coroutine func
            PUG ban a member.
        search_ban(ctx, *, content) : coroutine func
            Search a ban.
        update_ban(ctx, *, content) : coroutine func
            Update a ban.
        lift_ban(ctx, *, content) : coroutine func
            Lift a ban.
        warn_member(ctx, user, *, reason) : coroutine func
            Warn a member.
        bans(ctx) : coroutine func
            Get ban list as CSV.

    Methods:
        cog_unload() : func
            Stop all tasks.
        add_global_ban(bot, member, reason) : staticmethod
            Add global ban role to member or ban member from guild.
        remove_global_ban(bot, member, reason) : staticmethod
            Remove global ban role from member or unban user from guild.
        add_pugban_status(bot, member, reason) : staticmethod
            Add PUG ban role to member.
        remove_pugban_status(bot, member, reason) : staticmethod
            Remove PUG ban role from member.
        _check_ban_messages(guild, channel, update_lifted, ban_lift_reason,
                admin, update_length, **kwargs) : local coroutine func
            Check ban messages and modify embed.
        _get_ban_length(length, created) : local func
            Estimate ban length.

    """

    lift_expired_bans_frequency = 60

    def __init__(self, bot: COD4PUGBot):
        self.bot = bot
        self.support_bans_channel = self.bot.get_channel(
            self.bot.channel_ids["support"]["bans"]
        )
        self._first_connect = True
        global COMMAND_PREFIX
        COMMAND_PREFIX = self.bot.command_prefix

    @discord.ext.commands.Cog.listener()
    async def on_ready(self):
        if not self._first_connect:
            return
        self._first_connect = False
        self._lift_expired_bans.start()

    def cog_unload(self):
        self._lift_expired_bans.cancel()

    @discord.ext.tasks.loop(seconds=lift_expired_bans_frequency)
    async def _lift_expired_bans(self):
        """Lift bans that expired."""
        for guild in self.bot.guilds:
            global_ban_role = self._get_global_ban_role(guild)
            pug_ban_role = self._get_pug_ban_role(guild)
            command = commands.LiftExpiredBans(
                guild=guild,
                global_ban_role=global_ban_role,
                pug_ban_role=pug_ban_role,
                support_bans_channel=self.support_bans_channel,
                bot=self.bot,
            )
            await self.bot.handle_command_without_db_access(command)

    def _get_global_ban_role(self, guild: discord.Guild) -> Optional[discord.Role]:
        """Get the global ban role on the Discord guild."""
        if self.bot.role_ids["global_banned"] is not None:
            return guild.get_role(self.bot.role_ids["global_banned"])
        return None

    def _get_pug_ban_role(self, guild: discord.Guild) -> Optional[discord.Role]:
        """Get the PUG ban role on the Discord guild."""
        if self.bot.role_ids["pug_banned"] is not None:
            return guild.get_role(self.bot.role_ids["pug_banned"])
        return None

    async def _is_administration_channel(
        self, ctx: discord.ext.commands.Context
    ) -> bool:
        """Check if channel is the administration channel."""
        return await utils.is_channel(
            bot=self.bot,
            ctx=ctx,
            channel="administration",
            category="admin_section",
            send_message=True,
        )

    @discord.ext.commands.command(
        name="kick",
        brief="Kick a member.",
        description="Kick @-mentioned member.",
        usage="@member <reason>",
        pass_context=True,
    )
    @discord.ext.commands.has_permissions(kick_members=True)
    async def kick_member(
        self, ctx: discord.ext.commands.Context, member: discord.Member, *, content: str
    ) -> None:
        if not await self._is_administration_channel(
            ctx
        ) or not await utils.message_has_mentions(ctx.message):
            return

        if not content:
            event = events.CommandFailed(
                channel=ctx.channel,
                member=ctx.message.author,
                message="you need to state a reason for the kick.",
            )
            await self.bot.handle_event(event)
            return

        command = commands.KickMember(
            channel=ctx.channel,
            member=member,
            reason=content,
            admin=ctx.message.author,
            support_bans_channel=self.support_bans_channel,
        )

        await self.bot.handle_command_without_db_access(command)
        return

    @discord.ext.commands.command(
        name="global_ban",
        brief="Temporarily ban a user.",
        description="Globally ban @-mentioned member.",
        usage=(
            """@member duration="<duration>" reason="<reason for ban>"\n\n"""
            """Duration can be:\n"""
            """\t- h/hours\n"""
            """\t- d/days\n"""
            """\t- w/weeks\n"""
            """\t- m/months\n"""
            """\t- y/years\n"""
            """\t- p/permanent\n\n"""
            """Examples:\n"""
            """\t- duration="1h"\n"""
            """\t- duration="1week"\n"""
            """\t- duration="6 months"\n"""
            """\t- duration="p" """
        ),
        pass_context=True,
    )
    @discord.ext.commands.has_permissions(ban_members=True)
    async def global_ban_member(
        self, ctx: discord.ext.commands.Context, member: discord.Member, *, content: str
    ):
        if not await self._is_administration_channel(
            ctx
        ) or not await utils.message_has_mentions(ctx.message):
            return
        reason = await _get_keyword_from_content(ctx, content, keyword="reason")
        duration = await _get_keyword_from_content(ctx, content, keyword="duration")
        if reason is None or duration is None:
            return

        role = self._get_global_ban_role(ctx.guild)

        command = commands.GloballyBanMember(
            channel=ctx.channel,
            member=member,
            reason=reason,
            duration=duration,
            admin=ctx.message.author,
            support_bans_channel=self.support_bans_channel,
            role=role,
        )
        await self.bot.handle_command_with_db_access(command)
        return

    @discord.ext.commands.command(
        name="pug_ban",
        brief="Ban a member from PUGs.",
        description="Temporarily ban @-mentioned member from PUGs.",
        usage=(
            """@user duration="<duration>" reason="<explanation>"\n\n"""
            """Duration can be:\n"""
            """\t- h/hours\n"""
            """\t- d/days\n"""
            """\t- w/weeks\n"""
            """\t- m/months\n"""
            """\t- y/years\n"""
            """\t- p/permanent\n\n"""
            """Examples:\n"""
            """\t- duration="1h"\n"""
            """\t- duration="1week"\n"""
            """\t- duration="6 months"\n"""
            """\t- duration="p" """
        ),
        pass_context=True,
    )
    @discord.ext.commands.has_permissions(ban_members=True)
    async def pug_ban_member(
        self, ctx: discord.ext.commands.Context, member: discord.Member, *, content: str
    ):
        if not await self._is_administration_channel(
            ctx
        ) or not await utils.message_has_mentions(ctx.message):
            return

        reason = await _get_keyword_from_content(ctx, content, keyword="reason")
        duration = await _get_keyword_from_content(ctx, content, keyword="duration")
        if reason is None or duration is None:
            return

        role = self._get_pug_ban_role(ctx.guild)

        command = commands.PUGBanMember(
            channel=ctx.channel,
            member=member,
            reason=reason,
            duration=duration,
            admin=ctx.message.author,
            support_bans_channel=self.support_bans_channel,
            role=role,
        )
        await self.bot.handle_command_with_db_access(command)
        return

    @discord.ext.commands.command(
        name="search_ban",
        brief="Search ban entry.",
        description="Search ban of @-mentioned member in bans.",
        usage=(
            """<search criterion>\n\n"""
            """Search criterion can be:\n"""
            """\t- @member\n"""
            """\t- nick="<name on the Discord server>"\n"""
            """\t- name="<name of the Discord account>"\n"""
            """\t- discord_id="<Discord ID>" """
        ),
        pass_context=True,
    )
    @discord.ext.commands.has_permissions(ban_members=True)
    async def search_ban(self, ctx: discord.ext.commands.Context, *, content: str):
        if not await self._is_administration_channel(ctx):
            return

        if _message_has_mentions(ctx.message, count=1):
            [member] = ctx.message.mentions
            search_criteria = {"discord_id": member.id}
        else:
            search_criteria = await utils.get_kwargs(ctx=ctx, content=content)
            if search_criteria is None:
                return

        command = commands.PostBanInformation(
            channel=ctx.channel,
            admin=ctx.message.author,
            search_criteria=search_criteria,
        )
        await self.bot.handle_command_with_db_access(command)
        return

    @discord.ext.commands.command(
        name="update_ban",
        brief="Update ban of a user.",
        description="Update ban of @-mentioned user.",
        usage=(
            """<user> <properties to update>\n\n"""
            """User can be referenced by:\n"""
            """\t- @user"""
            """\t- discord_id="<Discord ID>"""
            """Properties to update can be:"""
            """\t- duration="<duration>"\n"""
            """\t- global="0/1"\n"""
            """\t- pugs="0/1"\n"""
            """\t- reason="<explanation>"\n\n"""
            """Duration can be:\n"""
            """\t- h/hours\n"""
            """\t- d/days\n"""
            """\t- w/weeks\n"""
            """\t- m/months\n"""
            """\t- y/years\n"""
            """\t- p/permanent\n\n"""
            """Examples:\n"""
            """\t- duration="1h"\n"""
            """\t- duration="1week"\n"""
            """\t- duration="6 months"\n"""
            """\t- duration="p" """
        ),
        pass_context=True,
    )
    @discord.ext.commands.has_permissions(ban_members=True)
    async def update_ban(self, ctx: discord.ext.commands.Context, *, content: str):
        if not await self._is_administration_channel(ctx):
            return

        kwargs = await utils.get_kwargs(ctx=ctx, content=content)
        if not kwargs:
            return
        member = (
            ctx.message.mentions[0]
            if _message_has_mentions(ctx.message, count=1)
            else None
        )

        global_ban_role = self._get_global_ban_role(ctx.guild)
        pug_ban_role = self._get_pug_ban_role(ctx.guild)

        command = commands.UpdateBan(
            channel=ctx.channel,
            admin=ctx.message.author,
            member=member,
            kwargs=kwargs,
            global_ban_role=global_ban_role,
            pug_ban_role=pug_ban_role,
            support_bans_channel=self.support_bans_channel,
            bot=self.bot,
        )
        await self.bot.handle_command_with_db_access(command)
        return

    @discord.ext.commands.command(
        name="lift_ban",
        brief="Lift ban of a user.",
        description="Lift ban of @-mentioned user.",
        usage="""@user reason="<explanation>" """,
        pass_context=True,
    )
    @discord.ext.commands.has_permissions(ban_members=True)
    async def lift_ban(self, ctx: discord.ext.commands.Context, *, content: str):
        if not await self._is_administration_channel(ctx):
            return

        if _message_has_mentions(ctx.message, count=1):
            [member] = ctx.message.mentions
            kwargs = None
        else:
            member = None
            temp_kwargs = await utils.get_kwargs(ctx=ctx, content=content)
            kwargs = {
                key: value
                for key, value in temp_kwargs.items()
                if key in VALID_MEMBER_INFO
            }

        if member is None and not kwargs:
            await ctx.channel.send(
                f"""Sorry {ctx.message.author.mention}, """
                f"""no member information given. Either mention """
                f"""a member or use keywords nick, name or discord_id."""
            )
            return

        global_ban_role = self._get_global_ban_role(ctx.guild)
        pug_ban_role = self._get_pug_ban_role(ctx.guild)
        reason = await _get_keyword_from_content(ctx, content, keyword="reason")

        command = commands.LiftBan(
            channel=ctx.channel,
            admin=ctx.message.author,
            member=member,
            kwargs=kwargs,
            reason=reason,
            global_ban_role=global_ban_role,
            pug_ban_role=pug_ban_role,
            support_bans_channel=self.support_bans_channel,
            bot=self.bot,
        )
        await self.bot.handle_command_with_db_access(command)
        return

    @discord.ext.commands.command(
        name="warn",
        brief="Warn a user.",
        description="Warn @-mentioned user.",
        usage="""@user <explanation>""",
        pass_context=True,
    )
    @discord.ext.commands.has_permissions(administrator=True)
    async def warn_member(
        self, ctx: discord.ext.commands.Context, member: discord.Member, *, reason: str
    ):
        if not await self._is_administration_channel(ctx) or not _message_has_mentions(
            ctx.message, count=1
        ):
            return

        command = commands.WarnMember(
            channel=ctx.channel,
            admin=ctx.message.author,
            member=member,
            reason=reason,
        )
        await self.bot.handle_command_without_db_access(command)
        return

    @discord.ext.commands.command(
        name="bans",
        brief="Get the bans as CSV table.",
        description="Get the bans as CSV file via direct message by the bot.",
        pass_context=True,
    )
    @discord.ext.commands.has_guild_permissions(administrator=True)
    async def bans(self, ctx: discord.ext.commands.Context):
        command = commands.ViewBans(
            channel=ctx.channel,
            admin=ctx.message.author,
        )
        await self.bot.handle_command_with_db_access(command)

    @discord.ext.commands.Cog.listener()
    async def on_member_ban(
        self, guild: discord.Guild, user: Union[discord.User, discord.Member]
    ):
        """Whenever a member gets manually banned, add to bans.

        Parameters:
            guild : discord.Guild
                Guild the user was banned from.
            user : discord.User
                User that was banned.

        """
        duration = "p"
        command = commands.AddGlobalBanToDatabase(
            member=user,
            duration=duration,
            support_bans_channel=self.support_bans_channel,
        )
        await self.bot.handle_command_with_db_access(command)

    @discord.ext.commands.Cog.listener()
    async def on_member_unban(
        self, guild: discord.Guild, user: Union[discord.User, discord.Member]
    ):
        """Whenever a member gets manually unbanned, remove from bans.

        Parameters:
            guild : discord.Guild
                Guild the user was unbanned from.
            user : discord.User
                User that was unbanned.

        """
        command = commands.RemoveBanFromDatabase(
            member=user,
            support_bans_channel=self.support_bans_channel,
            bot=self.bot,
        )
        await self.bot.handle_command_with_db_access(command)


async def _get_keyword_from_content(
    ctx: discord.ext.commands.Context, content: str, keyword: str
) -> Optional[str]:
    """Extract a reason from given content."""
    try:
        kwargs = await utils.get_kwargs(ctx=ctx, content=content)
        return kwargs[keyword]
    except Exception:
        logger.error(
            "Could not key keyword %s from content %s", keyword, content, exc_info=True
        )
        await ctx.channel.send(
            f"Sorry {ctx.message.author.mention}, you need to give a {keyword} for the ban."
        )
        return None


def _message_has_mentions(message: discord.Message, count: int = 1) -> bool:
    """Check whether a message has a certain amount of mentions.

    Parameters:
        message : discord.Message
        count : int, default 1
            Amount of mentions the message should have.
    """
    return message.mentions and len(message.mentions) == count
