import dataclasses
import datetime
from typing import Optional
from typing import Union

import discord.ext.commands

from cod4pugbot.domain.commands.base_command import Command


@dataclasses.dataclass(frozen=True)
class KickMember(Command):
    """Member was kicked from a guild.

    Parameters:
        channel : discord.TextChannel
            Channel where the command was invoked.
        member : discord.Member
            Member to be kicked.
        reason : str
            Reason for the kick.
        admin : str
            Admin who kicked the user.
        support_bans_channel : discord.TextChannel
            Support bans channel where to post the kick information.

    """

    channel: discord.TextChannel
    member: discord.Member
    reason: str
    admin: discord.Member
    support_bans_channel: discord.TextChannel


@dataclasses.dataclass(frozen=True)
class _CreatedAt(Command):
    """Ban a member.

    Parameters:
        created_at : datetime.datetime
            Time at which the ban was created.

    """

    created_at: datetime.datetime = dataclasses.field(
        default=datetime.datetime.now(datetime.timezone.utc)
    )


@dataclasses.dataclass(frozen=True)
class _BanMember(Command):
    """Member was banned by an admin.

    Parameters:
        channel : discord.TextChannel
            Channel where the command was invoked.
        member : discord.Member
            Member to be banned.
        reason : str
            Reason for the ban.
        duration : str
            Duration of the ban.
        admin : discord.Member
            Admin who banned the member.
        support_bans_channel : discord.TextChannel
            Support bans channel where to post the ban information.

    """

    channel: discord.TextChannel
    member: discord.Member
    reason: str
    duration: str
    admin: discord.Member
    support_bans_channel: discord.TextChannel


@dataclasses.dataclass(frozen=True)
class _GloballyBanMember(_BanMember):
    """Member was globally banned by an admin.

    Parameters:
        role : discord.Role or None
            The role to add to the user.
            Must be the respective role of the ban type.
            Can be ``None`` in case of no existent role. Members then will
            be banned from the guild completely.

    """

    role: Optional[discord.Role]


@dataclasses.dataclass(frozen=True)
class _PUGBanMember(_BanMember):
    """Member was PUG banned by an admin.

    Parameters:
        role : discord.Role
            The role to add to the user.
            Must be the respective role of the ban type.

    """

    role: discord.Role


@dataclasses.dataclass(frozen=True)
class GloballyBanMember(_CreatedAt, _GloballyBanMember):
    """Member was globally banned by an admin."""


@dataclasses.dataclass(frozen=True)
class PUGBanMember(_CreatedAt, _PUGBanMember):
    """Member was PUG banned by an admin."""


BanMember = Union[GloballyBanMember, PUGBanMember]


@dataclasses.dataclass(frozen=True)
class PostBanInformation(Command):
    """Post information about a specific ban.

    Parameters:
        channel : discord.TextChannel
            Channel where the command was invoked.
        admin : discord.Member
            Admin who invoked the command.
        search_criteria : dict
            Criteria to apply for the ban search.

    """

    channel: discord.TextChannel
    admin: discord.Member
    search_criteria: dict


@dataclasses.dataclass(frozen=True)
class UpdateBan(Command):
    """Update an existing ban.

    Parameters:
        channel : discord.TextChannel
            Channel where the command was invoked
        admin : discord.Member
            Admin who invoked the command.
        member : discord.Member
            Member whose ban to update.
        kwargs : dict
            Keyword arguments given by the admin.
            These can contain the member info and ban information to update.
        updated_at : datetime.datetime
            Timestamp when the ban was updated by the admin.
        global_ban_role : discord.Role
        pug_ban_role : discord.Role
        support_bans_channel : discord.TextChannel

    """

    channel: discord.TextChannel
    admin: discord.Member
    member: discord.Member
    kwargs: dict[str, str]
    global_ban_role: Optional[discord.Role]
    pug_ban_role: discord.Role
    support_bans_channel: discord.TextChannel
    bot: discord.ext.commands.Bot
    updated_at: datetime.datetime = dataclasses.field(
        default=datetime.datetime.now(datetime.timezone.utc)
    )


@dataclasses.dataclass(frozen=True)
class LiftBan(Command):
    """Lift an existing ban.

    Parameters:
        channel : discord.TextChannel
            Channel where the command was invoked
        admin : discord.Member
            Admin who invoked the command.
        member : discord.Member, optional
            Member whose ban to update.
        kwargs : dict, optional
            Keyword arguments given by the admin.
            These can contain the member info.
        reason : str
            Reason for the ban lift.
        global_ban_role : discord.Role
        pug_ban_role : discord.Role
        support_bans_channel : discord.TextChannel

    """

    channel: discord.TextChannel
    admin: discord.Member
    member: Optional[discord.Member]
    kwargs: Optional[dict[str, str]]
    reason: str
    global_ban_role: Optional[discord.Role]
    pug_ban_role: discord.Role
    support_bans_channel: discord.TextChannel
    bot: discord.ext.commands.Bot


@dataclasses.dataclass(frozen=True)
class WarnMember(Command):
    """Warn a member.

    Parameters:
        channel : discord.TextChannel
            Channel where the command was invoked.
        admin : discord.Member
            Admin who invoked the command.
        member : disccord.Member
            Member to warn.
        reason : str
            Reason for the warning.

    """

    channel: discord.TextChannel
    admin: discord.Member
    member: discord.Member
    reason: str


@dataclasses.dataclass(frozen=True)
class ViewBans(Command):
    """View the bans.

    Parameters:
        channel : discord.TextChannel
            Channel where the command was invoked.
        admin : discord.Member
            Admin who invoked the command.

    """

    channel: discord.TextChannel
    admin: discord.Member


@dataclasses.dataclass(frozen=True)
class _AddGlobalBanToDatabase(Command):
    """Add a global ban to the database.

    Parameters:
        member : discord.User or discord.Member
            User who was banned.
        support_bans_channel : discord.TextChannel
        duration : str, default "p"
            Duration of the ban.
        reason : str, default "Manually banned by an admin"
            Reason for the ban.

    """

    member: Union[discord.User, discord.Member]
    support_bans_channel: discord.TextChannel
    duration: str = "p"
    reason: str = "Manually banned by an admin."


@dataclasses.dataclass(frozen=True)
class AddGlobalBanToDatabase(_CreatedAt, _AddGlobalBanToDatabase):
    """Add a global ban to the database.

    I.e. when a user was banned by an admin via Discord UI.

    """


@dataclasses.dataclass(frozen=True)
class RemoveBanFromDatabase(Command):
    """Remove a global ban to the database.

    Parameters:
        member : discord.User or discord.Member
            User who was unbanned.
        support_bans_channel : discord.TextChannel
        reason : str, default "Manually unbanned by an admin"
            Reason for the ban.

    I.e. when a user was unbanned by an admin via Discord UI.

    """

    member: Union[discord.User, discord.Member]
    support_bans_channel: discord.TextChannel
    bot: discord.ext.commands.Bot
    reason: str = "Manually unbanned by an admin."


@dataclasses.dataclass(frozen=True)
class LiftExpiredBans(Command):
    """Lift all expires bans."""

    guild: discord.Guild
    global_ban_role: Optional[discord.Role]
    pug_ban_role: discord.Role
    support_bans_channel: discord.TextChannel
    bot: discord.ext.commands.Bot
    reason: str = "Ban has expired."
