import abc
import dataclasses
import datetime

import discord.ext.commands

from cod4pugbot.domain import model
from cod4pugbot.domain.events.base_event import Event


@dataclasses.dataclass(frozen=True)
class KickOrBanEvent(Event):
    """Member was kicked/banned from a guild.

    Parameters:
        member : discord.Member
            Member that was kicked/banned.
        reason : str
            Reason for the kick/ban.
        support_bans_channel : discord.TextChannel
            Support bans channel where to post the kick/ban information.

    """

    member: discord.Member
    reason: str
    support_bans_channel: discord.TextChannel

    @property
    @abc.abstractmethod
    def message(self) -> str:
        """Send this message to the member."""


@dataclasses.dataclass(frozen=True)
class PerformedViaCommand:
    """Member was kicked/banned by an admin via command.

    Parameters:
        channel : discord.TextChannel
            Channel where the command was invoked.
        admin : str
            Admin who kicked/banned the member.

    """

    channel: discord.TextChannel
    admin: discord.Member


@dataclasses.dataclass(frozen=True)
class MemberKicked(PerformedViaCommand, KickOrBanEvent):
    """Member was kicked from a guild."""

    @property
    def message(self) -> str:
        """Message to be sent to the banned member."""
        return f"You have been kicked from {self.support_bans_channel.guild}"


@dataclasses.dataclass(frozen=True)
class MemberBanned(KickOrBanEvent):
    """Member was banned."""

    ban: model.Ban

    @property
    def message(self) -> str:
        """Message to be sent to the banned member."""
        return f"You have been banned from {self.support_bans_channel.guild}"


@dataclasses.dataclass(frozen=True)
class MemberGloballyBanned(PerformedViaCommand, MemberBanned):
    """Member was globally banned."""


@dataclasses.dataclass(frozen=True)
class MemberPUGBanned(PerformedViaCommand, MemberBanned):
    """Member was banned from PUGs."""


@dataclasses.dataclass(frozen=True)
class BanUpdated(Event):
    """A ban was updated."""

    channel: discord.TextChannel
    admin: discord.Member
    member: discord.Member
    ban: model.Ban
    updated_at: datetime.datetime
    support_bans_channel: discord.TextChannel
    bot: discord.ext.commands.Bot

    @property
    def message(self) -> str:
        """Message to be sent to the banned member."""
        return f"Your ban from {self.support_bans_channel.guild} has been updated"


@dataclasses.dataclass(frozen=True)
class _BanLifted(Event):
    """A ban was lifted.

    Parameters:
        member : discord.Member
            Member who was unbanned.
        reason : str
            Reason for the ban lift.
        support_bans_channel : discord.TextChannel
            Support bans channel where the ban embed was posted.
        bot : discord.ext.commands.Bot
            The bot.
            Necessary to fetch the message associated with the ban.

    """

    member: discord.Member
    reason: str
    support_bans_channel: discord.TextChannel
    bot: discord.ext.commands.Bot

    @property
    def message(self) -> str:
        """Message to be sent to the banned member."""
        return f"You have been unbanned from {self.support_bans_channel.guild}"


@dataclasses.dataclass(frozen=True)
class BanLifted(_BanLifted):
    """A ban was lifted.

    Parameters:
        admin : discord.Member
            Admin who lifted the ban.

    """

    admin: discord.Member


@dataclasses.dataclass(frozen=True)
class MemberManuallyGloballyBanned(MemberBanned):
    """Member was banned manually globally from a guild.

    Parameters:
        member : discord.User
            User that was banned.

    """

    member: discord.User


@dataclasses.dataclass(frozen=True)
class BanLiftedManually(_BanLifted):
    """Member was unbanned manually from a guild."""
