from .ban import create_new_ban
from cod4pugbot.domain.features.administration import commands
from cod4pugbot.domain.features.administration import events
from cod4pugbot.service_layer import unit_of_work


async def add_global_ban_to_database(
    command: commands.AddGlobalBanToDatabase,
    uow: unit_of_work.DatabaseUnitOfWork,
) -> None:
    """Add a global ban to the database.

    E.g. when a member was manually banned by an admin via the Discord UI
    instead of a command.

    """
    new_ban = create_new_ban(
        member=command.member,
        reason=command.reason,
        duration=command.duration,
        created_at=command.created_at,
        global_ban=True,
    )
    with uow:
        existing_bans = uow.bans.entries
        if new_ban in existing_bans:
            uow.bans.update(new_ban)
        else:
            uow.bans.add(new_ban)
        event = _create_ban_event(command)
        uow.add_new_event(event)


def _create_ban_event(
    command: commands.AddGlobalBanToDatabase,
) -> events.MemberManuallyGloballyBanned:
    return events.MemberManuallyGloballyBanned(
        member=command.member,
        reason=command.reason,
        support_bans_channel=command.support_bans_channel,
    )
