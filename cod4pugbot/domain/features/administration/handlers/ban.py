import datetime
import re
from typing import Optional
from typing import Union

import discord
from dateutil.relativedelta import relativedelta

from cod4pugbot.domain import model
from cod4pugbot.domain import utils
from cod4pugbot.domain.features.administration import commands
from cod4pugbot.domain.features.administration import events
from cod4pugbot.service_layer import unit_of_work

PERMANENT_DURATION_NAME = "permanent"


async def ban_member(
    command: commands.BanMember, uow: unit_of_work.DatabaseUnitOfWork
) -> None:
    """Globally ban a member."""
    new_ban = create_new_ban(
        member=command.member,
        reason=command.reason,
        duration=command.duration,
        created_at=command.created_at,
        pug_ban=_is_pug_ban(command),
        global_ban=_is_global_ban(command),
    )
    with uow:
        existing_bans = uow.bans.entries
        if new_ban in existing_bans:
            [existing_ban] = [ban for ban in existing_bans if ban == new_ban]
            utils.add_command_failed_event_to_uow(
                uow,
                channel=command.channel,
                member=command.admin,
                message=(
                    f"this user is already banned:\n"
                    f"{existing_ban.to_markdown()}\n\n"
                    f"If you want to change the ban length or type use `update_ban`."
                ),
            )
            return

        uow.bans.add(new_ban)

    await _ban_member_or_add_role(command)

    event = _create_ban_event(command=command, ban=new_ban)
    uow.add_new_event(event)
    utils.add_command_successful_event_to_uow(
        uow,
        channel=command.channel,
        member=command.admin,
        message=f"member {command.member.mention} successfully banned.",
    )
    return


def create_new_ban(
    member: Union[discord.Member, discord.User],
    duration: str,
    reason: str,
    created_at: datetime.datetime,
    pug_ban: bool = False,
    global_ban: bool = False,
) -> model.Ban:
    """Create a ban model."""
    duration, expires_at = calculate_ban_expiration_date(
        length=duration, created_at=created_at
    )
    return model.Ban(
        discord_id=member.id,
        reason=reason,
        duration=duration,
        created_at=created_at,
        expires_at=expires_at,
        pug_ban=pug_ban,
        global_ban=global_ban,
    )


def _is_pug_ban(command: commands.BanMember) -> bool:
    return isinstance(command, commands.PUGBanMember)


def _is_global_ban(command: commands.BanMember) -> bool:
    return isinstance(command, commands.GloballyBanMember)


def _create_ban_event(
    command: commands.BanMember, ban: model.Ban
) -> events.MemberBanned:
    """Create the event for the respective command."""
    if isinstance(command, commands.GloballyBanMember):
        event = events.MemberGloballyBanned
    elif isinstance(command, commands.PUGBanMember):
        event = events.MemberPUGBanned
    return event(
        channel=command.channel,
        member=command.member,
        reason=command.reason,
        admin=command.admin,
        support_bans_channel=command.support_bans_channel,
        ban=ban,
    )


def calculate_ban_expiration_date(
    length: str, created_at: datetime.datetime
) -> tuple[str, Optional[datetime.datetime]]:
    """Turn ban duration into length and expired date.

    Parameters:
       length : str
           Ban length.
           Can be:
               - d/day
               - w/week
               - m/month
               - y/year
               - p/perm/permament
       created_at : datetime.datetime
           Date from the bans at which the ban was created.
           Used to determine when a ban expires if it gets updated.

    Returns:
       expires : datetime.datetime or None
           Expiration date of the ban.
           ``None`` if ban is permanently.

    """
    if length == "p":
        duration = (1, "p")
    else:
        [duration] = re.findall(r"(?:([0-9]+)[\s]*([A-z]))", length)

    amount, bantype = duration
    amount = int(amount)
    bantype = bantype.lower()

    if bantype == "h":
        bantype = "hour"
        expires = created_at + relativedelta(hours=amount)
    elif bantype == "d":
        bantype = "day"
        expires = created_at + relativedelta(days=amount)
    elif bantype == "w":
        bantype = "week"
        expires = created_at + relativedelta(weeks=amount)
    elif bantype == "m":
        bantype = "month"
        expires = created_at + relativedelta(months=amount)
    elif bantype == "y":
        bantype = "year"
        expires = created_at + relativedelta(years=amount)
    elif bantype == "p":
        bantype = PERMANENT_DURATION_NAME
        expires = None

    if amount > 1:
        amount += "s"

    duration = f"{amount} {bantype}" if bantype != "permanent" else "permanent"

    return duration, expires


async def _ban_member_or_add_role(command: commands.BanMember) -> None:
    """Add role or ban member from guild according to respective ban type."""
    if isinstance(command, commands.GloballyBanMember):
        await global_ban_member_from_guild(
            member=command.member, reason=command.reason, role=command.role
        )
    elif isinstance(command, commands.PUGBanMember):
        await pug_ban_member(
            member=command.member, reason=command.reason, role=command.role
        )


async def global_ban_member_from_guild(
    member: discord.Member,
    role: Optional[discord.Role],
    reason: str = "Banned by admin",
) -> None:
    """Globally ban a member from its guild or add the global ban role.

    Parameters:
        member : discord.Member
        role : discord.Role or None
            Role to be added to the member.
            If ``None``, member will be banned from the guild.
        reason : str, default "Banned by admin"

    """
    if role is None:
        await member.ban(reason)
        return
    await member.add_roles(role, reason=reason)


async def pug_ban_member(
    member: discord.Member, reason: str, role: discord.Role
) -> None:
    """Add PUG ban role to member.

    Parameters:
        member : discord.Member
        reason : str
        role : discord.Role
            PUG ban discord role.

    """
    await member.add_roles(role, reason=reason)
