import datetime
import re
from typing import Optional
from typing import Union

import discord.ext.commands

from .ban import PERMANENT_DURATION_NAME
from cod4pugbot.domain.features.administration import events

TIME_STRING_FORMAT = "%Y-%m-%d %H:%M:%S"
KICK_TITLE = "KICK"
KICK_COLOR = discord.Color.gold()
PUG_BAN_TITLE = "PUG BAN"
PUG_BAN_COLOR = discord.Color.orange()
GLOBAL_BAN_TITLE = "GLOBAL BAN"
GLOBAL_BAN_COLOR = discord.Color.red()
DURATION_PERMANENT = "ARM\nPERMANENT\n"
EXPIRES_NEVER = "ARM\nNEVER\n"
ONGOING_BAN_STATUS = "```ARM\nONGOING\n```"
BAN_LIFTED_STATUS = "```css\nLIFTED\n```"
BAN_LIFTED_COLOR = discord.Color.green()


class NoBanMessageFoundException(Exception):
    """There was no ban message found."""


async def send_embed_to_bans_channel(event: events.KickOrBanEvent) -> None:
    """Send embed to the bans channel."""
    embed = _create_embed(event)
    await event.support_bans_channel.send(embed=embed)


def _create_embed(event: events.KickOrBanEvent) -> discord.Embed:
    title = _create_title(event)
    color = _create_color(event)
    admin = event.admin if isinstance(event, events.PerformedViaCommand) else None
    duration = _create_duration_string(event)
    expires = _create_expires_string(event)
    status = _create_status_string(event)
    return _create_information_embed(
        title=title,
        color=color,
        member=event.member,
        reason=event.reason,
        admin=admin,
        duration=duration,
        expires=expires,
        status=status,
    )


def _create_title(event: events.KickOrBanEvent) -> str:
    """Create title for respective event."""
    if isinstance(event, events.MemberKicked):
        return KICK_TITLE
    elif isinstance(event, events.MemberPUGBanned):
        return PUG_BAN_TITLE
    elif isinstance(
        event, (events.MemberGloballyBanned, events.MemberManuallyGloballyBanned)
    ):
        return GLOBAL_BAN_TITLE
    return ""


def _create_color(event: events.KickOrBanEvent) -> discord.Color:
    """Create color for respective event."""
    if isinstance(event, events.MemberKicked):
        return KICK_COLOR
    elif isinstance(event, events.MemberPUGBanned):
        return PUG_BAN_COLOR
    elif isinstance(
        event, (events.MemberGloballyBanned, events.MemberManuallyGloballyBanned)
    ):
        return GLOBAL_BAN_COLOR
    return discord.Color.dark_grey()


def _create_duration_string(
    event: Union[events.KickOrBanEvent, events.MemberBanned]
) -> Optional[str]:
    if isinstance(event, events.MemberKicked):
        return None
    if event.ban.duration == PERMANENT_DURATION_NAME:
        return DURATION_PERMANENT
    return event.ban.duration


def _create_expires_string(
    event: Union[events.KickOrBanEvent, events.MemberBanned]
) -> Optional[str]:
    if isinstance(event, events.MemberKicked):
        return None
    return event.ban.expires_at.strftime(TIME_STRING_FORMAT)


def _create_status_string(
    event: Union[events.KickOrBanEvent, events.MemberBanned]
) -> Optional[str]:
    if isinstance(event, events.MemberKicked):
        return None
    return ONGOING_BAN_STATUS


def _create_information_embed(
    title: str,
    color: discord.Color,
    member: discord.Member,
    reason: str,
    admin: Optional[discord.Member] = None,
    duration: Optional[Union[datetime.datetime, str]] = None,
    expires: Optional[Union[datetime.datetime, str]] = None,
    status: Optional[str] = None,
) -> discord.Embed:
    """Create embed with information about the action."""
    newline = "\n"
    embed = discord.Embed(title=title, color=color)
    embed.add_field(name="Member", value=member.mention, inline=True)
    if duration is not None:
        embed.add_field(name="Duration", value=duration, inline=True)
    if expires is not None:
        embed.add_field(name="Expires", value=expires, inline=True)
    if status is not None:
        embed.add_field(name="Status", value=status, inline=True)
    embed.add_field(
        name="Reason", value=f"```{newline}{reason}{newline}```", inline=False
    )
    if admin is not None:
        embed.add_field(
            name=_construct_action_type_from_title(title),
            value=admin.mention,
            inline=True,
        )
    return embed


def _construct_action_type_from_title(title: str) -> str:
    """Construct a string represented the type of action performed by the admin."""
    if "ban" in title.lower():
        return "Banned by"
    elif "kick" in title.lower():
        return "Kicked by"
    return "By"


async def update_ban_embed(event: events.BanUpdated) -> None:
    """Update a ban's embed in the support bans channel."""
    message = await _get_ban_embed_message(
        channel=event.support_bans_channel,
        member=event.member,
        bot=event.bot,
    )

    [embed] = message.embeds
    embed = embed.to_dict()
    embed_fields = embed["fields"]

    newline = "\n"
    embed["title"] = PUG_BAN_TITLE if event.ban.pug_ban else GLOBAL_BAN_TITLE
    embed["color"] = PUG_BAN_COLOR if event.ban.pug_ban else GLOBAL_BAN_COLOR
    embed_fields[1]["value"] = event.ban.duration
    embed_fields[2]["value"] = event.ban.expires_at
    embed_fields[4]["value"] = f"```{newline}{event.ban.reason}{newline}```"
    if all("updated" not in val["name"].lower() for val in embed["fields"]):
        embed_fields.append(
            {
                "name": "Updated by",
                "value": f"{event.admin.mention}",
                "inline": True,
            }
        )
    else:
        embed_fields[6]["value"] = f"{event.admin.mention}"

    await message.edit(embed=discord.Embed.from_dict(embed))


async def _get_ban_embed_message(
    channel: discord.TextChannel,
    member: discord.Member,
    bot: discord.ext.commands.Bot,
) -> discord.Message:
    """Get the message containing the embed of the ban of the member.

    Raises:
        NoBanMessageFoundException
            If there was no active ban message found for the member.

    """
    async for message in channel.history():
        if message.author != bot.user or not message.embeds or len(message.embeds) > 1:
            continue

        [embed] = message.embeds
        embed = embed.to_dict()
        embed_fields = embed["fields"]
        if "lifted" in embed_fields[3]["value"].lower():
            continue

        discord_id = int("".join(re.findall(r"[0-9]*", embed_fields[0]["value"])))

        if discord_id != member.id:
            continue
        return message
    raise NoBanMessageFoundException("No ban message found for member %s", member)


async def change_ban_embed_to_lifted(
    event: Union[events.BanLifted, events.BanLiftedManually]
) -> None:
    """Change a ban embed to lifted status."""
    message = await _get_ban_embed_message(
        channel=event.support_bans_channel,
        member=event.member,
        bot=event.bot,
    )

    [embed] = message.embeds
    embed = embed.to_dict()
    embed_fields = embed["fields"]

    embed_fields[3]["value"] = BAN_LIFTED_STATUS
    embed["color"] = BAN_LIFTED_COLOR
    newline = "\n"
    embed_fields.append(
        {
            "name": "Reason for ban lift",
            "value": f"```{newline}{event.reason}{newline}```",
            "inline": False,
        }
    )
    if isinstance(event, events.BanLifted):
        embed_fields.append(
            {"name": "Lifted by", "value": f"{event.admin.mention}", "inline": False}
        )
    await message.edit(embed=discord.Embed.from_dict(embed))
