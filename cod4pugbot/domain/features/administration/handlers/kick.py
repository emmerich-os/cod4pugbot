from cod4pugbot.domain import utils
from cod4pugbot.domain.features.administration import commands
from cod4pugbot.domain.features.administration import events
from cod4pugbot.service_layer import unit_of_work


async def kick_member_from_guild(
    command: commands.KickMember, uow: unit_of_work.UnitOfWork
) -> None:
    """Kick a member from a guild."""
    await command.member.kick(reason=command.reason)
    kick_event = events.MemberKicked(
        channel=command.channel,
        member=command.member,
        reason=command.reason,
        admin=command.admin,
        support_bans_channel=command.support_bans_channel,
    )
    uow.add_new_event(kick_event)
    utils.add_command_successful_event_to_uow(
        uow,
        channel=command.channel,
        member=command.admin,
        message=f"member {command.member.mention} was successfully kicked.",
    )
