from cod4pugbot.domain.features.administration import commands
from cod4pugbot.domain.features.administration import events
from cod4pugbot.service_layer import unit_of_work


async def remove_ban_from_database(
    command: commands.RemoveBanFromDatabase,
    uow: unit_of_work.DatabaseUnitOfWork,
) -> None:
    """Remove a global ban from the database.

    E.g. when a member was manually unbanned by an admin via the Discord UI
    instead of a command.

    """
    with uow:
        ban = uow.bans.get({"discord_id": command.member.id})
        uow.bans.delete(ban)
    event = _create_ban_lifted_event(command)
    uow.add_new_event(event)


def _create_ban_lifted_event(
    command: commands.RemoveBanFromDatabase,
) -> events.BanLiftedManually:
    return events.BanLiftedManually(
        member=command.member,
        reason=command.reason,
        support_bans_channel=command.support_bans_channel,
        bot=command.bot,
    )
