from cod4pugbot.db.repository import NotFoundInDatabaseError
from cod4pugbot.domain import utils
from cod4pugbot.domain.features.administration import commands
from cod4pugbot.service_layer import unit_of_work


def post_ban_information(
    command: commands.PostBanInformation, uow: unit_of_work.DatabaseUnitOfWork
) -> None:
    """Post ban information to a channel."""
    with uow:
        try:
            member = uow.members.get(command.search_criteria)
            ban = uow.bans.get({"discord_id": member.discord_id})
        except NotFoundInDatabaseError:
            utils.add_command_failed_event_to_uow(
                uow,
                channel=command.channel,
                member=command.admin,
                message="no matching ban found.",
            )
        else:
            utils.add_command_successful_event_to_uow(
                uow,
                channel=command.channel,
                member=command.admin,
                message=f"matching ban found:\n{ban.to_markdown()}",
            )
