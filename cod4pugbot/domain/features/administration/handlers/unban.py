from typing import Optional
from typing import Union

import discord

from cod4pugbot.db import repository
from cod4pugbot.domain import model
from cod4pugbot.domain import utils
from cod4pugbot.domain.features.administration import commands
from cod4pugbot.domain.features.administration import events
from cod4pugbot.service_layer import unit_of_work


async def lift_ban(
    command: commands.LiftBan, uow: unit_of_work.DatabaseUnitOfWork
) -> None:
    """Lift an existing ban."""
    with uow:
        try:
            if command.member is not None:
                search_criteria = {"discord_id": command.member.id}
            else:
                member = uow.members.get(command.kwargs)
                search_criteria = {"discord_id": member.discord_id}
            ban = uow.bans.get(search_criteria)
        except repository.NotFoundInDatabaseError:
            identifiers = (
                command.member.mention if command.member is not None else command.kwargs
            )
            utils.add_command_failed_event_to_uow(
                uow,
                channel=command.channel,
                member=command.admin,
                message=f"no ban found for {identifiers}.",
            )
        else:
            uow.bans.delete(ban)
            if command.member is None:
                member = await command.channel.guild.fetch_member(ban.discord_id)
            else:
                member = command.member
            await _unban_member(
                ban,
                guild=command.channel.guild,
                member=member,
                reason=command.reason,
                global_ban_role=command.global_ban_role,
                pug_ban_role=command.pug_ban_role,
            )
            utils.add_command_successful_event_to_uow(
                uow,
                channel=command.channel,
                member=command.admin,
                message=f"member {member.mention} successfully unbanned.",
            )
            ban_lifted_event = events.BanLifted(
                member=member,
                reason=command.reason,
                admin=command.admin,
                support_bans_channel=command.support_bans_channel,
                bot=command.bot,
            )
            uow.add_new_event(ban_lifted_event)


async def lift_expired_bans(
    command: commands.LiftExpiredBans, uow: unit_of_work.DatabaseUnitOfWork
) -> None:
    """Lift all expired bans."""
    with uow:
        expired_bans = uow.bans.expired
        for ban in expired_bans:
            member = await command.guild.fetch_member(ban.discord_id)
            await _unban_member(
                ban,
                guild=command.guild,
                member=member,
                reason=command.reason,
                global_ban_role=command.global_ban_role,
                pug_ban_role=command.pug_ban_role,
            )
            await member.send(
                f"You have been unbanned from {command.guild.name}. Your ban has expired."
            )
            ban_lifted_event = events.BanLifted(
                member=member,
                reason=command.reason,
                support_bans_channel=command.support_bans_channel,
                bot=command.bot,
                admin=command.bot,
            )
            uow.add_new_event(ban_lifted_event)


async def _unban_member(
    ban: model.Ban,
    guild: discord.Guild,
    member: discord.Member,
    reason: str,
    global_ban_role: Optional[discord.Role],
    pug_ban_role: discord.Role,
) -> None:
    if ban.global_ban:
        await globally_unban_member_from_guild(
            guild=guild,
            member=member,
            role=global_ban_role,
            reason=reason,
        )
    else:
        await pug_unban_member(
            member=member,
            reason=reason,
            role=pug_ban_role,
        )


async def globally_unban_member_from_guild(
    guild: discord.Guild,
    member: Union[discord.User, discord.Member],
    role: Optional[discord.Role],
    reason: str = "Unbanned by admin",
) -> None:
    """Globally unban user or remove global ban role.

    Parameters:
        guild : discord.Guild
            Guild from which to unban the member
        member : discord.Member or discord.User
        role : discord.Role or None
            Role to be added to the member.
            If ``None``, member will be banned from the guild.
        reason : str, default "Banned by admin"


    """
    if isinstance(member, discord.Member) and role is not None:
        await member.remove_roles(role, reason=reason)
        return
    try:
        await guild.unban(member, reason=reason)
    except discord.errors.NotFound:
        raise discord.errors.NotFound("No such member %s found", member)
    return


async def pug_unban_member(
    member: discord.Member,
    reason: str,
    role: discord.Role,
):
    """Remove PUG ban role from member."""
    await member.remove_roles(role, reason=reason)
