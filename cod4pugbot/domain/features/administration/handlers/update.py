from typing import Optional

import discord

from .ban import calculate_ban_expiration_date
from .ban import global_ban_member_from_guild
from .ban import pug_ban_member
from .unban import globally_unban_member_from_guild
from .unban import pug_unban_member
from cod4pugbot.db.repository import NotFoundInDatabaseError
from cod4pugbot.domain import model
from cod4pugbot.domain import utils
from cod4pugbot.domain.exceptions import InvalidUserInputException
from cod4pugbot.domain.features.administration import commands
from cod4pugbot.domain.features.administration import events
from cod4pugbot.service_layer import unit_of_work


VALID_MEMBER_INFO = ("discord_id", "name", "nick", "pb_guid")
VALID_BAN_INFO = ("duration", "reason", "global", "pugs", "expires_at")


async def update_ban_information(
    command: commands.UpdateBan, uow: unit_of_work.DatabaseUnitOfWork
) -> None:
    """Update ban information of a certain player."""
    with uow:
        try:
            _verify_user_input(command.kwargs)
            ban_info_to_update = _get_ban_info_from_kwargs(command.kwargs)
            if command.member is not None:
                member = uow.members.get({"discord_id": command.member.id})
            else:
                search_criteria = _get_member_info_from_kwargs(command.kwargs)
                member = uow.members.get(search_criteria)
            current_ban = uow.bans.get({"discord_id": member.discord_id})
        except InvalidUserInputException as e:
            utils.add_command_failed_event_to_uow(
                uow,
                channel=command.channel,
                member=command.admin,
                message=f"command failed: {e}",
            )
        except NotFoundInDatabaseError:
            utils.add_command_failed_event_to_uow(
                uow,
                channel=command.channel,
                member=command.admin,
                message="no matching ban found.",
            )
        else:
            updated_ban = _update_ban(current_ban, info_to_update=ban_info_to_update)
            uow.bans.update(updated_ban)
            if command.member is not None:
                await _ban_member_and_manage_roles(
                    guild=command.channel.guild,
                    member=command.member,
                    ban=updated_ban,
                    global_ban_role=command.global_ban_role,
                    pug_ban_role=command.pug_ban_role,
                )
            ban_updated_event = events.BanUpdated(
                channel=command.support_bans_channel,
                member=command.member,
                admin=command.admin,
                ban=updated_ban,
                updated_at=command.updated_at,
                support_bans_channel=command.support_bans_channel,
                bot=command.bot,
            )
            uow.add_new_event(ban_updated_event)
            utils.add_command_successful_event_to_uow(
                uow,
                channel=command.channel,
                member=command.admin,
                message=f"ban successfully updated:\n{updated_ban.to_markdown()}",
            )


def _verify_user_input(kwargs: dict[str, str]) -> None:
    """Verify whether user input contains all necessary information.

    Raises:
        InvalidUserInputException

    """
    if all(var not in kwargs for var in ["duration", "global", "pugs", "reason"]):
        raise InvalidUserInputException(
            """You need to give a reason or duration for the updated ban """
            """(e.g. reason="<reason>" or duration="1 week"), """
            """or change the ban type (global/pugs="0/1")."""
        )
    elif "global" in kwargs and "pugs" in kwargs:
        raise InvalidUserInputException(
            "You can only change global OR pugs ban status, not both simultaneously."
        )


def _get_member_info_from_kwargs(kwargs: dict[str, str]) -> dict[str, str]:
    """Get only information specific to a member from the kwargs."""
    return {key: value for key, value in kwargs.items() if key in VALID_MEMBER_INFO}


def _get_ban_info_from_kwargs(kwargs: dict[str, str]) -> dict[str, str]:
    """Get only ban-specific information from the kwargs."""
    return {key: value for key, value in kwargs.items() if key in VALID_BAN_INFO}


def _update_ban(ban: model.Ban, info_to_update: dict[str, str]) -> model.Ban:
    """Update ban information."""
    if "pugs" in info_to_update and "global" in info_to_update:
        raise InvalidUserInputException(
            "Cannot update PUG and global ban type both simultaneously"
        )

    if (length := info_to_update.get("duration")) is not None:
        ban.duration, ban.expires_at = calculate_ban_expiration_date(
            length=length, created_at=ban.created_at
        )
    if (reason := info_to_update.get("reason")) is not None:
        ban.reason = reason
    if (pug_ban := info_to_update.get("pugs")) is not None:
        ban.pug_ban = True if pug_ban == "1" else False
        ban.global_ban = not ban.pug_ban
    elif (global_ban := info_to_update.get("global")) is not None:
        ban.global_ban = True if global_ban == "1" else False
        ban.pug_ban = not ban.global_ban

    return ban


async def _ban_member_and_manage_roles(
    guild: discord.Guild,
    member: discord.Member,
    ban: model.Ban,
    global_ban_role: Optional[discord.Role],
    pug_ban_role: discord.Role,
) -> None:
    """Ban member by managing their roles or banning them from the Discord.

    Raises:
        InvalidUserInputException

    """
    if ban.global_ban and ban.pug_ban:
        raise InvalidUserInputException("Incorrect ban specifications %s", ban)

    if ban.global_ban:
        await global_ban_member_from_guild(
            member=member, reason=ban.reason, role=global_ban_role
        )
        await pug_unban_member(member=member, reason=ban.reason, role=pug_ban_role)
    elif ban.pug_ban:
        await pug_ban_member(member=member, reason=ban.reason, role=pug_ban_role)
        await globally_unban_member_from_guild(
            guild=guild, member=member, reason=ban.reason, role=global_ban_role
        )
