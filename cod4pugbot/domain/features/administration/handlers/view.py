import csv
import io

import discord

from cod4pugbot.domain import model
from cod4pugbot.domain import utils
from cod4pugbot.domain.features.administration import commands
from cod4pugbot.service_layer import unit_of_work


async def send_all_bans_as_csv(
    command: commands.ViewBans, uow: unit_of_work.DatabaseUnitOfWork
) -> None:
    """Send all bans as CSV to the channel."""
    bans = _get_all_bans(uow)
    if not bans:
        utils.add_command_failed_event_to_uow(
            uow=uow,
            channel=command.channel,
            member=command.admin,
            message="there are no existing bans.",
        )
        return
    await _send_bans_as_csv_to_member(member=command.admin, bans=bans)
    utils.add_command_successful_event_to_uow(
        uow=uow,
        channel=command.channel,
        member=command.admin,
        message="you received the CSV per direct message.",
    )
    return


def _get_all_bans(uow: unit_of_work.DatabaseUnitOfWork) -> list[model.Ban]:
    with uow:
        return uow.bans.entries


async def _send_bans_as_csv_to_member(
    member: discord.Member, bans: list[model.Ban]
) -> None:
    csv_stream = _format_ban_models_to_csv_string(bans)
    await member.send(
        "Full ban list as CSV (can be opened with Excel or any text editor).",
        file=discord.File(csv_stream, "bans.csv"),
    )


def _format_ban_models_to_csv_string(bans: list[model.Ban]) -> io.StringIO:
    string_io = io.StringIO()
    writer = csv.writer(string_io, quoting=csv.QUOTE_NONNUMERIC, delimiter=";")
    for i, ban in enumerate(bans):
        if i == 0:
            writer.writerow(ban.to_dict(rename_keys=True).keys())
        writer.writerow(ban.to_dict().values())
    return string_io
