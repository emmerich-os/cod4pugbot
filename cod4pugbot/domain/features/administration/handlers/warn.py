from cod4pugbot.domain import utils
from cod4pugbot.domain.features.administration import commands
from cod4pugbot.service_layer import unit_of_work


async def warn_member(
    command: commands.WarnMember, uow: unit_of_work.UnitOfWork
) -> None:
    """Warn a member."""
    await command.member.send(
        "You have been warned:\n"
        f"```\n{command.reason}\n```\n"
        "Getting warned to many times will result in a temporary ban."
    )
    utils.add_command_successful_event_to_uow(
        uow,
        channel=command.channel,
        member=command.admin,
        message=f"member {command.member.mention} has been warned.",
    )
