import discord.ext.commands

from . import commands
from cod4pugbot.cod4pugbot import COD4PUGBot


class ContactAdmin(discord.ext.commands.Cog, name="Contact Admins"):
    """Contact admins when players call an admin.

    Attributes:
        bot : commands.Bot
            COD4PUGBot.
            Important to have access to all bot configurations.

    Commands:
        contact_admin(ctx, *, message) : coroutine func
            Send a direct message to all admins when a user calls an admin.

    """

    def __init__(self, bot: COD4PUGBot):
        self.bot = bot

    def _is_admin_and_not_bot(self, member: discord.Member) -> bool:
        """Check if a member is an admin and not the bot."""
        return member != self.bot.user and member.guild_permissions.administrator

    @discord.ext.commands.command(
        name="admin",
        brief="Contact admins.",
        description="Send a direct message to all admins. Abuse of this command will be punished.",
        usage="""<message>""",
        pass_context=True,
    )
    async def contact_admin(self, ctx: discord.ext.commands.Context, *, content: str):
        admins = [
            member for member in ctx.guild.members if self._is_admin_and_not_bot(member)
        ]

        command = commands.ContactAdmins(
            member=ctx.message.author,
            channel=ctx.channel,
            message=ctx.message,
            content=content,
            admins=admins,
        )

        await self.bot.handle_command_without_db_access(command)
