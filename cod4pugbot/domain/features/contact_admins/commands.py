import dataclasses

import discord

from cod4pugbot.domain.commands.base_command import Command


@dataclasses.dataclass(frozen=True)
class ContactAdmins(Command):
    """Contact all admins.

    Parameters:
        member : discord.Member
            Member who invoked the command.
        channel : discord.TextChannel
            Channel in which the command was invoked.
        message : discord.Message
            Message by which the command was invoked.
        content : str
            Content of the message that followed the command.
        admins : list
            Admins to contact

    """

    member: discord.Member
    channel: discord.TextChannel
    message: discord.Message
    content: str
    admins: list[discord.Member]
