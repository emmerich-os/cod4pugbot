import discord.ext.commands

from . import commands
from cod4pugbot.domain import utils
from cod4pugbot.service_layer import unit_of_work


async def send_embed_to_admins(
    command: commands.ContactAdmins, uow: unit_of_work.UnitOfWork
) -> None:
    """Send an embed to all admins."""
    if not command.admins:
        utils.add_command_failed_event_to_uow(
            uow,
            channel=command.channel,
            member=command.member,
            message="no admins could be contacted.",
        )
        return

    embed = _create_admin_contact_embed(
        message=command.message, content=command.content
    )
    for admin in command.admins:
        await admin.send(embed=embed)

    utils.add_command_successful_event_to_uow(
        uow,
        channel=command.channel,
        member=command.member,
        message=(
            "the admins have been notified. "
            "They will get in contact with you as soon as possible. "
            "Please be patient."
        ),
    )


def _create_admin_contact_embed(
    message: discord.Message, content: str
) -> discord.Embed:
    """Create an embed from a context."""
    embed = discord.Embed(
        title="ADMIN REQUESTED",
        color=discord.Color.red(),
    )
    embed.add_field(name="By member", value=f"{message.author.mention}", inline=False)
    embed.add_field(name="In channel", value=f"{message.channel.mention}", inline=False)
    embed.add_field(name="Reason", value=content, inline=False)

    attachments = [attachment.url for attachment in message.attachments]

    if attachments:
        newline = "\n"
        embed.set_thumbnail(url=attachments[0])
        if len(attachments) > 1:
            embed.add_field(
                name="Further attachments",
                value=f"{f'{newline}'.join(attachments[1:])}",
                inline=False,
            )

    return embed
