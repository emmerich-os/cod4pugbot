import datetime
import json
import logging
import sys
import traceback
from pathlib import PurePath

import discord
from discord.ext import commands

import cod4pugbot.domain.utils as utils


logger = logging.getLogger(__name__)


class Errors(commands.Cog):
    """Error handling of commands and the code.

    Attributes:
        bot : commands.Bot
            COD4PUGBot.
            Important to have access to the bot configurations.
        exceptions : dict
            All possible Discord exceptions that could appear.

    Listeners:
        on_error(event, *args, **kwargs) : coroutine func
            Post error message in error channel when a bot error appears.
        on_command_error(ctx, error) : coroutine func
            Post a message in error channel when a command error appears.
        on_raw_reaction_remove(raw_reaction) : coroutine func
            Modify the corresponding error message embed.
        on_raw_reaction_add(raw_reaction) : coroutine func
            Modify the corresponding error message embed.

    Commands:
        report_error(ctx, *, content) : coroutine func
            Users can report bot errors they noticed.
            Helpful for debugging the bot.

    """

    def __init__(self, bot: commands.Bot):
        self.bot = bot
        self.exceptions = {
            commands.ConversionError: "A Converter class raises non-CommandError.",
            commands.MissingRequiredArgument: "Please pass in all arguments the command requires.",
            commands.ArgumentParsingError: "Parser fails to parse your input.",
            commands.UnexpectedQuoteError: "Parser encounters a quote mark inside a non-quoted "
            "string.",
            commands.InvalidEndOfQuotedStringError: "A space is expected after the closing quote in"
            " a string but a different character is found.",
            commands.ExpectedClosingQuoteError: "A quote character is expected but not found.",
            commands.BadArgument: "A parsing or conversion failure is encountered on an argument to"
            " pass into the command.",
            commands.BadUnionArgument: "A typing.Union converter fails for all its associated"
            " types.",
            commands.PrivateMessageOnly: "This operation does not work outside of private messages",
            commands.NoPrivateMessage: "This operation does not work in private messages.",
            commands.CheckFailure: "The predicates in Command.checks have failed.",
            commands.CheckAnyFailure: "All predicates in check_any() fail.",
            commands.CommandNotFound: "This is an invalid command.",
            commands.DisabledCommand: "This command is currently disabled.",
            commands.CommandOnCooldown: "This command is on cooldown.",
            commands.MaxConcurrencyReached: "This command has reached its maximum concurrency.",
            commands.NotOwner: "You are not the owner of the bot.",
            commands.MissingPermissions: "You do not have the required permissions.",
            commands.BotMissingPermissions: "The bot’s member lacks permissions to run this"
            " command.",
            commands.MissingRole: "You lack a role to run this command.",
            commands.BotMissingRole: "The bot’s member lacks a role to run this command.",
            commands.MissingAnyRole: "You lack any of the roles specified to run this command.",
            commands.BotMissingAnyRole: "The bot’s member lacks any of the roles specified to run"
            " this command.",
            commands.NSFWChannelRequired: "This channel does not have the required NSFW setting.",
            commands.ExtensionAlreadyLoaded: "This extension has already been loaded.",
            commands.ExtensionNotLoaded: "This extension could not be loaded.",
            commands.NoEntryPointError: "This extension does not have a setup entry point"
            " function.",
            commands.ExtensionFailed: "This extension failed to load during execution of the module"
            " or setup entry point.",
            commands.ExtensionNotFound: "This extension could not be found.",
            commands.ExtensionError: "Extension related error.",
            commands.UserInputError: "Wrong input.",
            commands.CommandError: "Command error.",
            commands.CommandInvokeError: "The command being invoked raised an exception.",
        }

    @commands.Cog.listener()
    async def on_error(self, event, *args, **kwargs):
        """Post bot error messages in error channel.

        Parameters:
            event
                Event that triggered the error.

        """
        channel = self.bot.get_channel(self.bot.channel_ids["admin_section"]["errors"])

        exception_type, exception_value, exception_traceback = sys.exc_info()
        exception_traceback = traceback.format_tb(exception_traceback)
        module = PurePath(
            exception_traceback[0]
            .split("\n")[0]
            .split(",")[0]
            .split(" ")[-1]
            .replace('"', "")
        ).parts[-1]

        logger.error(
            "%s",
            "\t".join(
                traceback.format_exception(
                    exception_type, exception_value, exception_traceback
                )
            ),
        )
        function = exception_traceback[0].split("\n")[0].split(",")[-1].split(" ")[-1]
        error_post = discord.Embed(
            title=f"""BOT ERROR: {exception_type.__name__} during event {event}""",
            description=f"""In Cog `{module}` in function `{function}()`""",
            timestamp=datetime.datetime.now(tz=self.bot.timezone),
            color=0x727272,
        )
        error_post.add_field(
            name="Error message:", value=f"""```{exception_value}```""", inline=False
        )
        error_post.add_field(
            name="Traceback:",
            value=f"""```{''.join(exception_traceback)}```""",
            inline=False,
        )

        await channel.send(embed=error_post)

    @commands.Cog.listener()
    async def on_command_error(self, ctx: commands.Context, error: Exception):
        """Send message to channel if error occurs when calling a command.

        Parameters:
            ctx : commands.Context
                Context of the error.
            error : discord.ext.commands Exception
                Exception including traceback.

        """
        channel = self.bot.get_channel(self.bot.channel_ids["admin_section"]["errors"])

        etype = type(error)
        trace = error.__traceback__
        lines = traceback.format_exception(etype, error, trace)

        lines_temp = []
        for line in lines:
            if (
                "The above exception was the direct cause of the following exception"
                in line
            ):
                break
            else:
                lines_temp.append(line)

        lines = lines_temp
        del lines_temp

        traceback_text = "".join(lines)

        logger.error("%s", ctx.message.content, exc_info=error)

        error_message = [f"Sorry {ctx.author.mention}."]
        for exception, message in self.exceptions.items():
            if isinstance(error, exception):
                error_message.append(message)
                exc = exception
                break
        error_message.append(
            f"Use {ctx.prefix}help or contact an admin ({ctx.prefix}admin)."
        )
        await ctx.send(" ".join(error_message))

        error_post = discord.Embed(
            title=f"""COMMAND ERROR: {exc.__name__}""",
            description=f"""Triggered by {ctx.message.author.mention} """
            f"""while calling `{self.bot.command_prefix}{ctx.command}`""",
            timestamp=datetime.datetime.now(tz=self.bot.timezone),
            color=0x727272,
        )
        error_post.add_field(
            name="Command execution",
            value=f"""`{ctx.message.content}`""",
            inline=False,
        )
        error_post.add_field(
            name="discord.py message", value=f"""```{error}```""", inline=False
        )
        if len("".join(traceback_text)) < 1024:
            error_post.add_field(
                name="Traceback", value=f"""```{traceback_text}```""", inline=False
            )
        else:
            traceback_text = []
            first_message = True
            for line in lines:
                if line.lower().startswith("traceback"):
                    if first_message:
                        traceback_text.append(line)
                    else:
                        name = None
                        error_post.add_field(
                            name=name,
                            value=f"```{''.join(traceback_text)}```",
                            inline=False,
                        )
                        traceback_text = [line]
                elif len("".join(traceback_text + [line])) > 1024:
                    if first_message:
                        name = "Traceback"
                    else:
                        first_message = False
                        name = None

                    error_post.add_field(
                        name=name,
                        value=f"```{''.join(traceback_text)}```",
                        inline=False,
                    )
                    traceback_text = [line]
                else:
                    traceback_text.append(line)
            if traceback_text:
                error_post.add_field(
                    name=name,
                    value=f"""```{''.join(traceback_text)}```""",
                    inline=False,
                )
        error_post.add_field(
            name="Status", value="```brainfuck\nPENDING\n```", inline=False
        )

        message = await channel.send(embed=error_post)
        await message.add_reaction(emoji="\u2705")

    @commands.command(
        name="report_error",
        brief="Report any command or Bot errors to the admins.",
        description="Whenever the Bot does not behave as expected, "
        "members are encouraged to report the incident "
        "stating the command as they executed it, the response "
        "by the Bot and what they expected the Bot to behave like.",
        usage=(
            """command="<command us used>" response="<whatever the bot did>" """
            """description="<what you expected the bot to do>" """
        ),
        pass_context=True,
    )
    async def report_error(self, ctx: commands.Context, *, content):
        channel = self.bot.get_channel(self.bot.channel_ids["admin_section"]["errors"])

        kwargs = await utils.get_kwargs(ctx=ctx, content=content, error_report=True)

        if kwargs is None:
            await ctx.channel.send(
                """You need to give arguments describing the incident """
                """(i.e. command="command", response="Response", """
                """description="Description")."""
            )

        if "command" not in kwargs:
            await ctx.channel.send(
                "You need to give the command as you executed it "
                'as an argument (i.e. command="Command").'
            )
            return
        elif "response" not in kwargs:
            await ctx.channel.send(
                "You need to give the response of the bot "
                'as an argument (i.e. response="Response").'
            )
            return

        newline = "\n"

        error_post = discord.Embed(
            title="COMMAND ERROR REPORTED",
            timestamp=datetime.datetime.now(tz=self.bot.timezone),
            color=0x727272,
        )
        error_post.add_field(
            name="Reported by", value=f"{ctx.message.author.mention}", inline=False
        )
        error_post.add_field(
            name="Command", value=f"""`{kwargs["command"]}`""", inline=False
        )
        error_post.add_field(
            name="Response",
            value=f"""```{newline}{kwargs["response"]}{newline}```""",
            inline=False,
        )
        if "description" in kwargs:
            error_post.add_field(
                name="Description",
                value=f"""```{newline}{kwargs["description"]}{newline}```""",
                inline=False,
            )
        error_post.add_field(
            name="Status", value="```brainfuck\nPENDING\n```", inline=False
        )
        message = await channel.send(embed=error_post)
        await message.add_reaction(emoji="\u2705")
        await ctx.message.channel.send(
            f"Thanks {ctx.message.author.mention}, your report has been "
            "forwarded to the admins."
        )

    @commands.Cog.listener()
    @commands.has_guild_permissions(administrator=True)
    async def on_raw_reaction_add(self, raw_reaction: discord.RawReactionActionEvent):
        """On reaction to register messages in admin register channel.

        Parameters:
            raw_reaction : discord.RawReactionActionEvent
                Reaction event.

        """
        user, reaction = await utils.raw_reaction_to_reaction(
            bot=self.bot, raw_reaction=raw_reaction
        )

        if user == self.bot.user:
            return

        if user == self.bot.user or reaction.message.author != self.bot.user:
            return
        elif not await utils.is_channel(
            bot=self.bot,
            channel_id=reaction.message.channel.id,
            channel="errors",
            category="admin_section",
        ):
            return
        elif reaction.count > 2:
            await reaction.remove(user)
            return
        elif not reaction.message.embeds:
            return
        elif reaction.emoji != "\u2705":
            await reaction.remove(user)
            return

        [embed] = reaction.message.embeds
        embed = embed.to_dict()
        logger.debug("%s", json.dumps(embed, indent=4, default=str))

        if reaction.emoji == "\u2705":
            for i, field in enumerate(embed["fields"]):
                if "status" in field["name"].lower():
                    embed["fields"][i]["value"] = "```css\nREVIEWED\n```"
                    embed["color"] = 0x00FF00
                    break

        await reaction.message.edit(embed=discord.Embed.from_dict(embed))

    @commands.Cog.listener()
    @commands.has_guild_permissions(administrator=True)
    async def on_raw_reaction_remove(
        self, raw_reaction: discord.RawReactionActionEvent
    ):
        """On reaction to register messages in admin register channel.

        Parameters:
            raw_reaction : discord.RawReactionActionEvent
                Reaction event.

        """
        user, reaction = await utils.raw_reaction_to_reaction(
            bot=self.bot, raw_reaction=raw_reaction
        )

        if user == self.bot.user:
            return

        if user == self.bot.user or reaction.message.author != self.bot.user:
            return
        elif not await utils.is_channel(
            bot=self.bot,
            channel_id=reaction.message.channel.id,
            channel="errors",
            category="admin_section",
        ):
            return
        elif not reaction.message.embeds or any(
            r.count >= 2 for r in reaction.message.reactions
        ):
            return
        elif reaction.emoji != "\u2705":
            return

        [embed] = reaction.message.embeds
        embed = embed.to_dict()
        if reaction.emoji == "\u2705":
            for i, field in enumerate(embed["fields"]):
                if "status" in field["name"].lower():
                    embed["fields"][i]["value"] = "```brainfuck\nPENDING\n```"
                    embed["color"] = 0x727272
                    break

        await reaction.message.edit(embed=discord.Embed.from_dict(embed))
