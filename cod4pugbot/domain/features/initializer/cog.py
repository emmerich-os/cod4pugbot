import logging

import discord.ext.commands

import cod4pugbot.config
from cod4pugbot.cod4pugbot import COD4PUGBot


logger = logging.getLogger(__name__)


class Initializer(discord.ext.commands.Cog):
    """Initializes roles, categories and channels the bot needs to operate.

    Attributes:
        bot : discord.ext.commands.Bot
            COD4PUGBot.
            Important to have access to the bot configurations.

    Commands:
        init(ctx) : coroutine func
            Initialize all roles, categories and channels.

    Methods:
         _create_roles() : local coroutine func
            Create missing roles and write the corresponding IDs into
            the `config.yml`.
         _create_admin_category() : local coroutine func
            Create admin category if missing and write the corresponding
            ID into the `config.yml`.
         _create_admin_channels() : local coroutine func
            Create missing admin channels and write the corresponding IDs
            into the `config.yml`.
            Admin channels are:
                - register-requests
                - reports
                - administration (bans/kicks)
                - errors (bot errors/code errors)
         _create_support_category() : local coroutine func
            Create support category if missing and write the corresponding
            IDs into the `config.yml`.
         _create_support_channels() : local coroutine func
            Create support channels and write the corresponding IDs into
            the `config.yml`.
            Support channels are:
                - register
                - report
                - bans

    """

    def __init__(self, bot: COD4PUGBot):
        self.bot = bot

    @discord.ext.commands.command(
        name="init",
        brief="Initialize all roles, categories and channels.",
        pass_context=True,
    )
    @discord.ext.commands.has_permissions(administrator=True)
    async def init(self, ctx: discord.ext.commands.Context):
        """Initialize all roles, categories and channels the bot needs to operate."""
        if ctx.message.author != self.bot.guild.owner:
            return
        guild = ctx.guild
        await self._create_roles(guild=guild)
        await self._create_admin_category(guild=guild)
        await self._create_admin_channels(guild=guild)
        await self._create_support_category(guild=guild)
        await self._create_support_channels(guild=guild)

    async def _create_roles(self, guild: discord.Guild):
        """Create all necessary roles.

        Parameters:
            guild : discord.Guild
                Guild in which to create the roles.

        """
        logger.debug("creating roles")
        for role, details in self.bot.roles_new.items():
            if self.bot.role_ids[role] is None:
                if role["permissions"] is not None:
                    role_object = await guild.create_role(
                        name=role["name"],
                        reason="COD4PUGBot initialized.",
                        permissions=role["permissions"],
                        color=role["color"],
                        hoist=role["hoist"],
                        mentionable=role["mentionable"],
                    )
                else:
                    role_object = await guild.create_role(
                        name=role["name"],
                        reason="COD4PUGBot initialized.",
                        color=role["color"],
                        hoist=role["hoist"],
                        mentionable=role["mentionable"],
                    )
                self.bot.role_ids[role] = role_object.id
                cod4pugbot.config.save_to_config(("role_ids", role), role_object.id)
            else:
                role_object = guild.get_role(self.bot.role_ids[role])
            self.bot.roles_new[role]["id"] = role_object.id

    async def _create_admin_category(self, guild: discord.Guild):
        """Create the admin category.

        Parameters:
            guild : discord.Guild
                Guild in which to create the category.

        """
        logger.debug("creating admin category")
        if self.bot.category_ids["admin_section"] is None:
            overwrites = {
                self.bot.roles_new["head_admin"]["object"]: discord.PermissionOverwrite(
                    read_messages=True,
                    connect=True,
                ),
                self.bot.roles_new["admin"]["object"]: discord.PermissionOverwrite(
                    read_messages=True,
                    connect=True,
                ),
                guild.default_role: discord.PermissionOverwrite(
                    read_messages=False,
                    connect=False,
                ),
            }
            category = await guild.create_category_channel(
                name=self.bot.categories_new["admin_section"]["name"],
                reason="COD4PUGBot connected.",
                overwrites=overwrites,
            )
            self.bot.category_ids["admin_section"] = category.id
            cod4pugbot.config.save_to_config(
                ("category_ids", "admin_section"), category.id
            )
        else:
            for cat in guild.categories:
                if cat.id == self.bot.category_ids["admin_section"]:
                    category = cat
                    break
        self.bot.categories_new["admin_section"]["id"] = category.id

    async def _create_admin_channels(self, guild: discord.Guild):
        """Create the admin channels register, reports, administration and erros.

        Parameters:
            guild : discord.Guild
                Guild in which to create the channels.

        """
        logger.debug("creating admin channels")
        for chan, details in self.bot.categories_new["admin_section"][
            "channels"
        ].items():
            if self.bot.channel_ids["admin_section"][chan] is None:
                category = self.bot.get_channel(self.bot.category_ids["admin_section"])
                channel = await category.create_text_channel(
                    name=details["name"],
                    reason="COD4PUGBot initialized.",
                )
                self.bot.channel_ids["admin_section"][chan] = channel.id
                cod4pugbot.config.save_to_config(
                    ("channel_ids", "admin_section", chan), channel.id
                )
            else:
                channel = self.bot.get_channel(
                    self.bot.channel_ids["admin_section"][chan]
                )
            details["id"] = channel.id

    async def _create_support_category(self, guild: discord.Guild):
        """Create the support category.

        Parameters:
            guild : discord.Guild
                Guild in which to create the category.

        """
        logger.debug("creating support category")
        if self.bot.category_ids["support"] is None:
            overwrites = {
                guild.default_role: discord.PermissionOverwrite(
                    read_messges=True, send_messages=True
                ),
            }
            category = await self.guild.create_category_channel(
                name=self.bot.categories_new["support"]["name"],
                reason="COD4PUGBot initiated.",
                overwrites=overwrites,
            )
            self.bot.category_ids["support"] = category.id
            cod4pugbot.config.save_to_config(("category_ids", "support"), category.id)
        else:
            category = self.bot.get_channel(self.bot.category_ids["support"])
        self.bot.categories_new["support"]["id"] = category.id

    async def _create_support_channels(self, guild: discord.Guild):
        """Create the support channels register, report and bans.

        Parameters:
            guild : discord.Guild
                Guild in which to create the channels.

        """
        logger.debug("creating support channels")
        for chan, details in self.bot.categories_new["support"]["channels"].items():
            if self.bot.channel_ids["support"][chan] is None:
                category = self.bot.get_channel(self.bot.category_ids["support"])
                channel = await category.create_text_channel(
                    name=details["name"],
                    reason="COD4PUGBot initialized.",
                )
                self.bot.channel_ids["support"][chan] = channel.id
                cod4pugbot.config.save_to_config(
                    ("channel_ids", "support", chan), channel.id
                )
            else:
                channel = self.bot.get_channel(self.bot.channel_ids["support"][chan])
            details["id"] = channel.id
