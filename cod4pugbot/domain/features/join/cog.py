import logging

import discord
from discord.ext import commands

import cod4pugbot.domain.features.administration.cog as administration
import cod4pugbot.domain.utils as utils
from cod4pugbot.db import SQLClient

logger = logging.getLogger(__name__)


class Join(commands.Cog):
    """Actions when a user connects to the discord server.

    Attributes:
        bot : commands.Bot
            COD4PUGBot.
            Important to have access to the bot configurations.

    Listeners:
        on_member_join(member) : coroutine func
            Send direct message, assign roles and update data base.

    """

    def __init__(self, bot: commands.Bot):
        self.bot = bot

    @commands.Cog.listener()
    async def on_member_join(self, member: discord.Member):
        """Send a direct message, assign roles and update members, bans and/or stats tables.

        Parameters:
            member : discord.Member
                Member who joined.

        """
        logger.debug("in cogs.join.on_member_join")
        guild = member.guild
        try:
            await member.send(
                f"Hi {member.name}, welcome to the {guild.name} Discord server!"
            )
        except discord.errors.Forbidden:
            pass

        kwargs = await utils.get_kwargs(member=member)

        client = SQLClient(**self.bot.db_info)

        members = client.select(view="members_print", **kwargs)

        if members is not None and (
            members := utils.check_df(df=members, to_dict=True)
        ):
            columns_to_update = {}
            for kwarg, value in kwargs.items():
                if kwarg in members and value != members[kwarg]:
                    columns_to_update.setdefault(kwarg, value)
            if columns_to_update:
                client.update(table="members", called_by_bot=True, **kwargs)
            await member.add_roles(
                guild.get_role(self.bot.role_ids["registered"]),
                reason="Joined the Discord server and was already registered.",
            )
        else:
            await member.add_roles(
                guild.get_role(self.bot.role_ids["unregistered"]),
                reason="Joined the Discord server.",
            )

        bans = client.select(view="bans_print", **kwargs)

        if bans is not None and (bans := utils.check_df(df=bans, to_dict=True)):
            if bans["global"] and self.bot.role_ids["global_banned"] is not None:
                await administration.Administration.add_global_ban(
                    bot=self.bot,
                    member=member,
                    reason="Joined the Discord server and was PUG banned.",
                )
            if bans["pugs"] and self.bot.role_ids["pug_banned"] is not None:
                await administration.Administration.add_pugban_status(
                    bot=self.bot,
                    member=member,
                    reason="Joined the Discord server and was PUG banned.",
                )

        logger.debug("returning from cogs.join.on_member_join")
