import discord.ext.commands

from cod4pugbot.cod4pugbot import COD4PUGBot


class Loader(discord.ext.commands.Cog, name="Extension Manager"):
    """ADMIN ONLY: Load or unload extensions.

    Attributes:
        bot : discord.ext.commands.Bot
            COD4PUGBot.
            Important to have access to the bot configurations.

    Commands:
        load(extension) : coroutine func
            Load extension.
        unload(extension) : coroutine func
            Unload extension.

    Error handlers:
        load_error(ctx, error) : coroutine func
            Send message if user does not have required permissions.
        unload_error(ctx, error) : coroutine func
            Send message if user does not have required permissions.

    """

    def __init__(self, bot: COD4PUGBot):
        self.bot = bot

    @discord.ext.commands.command(
        name="load",
        brief="Load a specific extension.",
        description="Load a specific extension from the list of extensions the bot offers.",
        usage="<extension>",
        pass_context=True,
    )
    @discord.ext.commands.has_guild_permissions(administrator=True)
    async def load(self, extension: str):
        self.bot.load_extension(f"cogs.{extension}")

    @load.error
    async def load_error(
        self,
        ctx: discord.ext.commands.Context,
        error: discord.ext.commands.MissingPermissions,
    ):
        """If `load` returns a PermissionError, sent message to channel."""
        if isinstance(error, discord.ext.commands.MissingPermissions):
            await ctx.channel.send(
                f"Sorry {ctx.message.author}, you do not have permissions to execute this command!"
            )

    @discord.ext.commands.command(
        name="unload",
        brief="Unload a specific extension.",
        description="Unload a specific extension that has been loaded into the bot.",
        usage="<extension>",
        pass_context=True,
    )
    @discord.ext.commands.has_guild_permissions(administrator=True)
    async def unload(self, extension: str):
        self.bot.unload_extension(f"cogs.{extension}")

    @unload.error
    async def unload_error(
        self,
        ctx: discord.ext.commands.Context,
        error: discord.ext.commands.MissingPermissions,
    ):
        """If `unload` returns a PermissionError, sent message to channel."""
        if isinstance(error, discord.ext.commands.MissingPermissions):
            await ctx.channel.send(
                f"Sorry {ctx.message.author}, you do not have permissions to execute this command!"
            )
