import datetime
import logging
import re
import socket
from typing import Optional

import discord
import pytz
from ip2geotools.databases.noncommercial import DbIpCity

from cod4pugbot.domain.features.pugs.cod4.scorebot import EMOJIS
from cod4pugbot.domain.features.pugs.cod4.scorebot import EVENTS
from cod4pugbot.domain.features.pugs.cod4.scorebot import MAP_URLS
from cod4pugbot.domain.features.pugs.cod4.scorebot import RoundWinner
from cod4pugbot.domain.features.pugs.cod4.scorebot import StartText
from cod4pugbot.domain.features.pugs.cod4.scorebot.killfeed_events import Event
from cod4pugbot.domain.features.pugs.cod4.utils import EVENTS_MAP
from cod4pugbot.domain.features.pugs.cod4.utils import Player
from cod4pugbot.domain.features.pugs.cod4.utils import PromodTeam
from cod4pugbot.domain.features.pugs.cod4.utils import StatsTracker

logger = logging.getLogger(__name__)


class Server:
    """CoD4 game server information.

    The information are retrieved by sending UDP packages to the server
    with `getinfo`, `getstatus` and `rcon <rcon password> pb_sv_plist`.
    All available server information can then be turned into a
    `discord.Embed`.
    If the server has the Promod scorebot enabled, detailed information
    about the scoreboard and killfeed can also be displayed. The scoreboard
    can be shown both in the embed as well as in a message. The advantage of
    a message is that the scoreboard uses the CoD4 icons for attack/defence
    and dead/carrying bomb. The killfeed is turned into a message with the
    weapon icons being displayed.

    Attributes:
        name : str
            Server name.
        ip : str
            Server IP.
        port : str
            Server Port.
        password : str or None
            Server password.
        rcon_password : str or None
            Server rcon password.
        location : dict
            Location information of the server.
            Country, city and region.
        mod : str
            CoD4 mod installed on the server.
        slots: int
            Number of server slots.
        clients: int
            Number of clients on the server.
        map : str
            Name of the map currently running.
        map_uptime : `datetime.timedelta`
            Uptime of the map.
        gametype : str
            Gametype of the map.
        players : list
            List of players on the server.
        promod_version : str or None
            Promod version.
        promod_mode : str or None
            Promod mode.
        attack : dict
            Attacking team's info.
            Score and players.
        defence : dict
            Defending team's info.
            Score and players.
        prev_tick : str
            Last promod scorebot tick ID.
        prev_tick_info : list or None
            Last promod scorebot tick information.
        counter : dict
            Counts maps and rounds, total and for each team.
            Important to determine if a team has won a map and/or the PUG.
        start_winner_check : bool
            Whether to start checking for a winner.
            Check starts as soon as a half starts or a timeout ends.
        stats_tracker : `StatsTracker`
            Tracks the stats of the players.
        round : int
            Current round.
        max_rounds : int
            Maximum rounds.
            Calculated from `promod_mode`.
        killfeed_messages : list or []
            Killfeed messages.
            Gets flushed whenever a new round starts.
            Defaults to [].

    Methods:
        connect(ip, port, password, rcon_password, pugs) : classmethod
            Connect to the server.
        _send_requests(server) : local func
            Send info, status and rcon pb_plist request and get responses.
        _get_details(server) : local func
            Get all details containted in the server responses.
        _save_stats(map_winner, attack_score, defence_score) : local func
            Save statistics of the players.
        _check_event_for_winner(event) : local func
            Check if a team has won a map and/or the PUG.
        format_scoreboard(to_message) : func
            Format scoreboard to display in a `discord.Embed` or `discord.Message`.
        format_killfeed() : func
            Format killfeed to display in a `discord.Message`.
        refresh() : func
            Refresh server information.
        to_embed() : func
            Format all server information to a `discord.Embed`.

    """

    encoding = "iso-8859-1"

    @classmethod
    def connect(
        cls,
        pug,
        ip: str,
        port: int,
        password: str = None,
        rcon_password: str = None,
    ):
        """Connect to CoD4 server.

        Parameters:
            pug : RunningPUG
                The corresponding PUG.
            ip : str
                Server IP.
            port : int
                Server port.
            password : str, optional
                Server password.
                Defaults to `None`.
            rcon_password : str, optional
                Server rcon password.
                Defaults to `None`.

        """
        server = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        server.connect((ip, int(port)))
        self = cls(
            server=server,
            ip=ip,
            port=port,
            password=password,
            rcon_password=rcon_password,
            pug=pug,
        )
        server.close()
        return self

    def __init__(
        self,
        pug,
        server: socket.socket,
        ip: str,
        port: int,
        password: str = None,
        rcon_password: str = None,
    ):
        """Create a server.

        Parameters:
            pug : RunningPUG
                The PUG the server was added to.
            server : socket.socket
                Server connection.
            ip : str
                Server IP.
            port : int
                Server port.
            password : str, optional
                Server password.
                Defaults to `None`.
            rcon_password : str, optional
                Server rcon password.
                Defaults to `None`.

        """
        self.pug = pug
        self.ip = ip
        self.port = int(port)
        self.password = password
        self.rcon_password = rcon_password
        self.counter = {
            "maps": 0,
            "rounds": 0,
            "attack": {"maps": 0, "rounds": 0},
            "defence": {"maps": 0, "rounds": 0},
        }
        self.start_winner_check: bool = False
        self.stats_tracker = StatsTracker(pug=self.pug)
        self.killfeed: list = []
        self._clients = None
        self.map: str = None
        self.gametype: str = None
        self.promod_mode: str = None
        self.players: list = None
        self.attack: PromodTeam = None
        self.defence: PromodTeam = None
        self.map_uptime: int = None
        self.prev_tick_info: list = None
        self.prev_tick: str = None
        self.name: str = None
        self.mod: str = None
        self.slots: int = None
        self.promod_version: str = None

        info, status = self._get_details(server=server)

        if info is not None and status is not None:
            self.name = (
                re.sub(r"\^[0-9]", "", info["hostname"])
                if "hostname" in info
                else re.sub(r"\^[0-9]", "", status["hostname"])
                if "sv_hostname" in status
                else None
            )
            self.mod = info["game"].split("/")[-1] if "game" in info else None
            self.slots = (
                int(info["sv_maxclients"])
                if "sv_maxclients" in info
                else int(status["sv_maxclients"])
                if "sv_maxclients" in status
                else None
            )
            self.promod_version = status.get("__promod_version")

        self._get_location()

    def __repr__(self) -> str:
        """Name when printed."""
        return f"<{self.__class__.__name__} {self.ip=} {self.port=}>"

    def __str__(self) -> str:
        """Name when formatted to string."""
        return f"<{self.__class__.__name__} {self.ip=} {self.port=}>"

    @property
    def ip_port(self) -> str:
        """Return IP:Port of the server."""
        return f"{self.ip}:{self.port}"

    @property
    def teams(self) -> Optional[list]:
        """Return Attack and Defence.

        Only possible if Promod scorebot is enabled.

        """
        if self.attack is not None and self.defence is not None:
            return [self.attack, self.defence]
        return None

    @property
    def promod_players(self) -> Optional[list]:
        """Return players in Attack and Defence.

        Only possible if Promod scorebot is enabled.

        """
        if (
            self.teams is not None
            and self.attack.players is not None
            and self.defence.players is not None
        ):
            return self.attack.players + self.defence.players
        return None

    @property
    def clients(self) -> Optional[int]:
        """Return number of clients connected to the server.

        Is retrieved from (in priority order):
            1. Server info.
            2. Number of players in Attack and Defence.
               Only works if Promod scorebot is enabled.

        """
        if self._clients is not None:
            return self._clients
        elif self._clients is None and self.promod_players is not None:
            return len(self.promod_players)
        return None

    @property
    def round(self) -> Optional[int]:
        """Return current round played."""
        if self.teams is not None:
            return self.attack.score + self.defence.score
        return None

    @property
    def max_rounds(self) -> Optional[int]:
        """Calculate maximum number of rounds played.

        Depends on Promod mode (i.e. MR).

        """
        if self.promod_mode is not None:
            return 2 * int(re.findall(r"mr([0-9]{1,2})", self.promod_mode)[0])
        return None

    @property
    def draw_rounds(self) -> Optional[int]:
        """Calculate number of rounds needed for draw."""
        if self.promod_mode is None:
            return None
        elif self.pug.mode == "bo1" or self.pug.mode == "bo2":
            return int(self.max_rounds / 2 * self.pug.max_maps)
        return int(self.max_rounds / 2)

    def _get_location(self) -> None:
        """Get server geolocation information."""
        try:
            response = DbIpCity.get(self.ip, api_key="free")
            self.location = {
                "country": response.country,
                "city": response.city,
                "region": response.region,
            }
        except Exception:
            self.location = None
            logger.error(
                "Location of server %s could not be determined", self.ip, exc_info=True
            )

    def _send_request(self, server: socket.socket, request: bytes) -> Optional[list]:
        """Send request to server.

        Parameters:
            server : socket.socket
                Server to request information from.
            request : bytes
                Request to send to the server.

        Returns:
            list
                Response of the server.

        """
        server.send(request)
        server.settimeout(1.0)
        try:
            return server.recv(4096).decode(self.encoding).split("\n")
        except socket.timeout:
            logger.error(
                "Request '%s' for server %s timed out",
                self.ip_port,
                request,
                exc_info=True,
            )
        return None

    def _send_requests(
        self,
        server: socket.socket,
    ) -> tuple[Optional[dict], Optional[dict], Optional[list]]:
        """Send requests to server.

        Parameters:
            server : `socket.socket`
                Connection to server.

        Returns:
            dict
                Info response information.
            dict
                Status response information.
            dict
                Rcon PB information.

        """
        info = self._send_request(server=server, request=b"""\xFF\xFF\xFF\xFFgetinfo""")
        status = self._send_request(
            server=server, request=b"""\xFF\xFF\xFF\xFFgetstatus"""
        )
        pb_plist = (
            self._send_request(
                server=server,
                request=f"\xFF\xFF\xFF\xFFrcon {self.rcon_password} pb_sv_plist".encode(
                    self.encoding
                ),
            )
            if self.rcon_password is not None
            else None
        )

        return (
            dict(zip(info[1].split("\\")[1::2], info[1].split("\\")[2::2]))
            if info is not None
            else None,
            dict(
                zip(
                    [*status[1].split("\\")[1::2], "players"],
                    [*status[1].split("\\")[2::2], status[2:-1]],
                )
            )
            if status is not None
            else None,
            pb_plist,
        )

    def _update_basic_details(self, info: dict, status: dict) -> None:
        """Update basic server details.

        Parameters:
            info : dict
                Server information.
            status : dict
                Server status information.

        """
        if info is not None:
            self._clients = int(info["clients"]) if "clients" in info else None
        if info is not None and status is not None:
            self.map = info.get("mapname") or status.get("mapname")
            self.gametype = status.get("g_gametype") or info.get("gametype")
        if status is not None:
            self.promod_mode = status.get("__promod_mode")

    @staticmethod
    def _update_players_and_teams(
        status: dict,
    ) -> tuple[Optional[list], Optional[PromodTeam], Optional[PromodTeam]]:
        """Update Players, Attack and Defence.

        Updating of Attack and Defence only possible if Promod scorebot is enabled.

        Parameters:
            status : dict
                Server status.

        Returns:
            list or None
                Player list with score and ping.
            PromodTeam or None
                Attack with its players as `PromodPlayer`.
            PromodTeam or None
                Defence with its players as `PromodPlayer`.

        """
        if status is None:
            return

        players = (
            (
                sorted(
                    (Player(info=info) for info in status["players"]),
                    key=lambda x: x.score,
                    reverse=True,
                )
                if status["players"]
                else None
            )
            if len(status) >= 2
            else None
        )

        attack = (
            PromodTeam(name="attack", info=status["__promod_attack_score"])
            if "__promod_attack_score" in status
            else None
        )

        defence = (
            PromodTeam(name="defence", info=status["__promod_defence_score"])
            if "__promod_defence_score" in status
            else None
        )

        return players, attack, defence

    def _update_map_uptime(self, status: dict) -> Optional[int]:
        """Update map uptime.

        Only possible if `g_mapStartTime` given in server status.

        Parameters:
            status : dict
                Server status.

        Returns
            int or None
                Map uptime.

        """
        if status is None:
            return

        try:
            map_starttime = (
                datetime.strptime(status["g_mapStartTime"], "%a %b %d %H:%M:%S %Y")
                if "g_mapStartTime" in status
                else None
            )
            if map_starttime is not None:
                time_delta = datetime.datetime.now(
                    tz=pytz.timezone("UTC")
                ) - map_starttime.replace(tzinfo=pytz.timezone("UTC"))
                return round(time_delta.seconds / 60)
        except ValueError:
            logger.error(
                "Map uptime could not be calculated for server %s with status %s",
                self.ip_port,
                status,
                exc_info=True,
            )
        return None

    def _update_player_info(self) -> None:
        """Update the team player infos (score and ping)."""
        if self.teams is not None and self.players is not None:
            for team in self.teams:
                if team.players is None:
                    continue
                for player in self.players:
                    team.add_info(
                        name=player.name, score=player.score, ping=player.ping
                    )

    def _update_player_pb_info(self, pb_plist: list) -> None:
        """Update players PB GUIDs and IPs from PB player list.

        Parameters:
            pb_plist : list
                PB player list.

        """
        if pb_plist is None or len(pb_plist) <= 2:
            return

        try:
            # Second last entry contains number of players on server.
            if int(re.findall(r"([0-9]{1,2}) Player", pb_plist[-2])[0]) == 0:
                return

            # First and last two elements unnecessary information.
            for entry in pb_plist[2:-2]:
                name = re.sub(r"\^[0-9]", "", re.findall(r"\"(.*)\"", entry)[0])
                pb_guid = re.findall(r"[A-z0-9]{32}", entry)[0][-8:]
                ip = re.findall(
                    r"([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3})", entry
                )[0]
                if self.teams is not None:
                    for team in self.teams:
                        player = team.add_info(name=name, pb_guid=pb_guid, ip=ip)
                        self.stats_tracker.update_player(player)
                if self.players is not None:
                    for player in self.players:
                        if name == player:
                            player.pb_guid = pb_guid
                            player.ip = ip
        except IndexError:
            logger.error(
                "No players in pb_plist %s of server %s",
                pb_plist,
                self.ip_port,
                exc_info=True,
            )
        return

    def _update_and_convert_promod_ticker(self, status: dict) -> Optional[list[Event]]:
        """Update Promod ticker information and convert to corresponding ´Event´.

        Parameters:
            status : dict
                Server status.

        Returns:
            list or None
                Ticker information.

        """
        if status is None or "__promod_ticker" not in status:
            return None

        ticker_info = status["__promod_ticker"].split("\x01")

        # First element of ticker info is a number enumerating the Promod scorebot ticks.
        # It only gets incremented if new information are contained.
        if self.prev_tick != (cur_tick := ticker_info[0]):
            logger.debug("New ticker for server %s: %s", self.ip_port, ticker_info)
            self.prev_tick = cur_tick
            if len(ticker_info) <= 1:
                return None
            ticker_events = []
            ticker_info = ticker_info[1:]
            for i, event in enumerate(ticker_info):
                if not (event in EVENTS and event in EVENTS_MAP):
                    continue
                info = (
                    ticker_info[i : i + EVENTS[event] + 3]
                    if (
                        len(ticker_info[i:]) > EVENTS[event] + 3
                        and ticker_info[i + EVENTS[event] + 1] == "assist_by"
                    )
                    else ticker_info[i : i + EVENTS[event] + 1]
                )
                ticker_events.append(EVENTS_MAP[event](info=info))
            return ticker_events
        return None

    def _save_stats(self, map_winner: str, attack_score: int, defence_score: int):
        """Save stats.

        Parameters:
            map_winner : str
                Name of the team that won the map.
            attack_score : int
                Attacking team's round score.
            defence_score : int
                Defending team's round score.

        """
        logger.debug(
            "Saving stats for server %s. %s won, score is %s-%s",
            self.ip_port,
            map_winner,
            attack_score,
            defence_score,
        )
        self.stats_tracker.save_stats_to_db(
            killfeed=self.killfeed,
            map_winner=map_winner,
            attack_score=attack_score,
            defence_score=defence_score,
        )
        self.stats_tracker = StatsTracker(pug=self.pug)
        self.killfeed = []

    def _check_event_for_winner(self, event) -> tuple[bool, bool]:
        """Check if a team has won the PUG or a map is finished.

        If a team wins a round, the team scores reveal whether a team
        has won the map or the map is a draw. This depends on the PUG
        mode (BO1, BO2 etc.) and the PUG size (1v1, 2v2 etc.).
        If a map is finished (i.e. all rounds played or number of rounds
        to win are reached), the stats are saved to the data base table.
        If the winning team has won enough maps, the PUG is ended.

        Parameters:
            event : Event
                Event which is being checked for a winner.

        Returns:
            bool
                If `True` stats can be saved because a map/match is finished.
            bool
                If `True` PUG can be ended because match is finished.

        """
        save_stats = False
        end_pug = False

        logger.debug("Checking for winner on server %s", self.ip_port)

        if isinstance(event, StartText) and event == "1st_half_started":
            self.start_winner_check = True
        elif not self.start_winner_check or self.max_rounds is None:
            return save_stats, end_pug

        if isinstance(event, RoundWinner):
            logger.debug(
                "Round on server %s won by %s. Score: %s-%s",
                self.ip_port,
                event.winner,
                event.attack_score,
                event.defence_score,
            )
            self.counter["rounds"] += 1
            self.counter[event.winner]["rounds"] += 1

            if self.pug.mode == "bo1" or self.pug.mode == "bo2":
                if event.attack_score == event.defence_score == self.draw_rounds:
                    self.counter["maps"] += 1
                    self.pug.attack["result"] = "draw"
                    self.pug.defence["result"] = "draw"
                    save_stats = True
                    end_pug = True
                elif event.attack_score > self.draw_rounds:
                    self.counter["maps"] += 1
                    self.counter["attack"]["maps"] += 1
                    self.pug.attack["result"] = "win"
                    self.pug.defence["result"] = "loss"
                    save_stats = True
                    end_pug = True
                elif event.defence_score > self.draw_rounds:
                    self.counter["maps"] += 1
                    self.counter["defence"]["maps"] += 1
                    self.pug.attack["result"] = "loss"
                    self.pug.defence["result"] = "win"
                    save_stats = True
                    end_pug = True
            else:
                # increment counter
                if event.attack_score == event.defence_score == self.draw_rounds:
                    save_stats = True
                elif event.attack_score > self.draw_rounds:
                    self.counter["maps"] += 1
                    self.counter["attack"]["maps"] += 1
                    save_stats = True
                elif event.defence_score > self.draw_rounds:
                    self.counter["maps"] += 1
                    self.counter["defence"]["maps"] += 1
                    save_stats = True

                # save results in PUG
                if self.counter["attack"]["maps"] >= self.pug.win_maps:
                    self.pug.attack["result"] = "win"
                    self.pug.defence["result"] = "loss"
                    end_pug = True
                elif self.counter["defence"]["maps"] >= self.pug.win_maps:
                    self.pug.attack["result"] = "loss"
                    self.pug.defence["result"] = "win"
                    end_pug = True

        if save_stats:
            self.start_winner_check = False

        return save_stats, end_pug

    def _add_to_killfeed_and_check_for_winner(self, events: list[Event]):
        """Format recent killfeed to message.

        If a winner has been found, the PUG will be ended.
        If a map is finished **or** a winner has been found, save stats to DB.
        In any of the two above cases, adding to the killfeed will stop, because of

        Process the Promod ticker information. After each event name,
        a fixed number of information follows, which can be found in
        `cogs.utils.cod4.sorebot.dicts.EVENTS`. After each event,
        the exact amount of information following is then used to create
        the corresponding `Event` instance, which is stored in the killfeed
        and passed to the stats tracker.
        This function also checks for a winner after each event.

        Parameters:
            events : list
                Ticker events.

        """
        if events is None:
            return

        for event in events:
            self.killfeed.append(event)
            save_stats, end_pug = self._check_event_for_winner(event=event)
            if save_stats:
                self._save_stats(
                    map_winner=event.winner,
                    attack_score=event.attack_score,
                    defence_score=event.defence_score,
                )
            if end_pug:
                self.pug.end_pug(reported_by_scorebot=True)

            if save_stats or end_pug:
                break
        return

    def _get_details(
        self, server: socket.socket
    ) -> tuple[Optional[dict], Optional[dict]]:
        """Get server details.

        Parameters:
            server : `socket.socket`
                Connection to server.

        Returns:
            dict or None
                Server information.
                Only returned if server responded.
            dict or None
                Server status.
                Only returned if server responded.

        """
        info, status, pb_plist = self._send_requests(server=server)

        self._update_basic_details(info=info, status=status)
        self.players, self.attack, self.defence = self._update_players_and_teams(
            status=status
        )
        self.map_uptime = self._update_map_uptime(status=status)
        self._update_player_info()
        self._update_player_pb_info(pb_plist=pb_plist)
        self.current_killfeed = self._update_and_convert_promod_ticker(status=status)
        self._add_to_killfeed_and_check_for_winner(events=self.current_killfeed)

        return info, status

    def refresh(self):
        """Refresh server information."""
        logger.debug("Refreshing server %s", self.ip_port)
        server = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        server.connect((self.ip, self.port))
        self._get_details(server=server)
        server.close()

    def format_scoreboard(self, to_message: bool = False) -> list[str]:
        r"""Format scores to scoreboard.

        Parameters:
            to_message : bool, optional
                `True` if the scoreboard is to be posted in a `discord.Message`,
                `False` if in multi-line code.
                Defaults to `False`.

        Returns:
            list
                Lines of the scoreboard.
                Can be joined e.g. by "\n" or "> \n".

        """
        scoreboard = []
        if self.teams is not None and self.promod_players is not None:
            newline = "\n"
            if to_message:
                scoreboard.append(
                    f"""{EMOJIS["attack"]} {self.attack.score} - """
                    f"""{self.defence.score} {EMOJIS["defence"]}{newline}"""
                )
                template = " {status} {name:<16}{score:>6}{kills:>6}{assists:>8}{deaths:>7}{ping:>5}"
                dashes = "{:-^52}"
            else:
                scoreboard.append(
                    f"""{self.attack.name_to_string()} {self.attack.score}"""
                    """ - """
                    f"""{self.defence.score} {self.defence.name_to_string()}{newline}"""
                )
                template = " {status:^4} {name:<16}{score:>6}{kills:>6}{assists:>8}{deaths:>7}{ping:>5}"
                dashes = "{:-^54}"
            header = {
                "status": "  " if to_message else "",
                "name": "Name",
                "score": "Score",
                "kills": "Kills",
                "assists": "Assists",
                "deaths": "Deaths",
                "ping": "Ping",
            }
            for team in self.teams:
                if to_message:
                    scoreboard.append(
                        f"{EMOJIS[team.name]} {team.name_to_string()} ({len(team.players)})"
                    )
                else:
                    scoreboard.append(
                        f"\t{team.name_to_string()} ({len(team.players)})"
                    )
                scoreboard.append(template.format(**header))
                scoreboard.append(dashes.format(""))
                for player in team.players:
                    if to_message:
                        status = (
                            EMOJIS["dead"]
                            if not player.alive
                            else EMOJIS["bomb"]
                            if player.bombcarrier
                            else ""
                        )
                    else:
                        status = (
                            "DEAD"
                            if not player.alive
                            else "BOMB"
                            if player.bombcarrier
                            else ""
                        )
                    scoreboard.append(
                        template.format(
                            status=status,
                            name=player.name[:15],
                            score=player.score,
                            kills=player.kills,
                            assists=player.assists,
                            deaths=player.deaths,
                            ping=player.ping or "?",
                        )
                    )
                scoreboard.append("\n")
        elif self.players is not None:
            header = {"name": "Name", "score": "Score", "ping": "Ping"}
            template = "{name:<15}{score:>7}{ping:>5}"
            scoreboard.append(template.format(**header))
            scoreboard.append("{:-^27}".format(""))
            for player in self.players:
                scoreboard.append(
                    template.format(
                        name=player.name[:15], score=player.score, ping=player.ping
                    )
                )
        return scoreboard

    def to_embed(
        self, add_scoreboard: bool = False, color: int = 0x00FF94
    ) -> discord.Embed:
        """Convert server information to `discord.Embed`.

        Raises:
            Exception
                Raised if a map image is not accessible via given URL in
                `MAP_URLS` in `cogs.utils.cod4.scorebot.dicts`.

        Parameters:
            add_scoreboard : bool, optional
                If `True`, the scoreboard is displayed in the embed.
                Defaults to `False`.
            color : int, optional
                Color of the embed.
                Defaults to `0x00FF94`.

        Returns:
            `discord.Embed`
                Contains all server information available.

        """
        if self.promod_version is None:
            embed = discord.Embed(title=self.name, color=color)
        else:
            embed = discord.Embed(
                title=self.name, description=self.promod_version, color=color
            )
        embed.add_field(
            name="IP",
            value=f"{self.ip_port}",
            inline=True,
        )
        if self.location is not None:
            embed.add_field(
                name="Location",
                value=f"""{self.location["country"]}""",
                inline=True,
            )
        if self.clients is not None:
            embed.add_field(
                name="Players",
                value=f"{self.clients}/{self.slots}"
                if self.slots is not None
                else f"{self.clients}",
                inline=True,
            )
        if self.map is not None:
            embed.add_field(
                name="Map",
                value=f"{self.map}",
                inline=True,
            )
            if self.map in MAP_URLS:
                try:
                    embed.set_thumbnail(url=MAP_URLS[self.map])
                except Exception:
                    logger.error(
                        "No map image available under %s",
                        MAP_URLS[self.map],
                        exc_info=True,
                    )
        if self.round is not None and self.max_rounds is not None:
            embed.add_field(
                name="Round",
                value=f"{self.round}/{self.max_rounds}",
                inline=True,
            )
        if self.teams is not None:
            embed.add_field(
                name="Score",
                value=(
                    f"""{EMOJIS["attack"]} {self.attack.score} - """
                    f"""{self.defence.score} {EMOJIS["defence"]}"""
                ),
                inline=True,
            )
        if add_scoreboard and (self.players is not None or self.teams is not None):
            newline = "\n"
            embed.add_field(
                name="Scoreboard",
                value=f"""```{newline}{f"{newline}".join(self.format_scoreboard())}{newline}```""",
                inline=False,
            )
        embed.set_footer(
            text=(
                f"""Last updated: """
                f"""{datetime.datetime.now(tz=self.pug.cog.bot.timezone).strftime("%H:%M %Z")}"""
            )
        )
        return embed
