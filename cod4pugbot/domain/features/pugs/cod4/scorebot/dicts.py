import json
import pathlib

base_dir = pathlib.Path(__file__).absolute().parents[0].as_posix()

with open(base_dir + "/json/emojis.json") as emojis_file, open(
    base_dir + "/json/db_map.json"
) as db_map_file, open(base_dir + "/json/events.json") as events_file, open(
    base_dir + "/json/map_urls.json"
) as map_urls_file:
    EMOJIS = json.loads(emojis_file.read())
    DB_MAP = json.loads(db_map_file.read())
    EVENTS = json.loads(events_file.read())
    MAP_URLS = json.loads(map_urls_file.read())
