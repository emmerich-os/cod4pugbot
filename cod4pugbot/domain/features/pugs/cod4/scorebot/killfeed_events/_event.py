class Event:
    """Any killfeed event."""

    def __eq__(self, other):
        """Compare to object or string.

        Returns:
            `True` if other `Event` is the same, `False` otherwise
            `True` if event name is other, `False` otherwise.

        """
        if isinstance(other, self.__class__):
            return self.name == other.name
        return self.name == other

    def __repr__(self):
        """Name when printing."""
        items = (f"{k}={v}" for k, v in self.__dict__.items())
        return "<{} {}>".format(self.__class__.__name__, " ".join(items))

    def __str__(self):
        """Name when formatted to string."""
        items = (f"{k}={v}" for k, v in self.__dict__.items())
        return "<{} {}>".format(self.__class__.__name__, " ".join(items))
