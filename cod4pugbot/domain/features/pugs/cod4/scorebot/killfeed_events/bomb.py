import re

import cod4pugbot.domain.features.pugs.cod4
from ._event import Event


class PickupBomb(Event):
    """Bomb picked up.

    Attributes:
        name : str
            Name of the event.
            Should always be "pickup_bomb".
        player : str
            Name of the player that picked the bomb up.

    Methods:
        message() : func
            Formats the event to `str`.

    """

    def __init__(self, info: list) -> None:
        """Create bomb pick-up event.

        Parameters:
            info : list
                List with event information.
                Contains (in order):
                    - name of the event ("pickup_bomb")
                    - name of the player that picked the bomb up

        """
        self.name = info[0]
        self.player = re.sub(r"\^[0-9]", "", info[1])

    @property
    def message(self) -> str:
        """Print to message.

        Returns:
            message : str
                The event as a killfeed message.

        """
        return f"""``{self.player}`` picked the bomb up!"""

    def __eq__(self, other) -> bool:
        """Compare to object or string.

        Returns:
            `True` if other `Event` is the same, `False` otherwise
            `True` if other `PromodPlayer` picked the bomb up, `False` otherwise.
            `True` if event name is other, `False` otherwise.

        """
        if isinstance(other, Event):
            return self.name == other.name
        elif isinstance(other, cod4pugbot.cod4utils.PromodPlayer):
            return self.player == other.name
        return self.name == other


class DroppedBomb(Event):
    """Bomb dropped.

    Attributes:
        name : str
            Name of the event.
            Should always be "dropped_bomb".
        player : str
            Name of the player that dropped the bomb.

    Methods:
        message() : func
            Formats the event to `str`.

    """

    def __init__(self, info: list) -> None:
        """Create bomb drop event.

        Parameters:
            info : list
                List with event information.
                Contains (in order):
                    - name of the event ("dropped_bomb")
                    - name of the player that dropped the bomb

        """
        self.name = info[0]
        self.player = re.sub(r"\^[0-9]", "", info[1])

    @property
    def message(self) -> str:
        """Print to message.

        Returns:
            message : str
                The event as a killfeed message.

        """
        return f"""``{self.player}`` dropped the bomb!"""

    def __eq__(self, other) -> bool:
        """Compare to object or string.

        Returns:
            `True` if other `Event` is the same, `False` otherwise
            `True` if other `PromodPlayer` dropped the bomb, `False` otherwise.
            `True` if event name is other, `False` otherwise.

        """
        if isinstance(other, Event):
            return self.name == other.name
        elif isinstance(other, cod4pugbot.cod4utils.PromodPlayer):
            return self.player == other.name
        return self.name == other


class DefusedBy(Event):
    """Bomb defused.

    Attributes:
        name : str
            Name of the event.
            Should always be "defused_by".
        player : str
            Name of the player that defused the bomb.

    Methods:
        message() : func
            Formats the event to `str`.

    """

    def __init__(self, info: list) -> None:
        """Create bomb defuse event.

        Parameters:
            info : list
                List with event information.
                Contains (in order):
                    - name of the event ("defused_by")
                    - name of the player that defused the bomb

        """
        self.name = info[0]
        self.player = re.sub(r"\^[0-9]", "", info[1])

    @property
    def message(self) -> str:
        """Print to message.

        Returns:
            message : str
                The event as a killfeed message.

        """
        return f"""``{self.player}`` defused the bomb!"""

    def __eq__(self, other) -> bool:
        """Compare to object or string.

        Returns:
            `True` if other `Event` is the same, `False` otherwise
            `True` if other `PromodPlayer` defused the bomb, `False` otherwise.
            `True` if event name is other, `False` otherwise.

        """
        if isinstance(other, Event):
            return self.name == other.name
        elif isinstance(other, cod4pugbot.cod4utils.PromodPlayer):
            return self.player == other.name
        return self.name == other


class BombExploded(Event):
    """Bomb exploded.

    Attributes:
        name : str
            Name of the event.
            Should always be "bomb_exploded".

    Methods:
        message() : func
            Formats the event to `str`.

    """

    def __init__(self, info: list):
        """Create bomb explosion event.

        Parameters:
            info : list
                List with event information.
                Contains (in order):
                    - name of the event ("bomb_exploded")

        """
        self.name = info[0]

    @property
    def message(self) -> str:
        """Print to message.

        Returns:
            message : str
                The event as a killfeed message.

        """
        return """The bomb detonated!"""


class PlantedBy(Event):
    """Bomb planted.

    Attributes:
        name : str
            Name of the event.
            Should always be "planted_by".
        player : str
            Name of the player that planted the bomb.

    Methods:
        message() : func
            Formats the event to `str`.

    """

    def __init__(self, info: list) -> None:
        """Create plant event.

        Parameters:
            info : list
                List with event information.
                Contains (in order):
                    - name of the event ("planted_by")
                    - name of the player that planted the bomb

        """
        self.name = info[0]
        self.player = re.sub(r"\^[0-9]", "", info[1])

    @property
    def message(self) -> str:
        """Print to message.

        Returns:
            message : str
                The event as a killfeed message.

        """
        return f"""``{self.player}`` planted the bomb!"""

    def __eq__(self, other) -> bool:
        """Compare to object or string.

        Returns:
            `True` if other `Event` is the same, `False` otherwise
            `True` if other `PromodPlayer` is the planter, `False` otherwise.
            `True` if event name is other, `False` otherwise.

        """
        if isinstance(other, Event):
            return self.name == other.name
        elif isinstance(other, cod4pugbot.cod4utils.PromodPlayer):
            return self.player == other.name
        return self.name == other
