import re

import cod4pugbot.domain.features.pugs.cod4
from ._event import Event


class HQCaptured(Event):
    """Headquater captured.

    Attributes:
        name : str
            Name of the event.
            Should always be "hq_captured".
        player : str
            Name of the player that captured the HQ.

    Methods:
        message() : func
            Formats the event to `str`.

    """

    def __init__(self, info: list) -> None:
        """Create HQ captured event.

        Parameters:
            info : list
                List with event information.
                Contains (in order):
                    - name of the event ("hq_captured")
                    - name of the player that captured the HQ

        """
        self.name = info[0]
        self.player = re.sub(r"\^[0-9]", "", info[1])

    @property
    def message(self) -> str:
        """Print to message.

        Returns:
            message : str
                The event as a killfeed message.

        """
        return f"""Headquater captured by ``{self.player}``!"""

    def __eq__(self, other) -> bool:
        """Compare to object or string.

        Returns:
            `True` if other `Event` is the same, `False` otherwise
            `True` if other `PromodPlayer` captured the HQ, `False` otherwise.
            `True` if event name is other, `False` otherwise.

        """
        if isinstance(other, Event):
            return self.name == other.name
        elif isinstance(other, cod4pugbot.cod4PromodPlayer):
            return self.player == other.name
        return self.name == other


class HQDestroyed(Event):
    """Headquater destroyed.

    Attributes:
        name : str
            Name of the event.
            Should always be "hq_destroyed".
        player : str
            Name of the player that destroyed the HQ.

    Methods:
        message() : func
            Formats the event to `str`.

    """

    def __init__(self, info: list) -> None:
        """Create HQ destroyed event.

        Parameters:
            info : list
                List with event information.
                Contains (in order):
                    - name of the event ("hq_destroyed")
                    - name of the player that destroyed the HQ

        """
        self.name = info[0]
        self.player = re.sub(r"\^[0-9]", "", info[1])

    @property
    def message(self) -> str:
        """Print to message.

        Returns:
            message : str
                The event as a killfeed message.

        """
        return f"""Headquater destroyed by ``{self.player}``!"""

    def __eq__(self, other):
        """Compare to object or string.

        Returns:
            `True` if other `Event` is the same, `False` otherwise
            `True` if other `PromodPlayer` destroyed the HQ, `False` otherwise.
            `True` if event name is other, `False` otherwise.

        """
        if isinstance(other, Event):
            return self.name == other.name
        elif isinstance(other, cod4pugbot.cod4PromodPlayer):
            return self.player == other.name
        return self.name == other
