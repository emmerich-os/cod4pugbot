import re

import cod4pugbot.domain.features.pugs.cod4
from ..dicts import EMOJIS
from ._event import Event


class Kill(Event):
    """Kill event.

    Attributes:
        name : str
            Name of the event.
            Should always be "kill".
        killer : str
            Name of the killer.
        weapon : str
            Name of the weapon (games_mp.log like).
        killed : str
            Name of the killed player.
        headshot : bool
            True of kill was by headshot, False otherwise.
        assistant
            `AssistBy` if killer had an assistant, `None` otherwise.

    Methods:
        message() : func
            Formats the event to `str`.

    """

    def __init__(self, info: list) -> None:
        """Create kill event.

        Parameters:
            info : list
                List with event information.
                Contains (in order):
                    - name of the event ("kill")
                    - name of killer
                    - name of the weapon the killer used
                    - name of the killed player
                    - if kill was a headshot (0/1)
                If killer had an assistant:
                    - name of the event ("assist_by")
                    - name of the player that assisted the kill

        """
        self.name = info[0]
        self.killer = re.sub(r"\^[0-9]", "", info[1])
        self.weapon = info[2]
        self.killed = re.sub(r"\^[0-9]", "", info[3])
        self.headshot = bool(int(info[4]))
        if len(info) == 7:
            self.assistant = AssistBy(info=info[-2:])
        else:
            self.assistant = None

    @property
    def message(self) -> str:
        """Print to message.

        Returns:
            message : str
                The event as a killfeed message.

        """
        if self.headshot:
            if self.assistant is None:
                return (
                    f"""``{self.killer}`` {EMOJIS[self.weapon]}"""
                    f"""{EMOJIS["headshot"]} ``{self.killed}``"""
                )
            else:
                return (
                    f"""``{self.killer}`` + """
                    f"""``{self.assistant.player}`` {EMOJIS[self.weapon]}"""
                    f"""{EMOJIS["headshot"]} ``{self.killed}``"""
                )
        else:
            if self.assistant is None:
                return (
                    f"""``{self.killer}`` {EMOJIS[self.weapon]} """
                    f"""``{self.killed}``"""
                )
            else:
                return (
                    f"""``{self.killer}`` + """
                    f"""``{self.assistant.player}`` """
                    f"""{EMOJIS[self.weapon]} ``{self.killed}``"""
                )
        return ""

    def __eq__(self, other) -> bool:
        """Compare to object or string.

        Returns:
            - `True` if other `Event` is the same, `False` otherwise
            - `True` if other `PromodPlayer` is the killer, `False` otherwise.
            - `True` if event name is other, `False` otherwise.

        """
        if isinstance(other, Event):
            return self.name == other.name
        elif isinstance(other, cod4pugbot.cod4utils.PromodPlayer):
            return self.killer == other.name
        return self.name == other


class AssistBy(Event):
    """Kill assist.

    Attributes:
        name : str
            Name of the event.
            Should always be "assist_by".
        player : str
            Name of the player that assisted the killer.

    """

    def __init__(self, info: list) -> None:
        """Create kill assist event.

        Parameters:
            info : list
                List with event information.
                Contains (in order):
                    - name of the event ("assist_by")
                    - name of the player that assisted the kill

        """
        self.name = info[0]
        self.player = re.sub(r"\^[0-9]", "", info[1])

    def __eq__(self, other) -> bool:
        """Compare to object or string.

        Returns:
            - `True` if other `Event` is the same, `False` otherwise
            - `True` if other `PromodPlayer` is the assistant, `False` otherwise.
            - `True` if event name is other, `False` otherwise.

        """
        if isinstance(other, Event):
            return self.name == other.name
        elif isinstance(other, cod4pugbot.cod4utils.PromodPlayer):
            return self.player == other.name
        return self.name == other
