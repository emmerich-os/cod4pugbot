from ._event import Event


class Map(Event):
    """Map changed.

    Attributes:
        name : str
            Name of the event.
            Should always be "map".
        mapname : str
            Name of the new map.
        gametype : str
            Gametype of the new map.

    Methods:
        message() : func
            Formats the event to `str`.

    """

    def __init__(self, info: list) -> None:
        """Create map change event.

        Parameters:
            info : list
                List with event information.
                Contains (in order):
                    - name of the event ("map")
                    - name of the new map
                    - gametype of the new map

        """
        self.name = info[0]
        self.mapname = info[1]
        self.gametype = info[2]

    @property
    def message(self) -> str:
        """Print to message.

        Returns:
            message : str
                The event as a killfeed message.

        """
        return f"""Map changed to ``{self.mapname}`` ({self.gametype.upper()})."""


class MapComplete(Event):
    """Map completed.

    Attributes:
        name : str
            Name of the event.
            Should always be "map_complete".
        attack_score : int
            Attacking team's score.
        defence_score : int
            Defending team's score.

    Methods:
        message() : func
            Formats the event to `str`.

    """

    def __init__(self, info: list) -> None:
        """Create map completion event.

        Parameters:
            info : list
                List with event information.
                Contains (in order):
                    - name of the event ("map_complete")
                    - attacking team's score
                    - defending team's score

        """
        self.name = info[0]
        self.attack_score = int(info[2])
        self.defence_score = int(info[4])

    @property
    def message(self) -> str:
        """Print to message.

        Returns:
            message : str
                The event as a killfeed message.

        """
        return (
            """Map finished!\n"""
            f"""Final score: Attack ``{self.attack_score} - {self.defence_score}`` Defence."""
        )
