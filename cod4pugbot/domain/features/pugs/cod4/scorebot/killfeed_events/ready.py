from ._event import Event


class RdyText(Event):
    """Ready up phase.

    Attributes:
        name : str
            Name of the event.
            Can be "1st_half_ready_up", "2nd_half_ready_up",
            or "timeout_ready_up".

    Methods:
        message() : func
            Formats the event to `str`.

    """

    def __init__(self, info: list) -> None:
        """Create ready text event.

        Parameters:
            info : list
                List with event information.
                Contains (in order):
                    - name of the event ("1st_half_ready_up",
                        "2nd_half_ready_up", or "timeout_ready_up")

        """
        self.name = info[0]

    @property
    def message(self) -> str:
        """Print to message.

        Returns:
            message : str
                The event as a killfeed message.

        """
        return f"""{self.name.replace("_", " ")} phase started!"""
