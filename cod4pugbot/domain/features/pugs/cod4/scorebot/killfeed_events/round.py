from ..dicts import EMOJIS
from ._event import Event


class RoundWinner(Event):
    """Round winning team.

    Attributes:
        name : str
            Name of the event.
            This always is "round_winner".
        winner : str
            Name of the team that won the round, or "tie".
            If a team won, either "attack" or "defence".
        attack_score : int
            Attacking team's score.
        defence_score : int
            Defending team's score.

    Methods:
        message() : func
            Formats the event to `str`.

    """

    def __init__(self, info: list) -> None:
        """Create round win event.

        Parameters:
            info : list
                List with event information.
                Contains (in order):
                    - name of the event ("round_winner")
                    - name of the winning team
                    - attacking team's score
                    - defending team's score

        """
        self.name = info[0]
        self.winner = info[1]
        self.attack_score = int(info[2])
        self.defence_score = int(info[3])

    @property
    def message(self) -> str:
        """Print to message.

        Returns:
            message : str
                The event as a killfeed message.

        """
        newline = "\n"
        return (
            f"""{self.winner[0].upper()}{self.winner[1:]} won the round!{newline}"""
            f"""New score: {EMOJIS["attack"]} ``{self.attack_score} - {self.defence_score}`` """
            f"""{EMOJIS["defence"]}."""
        )


class KnifeRound(Event):
    """Knife round.

    Attributes:
        name : str
            Name of the event.
            Should always be "knife_round".

    Methods:
    message() : func
        Formats the event to `str`.

    """

    def __init__(self, info: list) -> None:
        """Create knife round start event.

        Parameters:
            info : list
                List with event information.
                Contains (in order):
                    - name of the event ("knife_round")

        """
        self.name = info[0]

    @property
    def message(self) -> str:
        """Print to message.

        Returns:
            message : str
                The event as a killfeed message.

        """
        return """Knife round started!"""


class StartText(Event):
    """Start of half, match resumed after timeout or round.

    Attributes:
        name : str
            Name of the event.
            Can be "1st_half_started", "2nd_half_started",
            "match_resumed" (from timeout) or "round_start".
        round : int
            Round that started.

    Methods:
        message() : func
            Formats the event to `str`.

    """

    def __init__(self, info: list) -> None:
        """Create start event.

        Parameters:
            info : list
                List with event information.
                Contains (in order):
                    - name of the event, can be either of:
                        * "1st_half_started",
                        * "2nd_half_started"
                        * "match_resumed" (from timeout)
                        * "round_start"
                    - round that started

        """
        self.name = info[0]
        self.round = int(info[1])

    @property
    def message(self) -> str:
        """Print to message.

        Returns:
            message : str
                The event as a killfeed message.

        """
        if "half_started" in self.name:
            return (
                f"""{self.name.split("_")[0]} half started with round {self.round}!"""
            )
        elif self.name == "match_resumed":
            return """Match resumed from timeout."""
        elif self.name == "round_start":
            return f"""Round {self.round} started."""
        return ""
