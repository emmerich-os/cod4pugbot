import re

import cod4pugbot.domain.features.pugs.cod4
from ._event import Event


class TimeoutCancelled(Event):
    """Timeout cancelled.

    Attributes:
        name : str
            Name of the event.
            Should always be "timeout_cancelled".
        team : str
            Name of the team the timeout was cancelled for.
        player : str
            Name of the player that cancelled the timeout.

    Methods:
        message() : func
            Formats the event to `str`.

    """

    def __init__(self, info: list) -> None:
        """Create timeout cancel event.

        Parameters:
            info : list
                List with event information.
                Contains (in order):
                    - name of the event ("timeout_cancelled")
                    - name of the team the timeout was cancelled for
                    - name of the player that cancelled the timeout

        """
        self.name = info[0]
        self.team = info[1]
        self.player = re.sub(r"\^[0-9]", "", info[2])

    @property
    def message(self) -> str:
        """Print to message.

        Returns:
            message : str
                The event as a killfeed message.

        """
        return (
            f"""Timeout for team ``{self.team[0].upper()}{self.team[1:]}`` """
            f"""cancelled by ``{self.player}``!"""
        )

    def __eq__(self, other) -> bool:
        """Compare to object or string.

        Returns:
            `True` if other `Event` is the same, `False` otherwise
            `True` if other `PromodPlayer` cancelled the timeout, `False` otherwise.
            `True` if event name is other, `False` otherwise.

        """
        if isinstance(other, Event):
            return self.name == other.name
        elif isinstance(other, cod4pugbot.cod4utils.PromodPlayer):
            return self.player == other.name
        return self.name == other


class TimeoutCalled(Event):
    """Timeout called.

    Attributes:
        name : str
            Name of the event.
            Should always be "timeout_called".
        team : str
            Name of the team the timeout was called for.
        player : str
            Name of the player that called the timeout.

    Methods:
        message() : func
            Formats the event to `str`.

    """

    def __init__(self, info: list) -> None:
        """Create timeout call event.

        Parameters:
            info : list
                List with event information.
                Contains (in order):
                    - name of the event ("timeout_called")
                    - name of the team the timeout was called for
                    - name of the player that called the timeout

        """
        self.name = info[0]
        self.team = info[1]
        self.player = re.sub(r"\^[0-9]", "", info[2])

    @property
    def message(self) -> str:
        """Print to message.

        Returns:
            message : str
                The event as a killfeed message.

        """
        return (
            f"""Timeout called for team ``{self.team[0].upper()}{self.team[1:]}`` """
            f"""by ``{self.player}``!"""
        )

    def __eq__(self, other) -> bool:
        """Compare to object or string.

        Returns:
            `True` if other `Event` is the same, `False` otherwise
            `True` if other `PromodPlayer` called the timeout, `False` otherwise.
            `True` if event name is other, `False` otherwise.

        """
        if isinstance(other, Event):
            return self.name == other.name
        elif isinstance(other, cod4pugbot.cod4utils.PromodPlayer):
            return self.player == other.name
        return self.name == other
