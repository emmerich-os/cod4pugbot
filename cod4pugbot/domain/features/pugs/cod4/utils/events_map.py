from cod4pugbot.domain.features.pugs.cod4.scorebot import BombExploded
from cod4pugbot.domain.features.pugs.cod4.scorebot import Captured
from cod4pugbot.domain.features.pugs.cod4.scorebot import DefusedBy
from cod4pugbot.domain.features.pugs.cod4.scorebot import DroppedBomb
from cod4pugbot.domain.features.pugs.cod4.scorebot import HQCaptured
from cod4pugbot.domain.features.pugs.cod4.scorebot import HQDestroyed
from cod4pugbot.domain.features.pugs.cod4.scorebot import Kill
from cod4pugbot.domain.features.pugs.cod4.scorebot import KnifeRound
from cod4pugbot.domain.features.pugs.cod4.scorebot import Map
from cod4pugbot.domain.features.pugs.cod4.scorebot import MapComplete
from cod4pugbot.domain.features.pugs.cod4.scorebot import PickupBomb
from cod4pugbot.domain.features.pugs.cod4.scorebot import PlantedBy
from cod4pugbot.domain.features.pugs.cod4.scorebot import RdyText
from cod4pugbot.domain.features.pugs.cod4.scorebot import RoundWinner
from cod4pugbot.domain.features.pugs.cod4.scorebot import StartText
from cod4pugbot.domain.features.pugs.cod4.scorebot import TimeoutCalled
from cod4pugbot.domain.features.pugs.cod4.scorebot import TimeoutCancelled

EVENTS_MAP = {
    "round_winner": RoundWinner,
    "map_complete": MapComplete,
    "knife_round": KnifeRound,
    "1st_half_started": StartText,
    "2nd_half_started": StartText,
    "match_resumed": StartText,
    "round_start": StartText,
    "map": Map,
    "kill": Kill,
    "1st_half_ready_up": RdyText,
    "2nd_half_ready_up": RdyText,
    "timeout_ready_up": RdyText,
    "timeout_cancelled": TimeoutCancelled,
    "timeout_called": TimeoutCalled,
    "captured": Captured,
    "hq_captured": HQCaptured,
    "hq_destroyed": HQDestroyed,
    "pickup_bomb": PickupBomb,
    "dropped_bomb": DroppedBomb,
    "defused_by": DefusedBy,
    "bomb_exploded": BombExploded,
    "planted_by": PlantedBy,
}
