import re


class Player:
    """Player on a CoD4 server.

    Attributes:
        score : int
            Score of the player.
        ping : int
            Ping of the player.
        name : str
            Name of the player

    """

    def __init__(self, info: str) -> None:
        """Create a Promod player.

        Parameters:
            info : str
                Information of the player.
                Contains (in order, separated by spaces):
                    - score
                    - ping
                    - name

        """
        info = re.findall(r"([-]?\w+) ([-]?\w+) \"(.*)\"", info)[0]
        self.score = int(info[0])
        self.ping = int(info[1])
        self.name = re.sub(r"\^[0-9]", "", info[-1])

    def __eq__(self, other) -> bool:
        """Compare to `Player`.

        Returns:
            `True` if other `Player` is the same, `False` otherwise.
            `True` if other `PromodPlayer` is the same, `False` otherwise.
            `True` if player name is other, `False` otherwise.

        """
        if isinstance(other, self.__class__):
            return self.name == other.name
        elif isinstance(other, PromodPlayer):
            return self.name == other.name
        return self.name == other

    def __repr__(self) -> str:
        """Name when printing."""
        return f"<{self.__class__.__name__} {self.name=}>"

    def __str__(self) -> str:
        """Name when formatted to string."""
        return f"<{self.__class__.__name__} {self.name=}>"

    def __hash__(self):
        """Return name."""
        return self.name


class PromodPlayer:
    """Promod player on a CoD4 server.

    Attributes:
        name : str
            Name of the player.
        team : str
            Team the player belongs to.
        alive : bool
            `True` if player is alive, `False` if player is dead.
        kills : int
            Number of kills of the player.
        assists : int
            Number of assists of the player.
        deaths : int
            Number of deaths of the player.
        bombcarrier : bool
            `True` if player is carrying the bomb, `False` otherwise.
        score : int
            Score of the player calculated from it's kills and assists.
            Does not consider bomb plants and defuses.
            `Player` instance of the same player contains real score
            if existing.
        ping : int
            Defaults to `None`.
            `Player` instance of the same player contains the ping
            if existing.

    """

    def __init__(self, info: list, team: str) -> None:
        """Create a promod player.

        Parameters:
            info : list
                Information of the player.
                Contains (in order):
                    - name
                    - if player is alive (0/1)
                    - number of kills
                    - number of assists
                    - number of deaths
                    - if player carries the bomb (0/1)

        """
        self.team = team
        self.name = re.sub(r"\^[0-9]", "", info[0])
        self.alive = bool(int(info[1]))
        self.kills = int(info[2])
        self.assists = int(info[3])
        self.deaths = int(info[4])
        self.bombcarrier = bool(int(info[5]))
        self.score = 5 * self.kills + 3 * self.assists
        self.ping: int = None
        self.pb_guid: str = None
        self.ip: str = None
        self.aliases: list = [self.name]

    def __eq__(self, other) -> bool:
        """Compare to `Player` or `PromodPlayer`.

        Returns:
            `True` if other `Player` is the same, `False` otherwise.
            `True` if other `PromodPlayer` is the same, `False` otherwise.
            `True` if player name is other, `False` otherwise.

        """
        if isinstance(other, Player):
            return self.name == other.name
        elif isinstance(other, self.__class__):
            return self.name == other.name
        return self.name == other

    def __repr__(self) -> str:
        """Name when printed."""
        return f"<{self.__class__.__name__} {self.name=}>"

    def __str__(self) -> str:
        """Name when formatted to string."""
        return f"<{self.__class__.__name__} {self.name=}>"

    def __hash__(self):
        """Return name."""
        return self.name
