import json
import logging

import cod4pugbot.domain.features.pugs.pug
import cod4pugbot.domain.utils
from .players import PromodPlayer
from cod4pugbot.db import SQLClient
from cod4pugbot.domain.features.pugs.cod4.scorebot import DB_MAP
from cod4pugbot.domain.features.pugs.cod4.scorebot import DefusedBy
from cod4pugbot.domain.features.pugs.cod4.scorebot import Kill
from cod4pugbot.domain.features.pugs.cod4.scorebot import PlantedBy
from cod4pugbot.domain.features.pugs.cod4.scorebot import RoundWinner
from cod4pugbot.domain.features.pugs.cod4.scorebot import StartText
from cod4pugbot.domain.features.pugs.cod4.scorebot.killfeed_events import Event

logger = logging.getLogger(__name__)


class StatsTracker:
    """Save player statistics of players on a CoD4 Promod server.

    The stats tracker gathers all statistical information about players
    on a server. Important is that the server has PunkBuster and the
    Promod scorebot enabled. Otherwise, it is not possible to link scorebot
    information to the player's PB GUID and thereby save stats.

    The stats tracker is supposed to track stats for one map. When a map is
    finished, the stats are being saved into the bot's data base table with
    the respective match size (1v1 stats, 2v2 stats etc.). The check whether
    a map is finished is done in `cogs.utils.cod4.gameserver`.

    The stats tracker can track the following stats: maps played/won/drawed/
    lost, rounds played/won/lost, kills, assists, deaths, 3-kills, 4-kills,
    5-kills (aces), entry kills, entry deaths, headshots, kills with each
    weapon, suicides, team kills, bomb plants/defuses.

    Attributes:
        pug : `RunningPUG`
            Corresponding PUG the stats are being tracked for.
            It contains important information the bot needs to check if
            a team has won the PUG.
        players : list
            List of players that joined the server throughout the map.
            If a player disconnects, its stats remain saved. If a player
            reconnects, its stats get updated, not reset. When a map ends,
            the players saved get flushed.

    Methods:
        _add_value_to_player(player, varname, value) : staticmethod
            Add value to a player attribute.
        _update_player_score(player, player_new) : staticmethod
            Update score of a player from new promod ticker info.
        _add_player(self, player) : func
            Add player to stats tracker.
        _evaluate_killfeed(self) : func
            Evaluate killfeed events and estimate stats.
        update_player(self, player) : func
            Update a player from new promod ticker info.
        save_stats_to_db(self, map_winner, attack_score, defence_score) : func
            Save stats to data base table.

    """

    def __init__(self, pug) -> None:
        """Create a stats tracker.

        Parameters:
            pug : `RunningPUG`
                Corresponding PUG the stats are being tracked for.

        """
        self.pug = pug
        self.players: list = []

    @staticmethod
    def _add_value_to_player(player, varname, value: int = 1) -> None:
        """Add value to player attribute.

        Parameters:
            player : PromodPlayer
                Player whose stats are being updated.
            varname : str
                Name of the player attribute that is being updated.
            value : int, optional
                Value that is added to the attribute.
                Defaults to `1`.

        """
        if varname in vars(player):
            vars(player)[varname] += value
        else:
            vars(player)[varname] = value

    @staticmethod
    def _update_player_score(player: PromodPlayer, player_new: PromodPlayer) -> None:
        """Update score and consider reconnects.

        Reconnecting of a player is being handled. If the new kills,
        assists or deaths are lower than the previous, the player
        reconnected. Then, the new stats need to be added to the old.
        This does not apply for the score because of possible team kills.
        If the kills are less, and score is less as well, a reconnect appeared.

        Parameters:
            player : PromodPlayer
                Player whose score is being updated.
            player_new : PromodPlayer
                New player information delivered by the promod ticker.

        """
        details = ("score", "kills", "assists", "deaths")
        vars_old = vars(player)
        vars_new = vars(player_new)

        if vars_new["team"] != vars_old["team"]:
            vars_old["team"] = vars_new["team"]

        if vars_new["name"] not in vars_old["aliases"]:
            vars_old["aliases"].append(vars_new["name"])

        if vars_new["deaths"] > vars_old["deaths"]:
            # if deaths increased, player did not reconnect, hence
            # set new details
            for detail in details:
                if vars_new[detail] == 0:
                    continue
                vars_old[detail] = vars_new[detail]
                vars_old[f"prev_{detail}"] = vars_new[detail]
        elif vars_new["deaths"] < vars_old["deaths"]:
            # if deaths decreased, player definitely reconnected,
            # thus update by adding new stats
            for detail in details:
                if vars_new[detail] == 0:
                    continue
                vars_old[detail] += vars_new[detail]
                vars_old[f"prev_{detail}"] = vars_new[detail]
        else:
            # deaths are equal to before, hence player did not die
            # or reconnected
            # maybe relocate updating of stats to server class
            if vars_new["kills"] > vars_old["kills"]:
                for detail in details:
                    if vars_new[detail] == 0:
                        continue
                    vars_old[detail] = vars_new[detail]
                    vars_old[f"prev_{detail}"] = vars_new[detail]

    def _add_player(self, player: PromodPlayer) -> bool:
        """Add new player.

        To ensure that stats get updated when a player reconnects,
        new attributes are added to the `PromodPlayer` that save the
        previous score, kills, assists and deaths, and evaluated score,
        kills, assists and deaths. The evaluated attributes are used to
        later compare if the evaluation of the killfeed gives a different
        result than the tracking of the scores.

        Parameters:
            player : PromodPlayer
                Player that should be added to the stats tracker.

        Returns:
            `True` if player was added to the playerlist, `False` otherwise.

        """
        if any(player.pb_guid == player_.pb_guid for player_ in self.players):
            return False

        self.players.append(player)
        for detail in ["score", "kills", "assists", "deaths"]:
            vars(self.players[-1])[f"prev_{detail}"] = vars(self.players[-1])[detail]
            vars(self.players[-1])[f"evaluated_{detail}"] = 0
        return True

    def _format_killfeed_to_rounds(self, killfeed: list[Event]) -> list[list[Event]]:
        """Format killfeed to list with killfeed of each round.

        Concatenates killfeed events between any `StartText` and `RoundWinner` event.

        Parameters:
            killfeed : list
                Killfeed with `Event`s.

        Returns:
        list
            Killfeed of each round.

        """
        rounds = []
        round_cur = []

        for event in killfeed:
            if isinstance(event, StartText):
                if round_cur:
                    rounds.append(round_cur)
                round_cur = [event]
            elif isinstance(event, RoundWinner):
                round_cur.append(event)
                rounds.append(round_cur)
                round_cur = []
            elif isinstance(event, (Kill, DefusedBy, PlantedBy)):
                round_cur.append(event)

        self.n_rounds = len(rounds)

        return rounds

    def _evaluate_kill_event(
        self,
        player: PromodPlayer,
        event: Event,
        n_kills: int,
        first_kill: bool,
    ) -> tuple[int, bool, bool]:
        """Evaluate `Kill` event.

        Parameters:
            player : PromodPlayer
            event : Event
            n_kills : int
                Number of kills of the player in the current round
            first_kill : bool
                If first kill is still to happen.

        Returns:
            int
                Number of kills.
            bool
                `True` if player had the opening kill, `False` otherwise.
            bool
                `True` if player had the opening kill, `False` otherwise.

        """
        opening_kill = False
        opening_death = False

        if not isinstance(event, Kill):
            return n_kills, opening_kill, opening_death

        if any(alias == event.killer for alias in player.aliases):
            if any(alias == event.killed for alias in player.aliases):
                self._add_value_to_player(player, varname="suicides")
            else:
                killed = None
                for player_ in self.players:
                    if event.killed in player_.aliases:
                        killed = player_
                        break
                if killed is None:
                    logger.debug(
                        "Killed player %s not in players %s", event.killed, self.players
                    )
                    return n_kills, opening_kill, opening_death
                if player.team == killed.team:
                    self._add_value_to_player(player, varname="team_kills")
                    self._add_value_to_player(
                        player, varname="evaluated_score", value=-5
                    )
                else:
                    n_kills += 1
                    self._add_value_to_player(player, varname="evaluated_kills")
                    self._add_value_to_player(
                        player=player,
                        varname="evaluated_score",
                        value=5,
                    )
                    self._add_value_to_player(player, varname=DB_MAP[event.weapon])
                    if event.headshot:
                        self._add_value_to_player(player, varname="headshot_kills")
                    if first_kill:
                        opening_kill = True
                        self._add_value_to_player(player, varname="opening_kills")
        elif event.assistant is not None and any(
            alias == event.assistant.player for alias in player.aliases
        ):
            self._add_value_to_player(player, varname="evaluated_assists")
            self._add_value_to_player(player, varname="evaluated_score", value=3)
        elif any(alias == event.killed for alias in player.aliases):
            self._add_value_to_player(player, varname="evaluated_deaths")
            if first_kill:
                opening_death = True
                self._add_value_to_player(player, varname="opening_deaths")

        return n_kills, opening_kill, opening_death

    def _evaluate_defused_by_event(self, player: PromodPlayer, event: Event) -> None:
        """Evaluate `DefusedBy` event.

        Check if `event` is `DefusedBy` and if the player is associated to the event.
        Increment bomb defuses (+1) and evaluated score (+3) of player if he defused.

        Parameters:
            player : PromodPlayer
            event : Event

        """
        if not isinstance(event, DefusedBy) or all(
            alias != event.player for alias in player.aliases
        ):
            return

        self._add_value_to_player(player, varname="bomb_defuses")
        self._add_value_to_player(player, varname="evaluated_score", value=3)

    def _evaluate_planted_by_event(self, player: PromodPlayer, event: Event) -> None:
        """Evaluate `PlantedBy` event.

        Check if `event` is `PlantedBy` and if the player is associated to the event.
        Increment bomb plants (+1) and evaluated score (+3) of player if he planted.

        Parameters:
            player : PromodPlayer
            event : Event

        """
        if not isinstance(event, PlantedBy) or all(
            alias != event.player for alias in player.aliases
        ):
            return

        self._add_value_to_player(player, varname="bomb_plants")
        self._add_value_to_player(player, varname="evaluated_score", value=3)

    def _evaluate_round_winner_event(
        self,
        player: PromodPlayer,
        event: Event,
        n_kills: int,
        opening_kill: bool,
        opening_death: bool,
    ) -> None:
        """Evaluate `RoundWinner` event and add possible stats to player.

        Parameters:
            player : PromodPlayer
            event : Event
            n_kills : int
                Number of kills the player had in this round.
            opening_kill : bool
                If the player had the opening kill this round.
            opening_death : bool
                If the player had the opening death this round.

        """
        if not isinstance(event, RoundWinner):
            return

        self._add_value_to_player(player, varname="rounds_played")

        if event.winner == player.team:
            self._add_value_to_player(player, varname="round_wins")
            if n_kills > 0:
                self._add_value_to_player(player, varname="impact_kills", value=n_kills)
                self._add_value_to_player(player, varname="rounds_converted")
                if opening_kill:
                    self._add_value_to_player(player, varname="opening_kills_converted")
        else:
            self._add_value_to_player(player, varname="round_losses")
            if opening_death:
                self._add_value_to_player(player, varname="opening_deaths_converted")

    def _add_multi_kill_to_player(self, player: PromodPlayer, n_kills: int) -> None:
        """Increment number of multi kills of player for respective number of kills.

        Parameters:
            player : PromodPlayer
            n_kills : int
                Number of kills.

        """
        multiple_kills_vars = {
            1: "one_kills",
            2: "two_kills",
            3: "three_kills",
            4: "four_kills",
            5: "five_kills",
        }
        varname = multiple_kills_vars.get(n_kills)
        if varname is not None:
            self._add_value_to_player(player, varname=varname)

    def _evaluate_killfeed(self, rounds: list[list[Event]]) -> None:
        """Evaluate killfeed `Event`s.

        Evaluate all rounds of the killfeed. For each player, find all events
        he was involved in and save the corresponding stats and meanwhile calculate
        the score from kills, assists, plants and defuses.
        This involves:
            - kills
                - kills, assists, deaths
                - entry kills
                - entry deaths
                - 3 kills, 4 kills and 5 kills
                - kills with each weapon
                - headshots
                - suicides
                - team kills
            - bomb plants
            - bomb defuses

        Parameters:
            rounds : list
                Each round's killfeed with `Event`s.

        """
        for round_ in rounds:
            first_kill = True
            for player in self.players:
                n_kills = 0
                opening_kill = False
                opening_death = False
                for event in round_:
                    # avoid overwriting of opening_kill and opening_death subsequent to first
                    # kill event
                    if first_kill:
                        (
                            n_kills,
                            opening_kill,
                            opening_death,
                        ) = self._evaluate_kill_event(
                            player=player,
                            event=event,
                            n_kills=n_kills,
                            first_kill=first_kill,
                        )
                    else:
                        n_kills, _, _ = self._evaluate_kill_event(
                            player=player,
                            event=event,
                            n_kills=n_kills,
                            first_kill=first_kill,
                        )
                    self._evaluate_planted_by_event(player=player, event=event)
                    self._evaluate_defused_by_event(player=player, event=event)

                    if isinstance(event, Kill):
                        first_kill = False
                self._evaluate_round_winner_event(
                    player=player,
                    event=event,
                    n_kills=n_kills,
                    opening_kill=opening_kill,
                    opening_death=opening_death,
                )
                self._add_multi_kill_to_player(player, n_kills=n_kills)

    @staticmethod
    def _update_stats(
        stats: dict, player: PromodPlayer, map_winner: str
    ) -> dict[str, int]:
        """Update stats using the player's variables.

        Parameters:
            stats : dict
            player : PromodPlayer
            map_winner : str
                Map winning team.

        Return:
            dict
                Updated stats.

        """
        vars_new = vars(player)
        logger.debug("Player vars %s", json.dumps(vars_new, indent=4, default=str))

        for detail in ["score", "kills", "assists", "deaths"]:
            if vars_new[f"evaluated_{detail}"] > vars_new[detail]:
                vars_new[detail] = vars_new[f"evaluated_{detail}"]

        stats["maps_played"] += 1
        if map_winner == "tie":
            stats["map_draws"] += 1
        elif player.team == map_winner:
            stats["map_wins"] += 1
        else:
            stats["map_losses"] += 1

        for var, val in vars_new.items():
            if var not in stats:
                continue
            stats[var] += val

        return stats

    def update_player(self, player: PromodPlayer) -> None:
        """Update stats of a player.

        Parameters:
            player : PromodPlayer
                Player to update.
                If the player is not tracked yet, it gets added.
                If it gets tracked already, its stats get updated.

        """
        if not isinstance(player, cod4pugbot.cod4.utils.PromodPlayer):
            return

        if self._add_player(player):
            return

        for player_ in self.players:
            if player_.pb_guid != player.pb_guid:
                continue
            self._update_player_score(player_, player_new=player)

        return

    def save_stats_to_db(
        self,
        killfeed: list = None,
        map_winner: str = None,
    ) -> None:
        """Save player stats to data base table.

        At first, the PB GUID of the player is searched in the registered
        members list and the Discord ID retrieved. The stats then get added
        to the entry in the stats table with the corresponding Discord ID.
        If the evaluated kills are greater than those tracked by the scores,
        the attempt to consider reconnects failed (which might be the case
        if the player has the same stats every time reconnecting, which is
        very unlikely, though).
        K/D ratio and K-D difference as well as won maps and rounds get
        calculated.
        If the map winner is given, map wins, draws, and losses as well as
        map win ratio gets estimated.
        If the team scores are given, round wins, draws, and losses as well as
        round win ratio gets estimated.

        Parameters:
            killfeed : list, optional
                Killfeed with `Event`s.
                Defaults to `None`.
            map_winner : str, optional
                Team that won the map.
                Defaults to `None`.

        """
        if killfeed is None:
            return

        rounds = self._format_killfeed_to_rounds(killfeed)
        self._evaluate_killfeed(rounds)

        client = SQLClient(**self.pug.cog.bot.db_info)
        for player in self.players:
            member_info = client.select(
                view="members_print", **{"pb_guid": player.pb_guid}
            )

            if member_info is None:
                logger.debug(
                    "Player %s not found in registered members (PB GUID: %s)",
                    player,
                    player.pb_guid,
                )
                continue

            discord_id = member_info.iloc[0]["discord_id"]

            stats = client.select(
                view=f"stats_{self.pug.size}", **{"discord_id": discord_id}
            )

            if stats is None:
                member = self.pug.guild.get_member(discord_id)
                client.insert(
                    table=f"stats_{self.pug.size}",
                    **cod4pugbot.cogs.utils.get_kwargs(
                        member=member,
                    ),
                )

            stats = stats.iloc[0].to_dict()
            logger.debug(
                "Old player stats: %s", json.dumps(stats, indent=4, default=str)
            )

            stats = self._update_stats(stats, player=player, map_winner=map_winner)
            logger.debug(
                "New player stats: %s", json.dumps(stats, indent=4, default=str)
            )

            client.update(
                table=f"stats_{self.pug.size}",
                before={"discord_id": discord_id},
                after=stats,
            )
