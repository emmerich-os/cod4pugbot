from typing import Optional

from cod4pugbot.domain.features.pugs.cod4.utils import PromodPlayer


class PromodTeam:
    """Promod team on a CoD4 server.

    Attributes:
        name : str
            Name of the team.
        score : int
            Round score of the team.
        players : list, optional
            List with `PromodPlayer`s in the team.

    Methods:
        add_info(name, pb_guid, ip) : func
            Add information to a player in the team.

    """

    def __init__(self, name: str, info: str) -> None:
        r"""Initialize team with players sorted by score.

        Parameters:
            name : str
                Name of the team.
                Either `"attack"` or `"defence"`.
            info : str
                Promod ticker info of the respective team.
                String information are separated by `"\x01`".

        """
        info = info.split("\x01")
        self.name = name
        self.score = int(info[0])
        info = info[1:]
        self.players = (
            sorted(
                (
                    PromodPlayer(info=info[i : i + 6], team=self.name)
                    for i in range(0, len(info), 6)
                ),
                key=lambda x: x.score,
                reverse=True,
            )
            if "none" not in info
            else None
        )

    def name_to_string(self) -> str:
        """Format name to string with first letter as upper case."""
        return f"{self.name[0].upper()}{self.name[1:]}"

    def add_info(self, name: str, **kwargs) -> Optional[PromodPlayer]:
        """Add info to player if player in pb_plist.

        Parameters:
            name : str
                Name of the player to whom the information are added.

        Returns:
            player : PromodPlayer
                If player's information were updated.

        """
        if self.players is None:
            return None
        for player in self.players:
            if name == player:
                for info, value in kwargs.items():
                    setattr(player, info, value)
                return player

    def __eq__(self, other) -> bool:
        """Compare to `Player`.

        Returns:
            `True` if other `PromodTeam` is the same, `False` otherwise.
            `True` if team name is equal to other, `False` otherwise.

        """
        return (
            self.name == other.name
            if isinstance(other, self.__class__)
            else self.name == other
        )

    def __repr__(self) -> str:
        """Name when printed."""
        return f"<{self.__class__.__name__} {self.name=}>"

    def __str__(self) -> str:
        """Name when formatted to string."""
        return f"<{self.__class__.__name__} {self.name=}>"
