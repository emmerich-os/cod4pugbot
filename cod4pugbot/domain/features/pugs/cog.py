import datetime
import json
import logging
import os
import re

import discord.ext.commands
from discord.ext import tasks

import cod4pugbot.config
from . import pug as pugs
from cod4pugbot.db import SQLClient
from cod4pugbot.domain import utils

logger = logging.getLogger(__name__)

COMMAND_PREFIX: str = None


class PUGs(discord.ext.commands.Cog, name="PUG"):
    """PUG feature.

    Listeners:
        on_ready() : coroutine func
            At first connect, initialize all PUG categories and channels, and PUGs.
        on_member_update(before, after) : coroutine func
            Remove AFK/offline users from active PUGs.
        on_raw_reaction_add(raw_reaction) : coroutine func
            On reaction add, add user to PUG or pick player, pick/veto map or
            report score.
        on_raw_reaction_remove(raw_reaction) : coroutine func
            On reaction remove, remove user from PUG.

    Tasks:
        _stop_ready_phase() : local coroutine func
            Stop ready up phase of PUGs when expired.
        _stop_running_pugs() : local coroutine func
            Stop running PUGs when expired and no score reported.
        _update_server_info() : local coroutine func
            Update server info of any server added to a PUG.

    Commands:
        add_server_to_pug(ctx, *, connect_command) : coroutine func
            Add given server to PUG.
        show_stats(ctx, *, content) : coroutine func
            Show player stats.
        afk_offline_immunity(ctx) : coroutine func
            Add/remove AFK/offline immunity.
        abort_pug(ctx) : coroutine func
            Abort PUG.
        call_sub(ctx) : coroutine func
            Call a sub for PUG.

    """

    server_refresh_rate = 4

    def __init__(self, bot: discord.ext.commands.Bot):
        self.bot = bot
        self.active_pugs = pugs.ActivePUGs()
        self.ao_protected: list = []
        self._first_connect = True
        global COMMAND_PREFIX
        COMMAND_PREFIX = self.bot.command_prefix

    async def _check_reaction(
        self, reaction: discord.Reaction, member: discord.Member, remove: bool = False
    ):
        """Check whether a reaction belongs to a PUG message.

        Parameters:
            reaction : discord.Reaction
                Reaction to check.
            member : discord.Member
                Member that added the reaction.
            remove : bool
                If `True`, remove reaction.

        Returns:
            pug_id : int
                Corresponding PUG ID.
                If the reaction belongs to a message of any active PUG,
                the respective PUG ID the message belongs to will be
                returned.

        """
        logger.debug(
            "checking reaction %s to message %s of member %s",
            reaction,
            reaction.message,
            member,
        )
        if member == self.bot.user or reaction.message.author != self.bot.user:
            logger.debug("returning in 1st if")
            return
        elif all(
            mode in self.bot.channel_ids["pugs"]
            and all(
                reaction.message.channel.id != channel_id
                for channel_id in self.bot.channel_ids["pugs"][mode].values()
            )
            for mode in ["bo1", "bo2", "bo3", "bo5"]
        ) and all(
            reaction.message.channel.id != channel_id
            for channel_id in [
                message.channel.id
                for message in [pug for pug in self.active_pugs.pugs.copy().values()]
            ]
        ):
            logger.debug("returning in 2nd if")
            return
        elif (
            (
                reaction.message.guild.get_role(self.bot.role_ids["global_banned"])
                in member.roles
            )
            or (
                reaction.message.guild.get_role(self.bot.role_ids["pug_banned"])
                in member.roles
            )
            or (
                reaction.message.guild.get_role(self.bot.role_ids["registered"])
                not in member.roles
            )
            or (
                reaction.emoji != "\u2705"
                and reaction.emoji != "\u26d4"
                and reaction.emoji != "\U0001F538"
            )
        ):
            if remove:
                await reaction.remove(member)
            logger.debug(
                "returning in 3rd if because %s does not match or user is globally/PUG banned.",
                reaction.emoji,
            )
            return
        channel = reaction.message.channel
        logger.debug(
            "reaction channel id: %s\n\tpugs channel ids: %s",
            channel.id,
            [pug.channel.id for pug in self.active_pugs.pugs.copy().values()],
        )
        if all(
            pug.channel.id != channel.id
            for pug in self.active_pugs.pugs.copy().values()
        ):
            logger.debug("returning due to channel id not matching")
            return

        logger.debug(
            "%s", json.dumps(self.active_pugs.pugs.copy(), indent=4, default=str)
        )

        for pug_id, pug in self.active_pugs.pugs.copy().items():
            logger.debug("pugs: %s", pug)
            for name, message in pug.messages.items():
                if message is None:
                    continue
                if isinstance(message, dict):
                    for name_, message_ in message.items():
                        logger.debug("%s %s %s", name_, message_, message_.id)
                        if name_ == "readyup" and reaction.message.id == message_.id:
                            if (
                                pug.status == "readyup"
                                and member not in pug.playerlist
                                and remove
                            ):
                                await reaction.remove(member)
                                return
                            return pug_id
                        elif reaction.message.id == message_.id:
                            return pug_id
                else:
                    logger.debug("%s %s %s", name, message, message.id)
                    if name == "readyup" and reaction.message.id == message.id:
                        if (
                            pug.status == "readyup"
                            and member not in pug.playerlist
                            and remove
                        ):
                            await reaction.remove(member)
                            return
                        return pug_id
                    elif reaction.message.id == message.id:
                        return pug_id
        return

    @discord.ext.commands.Cog.listener()
    async def on_ready(self):
        """Initialize all PUG types that are enabled in the config."""
        if not self._first_connect:
            return
        self._first_connect = False
        client = SQLClient(**self.bot.db_info)
        pugs = client.select(view="pugs_print")

        if utils.check_df(df=pugs):
            for pug_id, row in pugs.iterrows():
                if not row["attack"] or not row["defence"] or not row["maps"]:
                    client.delete(table="pugs", id=pug_id)
        if (
            "running_pugs" in self.bot.category_ids
            and self.bot.category_ids["running_pugs"] is not None
        ):
            category = self.bot.get_channel(self.bot.category_ids["running_pugs"])
            for channel in category.channels:
                await channel.delete()
        for guild in self.bot.guilds:
            for mode, sizes in self.bot.pugs["modes"].items():
                for size, init in sizes.items():
                    if init:
                        if "pugs" not in self.bot.category_ids:
                            self.bot.category_ids["pugs"] = {}
                        if mode not in self.bot.category_ids["pugs"]:
                            self.bot.category_ids["pugs"][mode] = {}
                        if mode not in self.bot.category_ids:
                            self.bot.category_ids[mode] = None
                        if self.bot.category_ids[mode] is None:
                            await self._create_pug_category(guild=guild, mode=mode)
                        if mode not in self.bot.channel_ids["pugs"]:
                            self.bot.channel_ids["pugs"][mode] = {}
                        if size not in self.bot.channel_ids["pugs"][mode]:
                            self.bot.channel_ids["pugs"][mode][size] = None
                        if self.bot.channel_ids["pugs"][mode][size] is None:
                            await self._create_pug_channel(
                                guild=guild, mode=mode, size=size
                            )

                        client = SQLClient(**self.bot.db_info)
                        pug_id = client.insert_and_return_row_id(
                            table="pugs", mode=mode, size=size
                        )
                        channel = self.bot.get_channel(
                            self.bot.channel_ids["pugs"][mode][size]
                        )
                        pug = await pugs.OpenPUG.create(
                            pug_id=pug_id, mode=mode, size=size, channel=channel
                        )
                        self.active_pugs.add_pug(pug_id=pug.id, pug=pug)
        self._stop_ready_phase.start()
        self._stop_running_pugs.start()
        self._update_server_info.start()

    def cog_unload(self):
        self._stop_ready_phase.cancel()
        self._stop_running_pugs.cancel()
        self._update_server_info.cancel()

    async def _create_pug_category(self, guild: discord.Guild, mode: str):
        """Create PUG category.

        Parameters:
            guild : discord.Guild
                Guild in which to create the PUG category.
            mode : str
                PUG mode whose category to create.

        """
        name = f"PUG {mode.upper()}"
        registered = guild.get_role(self.bot.role_ids["registered"])
        global_banned = guild.get_role(self.bot.role_ids["global_banned"])
        pug_banned = guild.get_role(self.bot.role_ids["pug_banned"])
        unregistered = guild.get_role(self.bot.role_ids["unregistered"])
        overwrites = {
            global_banned: discord.PermissionOverwrite(
                send_messages=False, add_reactions=False
            ),
            pug_banned: discord.PermissionOverwrite(
                send_messages=False, add_reactions=False
            ),
            guild.default_role: discord.PermissionOverwrite(
                send_messages=False, add_reactions=False
            ),
            unregistered: discord.PermissionOverwrite(
                send_messages=False, add_reactions=False
            ),
            registered: discord.PermissionOverwrite(
                send_messages=False, add_reactions=True
            ),
        }
        category = await guild.create_category_channel(
            name=name,
            reason=f"PUGs {mode.upper()} initiated.",
            overwrites=overwrites,
        )
        d = {
            "name": name,
            "object": category,
            "id": category.id,
        }
        cod4pugbot.config.set_value(
            d=self.bot.category_ids, keys=("pugs", mode), value=d
        )
        self.bot.category_ids[mode] = category.id
        cod4pugbot.config.save_to_config(("category_ids", mode), category.id)

    async def _create_pug_channel(self, guild: discord.Guild, mode: str, size: str):
        """Create PUG channels.

        Parameters:
            guild : discord.Guild
                Guild in which to create the channel.
            mode : str
                PUG mode.
                Defines which category it belongs to, i.e. in which category
                it will be created.
            size : str
                PUG size.

        """
        name = f"pugs-{size}"
        for cat in guild.categories:
            if cat.id == self.bot.category_ids[mode]:
                category = cat
        channel = await category.create_text_channel(
            name=name,
            reason=f"PUG {mode.upper()}-{size} initiated.",
        )
        self.channel = channel
        d = {
            "name": name,
            "object": channel,
            "id": channel.id,
        }
        cod4pugbot.config.set_value(
            d=self.bot.category_ids, keys=("pugs", mode, size), value=d
        )
        self.bot.category_ids[mode] = category.id
        cod4pugbot.config.save_to_config(
            ("channel_ids", "pugs", mode, size), channel.id
        )
        self.bot.channel_ids["pugs"][mode][size] = channel.id

    @tasks.loop(seconds=1)
    async def _stop_ready_phase(self):
        """Check whether ready-up phase of any PUG has expired."""
        for _, pug in self.active_pugs.pugs.copy().items():
            if pug.status != "readyup":
                continue
            now = datetime.datetime.now(tz=self.bot.timezone)
            if now > pug.ready_up_phase_end:
                await pug.abort_ready_up_phase()

    @tasks.loop(seconds=60)
    async def _stop_running_pugs(self):
        """Check whether maximum time of a PUG has expired and end it."""
        for _, pug in self.active_pugs.pugs.copy().items():
            if not (pug.status == "running" or pug.status == "pre-match"):
                continue
            now = datetime.datetime.now(tz=self.bot.timezone)
            if now > pug.max_time:
                await pug.end_pug()
                for member in pug.captains:
                    if pug.status == "running":
                        await member.send(
                            f"""PUG #{pug.id} expired and has been closed automatically. """
                            f"""Whenever you finish a PUG always report your loss/draw! """
                            f"""Violating this rule 3 times will lead to a temporary PUG ban!"""
                        )
                    elif pug.status == "pre-match" and pug.picker == member:
                        await member.send(
                            f"""PUG #{pug.id} expired and has been closed automatically. """
                            f"""The pre-match phase took longer than 15 minutes because """
                            f"""you did not pick in time. You have been warned! Getting warned """
                            f"""3 times will result in a temporary PUG ban!"""
                        )

    @discord.ext.tasks.loop(seconds=server_refresh_rate)
    async def _update_server_info(self):
        """Update the server info of any server added to a running PUG."""
        for pug in self.active_pugs.pugs.copy().values():
            if pug.status == "running" and pug.server is not None:
                await pug.refresh_server()

    @discord.ext.commands.Cog.listener()
    async def on_member_update(
        self,
        before: discord.Member,
        after: discord.Member,
    ):
        """Remove AFK/offline members from active PUGs if not protected.

        Parameters:
            before : discord.Member
                User information before updating.
            after : discord.Member
                User information after updating.

        """
        if (
            before.status == after.status
            or (str(after.status) != "offline" and str(after.status) != "idle")
            or after in self.ao_protected
        ):
            return
        guild = after.guild
        for pug in self.active_pugs.pugs.copy().values():
            if pug.status != "open":
                continue
            if after in pug.playerlist or after in pug.playerqueue:
                await pug.remove_player(member=after)
                channel = guild.get_channel(pug.channel.id)
                message = await channel.fetch_message(pug.messages["pugs"].id)
                await message.remove_reaction(emoji="\u2705", member=after)
                reason = "AFK" if str(after.status) == "idle" else "offline"
                message = await pug.channel.send(
                    f"""{after.mention} was removed from the PUG ({reason})."""
                )
                await message.delete(delay=30)

    @discord.ext.commands.Cog.listener()
    async def on_raw_reaction_add(self, raw_reaction: discord.RawReactionActionEvent):
        """On reaction to register messages in admin register channel.

        Parameters:
             raw_reaction : discord.RawReactionActionEvent
                Reaction that was added.

        """
        user, reaction = await utils.raw_reaction_to_reaction(
            bot=self.bot, raw_reaction=raw_reaction
        )

        if user == self.bot.user:
            return

        pug_id = await self._check_reaction(reaction=reaction, member=user, remove=True)
        if pug_id is None:
            return
        logger.debug("pugs id: %s", pug_id)
        pug = self.active_pugs.pugs[pug_id]

        member = await reaction.message.guild.fetch_member(user.id)

        if "pugs" in pug.messages and reaction.message.id == pug.messages["pugs"].id:
            if reaction.emoji == "\u2705":
                await pug.add_player(member)
        elif (
            "readyup" in pug.messages
            and pug.status == "readyup"
            and reaction.message.id == pug.messages["readyup"].id
        ):
            if reaction.emoji == "\u2705":
                await pug.ready_player(member)
            elif reaction.emoji == "\u26d4":
                await pug.abort_ready_up_phase(members=[member])
        elif pug.status == "pre-match":
            if pug.phase == "player pick" and reaction.emoji == "\u2705":
                await pug.pick_player(captain=user, reaction=reaction)
            elif pug.phase == "map elimination":
                if reaction.emoji == "\u2705":
                    await pug.pick_map(captain=user, reaction=reaction)
                elif reaction.emoji == "\u26d4":
                    await pug.veto_map(captain=user, reaction=reaction)
        elif (
            pug.status == "running"
            and reaction.message.id == pug.messages["score report"].id
            and (reaction.emoji == "\u26d4" or reaction.emoji == "\U0001F538")
        ):
            await pug.report_score(captain=member, reaction=reaction)

    @discord.ext.commands.Cog.listener()
    async def on_raw_reaction_remove(
        self, raw_reaction: discord.RawReactionActionEvent
    ):
        """On reaction to register messages in admin register channel.

        Parameters:
             raw_reaction : discord.RawReactionActionEvent
                Reaction that was removed.

        """
        user, reaction = await utils.raw_reaction_to_reaction(
            bot=self.bot, raw_reaction=raw_reaction
        )

        if user == self.bot.user:
            return

        pug_id = await self._check_reaction(reaction=reaction, member=user, remove=True)
        if pug_id is None:
            return
        logger.debug("pugs id: %s", pug_id)
        member = await reaction.message.guild.fetch_member(user.id)
        pug = self.active_pugs.pugs[pug_id]

        if (
            "pugs" in pug.messages.as_list()
            and reaction.message.id == pug.messages["pugs"].id
        ):
            if reaction.emoji == "\u2705":
                await pug.remove_player(member)
        elif (
            "readyup" in pug.messages
            and pug.status == "readyup"
            and pug.messages.readyup.id == reaction.message.id
        ):
            await pug.unready_player(member)

    @discord.ext.commands.command(
        name="server",
        brief="Add server to PUG.",
        description=(
            """Add a server with given IP, password and rcon password, optional """
            """to a PUG to make the bot track the scoreboard and killfeed. If an """
            """rcon password is provided, the bot can save stats of every player."""
        ),
        usage=(
            "connect <IP>; password <password>\n"
            f"{COMMAND_PREFIX}server connect <IP>; password <password>; rcon <rcon password>)",
        ),
        pass_context=True,
    )
    async def add_server_to_pug(
        self, ctx: discord.ext.commands.Context, *, connect_command: str
    ):
        await ctx.message.delete()
        for _, pug in self.active_pugs.pugs.copy().items():
            if pug.status != "running":
                continue
            elif ctx.message.channel.id == pug.channel.id:
                await pug.add_server(ctx=ctx, content=connect_command)

    @discord.ext.commands.command(
        name="stats",
        brief="Show PUG stats.",
        description="Show stats for any player.",
        usage=(
            """(@user) <size>\n\n"""
            """Multiple users can be @-mentioned.\n\n"""
            """Size can be:\n"""
            """\t- 1v1\n"""
            """\t- 2v2\n"""
            """\t- 3v3\n"""
            """\t- 5v5\n\n"""
            """An extended list of stats can be retrieved by """
            """Adding "all" to the message.\n\n"""
            """The full stats table can be retrieved via private message """
            f"""by using\n"""
            f"""{COMMAND_PREFIX}stats table" """
        ),
        pass_context=True,
    )
    async def show_stats(self, ctx: discord.ext.commands.Context, *, content):
        newline = "\n"
        kwargs: dict = {}
        size_arg = re.findall(r"([1,2,3,5]v[1,2,3,5])", ctx.message.content)
        if not size_arg:
            await ctx.channel.send(
                f"""Sorry {ctx.message.author.mention}, """
                f"""you need to specify which stats you want to """
                f"""be shown (1v1, 2v2, 3v3, 5v5)."""
            )
            return

        [size] = size_arg
        kwargs.setdefault("size", size)

        client = SQLClient(**self.bot.db_info)

        if ctx.message.mentions:
            for member in ctx.message.mentions:
                kwargs.setdefault("discord_id", member.id)
                if "all" in content:
                    show_stats = client.select(
                        view="stats_print", subtable=size, **kwargs
                    )

                else:
                    show_stats = client.select(
                        view="stats_print_partial", subtable=size, **kwargs
                    )
                if show_stats is not None:
                    message = (
                        f"""{kwargs["size"]} stats of {member.mention}:{newline}"""
                    )
                    await ctx.channel.send(
                        message + utils.format_db_return(db_return=show_stats)
                    )
                else:
                    await ctx.channel.send(
                        f"""Sorry {ctx.message.author}, no stats available for """
                        f"""{member.mention}."""
                    )
        elif "table" in content:
            path = client.select(view="stats_print", subtable=size, return_csv=True)
            await ctx.channel.send(
                "Stats table as CSV (can be opened with Excel or any text editor).",
                file=discord.File(path),
            )
            os.remove(path)
        else:
            kwargs.setdefault("discord_id", ctx.message.author.id)
            if "all" in content:
                show_stats = client.select(view="stats_print", subtable=size, **kwargs)
            else:
                show_stats = client.select(
                    view="stats_print_partial", subtable=size, **kwargs
                )
            if show_stats is not None:
                message = f"""{ctx.message.author.mention}, your {kwargs["size"]} stats:{newline}"""
                await ctx.channel.send(
                    message + utils.format_db_return(db_return=show_stats)
                )
            else:
                await ctx.channel.send(
                    f"""Sorry {ctx.message.author.mention}, no stats available."""
                )

    @discord.ext.commands.command(
        name="ao",
        brief="Enable/disable AFK/offline protection.",
        description=(
            "Enable or disable AFK/offline protection. "
            "If protected, users will not be removed from PUGs "
            "when going AFK or offline. "
            "This is especially useful for mobile users that want "
            "to join a PUG from their mobile phone and not get removed."
        ),
        pass_context=True,
    )
    async def afk_offline_immunity(self, ctx: discord.ext.commands.Context):
        if ctx.message.author == self.bot.user:
            return
        if ctx.message.author not in self.ao_protected:
            self.ao_protected.append(ctx.message.author)
            await ctx.channel.send(
                f"""{ctx.message.author.mention}, you are now AFK/offline protected."""
            )
        elif ctx.message.author in self.ao_protected:
            self.ao_protected.remove(ctx.message.author)
            await ctx.channel.send(
                f"""{ctx.message.author.mention}, your AFK/offline protection has been removed."""
            )

    @discord.ext.commands.command(
        name="abort",
        brief="Abort PUG.",
        description=(
            "Abort the PUG. Can be called by any user in the PUG. "
            "Always prefer calling a sub before aborting. "
            "Abuse will result in a PUG ban."
        ),
        pass_context=True,
    )
    async def abort_pug(self, ctx: discord.ext.commands.Context):
        for _, pug in self.active_pugs.pugs.copy().items():
            if pug.status != "running":
                continue
            elif ctx.message.channel.id == pug.channel.id:
                await pug.abort_pug()

    @discord.ext.commands.command(
        name="sub",
        brief="Call a sub.",
        description=(
            "Call a sub. A message in the sub channel will be posted "
            "linking the respective PUG channel the sub was called in. "
            "Can only be called by players in the PUG."
        ),
        pass_context=True,
    )
    async def call_sub(self, ctx: discord.ext.commands.Context):
        if ctx.message.author == self.bot.user:
            return

        for pug in self.active_pugs.pugs.copy().values():
            if pug.status != "running" or ctx.message.channel.id != pug.channel.id:
                continue
            if ctx.message.author not in pug.players:
                continue
            channel = self.bot.get_channel(self.bot.channel_ids["pugs"]["sub"])
            await channel.send(
                f"""\u2757 Sub needed in PUG {pug.channel.mention} \u2757"""
            )
            await ctx.channel.send(
                f"""{ctx.message.author.mention}, your sub request has been posted """
                f"""in {channel.mention}."""
            )
            return

    @discord.ext.commands.command(
        name="add",
        brief="Add dummy user to PUG",
        pass_context=True,
    )
    @discord.ext.commands.has_permissions(administrator=True)
    async def add_dummy(self, ctx: discord.ext.commands.Context, user: discord.User):
        """Add dummy user to PUG."""
        for pug_id, pug in self.active_pugs.pugs.copy().items():
            if ctx.message.channel.id == pug.channel.id:
                logger.debug("pugs message id: %s", pug.messages["pugs"].id)
                try:
                    message = await ctx.channel.fetch_message(pug.messages["pugs"].id)
                except discord.errors.NotFound:
                    self.active_pugs.remove_pug(pug_id=pug_id)
                break
        emoji = discord.PartialEmoji(name="\u2705")
        reaction = discord.RawReactionActionEvent(
            emoji=emoji,
            data={
                "message_id": message.id,
                "channel_id": message.channel.id,
                "user_id": user.id,
            },
            event_type="REACTION_ADD",
        )
        member = await ctx.guild.fetch_member(user.id)
        reaction.member = member
        await self.on_raw_reaction_add(raw_reaction=reaction)

    @discord.ext.commands.command(
        name="rm", brief="Remove dummy user from PUG", pass_context=True
    )
    @discord.ext.commands.has_permissions(administrator=True)
    async def remove_dummy(self, ctx: discord.ext.commands.Context, user: discord.User):
        """Remove dummy user from PUG."""
        for _, pug in self.active_pugs.pugs.copy().items():
            if ctx.message.channel.id == pug.channel.id:
                message = await ctx.channel.fetch_message(pug.messages["pugs"].id)
                break
        emoji = discord.PartialEmoji(name="\u2705")
        reaction = discord.RawReactionActionEvent(
            emoji=emoji,
            data={
                "message_id": message.id,
                "channel_id": message.channel.id,
                "user_id": user.id,
            },
            event_type="REACTION_REMOVE",
        )
        member = await ctx.guild.fetch_member(user.id)
        reaction.member = member
        await self.on_raw_reaction_remove(raw_reaction=reaction)

    @discord.ext.commands.command(
        name="ready", brief="Ready up dummy user", pass_context=True
    )
    @discord.ext.commands.has_permissions(administrator=True)
    async def ready_up_dummy(
        self, ctx: discord.ext.commands.Context, user: discord.User
    ):
        """Ready up dummy user."""
        for _, pug in self.active_pugs.pugs.copy().items():
            if ctx.message.channel.id == pug.channel.id:
                message = await ctx.channel.fetch_message(pug.messages["readyup"].id)
                break
        emoji = discord.PartialEmoji(name="\u2705")
        reaction = discord.RawReactionActionEvent(
            emoji=emoji,
            data={
                "message_id": message.id,
                "channel_id": message.channel.id,
                "user_id": user.id,
            },
            event_type="REACTION_ADD",
        )
        member = await ctx.guild.fetch_member(user.id)
        reaction.member = member
        await self.on_raw_reaction_add(raw_reaction=reaction)

    @discord.ext.commands.command(
        name="unready", biref="Unready dummy user", pass_context=True
    )
    @discord.ext.commands.has_permissions(administrator=True)
    async def abort_dummy(self, ctx: discord.ext.commands.Context, user: discord.User):
        """Unready dummy user."""
        for _, pug in self.active_pugs.pugs.copy().items():
            if ctx.message.channel.id == pug.channel.id:
                message = await ctx.channel.fetch_message(pug.messages["readyup"].id)
                break
        emoji = discord.PartialEmoji(name="\u26d4")
        reaction = discord.RawReactionActionEvent(
            emoji=emoji,
            data={
                "message_id": message.id,
                "channel_id": message.channel.id,
                "user_id": user.id,
            },
            event_type="REACTION_REMOVE",
        )
        member = await ctx.guild.fetch_member(user.id)
        reaction.member = member
        await self.on_raw_reaction_remove(raw_reaction=reaction)
