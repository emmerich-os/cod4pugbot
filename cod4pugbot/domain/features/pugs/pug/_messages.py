import discord


class Messages:
    """Messages associated to PUGs."""

    def __init__(
        self,
        pug: discord.Message = None,
        ready_up: discord.Message = None,
        players: dict[discord.Member, discord.Message] = None,
        maps: dict[str, discord.Message] = None,
        match: discord.Message = None,
        scoreboard: discord.Message = None,
        killfeed: discord.Message = None,
        score_report: discord.Message = None,
    ):
        """Set all messages."""
        self.pug = pug
        self.ready_up = ready_up
        self.players = players or {}
        self.maps = maps or {}
        self.match = match
        self.scoreboard = scoreboard
        self.killfeed = killfeed
        self.score_report = score_report

    def as_list(self) -> list[discord.Message]:
        """List all messages."""
        messages = [
            self.pug,
            self.ready_up,
            *list(self.players.values()),
            *list(self.maps.values()),
            self.match,
            self.scoreboard,
            self.killfeed,
            self.score_report,
        ]
        return [message for message in messages if message is not None]
