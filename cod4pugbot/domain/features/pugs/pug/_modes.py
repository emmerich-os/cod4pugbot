import enum


class Modes(enum.Enum):
    """PUG modes."""

    BO1 = "BO1"
    BO2 = "BO3"
    BO5 = "BO5"

    def __str__(self) -> str:
        """Return the value."""
        return self.value

    @property
    def max_maps(self) -> int:
        """Return the maximum amount of maps to be played according to the PUG mode."""
        return int(self.value[-1])
