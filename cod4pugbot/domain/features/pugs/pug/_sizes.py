import enum


class Sizes(enum.Enum):
    """PUG sizes."""

    ONE = "1v1"
    TWO = "2v2"
    THREE = "3v3"
    FIVE = "5v5"

    def __str__(self) -> str:
        """Return the value."""
        return self.value

    @property
    def max_players(self) -> int:
        """Return the maximum amount of players allowed according to the PUG size."""
        return 2 * int(self.value[0])
