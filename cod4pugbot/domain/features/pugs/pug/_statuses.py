import enum


class Statuses(enum.Enum):
    """PUG statuses."""

    OPEN = "open"
    READY_UP = "readyup"
    PRE_MATCH = "pre-match"
    RUNNING = "running"

    def __str__(self) -> str:
        """Return the value."""
        return self.value
