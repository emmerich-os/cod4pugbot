import dataclasses
from typing import Optional

import discord

from cod4pugbot.domain.features.pugs.cod4.scorebot import EMOJIS

Player = discord.Member


@dataclasses.dataclass
class Team:
    """A PUG team."""

    captain: Optional[Player] = None
    players: list[Player] = dataclasses.field(default_factory=list)
    icon: str = ""


@dataclasses.dataclass
class Attack(Team):
    """The initially attacking team."""

    icon: str = EMOJIS["attack"]


@dataclasses.dataclass
class Defence(Team):
    """The initially defending team."""

    icon: str = EMOJIS["defence"]
