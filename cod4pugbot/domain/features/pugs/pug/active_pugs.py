from typing import Optional

from .base_pug import PUG


class ActivePUGs:
    """Active PUGs.

    Contains all currently active PUGs.
    These can be open, in ready-up phase, pre-match phase or running.
    Once a PUG transitions between two phases, it keeps its ID, but
    the value gets replaced by its instance.

    """

    def __init__(self) -> None:
        self.pugs: dict[int, PUG] = {}

    def __repr__(self) -> str:
        """Name when printing."""
        return self.__str__()

    def __str__(self) -> str:
        """Name when formatted to string."""
        items = (f"{k}={v}" for k, v in self.__dict__.items())
        return f"<{self.__class__.__name__} {' '.join(items)}>"

    def add_pug(self, pug_id: int, pug: PUG) -> PUG:
        """Add PUG to list of active PUGs.

        Parameters:
            pug_id : int
                ID of the PUG.
            pug : cod4pugbot.cogs.utils.OpenPUG
                PUG that is activated.
                Can change to `PreMatchPUG` and `RunningPUG`.

        Returns:
            PUG

        """
        return self.pugs.setdefault(pug_id, pug)

    def remove_pug(self, pug_id: int) -> Optional[PUG]:
        """Remove PUG from active PUGs.

        Parameters:
            pug_id : int
                ID of the PUG.

        """
        return self.pugs.pop(pug_id, None)
