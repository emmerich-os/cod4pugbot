import logging

import discord

from . import _messages as messages
from . import _modes as modes
from . import _sizes as sizes
from . import _statuses as statuses

logger = logging.getLogger(__name__)


class PUG:
    """PUG base class."""

    id: int
    mode: modes.Modes
    size: sizes.Sizes
    status: statuses.Statuses

    def __repr__(self) -> str:
        """Return the name when printing."""
        return self.__str__()

    def __str__(self) -> str:
        """Return the name when formatted to string."""
        return f"<{self.__class__.__name__} {self.id=} {self.mode=} {self.size=} {self.status=}>"

    @property
    def max_players(self) -> int:
        """Return maximum amount of players in the PUG."""
        return self.size.max_players


class DiscordPUG:
    """A wrapper for Discord PUGs."""

    pug: PUG
    channel: discord.TextChannel
    messages: messages.Messages

    @property
    def size(self) -> sizes.Sizes:
        """Return the size of the wrapped PUG."""
        return self.pug.size

    @property
    def mode(self) -> modes.Modes:
        """Return mode of the wrapped PUG."""
        return self.pug.mode

    async def remove_reaction(
        self, member: discord.Member, emoji: str = "\u2705"
    ) -> None:
        """Remove a reaction."""
        if self.channel is None and self.messages is None:
            return
        for reaction in self.messages.pug.reactions:
            if reaction.emoji == emoji:
                users = await reaction.users().flatten()
                if member in users:
                    await reaction.remove(member)
                    await self.channel.send(
                        f"""{member.mention} was removed from the PUG """
                        """(pre-match phase of other PUG started).""",
                        delete_after=10,
                    )
                    return
