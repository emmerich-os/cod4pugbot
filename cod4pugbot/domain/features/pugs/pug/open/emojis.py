class JoinEmoji:
    """Emoji for the reaction required to ready up."""

    string = ":white_check_mark:"
    unicode = "\u2705"


class ReadyEmoji:
    """Emoji for the reaction required to ready up."""

    string = ":white_check_mark:"
    unicode = "\u2705"


class NotReadyEmoji:
    """Emoji for the reaction required to abort the ready-up phase."""

    string = ":no_entry:"
    unicode = "\u26d4"
