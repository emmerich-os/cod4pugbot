import datetime
import logging

import discord

from . import emojis
from .pug import OpenPUG
from cod4pugbot.domain.features.pugs.pug import _teams as teams

logger = logging.getLogger(__name__)

AO_COMMAND = "!ao"


class OpenPUGPost:
    """Represents a PUG as a Discord post."""

    def __init__(self, pug: OpenPUG):
        self._pug = pug

    def to_embed(self) -> discord.Embed:
        """Return the post as a discord embed."""
        embed = discord.Embed(
            title=self._title,
            description=self._description,
            color=self._color,
        )
        embed.add_field(
            name="Status",
            value=self._status,
            inline=True,
        )
        embed.add_field(
            name="Slots",
            value=self._slots,
            inline=True,
        )
        if self._pug.players:
            embed.add_field(
                name="Players",
                value=self._players,
                inline=False,
            )
        if self._pug.queue:
            embed.add_field(
                name="Queue",
                value=self._queue,
                inline=False,
            )
        embed.set_footer(text=f"Last updated: {datetime.datetime.now()} UTC")
        return embed

    @property
    def _title(self) -> str:
        return f"{self._pug.size} PUG ({self._pug.mode.upper()})"

    @property
    def _description(self) -> str:
        return (
            f"React with {emojis.JoinEmoji.string} to enter PUG, remove to leave PUG. "
            "If you go AFK or offline, you will be automatically removed from the PUG. "
            f"Send `{AO_COMMAND}` into any channel to enable AFK/offline protection."
        )

    @property
    def _color(self) -> int:
        if self._pug.is_open:
            return 0x0074FF
        elif self._pug.ready_up_started:
            return 0xFF7400
        return 0xFF0000

    @property
    def _status(self) -> str:
        if self._pug.is_open:
            style = "css"
        elif self._pug.ready_up_started:
            style = "ARM"
        else:
            style = ""
        return f"```{style}\n{self._pug.status.upper()}\n```"

    @property
    def _slots(self) -> str:
        return f"```{len(self._pug.players)}/{self._pug.max_players}```"

    @property
    def _players(self) -> str:
        players = _create_string_list_of_player_nicks(self._pug.players)
        return f"```\n{players}\n```"

    @property
    def _queue(self) -> str:
        players = _create_string_list_of_player_nicks(self._pug.queue)
        return f"```\n{players}\n```"


def _create_string_list_of_player_nicks(players: list[teams.Player]) -> str:
    names = [player.nick for player in players]
    return "\n".join(names)
