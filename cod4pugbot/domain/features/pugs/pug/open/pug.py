import copy
import datetime
import logging
from typing import Optional

from cod4pugbot.domain.features.pugs.pug import _modes as modes
from cod4pugbot.domain.features.pugs.pug import _sizes as sizes
from cod4pugbot.domain.features.pugs.pug import _statuses as statuses
from cod4pugbot.domain.features.pugs.pug import _teams as teams
from cod4pugbot.domain.features.pugs.pug.base_pug import PUG

logger = logging.getLogger(__name__)


class OpenPUG(PUG):
    """An open PUG.

    The open PUG is basically just a `discord.Embed` being posted into its
    respective PUG channel. Players can join by adding a reaction and leave
    by removing it. Every time someone joins/leaves, the embed gets modified.

    Once a PUG is full, a ready-up message gets posted, where
    players can ready-up by adding a reaction or abort by adding a different
    reaction. If the PUG is aborted, the player(s) that aborted or were not
    ready get removed from the PUG and the PUG is re-opened.

    """

    def __init__(
        self,
        pug_id: int,
        mode: modes.Modes,
        size: sizes.Sizes,
        ready_up_phase_length: int,
        queue: list[teams.Player] = None,
    ) -> None:
        """Create an open PUG.

        Uses coroutine functions, thus always use classmethod `create`.
        See help(OpenPUG.create) for detailed information.

        Parameters:
            pug_id : int:
                ID of the PUG.
            mode : str
                PUG mode.
                BO1, BO2, BO3 or BO5.
            size : str
                PUG size.
                1v1, 2v2, 3v3 or 5v5.

        """
        self.id = pug_id
        self.mode = mode
        self.size = size

        # TODO: Use sets instead of lists
        self._players: list[teams.Player] = []
        self._queue: list[teams.Player] = []

        self.ready_up_phase_length = datetime.timedelta(seconds=ready_up_phase_length)
        self._ready_up_phase_expires_at: Optional[datetime.datetime] = None

        self._ready: list[teams.Player] = []
        self._not_ready: list[teams.Player] = []

        self.status = statuses.Statuses.OPEN

        if queue is not None:
            for player in queue:
                self.add_player(player)

    @property
    def players(self) -> list[teams.Player]:
        """Return the players that are in the PUG."""
        return copy.deepcopy(self._players)

    @property
    def queue(self) -> list[teams.Player]:
        """Return the players that are in the queue of the PUG."""
        return copy.deepcopy(self._queue)

    @property
    def is_open(self) -> bool:
        """Return whether the PUG is open for new players."""
        return self.status == statuses.Statuses.OPEN

    @property
    def ready_up_started(self) -> bool:
        """Return whether the PUG is full and ready-up phase has started."""
        return self.status == statuses.Statuses.READY_UP

    @property
    def has_started(self) -> bool:
        """Return whether the PUG is full and ready-up phase has started."""
        return self.status == statuses.Statuses.PRE_MATCH

    @property
    def is_full(self) -> bool:
        """Return whether the PUG is full."""
        return len(self._players) == self.max_players

    @property
    def all_players_ready(self) -> bool:
        """Return whether all players are ready."""
        return len(self._ready) == self.max_players

    @property
    def ready_players(self) -> list[teams.Player]:
        """Return all players that aren't ready yet."""
        return copy.deepcopy(self._ready)

    @property
    def unready_players(self) -> list[teams.Player]:
        """Return all players that aren't ready yet."""
        return copy.deepcopy(self._not_ready)

    @property
    def ready_up_phase_expired(self) -> bool:
        """Return whether the ready-up phase has expired."""
        return datetime.datetime.now() > self._ready_up_phase_expires_at

    def add_player(self, player: teams.Player) -> None:
        """Add a player to PUG.

        If PUG is not full, player is put into players. If the PUG is full
        after afterwards, the ready-up phase will be started.
        If the PUG is full (i.e. in ready-up phase), the player will be put
        into the queue.
        If the PUG is open and the player is in the queue, he will be
        moved to the players.
        If a player was added to the players and the

        """
        logger.debug("Adding player %s to %s", player, self)
        if player in self._players or player in self._queue:
            return
        elif self.is_full:
            self._queue.append(player)
        else:
            self._players.append(player)

        if not self.ready_up_started and self.is_full:
            self._start_ready_up_phase()

    def remove_player(self, player: teams.Player) -> None:
        """Remove a player from PUG.

        If PUG is in ready-up phase and a player is removed from the players,
        the ready-up phase will be aborted.

        """
        logger.debug("Removing player %s from %s", player, self)
        if self.ready_up_started and player in self._players:
            players_to_remove = [player]
            self.abort_ready_up_phase(players_to_remove)
        elif player in self._players:
            self._players.remove(player)
        elif player in self._queue:
            self._queue.remove(player)

    def ready_player(self, player: teams.Player) -> None:
        """Mark player as ready."""
        logger.debug("Readying player %s in %s", player, self)

        if not self.ready_up_started:
            return

        if player in self._not_ready:
            self._not_ready.remove(player)
            self._ready.append(player)

        if self.all_players_ready:
            self.status = statuses.Statuses.PRE_MATCH

    def unready_player(self, player: teams.Player) -> None:
        """Mark player as not ready."""
        logger.debug("Un-readying player %s in %s", player, self)

        if not self.ready_up_started:
            return

        if player in self._ready:
            self._ready.remove(player)
            self._not_ready.append(player)

    def _start_ready_up_phase(self) -> None:
        """Start ready up phase."""
        logger.debug("Starting ready-up phase of %s", self)
        self._ready_up_phase_expires_at = (
            datetime.datetime.now() + self.ready_up_phase_length
        )
        self._not_ready = self._players.copy()
        self._ready = []
        self.status = statuses.Statuses.READY_UP

    def abort_ready_up_phase(self, players_to_remove: list = None) -> None:
        """Abort PUG's ready-up phase."""
        logger.debug(
            "Aborting ready-up phase of %s, removing %s", self, players_to_remove
        )

        if players_to_remove is None:
            players_to_remove = self._not_ready.copy()

        self.status = statuses.Statuses.OPEN

        for player in players_to_remove:
            self.remove_player(player)

        self._not_ready = []
        self._ready = []
        self._ready_up_phase_expires_at = None

        for player in self._queue.copy():
            self._queue.remove(player)
            self.add_player(player)
