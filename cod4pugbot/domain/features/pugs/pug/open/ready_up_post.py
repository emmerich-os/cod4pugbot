import logging

from . import emojis
from .pug import OpenPUG
from cod4pugbot.domain.features.pugs.pug import _teams as teams

logger = logging.getLogger(__name__)


def _create_string_list_of_player_nicks(players: list[teams.Player]) -> str:
    names = [player.nick for player in players]
    return "\n".join(names)


class OpenPUGReadyUpPost:
    """Represents a ready-up message of a PUG."""

    def __init__(self, pug: OpenPUG):
        self._pug = pug

    def as_string(self) -> str:
        """Return the ready-up post as a message."""
        unready = ", ".join(self._unready_players_as_mentions)
        return f"{self._base_message}\n> Waiting for: {unready}"

    @staticmethod
    def as_string_when_aborted(
        removed_players: list[teams.Player], players_were_in_other_pug: bool
    ) -> str:
        """Return the ready-up as a message when the ready-up phase was aborted."""
        mentions = ", ".join([player.mention for player in removed_players])
        verb = "is" if len(mentions) == 1 else "are"
        if players_were_in_other_pug:
            reason = "already in a running PUG"
        else:
            reason = "not ready"
        return f"> READY-UP phase aborted. {mentions} {verb} {reason}."

    @property
    def _base_message(self) -> str:
        return (
            "> **READY-UP phase started.**\n"
            f"> React with {emojis.ReadyEmoji.string} to __**ready up**__.\n"
            f"> React with {emojis.NotReadyEmoji.string} to __**abort**__.\n"
            f"> Duration of ready-up phase is {self._pug.ready_up_phase_length} seconds."
        )

    @property
    def _unready_players_as_mentions(self) -> list[str]:
        """Return all players that are not ready yet."""
        return [player.mention for player in self._pug.unready_players]
