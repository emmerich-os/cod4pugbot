import logging
from typing import Optional

import discord

from . import emojis
from . import post
from . import ready_up_post
from .pug import OpenPUG
from cod4pugbot.domain import utils
from cod4pugbot.domain.features.pugs.pug import _messages as messages
from cod4pugbot.domain.features.pugs.pug import _teams as teams
from cod4pugbot.domain.features.pugs.pug.base_pug import DiscordPUG

logger = logging.getLogger(__name__)


class OpenDiscordPUG(DiscordPUG):
    """Wraps an OpenPUG for Discord.

    The open PUG is basically just a `discord.Embed` being posted into its
    respective PUG channel. Players can join by adding a reaction and leave
    by removing it. Every time someone joins/leaves, the embed gets modified.

    Once a PUG is full, a ready-up message gets posted, where
    players can ready-up by adding a reaction or abort by adding a different
    reaction. If the PUG is aborted, the player(s) that aborted or were not
    ready get removed from the PUG and the PUG is re-opened.

    """

    def __init__(
        self,
        pug: OpenPUG,
        channel: discord.TextChannel,
        pug_messages: messages.Messages,
    ):
        """Set base ready-up message."""
        self._pug = pug
        self._channel = channel
        self._messages = pug_messages
        self._post = post.OpenPUGPost(self._pug)
        self._ready_up_post = ready_up_post.OpenPUGReadyUpPost(self._pug)

        self._create_pug_message_in_channel()

    async def _create_pug_message_in_channel(self) -> None:
        """Create PUG message."""
        await utils.clear_channel_history(self.channel)
        message = await self.channel.send(embed=self._post.to_embed())
        await message.add_reaction(emoji="\u2705")
        await message.pin()
        self.messages.pug = message

    async def add_player(self, player: teams.Player) -> None:
        """Add a player to PUG.

        The PUG post will be modified.

        If the PUG is full, the ready-up phase will be started and
        the ready-up message is posted.

        """
        self._pug.add_player(player)
        await self._modify_pug_message()

        if self._pug.ready_up_started:
            await self._create_ready_up_message()

    async def remove_player(self, player: teams.Player) -> None:
        """Remove a player from PUG."""
        if self._pug.ready_up_started and player in self._pug.players:
            await self.abort_ready_up_phase(players_to_remove=[player])
        else:
            self._pug.remove_player(player)
        await self._modify_pug_message()

    async def ready_player(self, player: teams.Player) -> None:
        """Mark player as ready."""
        self._pug.ready_player(player)
        if not self._pug.all_players_ready:
            await self._modify_ready_up_message()
        else:
            await self._delete_ready_up_message()

    async def unready_player(self, player: teams.Player) -> None:
        """Mark player as not ready."""
        self._pug.unready_player(player)
        await self._modify_ready_up_message()

    async def abort_ready_up_phase(
        self,
        players_to_remove: Optional[list] = None,
        players_were_in_other_pug: bool = False,
    ) -> None:
        """Abort PUG's ready-up phase.

        Parameters:
            players_to_remove : list, optional
                List of members that were not ready or already in another running PUG.
                Defaults to `None`.
            players_were_in_other_pug : bool, optional
                True if the PUG was aborted by the bot, False otherwise.
                The bot should only abort when players were already in another PUG that
                just started.
                Defaults to `None`.

        """
        if players_to_remove is None:
            players_to_remove = self._pug.unready_players

        self._pug.abort_ready_up_phase(players_to_remove)

        await utils.remove_emoji_reactions_from_message(
            emoji=emojis.ReadyEmoji.unicode, *players_to_remove
        )

        content = self._ready_up_post.as_string_when_aborted(
            removed_players=players_to_remove,
            players_were_in_other_pug=players_were_in_other_pug,
        )
        await self.messages.ready_up.edit(content=content)
        await self._delete_ready_up_message(delay=10)

    async def _modify_pug_message(self) -> None:
        """Modify PUG message."""
        if self.messages.pug is None:
            return
        await self.messages.pug.edit(embed=self._post.to_embed())

    async def _create_ready_up_message(self) -> None:
        message = await self.channel.send(content=self._ready_up_post.as_string())
        emojis_to_add = [emojis.ReadyEmoji.unicode, emojis.NotReadyEmoji.unicode]
        await utils.add_emojis_as_reactions_to_message(message, *emojis_to_add)

        self.messages.ready_up = message

        await self._modify_pug_message()

    async def _modify_ready_up_message(self) -> None:
        if self.messages.ready_up is None:
            return
        await self.messages.ready_up.edit(content=self._ready_up_post.as_string())

    async def _delete_ready_up_message(self, delay: int = 0) -> None:
        if self.messages.ready_up is None:
            return
        await self.messages.ready_up.delete(delay=delay)
        self.messages.ready_up = None
