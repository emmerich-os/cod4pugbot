import asyncio
import datetime
import itertools
import logging
import random
import re
from typing import Optional

import discord

import cod4pugbot.config
import cod4pugbot.domain.features.pugs.cog
from . import _messages as messages
from . import _modes as modes
from . import _sizes as sizes
from . import _statuses as statuses
from . import _teams as teams
from .base_pug import PUG
from .running_pug import RunningPUG

logger = logging.getLogger(__name__)


class PreMatchPUG(PUG):
    """A PUG in the pre-match phase (player picking and map elimination).

    Uses coroutine functions, thus always use classmethod `create`.
    See help(PreMatchPUG.create) for detailed information.

    In the pre-match PUG, captains, who are chosen randomly, are picking
    players for their team. Which captain starts is also random. Afterwards,
    if map elimination is enabled, the map elimination starts. If it is
    disabled, the map/s get picked randomly. When this is done, the PUG is
    replaced by a `RunningPUG`.

    """

    def __init__(
        self,
        pug_id: int,
        mode: modes.Modes,
        size: sizes.Sizes,
        players: list[teams.Player],
    ) -> None:
        """Create a pre-match PUG.

        Parameters:
            pug_id : int:
                ID of the PUG.
            mode : str
                PUG mode.
                BO1, BO2, BO3 or BO5.
            size : str
                PUG size.
                1v1, 2v2, 3v3 or 5v5.
            players : list
                Players in the PUG.

        """
        self.id = pug_id
        self.mode = mode
        self.size = size
        self._players = players.copy()
        pug_captain_role = self.guild.get_role(self.cog.bot.role_ids["pug_captain"])
        self.captains_pool = [
            player for player in players if pug_captain_role in player.roles
        ] or players.copy()
        self.map_elimination = self.cog.bot.pugs["map_elimination"]
        self.map_pool = self.cog.bot.pugs[f"maps_{self.size}"]
        self.maps = []
        self.status = statuses.Statuses.PRE_MATCH
        self.messages = messages.Messages()
        self.category_name = "RUNNING PUGS"
        self.picker: discord.Member = None
        self.max_time = datetime.datetime.now(
            tz=self.cog.bot.timezone
        ) + datetime.timedelta(minutes=15)
        self.delete_channel_delay = 60
        self.attack = teams.Attack()
        self.defence = teams.Defence()
        self._pick_captains()
        self.phase = "match" if self.max_players == 2 else "player pick"

    @classmethod
    async def create(
        cls,
        pug_id: int,
        mode: modes.Modes,
        size: sizes.Sizes,
        players: list[teams.Player],
    ) -> "PreMatchPUG":
        """Create a pre-match PUG.

        Parameters:
            pug_id : int
                ID of the PUG.
            mode : str
                PUG mode.
                BO1, BO2, BO3 or BO5.
            size : str
                PUG size.
                1v1, 2v2, 3v3 or 5v5.
            players : list
                List of players in the queue.
                If a PUG enters the pre-match phase, players in the queue
                will be added to the new open PUG.
                Defaults to `None`.

        """
        self = cls(
            pug_id=pug_id,
            mode=mode,
            size=size,
            players=players,
        )
        self.cog.active_pugs.pugs[pug_id] = self
        await self._create_match_channel()
        await self._remove_players_from_open_pugs()
        await self._create_match_message()
        return self

    @property
    def teams(self) -> list:
        """Create list with attack and defence as `dict`."""
        return [self.attack, self.defence]

    @property
    def players(self) -> list:
        """Create list with attack and defence players."""
        return self.attack.players + self.defence.players

    @property
    def captains(self) -> Optional[list]:
        """Create list with attack and defence captain."""
        captains = [self.attack.captain, self.defence.captain]
        return [captain for captain in captains if captain is not None]

    @property
    def max_maps(self) -> int:
        """Return maximum number of maps."""
        return int(self.mode[-1])

    @property
    def match_base_message(self) -> str:
        """Create match base message with captains, players and maps listed."""
        newline = "\n"
        attack = ", ".join(player.mention for player in self.attack["players"])
        defence = ", ".join(player.mention for player in self.defence["players"])
        message = (
            f"""{newline}"""
            f"""\t\t**{self.phase.upper()} PHASE STARTED**{newline}{newline}"""
            f"""> {self.attack["icon"]} **-** {attack}{newline}{newline}"""
            f"""\t\t**VERSUS**{newline}{newline}"""
            f"""> {self.defence["icon"]} **-** {defence}{newline}"""
        )
        if self.maps:
            message += f"""{newline}**Maps picked:** {", ".join(self.maps)}{newline}"""
        return message

    async def _remove_players_from_open_pugs(self) -> None:
        """Remove players from other active PUGs.

        All players in the PUG that also joined other open PUGs are
        being removed from these. If the other PUG is currently in the
        ready-up phase, it is aborted.

        """
        logger.debug("Removing players from other PUGs")
        for pug in self.cog.active_pugs.pugs.copy().values():
            if pug.id == self.id:
                continue
            if in_both_pugs := [
                player for player in self._players if player in pug.playerlist
            ]:
                if pug.status == "readyup":
                    await pug.abort_ready_up_phase(
                        members=in_both_pugs, aborted_by_bot=True
                    )
                for player in in_both_pugs:
                    await pug.remove_player(member=player)
                    await pug.remove_reaction(member=player)

    def _pick_captains(self) -> None:
        """Randomly pick two captains for each team."""
        logger.debug(
            "pick captains\n\tplayers: %s\n\tcaptains: %s",
            self._players,
            self.captains_pool,
        )
        for team in self.teams:
            if self.captains_pool:
                captain = random.choice(self.captains_pool)
                self.captains_pool.remove(captain)
            else:
                captain = random.choice(self._players)
            self._players.remove(captain)
            team["captain"] = captain
            team["players"].append(captain)
        self.pickers = self.captains.copy()
        random.shuffle(self.pickers)
        logger.debug("Captains: %s", self.captains)
        logger.debug("Players : %s", self._players)

    async def _create_match_channel(self) -> None:
        """Create match channel.

        Match channel is created in a category that is named "RUNNING PUGS".
        If it does not exist yet, it is being created.

        """
        logger.debug("match created")
        if all(
            category.name != self.category_name for category in self.guild.categories
        ):
            registered = self.guild.get_role(self.cog.bot.role_ids["registered"])
            global_banned = self.guild.get_role(self.cog.bot.role_ids["global_banned"])
            pug_banned = self.guild.get_role(self.cog.bot.role_ids["pug_banned"])
            unregistered = self.guild.get_role(self.cog.bot.role_ids["unregistered"])
            overwrites = {
                global_banned: discord.PermissionOverwrite(
                    send_messages=False, add_reactions=False
                ),
                pug_banned: discord.PermissionOverwrite(
                    send_messages=False, add_reactions=False
                ),
                self.guild.default_role: discord.PermissionOverwrite(
                    send_messages=False, add_reactions=False
                ),
                unregistered: discord.PermissionOverwrite(
                    send_messages=False, add_reactions=False
                ),
                registered: discord.PermissionOverwrite(
                    send_messages=False, add_reactions=True
                ),
            }
            category = await self.guild.create_category_channel(
                name=self.category_name,
                reason=f"PUG #{self.id} started.",
                overwrites=overwrites,
            )
            self.category = category
            self.cog.bot.category_ids["running_pugs"] = category.id
            cod4pugbot.config.save_to_config(
                ("category_ids", "runnung_pugs"), category.id
            )
        else:
            for category_channel in self.guild.categories:
                if category_channel.name == self.category_name:
                    category = category_channel
        channel = await category.create_text_channel(
            name=f"pugs-{self.id}",
            reason=f"PUG #{self.id} started.",
        )
        self.channel = channel

    async def _create_match_message(self) -> None:
        """Create match message in temporary match channel."""
        message = await self.channel.send(self.match_base_message)
        self.messages.match = message
        await self._start_player_pick_phase()

    async def _modify_match_message(self) -> None:
        """Modify match message in temporary match channel."""
        newline = "\n"
        logger.debug("picker: %s, phase: %s", self.picker, self.phase)
        message = None
        if self.picker is not None and self.phase == "player pick":
            message = (
                f"""{newline}{self.picker.mention}, it's your turn to """
                f"""__**pick**__ a player!{newline}"""
                f"""React with \u2705 to a message to pick the respective player."""
            )
        elif self.picker is not None and self.phase == "map elimination":
            if self.map_elimination_phase == "pick":
                emoji = "\u2705"
            elif self.map_elimination_phase == "veto":
                emoji = "\u26d4"
            message = (
                f"""{newline}{self.picker.mention}, it's your turn to """
                f"""__**{self.map_elimination_phase}**__ a map!{newline}"""
                f"""React with {emoji} to a message to {self.map_elimination_phase} """
                """the respective map."""
            )
        if message is not None:
            await self.messages.match.edit(content=self.match_base_message + message)

    async def _start_player_pick_phase(self) -> None:
        """Start player pick phase.

        If the PUG is 1v1, it starts immediately with the map elimination phase.
        Otherwise, it starts with the player pick phase.

        """
        self.picker_cycle = itertools.cycle(self.pickers)
        self.picker = next(self.picker_cycle)
        if self.max_players == 2:
            await self._start_map_elimination()
        else:
            self.phase = "player pick"
            for player in self._players:
                message = await self.channel.send(f"> {player.mention}")
                await message.add_reaction(emoji="\u2705")
                self.messages.players[player] = message
            await self._modify_match_message()

    async def _pick_random_maps(self) -> None:
        """Pick random maps.

        If map elimination is disabled, the right amount of maps (depends on
        the match mode) gets picked randomly.

        """
        for i in range(self.max_maps):
            map_picked = random.choice(self.map_pool)
            self.maps.append(map_picked)
            self.map_pool.remove(map_picked)

    async def _start_map_elimination(self) -> None:
        """Start map elimination phase.

        If map elimination is enabled, the map pick/veto order is defined
        according to the match mode of the PUG.

        """
        self.phase = "map elimination"
        if not self.map_elimination:
            await self._pick_random_maps()
            await self._start_pug()
            return
        else:
            if self.mode == "bo1":
                self.map_elimination_cycle = itertools.cycle(("veto",))
            elif self.mode == "bo2":
                self.map_elimination_cycle = iter(("pick", "pick"))
            elif self.mode == "bo3":
                self.map_elimination_cycle = iter(
                    ("veto", "veto", "pick", "pick", "random")
                )
            elif self.mode == "bo5":
                self.map_elimination_cycle = iter(
                    ("pick", "pick", "pick", "pick", "random")
                )
            self.map_elimination_phase = next(self.map_elimination_cycle)
            logger.debug("map pool: %s", self.map_pool)
            for map_ in self.map_pool:
                message = await self.channel.send(f"""> {map_}""")
                for emoji in ["\u2705", "\u26d4"]:
                    await message.add_reaction(emoji=emoji)
                self.messages.maps[map_] = message
            await self._modify_match_message()

    async def _delete_remaining_messages(self, messages: dict = None) -> None:
        """Delete remaining messages.

        Parameters:
            messages : dict, optional
                Dictionary with `discord.Message` to delete.
                Defaults to `None`-

        """
        if messages is None:
            messages = self.messages

        for key, value in messages.copy().items():
            if key == "match":
                continue
            if isinstance(value, dict):
                await self._delete_remaining_messages(messages=messages[key])
            elif isinstance(value, discord.Message):
                await messages[key].delete()
                messages.pop(key)
            if key in messages and not messages[key]:
                messages.pop(key)

    async def _start_pug(self) -> None:
        """Start PUG.

        Creates a `RunningPUG`.

        """
        await self._modify_match_message()
        await self._delete_remaining_messages()
        await RunningPUG.create(
            cog=self.cog,
            guild=self.guild,
            pug_id=self.id,
            mode=self.mode,
            size=self.size,
            attack=self.attack,
            defence=self.defence,
            maps=self.maps,
            channel=self.channel,
            messages=self.messages,
        )

    async def _add_player_to_team(
        self, captain: discord.Member, player: discord.Member
    ) -> None:
        """Add player to captain's team.

        Deletes the player message.

        Parameters:
            captain : discord.Member
                Team captain.
            player : discord.Member
                Player to add to the captain's team.

        """
        for team in self.teams:
            if captain == team["captain"]:
                team["players"].append(player)
                self._players.remove(player)
                await self.messages.players[player].delete()
                self.messages.players.pop(player)
                await self._modify_match_message()

    async def _add_map_to_maps(self, map_to_add: str) -> None:
        """Add map to PUG's maps to be played.

        Deletes the map message.

        Parameters:
            map_to_add : str
                Map to add to map's to be played.

        """
        if map_to_add in self.map_pool:
            self.maps.append(map_to_add)
            self.map_pool.remove(map_to_add)
            await self.messages.maps[map_to_add].delete()
            self.messages.maps.pop(map_to_add)
            await self._modify_match_message()

    async def _remove_map_from_map_pool(self, map_to_remove: str) -> None:
        """Remove map from PUG's map pool.

        Parameters:
            map_to_remove : str
                Map to remove from PUG's map pool.

        """
        if map_to_remove in self.map_pool:
            self.map_pool.remove(map_to_remove)
            await self.messages.maps[map_to_remove].delete()
            self.messages.maps.pop(map_to_remove)
            await self._modify_match_message()

    async def end_pug(self) -> None:
        """End PUG when not in running phase after certain amount of time."""
        newline = "\n"
        self.cog.active_pugs.remove_pug(pug_id=self.id)
        await self._delete_remaining_messages()
        captain = self.picker
        self.picker = None
        await self._modify_match_message()
        await self.channel.send(
            f"""> PUG has expired.{newline}"""
            f"""> {captain.mention} did not pick in time.{newline}"""
            f"""> This channel will be deleted in """
            f"""{int(self.delete_channel_delay / 60)} """
            f"""{"minutes" if self.delete_channel_delay > 60 else "minute"}."""
        )
        await asyncio.sleep(self.delete_channel_delay)
        await self.channel.delete(reason=f"PUG #{self.id} finished.")

    async def pick_player(
        self, captain: discord.Member, reaction: discord.Reaction
    ) -> None:
        """Pick player into team.

        Checks if the captain actually is a captain of either of the teams,
        if the captain is currently supposed to pick and if he used the correct
        reaction emoji.
        If only one player remains, it gets picked into the other team and the
        map elimination phase starts.

        Parameters:
            captain : discord.Member
                The captain that picked a player.
            reaction : discord.Reaction
                The reaction the captain used.

        """
        if (
            self.phase != "player pick"
            or captain not in self.captains
            or captain != self.picker
            or reaction.message not in self.messages.players.values()
            or reaction.message.author != self.cog.bot.user
            or reaction.emoji != "\u2705"
            or not reaction.message.mentions
        ):
            await reaction.remove(captain)
            return

        player = reaction.message.mentions[0]

        if player not in self._players:
            await reaction.remove(captain)
            return

        self._add_player_to_team(captain=captain, player=player)
        self.picker = next(self.picker_cycle)

        if len(self._players) == 1:
            self._add_player_to_team(captain=self.picker, player=self._players[0])
            if self.map_elimination:
                await self._modify_match_message()
                await self._start_map_elimination()
            else:
                await self._pick_random_maps()
                await self._start_pug()
        else:
            await self._modify_match_message()

    async def pick_map(
        self, captain: discord.Member, reaction: discord.Reaction
    ) -> None:
        """Pick a map.

        Checks if the map elimination phase is actually `veto`, if the captain
        actually is a captain of either of the teams, if the captain is
        currently supposed to pick and if he used the correct reaction emoji.
        The picked map is then added to the maps of the PUG and removed from
        the map pool. Then the other captain can pick/veto.
        If there is only one map left in the pool or the last pick phase is
        reached ("random"), the remaining maps are (randomly) put into the
        PUG maps.

        Parameters:
            captain : discord.Member
                The captain that picked a map.
            reaction : discord.Reaction
                The reaction the captain used.

        """
        if (
            self.phase != "map elimination"
            or self.map_elimination_phase != "pick"
            or captain not in self.captains
            or captain != self.picker
            or reaction.message not in self.messages.maps.values()
            or reaction.message.author != self.cog.bot.user
            or reaction.emoji != "\u2705"
        ):
            await reaction.remove(captain)
            return

        map_picked = re.findall(r"\w+", reaction.message.content)[0]

        if map_picked not in self.map_pool:
            logger.debug("map %s not found in map pool %s", map_picked, self.map_pool)
            await reaction.remove(captain)
            return

        self._add_map_to_maps(map_to_add=map_picked)
        self.picker = next(self.picker_cycle)

        try:
            self.map_elimination_phase = next(self.map_elimination_cycle)
        except StopIteration:
            await self._start_pug()
        finally:
            if self.map_elimination_phase == "random":
                await self._add_map_to_maps(map_to_add=random.choice(self.map_pool))
                await self._start_pug()
            elif len(self.map_pool) == 1:
                await self._add_map_to_maps(map_to_add=self.map_pool[0])
                await self._start_pug()

    async def veto_map(
        self, captain: discord.Member, reaction: discord.Reaction
    ) -> None:
        """Veto a map.

        Checks if the map elimination phase is actually `veto`, if the captain
        actually is a captain of either of the teams, if the captain is
        currently supposed to pick and if he used the correct reaction emoji.
        The vetoed map is then being removed from the map pool and the other
        captain can pick/veto.
        If there is only one map left in the pool or the last pick phase is
        reached ("random"), the remaining maps are (randomly) put into the
        PUG maps.

        Parameters:
            captain : discord.Member
                The captain that vetoed a map.
            reaction : discord.Reaction
                The reaction the captain used.

        """
        if (
            self.phase != "map elimination"
            or self.map_elimination_phase != "veto"
            or captain not in self.captains
            or captain != self.picker
            or reaction.message.author != self.cog.bot.user
            or reaction.message not in self.messages.maps.values()
            or reaction.emoji != "\u26d4"
        ):
            await reaction.remove(captain)
            return

        map_vetoed = re.findall(r"\w+", reaction.message.content)[0]

        if map_vetoed not in self.map_pool:
            logger.debug("map %s not found in map pool %s", map_vetoed, self.map_pool)
            await reaction.remove(captain)
            return

        self._remove_map_from_map_pool(map_to_remove=map_vetoed)
        self.picker = next(self.picker_cycle)

        try:
            self.map_elimination_phase = next(self.map_elimination_cycle)
        except StopIteration:
            await self._start_pug()
        finally:
            if self.map_elimination_phase == "random":
                await self._add_map_to_maps(map_to_add=random.choice(self.map_pool))
                await self._start_pug()
            elif len(self.map_pool) == 1:
                await self._add_map_to_maps(map_to_add=self.map_pool[0])
                await self._start_pug()
