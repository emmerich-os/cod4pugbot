import asyncio
import datetime
import logging
import math
import re

import async_timeout as at
import discord
from discord.ext import commands

import cod4pugbot.domain.features.pugs.cod4
import cod4pugbot.domain.utils
from . import _messages as pug_messages
from . import _modes as modes
from . import _sizes as sizes
from . import _statuses as statuses
from . import _teams as pug_teams
from .base_pug import PUG
from cod4pugbot import db


logger = logging.getLogger(__name__)


class RunningPUG(PUG):
    """A running PUG.

    Uses coroutine functions, thus always use classmethod `create`.
    See help(PreMatchPUG.create) for detailed information.

    In the pre-match PUG, captains, who are chosen randomly, are picking
    players for their team. Which captain starts is also random. Afterwards,
    if map elimination is enabled, the map elimination starts. If it is
    disabled, the map/s get picked randomly. When this is done, the PUG is
    replaced by a `RunningPUG`.

    """

    def __init__(
        self,
        pug_id: int,
        mode: modes.Modes,
        size: sizes.Sizes,
        attack: pug_teams.Attack,
        defence: pug_teams.Defence,
        maps: list,
        channel: discord.TextChannel,
        messages: pug_messages.Messages,
    ) -> None:
        """Init a running PUG.

        Parameters:
            cog : commands.Cog
                The `PUGs` Cog.
                This is important to make any bot information available.
            guild : discord.Guild
                Guild where the PUG is active.
            pug_id : int:
                ID of the PUG.
            mode : str
                PUG mode.
                BO1, BO2, BO3 or BO5.
            size : str
                PUG size.
                1v1, 2v2, 3v3 or 5v5.
            attack : dict
                Attacking team.
                Contains:
                    - icon (custom emoji from
                        cogs.utils.cod4.scorebot.dicts.EMOJIS)
                    - captain
                    - players (list of `discord.Members`)
            defence : dict
                Defending team.
                Contains:
                    - icon (custom emoji from
                        cogs.utils.cod4.scorebot.dicts.EMOJIS)
                    - captain
                    - players (list of `discord.Members`)
            maps : list
                Maps to be played.
            channel : discord.TextChannel
                Channel of the PUG.
            messages : dict
                PUG messages.
                Contains "match".

        """
        self.id = pug_id
        self.mode = mode
        self.size = size
        self.attack = attack
        self.defence = defence
        self.maps = maps
        self.status = statuses.Statuses.RUNNING
        self.channel = channel
        self.messages = messages
        self.server: cod4pugbot.cod4.Server = None
        self.max_time = datetime.datetime.now(
            tz=self.cog.bot.timezone
        ) + datetime.timedelta(minutes=len(self.maps) * 45)
        self.delete_channel_delay = 60
        self.elo_k = 32

    @classmethod
    async def create(
        cls,
        pug_id: int,
        mode: modes.Modes,
        size: sizes.Sizes,
        attack: dict,
        defence: dict,
        maps: list,
        channel: discord.TextChannel,
        messages: pug_messages.Messages,
    ) -> "RunningPUG":
        """Create a running PUG.

        Parameters:
            pug_id : int:
                ID of the PUG.
            mode : str
                PUG mode.
                BO1, BO2, BO3 or BO5.
            size : str
                PUG size.
                1v1, 2v2, 3v3 or 5v5.
            attack : dict
                Attacking team.
                Contains:
                    - icon (custom emoji from
                        cogs.utils.cod4.scorebot.dicts.EMOJIS)
                    - captain
                    - players (list of `discord.Members`)
            defence : dict
                Defending team.
                Contains:
                    - icon (custom emoji from
                        cogs.utils.cod4.scorebot.dicts.EMOJIS)
                    - captain
                    - players (list of `discord.Members`)
            maps : list
                Maps to be played.
            channel : discord.TextChannel
                Channel of the PUG.
            messages : dict
                PUG messages.
                Contains "match".

        """
        client = db.SQLClient(**cog.bot.db_info)
        client.update(
            table="pugs",
            id=pug_id,
            maps=maps,
            attack=attack["players"],
            defence=defence["players"],
        )
        self = cls(
            cog=cog,
            guild=guild,
            pug_id=pug_id,
            mode=mode,
            size=size,
            attack=attack,
            defence=defence,
            maps=maps,
            channel=channel,
            messages=messages,
        )
        await self._modify_match_message(init=True)
        await self._post_score_report_message()
        await self._modify_scoreboard_message(init=True)
        await self._modify_killfeed_message(init=True)
        self.cog.active_pugs.pugs[pug_id] = self
        return self

    @property
    def teams(self) -> list[pug_teams.Team]:
        """Create list with attack and defence as `dict`."""
        return [self.attack, self.defence]

    @property
    def players(self) -> list[pug_teams.Player]:
        """Create list with attack and defence players."""
        return self.attack.players + self.defence.players

    @property
    def captains(self) -> list[pug_teams.Player]:
        """Create list with attack and defence captain."""
        captains = [self.attack.captain, self.defence.captain]
        return [captain for captain in captains if captain is not None]

    @property
    def max_maps(self) -> int:
        """Return maximum number of maps to be played."""
        return self.mode.max_maps

    @property
    def win_maps(self) -> int:
        """Return number of maps needed to win the PUG."""
        return math.ceil(int(self.mode[-1]) / 2)

    async def _modify_match_message(self, init: bool = False) -> None:
        """Modify match message.

        Parameters:
            init : bool, optional
                Put `True` if the modification is at initialization of
                the PUG.
                Then the channel is made accessible to all players in the PUG.

        """
        newline = "\n"
        attack = ", ".join(player.mention for player in self.attack["players"])
        defence = ", ".join(player.mention for player in self.defence["players"])
        mapstr = "Map" if len(self.maps) == 1 else "Maps (in order)"
        if self.server is None:
            match_message = (
                f"""{newline}"""
                f"""\t\t**MATCH STARTED**{newline}{newline}"""
                f"""> {self.attack["icon"]} **-** {attack}{newline}{newline}"""
                f"""\t\t**VERSUS**{newline}{newline}"""
                f"""> {self.defence["icon"]} **-** {defence}{newline}{newline}"""
                f"""{mapstr}: {", ".join(self.maps)}{newline}{newline}"""
                f"""You may now connect to the server.{newline}"""
                f"""Use `{self.cog.bot.command_prefix}server connect <IP>; password <password>` """
                f"""to add a server.{newline}"""
                f"""If you include the rcon (`{self.cog.bot.command_prefix}server connect <IP>; """
                f"""password <password>; rcon <rcon password>`) and the server has Promod """
                f"""scorebot and PunkBuster enabled, stats will be tracked.{newline}"""
                f"""Your message will be deleted immediately to prevent others from stealing """
                f"""the rcon password.{newline}"""
            )
            await self.messages.match.edit(content=match_message)
        else:
            match_message = (
                f"""{newline}"""
                f"""\t\t**MATCH STARTED**{newline}{newline}"""
                f"""> {self.attack["icon"]} **-** {attack}{newline}{newline}"""
                f"""\t\t**VERSUS**{newline}{newline}"""
                f"""> {self.defence["icon"]} **-** {defence}{newline}{newline}"""
                f"""{mapstr}: {", ".join(self.maps)}{newline}{newline}"""
                f"""You may now connect to the server.{newline}"""
            )
            if self.server.password is None:
                match_message += (
                    f"""{newline}Server: ```connect {self.server.ip_port}```"""
                )
            else:
                match_message += (
                    f"""{newline}Server: ```connect {self.server.ip_port}; """
                    f"""password {self.server.password}```"""
                )
            await self.messages.match.edit(
                content=match_message,
                embed=self.server.to_embed(),
            )
        if init:
            for player in self.players:
                await self.channel.set_permissions(target=player, send_messages=True)

    async def _modify_scoreboard_message(self, init: bool = False) -> None:
        """Modify and post scoreboard message.

        Parameters:
            init : bool, optional
                Put `True` if the modification is at initialization of
                the PUG.
                Then the scoreboard message gets posted before the channel
                is made accessible to all players in the PUG.
                This makes it appear at the top of the channel any time.

        """
        if init:
            message = await self.channel.send(
                content=(
                    """The scoreboard will be posted once a server was added """
                    """and players joined the server."""
                )
            )
            self.messages.scoreboard = message
        elif (
            self.messages.scoreboard is not None
            and self.server is not None
            and (
                self.server.players is not None
                or self.server.promod_players is not None
            )
        ):
            await self.messages.scoreboard.edit(
                content="> "
                + "\n> ".join(self.server.format_scoreboard(to_message=True))
            )

    async def _modify_killfeed_message(self, init: bool = False) -> None:
        """Modify and post killfeed message.

        Parameters:
            init : bool, optional
                Put `True` if the modification is at initialization of
                the PUG.
                Then the killfeed message gets posted before the channel
                is made accessible to all players in the PUG.
                This makes it appear at the top of the channel any time.

        """
        if init:
            killfeed_message = (
                """If the server has the Promod scorebot enabled, """
                """the killfeed will be posted."""
            )
            message = await self.channel.send(content=killfeed_message)
            self.messages, killfeed = message
        elif self.messages.killfeed is not None and self.killfeed:
            killfeed_message = "> " + "\n> ".join(
                event.message for event in self.killfeed
            )
            await self.messages.killfeed.edit(content=killfeed_message)

    async def _post_score_report_message(self) -> None:
        """Post the score report message in the PUG channel."""
        newline = "\n"
        if self.mode == "bo1" or self.mode == "bo2":
            score_report_message = (
                f"""> {self.attack["captain"].mention}, """
                f"""{self.defence["captain"].mention}:{newline}"""
                f"""> React with \u26d4 to this message after the """
                f""" match to report your loss.{newline}"""
                f"""> React with \U0001F538 to report a draw."""
            )
            emojis = ["\u26d4", "\U0001F538"]
        else:
            score_report_message = (
                f"""> {self.attack["captain"].mention}, """
                f"""{self.defence["captain"].mention}:{newline}"""
                f"""> React with \u26d4 to this message after the match to report your loss."""
            )
            emojis = ["\u26d4"]
        message = await self.channel.send(score_report_message)
        await message.pin()
        for emoji in emojis:
            await message.add_reaction(emoji=emoji)
        self.messages.score_report = message

    def _calculate_team_elo(self) -> None:
        """Calculate average team Elo.

        Get the respective Elo for each player in the team from the database
        table of the respective PUG size (1v1, 2v2 etc.) and calculate
        the average of all players.
        """
        for team in self.teams:
            elo = 0
            for player in team["players"]:
                kwargs = {"discord_id": player.id}
                client = db.SQLClient(**self.cog.bot.db_info)
                search_stats = client.select(
                    view="stats_print", subtable=self.size, **kwargs
                )[0]
                if search_stats is not None:
                    kwargs = {
                        **kwargs,
                        "pugs_played": search_stats["pugs_played"] + 1,
                    }
                    client.update(table=f"stats_{self.size}", **kwargs)
                    elo += search_stats["elo"]
                else:
                    kwargs = kwargs | cod4pugbot.domain.utils.get_kwargs(member=player)
                    client.insert(table=f"stats_{self.size}", **kwargs)
                    elo += 1000

            team["elo"] = math.floor(elo / len(team["players"]))

    def _calculate_elo_change(self, team1: dict, team2: dict) -> tuple[int, int, int]:
        """Calculate Elo change for given teams.

        Parameters:
            team1 : dict
                Team the player is in.
                Needs to contain the "elo" key.
            team2 : dict
                Opposite team.
                Needs to contain the "elo" key.

        Returns:
            win_elo : int
                Elo gained at win.
                Always positive.
            lose_elo : int
                Elo lost at loss.
                Always negative.
            draw_elo : int
                Elo gained/lost at draw.

        """
        elo_diff = team2["elo"] - team1["elo"]
        percentage = 1 / (1 + pow(10, elo_diff / 400))
        win_elo = round(self.elo_k * (1 - percentage))
        lose_elo = -1 * (self.elo_k - win_elo)
        if team1["elo"] > team2["elo"]:
            draw_elo = 0.5 * lose_elo
        elif team1["elo"] < team2["elo"]:
            draw_elo = 0.5 * win_elo
        else:
            draw_elo = 0
        return win_elo, lose_elo, int(draw_elo)

    def _calculate_elo(self) -> None:
        """Calculate new Elo for players."""
        self._calculate_team_elo()
        for i, team in enumerate(self.teams):
            win_elo, lose_elo, draw_elo = self._calculate_elo_change(
                self.teams[i], self.teams[~i]
            )
            logger.debug(
                (
                    "captain: %s, elo: %s, result: %s, win_elo: %s, lose_elo: %s, draw_elo: %s, "
                    "teams_elo_equal: %s"
                ),
                team["captain"],
                team["elo"],
                team["result"],
                win_elo,
                lose_elo,
                draw_elo,
                self.teams[i]["elo"] == self.teams[~i]["elo"],
            )
            for player in team["players"]:
                kwargs = {"discord_id": player.id}
                client = db.SQLClient(**self.cog.bot.db_info)
                stats = client.select(view="stats_print", subtable=self.size, **kwargs)
                stats.setdefault("discord_id", kwargs["discord_id"])
                if team["result"] == "win":
                    stats["wins"] += 1
                    stats["elo"] += win_elo
                elif team["result"] == "loss":
                    stats["losses"] += 1
                    stats["elo"] += lose_elo
                elif team["result"] == "draw":
                    stats["draws"] += 1
                    stats["elo"] += draw_elo
                client.update(
                    table=f"stats_{self.size}",
                    before={"discord_id": stats["discord_id"]},
                    after=stats,
                )

    async def end_pug(
        self,
        captain: discord.Member = None,
        team: dict = None,
        reported_by_scorebot: bool = False,
    ) -> None:
        """End PUG.

        The win/draw/loss for each team is saved and the Elo change for
        each player in the PUG is calculated. Afterwards, the PUG channel
        gets deleted.

        Parameters:
            captain : discord.Member, optional
                The team captain that ended the PUG via reaction to the
                report score message.
                Defaults to `None`.
                        team : dict, optional
                Team the captain belongs to.
                Defaults to `None`.
            team : dict, optional
                Team of the captain.
                Defaults to `None`.
            reported_by_scorebot : bool, optional
                `True` if the PUG score is reported by the stats tracker (bot).
                Defaults to `None`.

        """
        newline = "\n"
        self.cog.active_pugs.remove_pug(pug_id=self.id)
        if reported_by_scorebot:
            self._calculate_elo()
            await self.messages.score_report.delete()
            message = (
                f"""> PUG has finished.{newline}"""
                f"""> Promod scorebot has reported a {self.attack["result"]} for Attack """
                f"""and a {self.defence["result"]} for Defence.{newline}"""
            )
        elif captain is not None:
            self._calculate_elo()
            if self.server is not None and self.server.stats.players:
                self.server.stats.save_stats_to_db()
            await self.messages.score_report.delete()
            message = (
                f"""> PUG has finished.{newline}"""
                f"""> {captain.mention} has reported a {team["result"]}.{newline}"""
            )
        else:
            message = (
                f"""> PUG has expired.{newline}"""
                f"""> No result have been reported.{newline}"""
            )

        await self.channel.send(
            message
            + (
                f"""> This channel will be deleted in {self.delete_channel_delay} """
                f"""{"seconds" if self.delete_channel_delay > 1 else "second"}."""
            )
        )
        await asyncio.sleep(self.delete_channel_delay)
        await self.channel.delete(reason=f"PUG #{self.id} finished.")

    async def abort_pug(self) -> None:
        """Abort running PUG."""
        self.cog.active_pugs.remove_pug(pug_id=self.id)
        client = db.SQLClient(**self.cog.bot.db_info)
        client.delete(table="pugs", pug_id=self.id)
        self.server = None
        newline = "\n"
        await self.channel.send(
            f"""> PUG was aborted.{newline}"""
            f"""> This channel will be deleted in {self.delete_channel_delay} """
            f"""{"seconds" if self.delete_channel_delay > 1 else "second"}."""
        )
        await asyncio.sleep(self.delete_channel_delay)
        await self.channel.delete(reason=f"PUG #{self.id}  aborted.")

    async def add_server(self, ctx: commands.Context, content: str) -> None:
        """Add server to PUG.

        Parameters:
            ctx : commands.Context
                Context of the command to add a server.
            content : str
                Content of the message after the command.
                Should contain the CoD4-like connect command.

        """
        regex = [
            r"([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}):([0-9]{1,5})",
            r"password[\s]*(\w+)",
            r"rcon[\s]*(\w+)",
        ]
        logger.debug("%s", content)

        if bool(re.search(regex[0], content)):
            ip, port = re.findall(regex[0], content)[0]
            port = int(port)
        else:
            await ctx.channel.send(
                f"Sorry {ctx.message.author.mention}, "
                "the connect command is invalid! I could not find any IP and port in there."
            )
            return

        password = (
            re.findall(regex[1], content)[0]
            if bool(re.search(regex[1], content))
            else None
        )
        rcon = (
            re.findall(regex[2], content)[0]
            if bool(re.search(regex[2], content))
            else None
        )

        for pug in self.cog.active_pugs.pugs.copy().values():
            if pug.status != "running":
                continue
            if (
                pug.server is not None
                and pug.server.ip == ip
                and pug.server.port == port
            ):
                await ctx.channel.send(
                    f"""Sorry {ctx.message.author.mention}, this server is currently in use """
                    f"""by PUG {pug.channel.mention}."""
                )
                return

        self.server = cod4pugbot.cod4.Server.connect(
            ip=ip,
            port=port,
            password=password,
            rcon_password=rcon,
            pug=self,
        )
        self.killfeed = []
        await self._modify_match_message()

    async def refresh_server(self) -> None:
        """Refresh server information and scoreboard/killfeed."""
        if self.server is None:
            return

        async with at.timeout(3.5):
            self.server.refresh()

        if self.server.players is not None or self.server.promod_players is not None:
            await self._modify_scoreboard_message()

        if self.server.current_killfeed is not None:
            for event in self.server.current_killfeed:
                if any(
                    event.name == start_message
                    for start_message in [
                        "1st_half_ready_up",
                        "2nd_half_ready_up",
                        "timeout_ready_up",
                        "1st_half_started",
                        "2nd_half_started",
                        "match_resumed",
                        "round_start",
                        "knife_round",
                    ]
                ):
                    await self._modify_killfeed_message()
                    await asyncio.sleep(3)
                    self.killfeed = [event]
                else:
                    self.killfeed.append(event)
                    await self._modify_killfeed_message()

        await self._modify_match_message()

    async def report_score(
        self,
        captain: discord.Member,
        reaction: discord.Reaction = None,
    ) -> None:
        """Report score/loss.

        Checks if the message is actually from the bot.

        Parameters:
            captain : discord.Member
                The team captain that ended the PUG via reaction to the
                report score message.
            reaction : discord.Reaction, optional
                The reaction added by the captain.
                Defaults to `None`.

        """
        if (
            captain not in self.captains
            or self.status != statuses.Statuses.RUNNING
            or reaction.message not in self.messages.as_list()
            or reaction.message.id != self.messages.score_report.id
            or reaction.message.author != self.cog.bot.user
            or all(reaction.emoji != emoji for emoji in ["\u26d4", "\U0001F538"])
        ):
            await reaction.remove(captain)
            return

        for team in self.teams:
            if captain in team.players and reaction.emoji == "\u26d4":
                team_temp = team
                team["result"] = "loss"
                break
            elif captain in team["players"] and reaction.emoji == "\U0001F538":
                team_temp = team
                team["result"] = "draw"
                break

        await self.end_pug(captain=captain, team=team_temp)
