import datetime
import os
import re

import discord
import pytz
from discord.ext import commands

import cod4pugbot.domain.utils as utils
from cod4pugbot.db import SQLClient


class Register(commands.Cog, name="Register"):
    """Register feature.

    Attributes:
        bot : commands.Bot
            COD4PUGBot.
            Important to have access to the bot configurations.

    Listeners:
        on_ready() : coroutine func
            At first connect, start all tasks.
        on_raw_reaction_add(raw_reaction) : coroutine func
            On reaction add, accept register request and register member.
        on_raw_reaction_remove(raw_reaction) : coroutine func
            On reaction remove, either deny register request or unregister user.

    Tasks:
        backup_members() : coroutine func
            Save back up of data base file.

    Commands:
        register(ctx, *, content) : coroutine func
            Register request with given PB GUID. Send embed to admin channel.
        register_member(ctx, user, *, content) : coroutine func
            Register member.
        remove_registered_member(ctx, *, content) : coroutine func
            Remove member.
        search_registered_member(ctx, *, content) : coroutine func
            Search member.
        update_member(ctx, *, content) : coroutine func
            Update member information.
        registered_members(ctx) : coroutine func
            Post member list.
        message(ctx, user, *, content) : coroutine func
            Message user via bot.

    Methods:
        cog_unload() : func
            If Cog is unloaded, stop all tasks.
        _check_register_requests(ctx, channel, **kwargs) : local coroutine func
            On register request, check previous register requests for same user.

    """

    def __init__(self, bot: commands.Bot):
        self.bot = bot
        self._first_connect = True

    async def _check_register_requests(
        self, ctx: commands.Context, channel: str, **kwargs
    ) -> bool:
        """Check embeds of older messages in register admin channel.

        Parameters:
             ctx : commands.Context
                Context of the register message.
             channel : str
                Name of the register channel.

        Keyword Parameters:
            discord_id : int
                Discord ID of the user that posted the register message.
            name : str
                Discord name of the user that posted the register message.
            nick : str
                Nick on the Discord server of the user that posted the register message.
            pb_guid : str
                PB GUID of the user that posted the register message.

        """
        guild = ctx.guild
        channel = guild.get_channel(self.bot.channel_ids["admin_section"][channel])
        async for message in channel.history():
            if (
                message.author != self.bot.user
                or not message.embeds
                or len(message.embeds) > 1
            ):
                continue

            [embed] = message.embeds
            embed = embed.to_dict()
            embed_fields = embed["fields"]
            if "REJECTED" in embed_fields[2]["value"]:
                continue

            member = await guild.fetch_member(
                int("".join(re.findall(r"[0-9]*", embed_fields[0]["value"])))
            )
            embed_details = {
                "nick": member.nick,
                "name": member.name + "#" + member.discriminator,
                "discord_id": member.id,
                "pb_guid": embed_fields[1]["value"],
                "status": embed_fields[2]["value"],
            }
            for embed_detail in embed_details.values():
                for value in kwargs.values():
                    if value == embed_detail:
                        await ctx.channel.send(
                            f"Sorry {ctx.message.author.mention}, "
                            "there already is a similar register request: "
                            "```HTTP\n"
                            "{}\n"
                            "```"
                            "Please contact an admin.".format(
                                "\n".join(
                                    [
                                        f"Nick: {embed_details['nick']}",
                                        f"Name: {embed_details['name']}",
                                        f"Discord ID: {embed_details['discord_id']}",
                                        f"PB GUID: {embed_fields[1]['value']}",
                                        f"Status: {embed_fields[2]['value'].splitlines()[1]}",
                                    ]
                                )
                            )
                        )
                        return True
        return False

    async def _is_support_register_channel(self, ctx: commands.Context) -> bool:
        """Check if the context channel is the register support channel."""
        return await utils.is_channel(
            bot=self.bot,
            ctx=ctx,
            channel="register",
            category="support",
            send_message=True,
        )

    async def _is_admin_register_channel(self, ctx: commands.Context) -> bool:
        """Check if the context channel is the register admin channel."""
        return await utils.is_channel(
            bot=self.bot,
            ctx=ctx,
            channel="register",
            category="admin_section",
            send_message=True,
        )

    @commands.command(
        name="register",
        brief="Request to get registered in the Discord server.",
        description=(
            "Register in the Discord server. "
            "You need an original Call of Duty 4 key, your PB GUID "
            "and a `/pb_plist` screenshot of it."
        ),
        usage="""<PB GUID> (+ attachment with the `/pb_plist` screenshot)""",
        pass_context=True,
    )
    async def register(self, ctx: commands.Context, *, content: str):
        if not await self._is_support_register_channel(
            ctx
        ) or not await utils.has_attachments(ctx=ctx, pb_guid=True):
            return

        member_info = await utils.get_kwargs(
            ctx=ctx,
            content=content,
            author_kwargs=True,
            pb_guid=True,
        )

        if not member_info:
            return
        elif "pb_guid" not in member_info:
            await ctx.channel.send(
                f"Sorry {ctx.message.author.mention}, you need to "
                "give a PB GUID (<PB GUID> or pb_guid=<PB GUID>)."
            )
            return

        client = SQLClient(**self.bot.db_info)
        members = client.select(view="members_print", **member_info)

        if members is None and not await self._check_register_requests(
            ctx=ctx, channel="register", **member_info
        ):
            register_embed = _create_register_request_embed(
                ctx=ctx,
                pb_guid=member_info["pb_guid"],
                timezone=self.bot.timezone,
            )
            register_channel = self.bot.get_channel(
                self.bot.channel_ids["admin_section"]["register"]
            )
            message = await register_channel.send(embed=register_embed)
            for emoji in ["\u2705", "\u26d4"]:
                await message.add_reaction(emoji=emoji)
            await ctx.message.author.send(
                f"Hello {ctx.message.author.name}, your register request was "
                "successful. It will be processed by the admin team soon."
            )
        elif members is not None:
            await ctx.message.author.send(
                f"Hello {ctx.message.author.name}, we already have a user "
                "registered with your name, Discord ID and/or PB GUID. "
                "Please contact an admin."
            )

    @commands.Cog.listener()
    @commands.has_guild_permissions(administrator=True)
    async def on_raw_reaction_add(self, raw_reaction: discord.RawReactionActionEvent):
        """On reaction to register messages in admin register channel.

        Parameters:
             raw_reaction : discord.RawReactionActionEvent
                Reaction that was added.

        """
        user, reaction = await utils.raw_reaction_to_reaction(
            bot=self.bot, raw_reaction=raw_reaction
        )

        if user == self.bot.user or reaction.message.author != self.bot.user:
            return
        elif not await utils.is_channel(
            bot=self.bot,
            channel_id=reaction.message.channel.id,
            channel="register",
            category="admin_section",
            send_message=True,
        ):
            return
        elif all(r.count >= 2 for r in reaction.message.reactions):
            await reaction.remove(user)
            return
        elif not reaction.message.embeds:
            return
        elif reaction.emoji != "\u2705" and reaction.emoji != "\u26d4":
            await reaction.remove(user)
            return

        kwargs = {}

        admin = await reaction.users().flatten()
        admin = admin[1]
        [embed] = reaction.message.embeds
        embed = embed.to_dict()
        embed_fields = embed["fields"]
        kwargs.setdefault(
            "discord_id", int(re.findall(r"[0-9]{18}", embed_fields[0]["value"])[0])
        )
        register_request_user = await reaction.message.guild.fetch_member(
            kwargs["discord_id"]
        )
        kwargs.setdefault(
            "name",
            register_request_user.name + "#" + register_request_user.discriminator,
        )
        kwargs.setdefault("nick", register_request_user.nick)
        kwargs.setdefault(
            "name",
            register_request_user.name + "#" + register_request_user.discriminator,
        )
        kwargs.setdefault(
            "pb_guid", re.findall(r"[A-z0-9]{8}", embed_fields[1]["value"])[0]
        )

        client = SQLClient(**self.bot.db_info)
        members = client.select(view="members_print", **kwargs)

        if reaction.emoji == "\u2705":
            if members is None:
                client = SQLClient(**self.bot.db_info)
                if client.insert(table="members", **kwargs):
                    guild = reaction.message.guild
                    await register_request_user.remove_roles(
                        guild.get_role(self.bot.role_ids["unregistered"]),
                        reason="Register request has been accepted.",
                    )
                    await register_request_user.add_roles(
                        guild.get_role(self.bot.role_ids["registered"]),
                        reason="Register request has been accepted.",
                    )
                    embed["fields"][2]["value"] = "```css\nACCEPTED\n```"
                    embed["color"] = 0x00FF00
                    await register_request_user.send(
                        "Your register request has been accepted."
                    )
                else:
                    await reaction.message.channel.send("Register failed.")
            else:
                await reaction.message.channel.send(
                    f"Register failed. Member {register_request_user.mention} is "
                    "already registered."
                )
        elif reaction.emoji == "\u26d4":
            await register_request_user.send(
                "Sorry, your register request has been rejected. Please contact "
                f"{admin.mention} for further information."
            )
            embed_fields[2]["value"] = "```ARM\nREJECTED\n```"
            embed["color"] = 0xFF0000

        await reaction.message.edit(embed=discord.Embed.from_dict(embed))

    @commands.Cog.listener()
    @commands.has_guild_permissions(administrator=True)
    async def on_raw_reaction_remove(
        self, raw_reaction: discord.RawReactionActionEvent
    ):
        """On reaction to register messages in admin register channel.

        Parameters:
             raw_reaction : discord.RawReactionActionEvent
                Reaction that was removed.

        """
        user, reaction = await utils.raw_reaction_to_reaction(
            bot=self.bot, raw_reaction=raw_reaction
        )

        if user == self.bot.user or reaction.message.author != self.bot.user:
            return
        elif not await utils.is_channel(
            bot=self.bot,
            channel_id=reaction.message.channel.id,
            channel="register",
            category="admin_section",
        ):
            return
        elif not reaction.message.embeds or any(
            r.count >= 2 for r in reaction.message.reactions
        ):
            return
        elif reaction.emoji != "\u2705" and reaction.emoji != "\u26d4":
            return

        [embed] = reaction.message.embeds
        embed = embed.to_dict()
        embed_fields = embed["fields"]
        discord_id = int(re.findall(r"[0-9]{18}", embed_fields[0]["value"])[0])
        register_request_user = await reaction.message.guild.fetch_member(discord_id)

        kwargs = {
            "nick": register_request_user.nick,
            "name": register_request_user.name
            + "#"
            + register_request_user.discriminator,
            "discord_id": discord_id,
            "pb_guid": re.findall(r"[A-z0-9]{8}", embed_fields[1]["value"])[0],
        }

        client = SQLClient(**self.bot.db_info)
        members = client.select(view="members_print", **kwargs)

        if reaction.emoji == "\u2705":
            if members is not None:
                if client.delete(table="members", **kwargs):
                    guild = reaction.message.guild
                    embed["fields"][2]["value"] = "```brainfuck\nPENDING\n```"
                    embed["color"] = 0x727272
                    await register_request_user.send(
                        "Your register entry has ben removed again and your "
                        "case is open again. Contact an admin for further information."
                    )
                    await register_request_user.remove_roles(
                        guild.get_role(self.bot.role_ids["registered"]),
                        reason="Register request has been revoked.",
                    )
                    await register_request_user.add_roles(
                        guild.get_role(self.bot.role_ids["unregistered"]),
                        reason="Register request has been revoked.",
                    )
                else:
                    await reaction.message.channel.send("Removing failed.")
        elif reaction.emoji == "\u26d4":
            await register_request_user.send(
                "Your reqister request has been re-opened. Contact "
                "an admin for further information."
            )
            embed["fields"][2]["value"] = "```brainfuck\nPENDING\n```"
            embed["color"] = 0x727272

        await reaction.message.edit(embed=discord.Embed.from_dict(embed))

    @commands.command(
        name="register_member",
        brief="Manually register member with a PB GUID.",
        description="Manually register member with a PB GUID.",
        usage="""@user pb_guid="<PB GUID>" """,
        pass_context=True,
    )
    @commands.has_guild_permissions(administrator=True)
    async def register_member(
        self, ctx: commands.Context, user: discord.Member, *, content: str
    ):
        if not await self._is_admin_register_channel(
            ctx
        ) or not await utils.has_mentions(ctx=ctx):
            return

        member = user
        guild = ctx.guild

        member_info = _get_member_info_and_pb_guid_from_input(
            ctx=ctx, member=member, content=content
        )

        if not member_info:
            await ctx.channel.send(
                f"""Sorry {ctx.message.author.mention}, could not process your input."""
            )
            return
        elif "pb_guid" not in member_info:
            await ctx.channel.send(
                f"Sorry {ctx.message.author.mention}, you need to "
                "give a PB GUID (<PB GUID> or pb_guid=<PB GUID>)."
            )
            return

        client = SQLClient(**self.bot.db_info)
        members = client.select(view="members_print", **member_info)

        if members is None:
            if client.insert(table="members", **member_info):
                register_request_user = await ctx.guild.fetch_member(
                    member_info["discord_id"]
                )
                await register_request_user.remove_roles(
                    guild.get_role(self.bot.role_ids["unregistered"]),
                    reason="Registered by admin.",
                )
                await register_request_user.add_roles(
                    guild.get_role(self.bot.role_ids["registered"]),
                    reason="Registered by admin.",
                )
                await ctx.channel.send("Register successful.")
            else:
                await ctx.channel.send("Register failed!")
        elif members := utils.check_df(df=members, to_dict=True):
            newline = "\n"
            lst = [f"{key}: {value}" for key, value in members.items()]
            await ctx.channel.send(
                f"There is already a user registered with this name, Discord ID or PB_GUID:\n"
                f"```HTTP\n"
                f"{f'{newline}'.join(lst)}\n"
                f"```"
            )
        else:
            await ctx.channel.send(
                f"""Sorry {ctx.message.author.mention}, register failed.\n"""
                f"""Please contact a head admin."""
            )

    @commands.command(
        name="remove_member",
        brief="Manually remove a registered member.",
        description="Manually remove a registered member.",
        usage=(
            """<user>\n\n"""
            """User can be referred to by:\n"""
            """\t- @user\n"""
            """\t- nick="<nick on the DIscord server>"\n"""
            """\t- name="<full Discord name (with #0000)>"\n"""
            """\t- discord_id="<Discord ID>"\n"""
            """\t- pb_guid="<PB_GUID>" """
        ),
        pass_context=True,
    )
    @commands.has_guild_permissions(administrator=True)
    async def remove_registered_member(self, ctx: commands.Context, *, content: str):
        if not await self._is_admin_register_channel(ctx):
            return

        member_info = _get_member_info_and_pb_guid_from_input_with_mention(
            ctx=ctx, content=content
        )

        if not member_info:
            return

        client = SQLClient(**self.bot.db_info)
        members = client.select(view="members_print", **member_info)

        if members is not None:
            if "discord_id" not in member_info:
                try:
                    member_info.setdefault("discord_id", members["discord_id"])
                except Exception:
                    await ctx.channel.send(
                        f"{ctx.message.author.mention}, there was no corresponding registered "
                        f"member found on the Discord server."
                    )
                    return

            if client.delete(table="members", **member_info):
                if "discord_id" in member_info:
                    register_request_user = await ctx.guild.fetch_member(
                        member_info["discord_id"]
                    )
                    if register_request_user is not None:
                        guild = ctx.guild
                        await register_request_user.remove_roles(
                            guild.get_role(self.bot.role_ids["registered"]),
                            reason="Register entry removed by admin.",
                        )
                        await register_request_user.add_roles(
                            guild.get_role(self.bot.role_ids["unregistered"]),
                            reason="Register entry removed by admin.",
                        )
                    else:
                        await ctx.channel.send(
                            f"Member {member_info['name']} not found on this Discord, "
                            "but register entry was removed succesfully."
                        )
                await ctx.channel.send("Removed successfully.")
            else:
                await ctx.channel.send("Removing failed!")
        else:
            await ctx.channel.send("No such member registered.")

    @commands.command(
        name="search_member",
        brief="Search the members for a specific member.",
        description="Search for a specific member in the list of registered members.",
        usage=(
            """<user to search>\n\n"""
            """Keywords to search for can be:\n"""
            """\t- @user\n"""
            """\t- nick="<nick on the DIscord server>"\n"""
            """\t- name="<full Discord name (with #0000)>"\n"""
            """\t- discord_id="<Discord ID>"\n"""
            """\t- pb_guid="<PB_GUID>" """
        ),
        pass_context=True,
    )
    @commands.has_guild_permissions(administrator=True)
    async def search_registered_member(self, ctx: commands.Context, *, content: str):
        if not await self._is_admin_register_channel(ctx):
            return

        member_info = _get_member_info_and_pb_guid_from_input_with_mention(
            ctx=ctx, content=content
        )

        if not member_info:
            return

        client = SQLClient(**self.bot.db_info)
        members = client.select(
            view="members_print", rename_columns=True, **member_info
        )

        if members is not None and (
            members := utils.check_df(df=members, to_dict=True)
        ):
            newline = "\n"
            await ctx.channel.send(
                f"Matching user found:\n"
                f"```HTTP\n"
                f"{f'{newline}'.join([f'{key}: {value}' for key, value in members.items()])}"
                f"\n```"
            )
        else:
            await ctx.channel.send(
                f"Sorry {ctx.message.author.mention}, no matching user found."
            )

    @commands.command(
        name="update_member",
        brief="Update given member in the members.",
        description="Update given member in the members.",
        usage=(
            """<user> <properties to update>\n\n"""
            """User can be referred to by:\n"""
            """\t- @user\n"""
            """\t- nick="<nick on the DIscord server>"\n"""
            """\t- name="<full Discord name (with #0000)>"\n"""
            """\t- discord_id="<Discord ID>"\n\n"""
            """Properties to update can be:\n"""
            """\t- pb_guid="<PB_GUID>"\n"""
            """\t- nick="<nick on the DIscord server>" """
        ),
        pass_context=True,
    )
    @commands.has_guild_permissions(administrator=True)
    async def update_member(self, ctx: commands.Context, *, content: str):
        if not await self._is_support_register_channel(ctx):
            return

        member_info = _get_member_info_and_pb_guid_from_input_with_mention(
            ctx=ctx, content=content
        )

        if not member_info:
            return

        client = SQLClient(**self.bot.db_info)
        members = client.select(view="members_print", **member_info)

        if members is not None:
            member_info.setdefault("discord_id", members["discord_id"])
            try:
                update = client.update(table="members", **member_info)
            except IndexError:
                await ctx.channel.send(
                    f"Sorry {ctx.message.author.mention}, these details are write protected."
                )
                return
            if update:
                updated_member = client.select(
                    view="members_print", discord_id=member_info["discord_id"]
                )
                newline = "\n"
                lst = [f"{key}: {value}" for key, value in updated_member.items()]
                await ctx.channel.send(
                    "Updated successfully:\n"
                    f"```HTTP\n"
                    f"{f'{newline}'.join(lst)}"
                    f"\n```"
                )
            else:
                await ctx.channel.send(
                    f"Sorry {ctx.message.author.mention}, removing failed!"
                )
        else:
            await ctx.channel.send(
                f"Sorry {ctx.message.author.mention}, no such member registered."
            )

    @commands.command(
        name="members",
        brief="Get the members as CSV table.",
        description="Get the members as CSV file via direct message by the bot.",
        pass_context=True,
    )
    @commands.has_guild_permissions(administrator=True)
    async def registered_members(self, ctx: commands.Context):
        client = SQLClient(**self.bot.db_info)
        path = client.select(view="members_print", return_csv=True)
        await ctx.author.send(
            "Memberlist as CSV (can be opened with Excel or any text editor).",
            file=discord.File(path),
        )
        os.remove(path)

    @commands.command(
        name="message",
        brief="Send private message to @-mentioned user via bot.",
        description=(
            "Make the bot send a private message to the @-mentioned user with given content.\n"
            "Can be used to explain rejection of a register request."
        ),
        usage="""@user <message>""",
        pass_context=True,
    )
    @commands.has_guild_permissions(administrator=True)
    async def message_member(
        self, ctx: commands.Context, user: discord.Member, *, content: str
    ):
        await user.send(content)
        await ctx.channel.send(
            f"""{ctx.message.author.mention}, a private message was sent to {user.mention}."""
        )


async def _get_member_info_and_pb_guid_from_input_with_mention(
    ctx: commands.Context, content: str
) -> dict:
    """Get kwargs from user input with a mention."""
    member_info = {}

    if ctx.message.mentions and len(ctx.message.mentions) == 1:
        [member] = ctx.message.mentions
        member_info = await utils.get_kwargs(ctx=ctx, member=member)
    if "pb_guid" in content:
        pb_guid = await utils.get_kwargs(ctx=ctx, content=content, pb_guid=True)
    else:
        pb_guid = await utils.get_kwargs(ctx=ctx, content=content)

    return member_info | pb_guid


async def _get_member_info_and_pb_guid_from_input(
    ctx: commands.Context, member: discord.Member, content: str
) -> dict:
    """Get member info from member and message content."""
    kwargs = await utils.get_kwargs(ctx=ctx, member=member)
    kwargs_temp = await utils.get_kwargs(ctx=ctx, content=content, pb_guid=True)

    if kwargs is not None and kwargs_temp is not None:
        return kwargs | kwargs_temp
    elif kwargs_temp is not None:
        return kwargs_temp
    return {}


def _create_register_request_embed(
    ctx: commands.Context, pb_guid: str, timezone: pytz.timezone
) -> discord.Embed:
    """Create an embed for a register request."""
    register_message = discord.Embed(
        title="Register request",
        description="Please check if the PB GUID matches with the pb_plist screenshot.",
        timestamp=datetime.datetime.now(tz=timezone),
        color=0x727272,
    )
    register_message.add_field(
        name="User", value=ctx.message.author.mention, inline=True
    )
    register_message.add_field(name="PB GUID", value=pb_guid, inline=True)
    register_message.add_field(
        name="Status", value="```brainfuck\nPENDING\n```", inline=True
    )
    register_message.set_thumbnail(url=ctx.message.attachments[0].url)
    return register_message
