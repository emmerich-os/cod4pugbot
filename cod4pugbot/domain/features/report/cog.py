import discord.ext.commands

import cod4pugbot.domain.utils as utils
from . import commands
from cod4pugbot.cod4pugbot import COD4PUGBot


class ReportSystem(discord.ext.commands.Cog, name="Report system"):
    """Report system for members to report other members to the admin team.

    Listeners:
        on_raw_reaction_add(raw_reaction) : coroutine func
            On reaction add, modify embed.
        on_raw_reaction_remove(raw_reaction) : coroutine func
            On reaction remove, modify embed.

    Commands:
        report_member(ctx, user, *, content) : coroutine func
            Report a member. Send embed to admin channel.

    """

    def __init__(self, bot: COD4PUGBot):
        self.bot = bot
        self.report_admin_channel = self.bot.get_channel(
            self.bot.channel_ids["admin_section"]["report"]
        )

    async def _is_report_support_channel(
        self, ctx: discord.ext.commands.context
    ) -> bool:
        """Check if channel is the support channel."""
        await self._is_report_channel(ctx, category="support")

    async def _is_report_admin_channel(self, ctx: discord.ext.commands.context) -> bool:
        """Check if channel is the administration channel."""
        await self._is_report_channel(ctx, category="admin_section")

    async def _is_report_channel(
        self, ctx: discord.ext.commands.Context, category: str
    ) -> bool:
        return await utils.is_channel(
            bot=self.bot,
            ctx=ctx,
            channel="report",
            category=category,
            send_message=True,
        )

    @discord.ext.commands.command(
        name="report",
        brief="Report a player to the admin team.",
        description=(
            "Report a player (e.g. for cheating or abusive behavior"
            "to the admin team. "
            "You need to @-mention the player, give a reason "
            "and provide evidence for your accusations (as attachments)."
        ),
        usage="""@user reason="<explanation>" (+ attachments)""",
        pass_context=True,
    )
    async def report_member(
        self, ctx: discord.ext.commands.Context, member: discord.Member, *, reason: str
    ):
        if (
            not await self._is_report_support_channel(ctx)
            or not await utils.message_has_mentions(ctx.message)
            or not await utils.message_has_attachments(ctx.message)
        ):
            return

        command = commands.ReportMember(
            channel=ctx.channel,
            message=ctx.message,
            member=member,
            reported_by=ctx.message.author,
            reason=reason,
            report_admin_channel=self.report_admin_channel,
        )
        await self.bot.handle_command_without_db_access(command)

    @discord.ext.commands.Cog.listener()
    @discord.ext.commands.has_guild_permissions(administrator=True)
    async def on_raw_reaction_add(self, raw_reaction: discord.RawReactionActionEvent):
        """On reaction to register messages in admin register channel.

        Parameters:
             raw_reaction : discord.RawReactionActionEvent
                Reaction that was added.

        """
        user, reaction = await utils.raw_reaction_to_reaction(
            bot=self.bot, raw_reaction=raw_reaction
        )

        if not await utils.is_channel(
            bot=self.bot,
            channel_id=reaction.message.channel.id,
            channel="report",
            category="admin_section",
        ):
            return

        correct_reaction, remove_reaction = _check_for_correct_user_and_reaction(
            user=user,
            reaction=reaction,
            bot=self.bot,
            was_added=True,
        )

        if remove_reaction:
            await reaction.remove(user)
        if not correct_reaction:
            return

        embed = _change_report_embed_status(
            reaction.message.embeds[0], set_reviewed=True
        )
        await reaction.message.edit(embed=embed)

    @discord.ext.commands.Cog.listener()
    @discord.ext.commands.has_guild_permissions(administrator=True)
    async def on_raw_reaction_remove(
        self, raw_reaction: discord.RawReactionActionEvent
    ):
        """On reaction to register messages in admin register channel.

        Parameters:
             raw_reaction : discord.RawReactionActionEvent
                Reaction that was removed.

        """
        user, reaction = await utils.raw_reaction_to_reaction(
            bot=self.bot, raw_reaction=raw_reaction
        )

        if not await utils.is_channel(
            bot=self.bot,
            channel_id=reaction.message.channel.id,
            channel="report",
            category="admin_section",
        ):
            return

        correct_reaction, _ = _check_for_correct_user_and_reaction(
            user=user,
            reaction=reaction,
            bot=self.bot,
            was_removed=True,
        )

        if not correct_reaction:
            return

        embed = _change_report_embed_status(
            reaction.message.embeds[0], set_pending=True
        )
        await reaction.message.edit(embed=embed)


def _change_report_embed_status(
    embed: discord.Embed, set_reviewed: bool = False, set_pending: bool = False
) -> discord.Embed:
    """Change report embed status (and color) to reviewed or not reviewed.

    Parameters:
        embed : discord.Embed
        set_reviewed : bool, optional
            If `True`, change embed's status to `REVIEWED`.
            Defaults to `False`.
        set_pending : bool, optional
            If `True`, change embed's status to `PENDING`.
            Defaults to `False`.

    Returns:
        discord.Embed

    """
    embed = embed.to_dict()
    if set_reviewed:
        for i, field in enumerate(embed["fields"]):
            if "status" in field["name"].lower():
                embed["fields"][i]["value"] = "```css\nREVIEWED\n```"
                embed["color"] = 0x00FF00
                break
    elif set_pending:
        for i, field in enumerate(embed["fields"]):
            if "status" in field["name"].lower():
                embed["fields"][i]["value"] = "```brainfuck\nPENDING\n```"
                embed["color"] = 0x727272
                break
    return discord.Embed.from_dict(embed)


def _check_for_correct_user_and_reaction(
    user: discord.Member,
    reaction: discord.Reaction,
    bot: COD4PUGBot,
    was_added: bool = False,
    was_removed: bool = False,
) -> tuple[bool, bool]:
    """Check if the reaction is correct.

    Checks:
        * if reaction performed by bot
        * if author of the message the reaction has been added to is the bot
        * if the message is in the correct channel (i.e. report channel of admin section)
        * if the message has embeds
        * if the reaction count is correct
        * if the reaction emoji is correct

    Parameters:
        user : discord.Member
            Member who added the reaction.
        reaction : discord.Reaction
        bot : discord.ext.commands.Bot
            The COD4PUGBot.
        was_added : bool
            `True` if the reaction was added, `False` otherwise.
            Defaults to `False`.
        was_removed : bool, optional
            `True` if the reaction was removed, `False` otherwise.
            Defaults to `False`.

    Returns:
        bool
            `True` if the reaction was correct, `False` otherwise.
        bool
            `True` if the reaction should be removed, `False` otherwise.

    """
    if was_added:
        if (
            user == bot.user
            or reaction.message.author != bot.user
            or not reaction.message.embeds
        ):
            return False, False
        elif reaction.emoji != "\u2705" or reaction.count > 2:
            return False, True
        return True, False
    elif was_removed:
        if (
            user == bot.user
            or reaction.message.author != bot.user
            or not reaction.message.embeds
            or reaction.emoji != "\u2705"
        ):
            return False, False
        return True, False
