import dataclasses

import discord

from cod4pugbot.domain.commands import Command


@dataclasses.dataclass(frozen=True)
class ReportMember(Command):
    """Report a member."""

    channel: discord.TextChannel
    message: discord.Message
    member: discord.Member
    reported_by: discord.Member
    reason: str
    report_admin_channel: discord.TextChannel
