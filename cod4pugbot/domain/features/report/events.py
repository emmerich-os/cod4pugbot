import dataclasses

import discord

from cod4pugbot.domain.events import Event


@dataclasses.dataclass(frozen=True)
class ReportSuccessful(Event):
    """A member has been successfully reported."""

    message: discord.Message
