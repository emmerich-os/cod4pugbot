import discord

from . import commands
from . import events
from cod4pugbot.domain import utils
from cod4pugbot.service_layer import unit_of_work

REPORT_EMBED_COLOR = 0x727272


async def report_member(
    command: commands.ReportMember, uow: unit_of_work.UnitOfWork
) -> None:
    """Report a member."""
    embed = _create_report_embed(
        reported_member=command.member,
        reported_by=command.reported_by,
        reason=command.reason,
        attachments=command.message.attachments,
    )

    message = await command.channel.send(embed=embed)
    await message.add_reaction(emoji="\u2705")

    event = events.ReportSuccessful(
        message=command.message,
    )
    uow.add_new_event(event)
    utils.add_command_successful_event_to_uow(
        uow,
        channel=command.channel,
        member=command.reported_by,
        message="your report has been forwarded to the admins.",
    )


def _create_report_embed(
    reported_member: discord.Member,
    reported_by: discord.Member,
    reason: str,
    attachments: list,
) -> discord.Embed:
    """Create a `discord.Embed` containing the main report information.

    Parameters:
        reported_member : discord.Member
            The member that has been reported
        reported_by : discord.Member
            The member that submitted the report.
        reason : str
            Reason why the member has been reported.
        attachments: list, optional
            URLs of files to send with the embed.

    Returns:
        discord.Embed

    """
    newline = "\n"
    attachments_urls = [attachment.url for attachment in attachments]

    embed = discord.Embed(
        title="MEMBER REPORTED",
        color=REPORT_EMBED_COLOR,
    )
    embed.add_field(
        name="Reported member",
        value=f"{reported_member.mention}",
        inline=True,
    )
    embed.add_field(name="Reported by", value=f"{reported_by.mention}", inline=True)
    embed.add_field(
        name="Reason", value=f"```{newline}{reason}{newline}```", inline=False
    )
    embed.set_thumbnail(url=attachments_urls[0])
    if len(attachments_urls) > 1:
        embed.add_field(
            name="Further attachments",
            value=f"{f'{newline}'.join(attachments_urls[1:])}",
            inline=False,
        )
    embed.add_field(name="Status", value="```brainfuck\nPENDING\n```", inline=False)

    return embed


async def delete_message(event: events.ReportSuccessful) -> None:
    """Delete the message with the submitted report information."""
    await event.message.delete()
