import functools
from typing import Union

import discord.ext.commands

from . import commands
from cod4pugbot.cod4pugbot import COD4PUGBot

Member = Union[discord.User, discord.Member]
Properties = dict[str, Union[int, str]]


RELEVANT_USER_PROPERTIES = ["name", "discriminator"]
RELEVANT_MEMBER_PROPERTIES = ["name", "nick", "discriminator"]


class Updater(discord.ext.commands.Cog):
    """Actions when a user updates his information or gets (un-)banned manually.

    Listeners:
        on_member_update(before, after, user=None) : coroutine func
            On member update, change information in data base.
        on_user_update(before, after) : coroutine func
            On user update, change information in data base.

    """

    def __init__(self, bot: COD4PUGBot):
        """Initialize the cog."""
        self.bot = bot

    @discord.ext.commands.Cog.listener()
    async def on_member_update(self, before: Member, after: Member) -> None:
        """Update information in members table whenever a member is updated.

        Parameters:
            before : discord.Member or discord.User
                User information before updating.
            after : discord.Member or discord.User
                User information after updating.

        """
        if not type(before) is type(after):
            raise ValueError(f"Types of before and after different: {before}, {after}")
        if not _details_have_changed(before, after):
            return

        details = _get_changed_details(before, after)

        command = commands.UpdateMember(
            member=after,
            details_to_update=details,
        )
        await self.bot.handle_command_with_db_access(command)
        return

    @discord.ext.commands.Cog.listener()
    async def on_user_update(self, before: discord.User, after: discord.User) -> None:
        """Update member in members table whenever a user is updated.

        Parameters:
            before : discord.Member
                User information before updating.
            after : discord.Member
                User information after updating.

        """
        await self.on_member_update(before=before, after=after)


@functools.singledispatch
def _details_have_changed(before: discord.User, after: discord.User) -> bool:
    properties = RELEVANT_USER_PROPERTIES
    return _any_property_has_changed(before, after, properties=properties)


@_details_have_changed.register
def _member_details_have_changed(before: discord.Member, after: discord.Member) -> bool:
    properties = RELEVANT_MEMBER_PROPERTIES
    return _any_property_has_changed(before, after, properties=properties)


def _any_property_has_changed(
    before: Member, after: Member, properties: Properties
) -> bool:
    if any(
        getattr(before, attribute) != getattr(after, attribute)
        for attribute in properties
    ):
        return True
    return False


@functools.singledispatch
def _get_changed_details(
    before: discord.User,
    after: discord.User,
) -> Properties:
    properties = RELEVANT_USER_PROPERTIES
    return _get_difference(before, after, attributes=properties)


@_get_changed_details.register
def _get_changed_member_details(
    before: discord.Member,
    after: discord.Member,
) -> Properties:
    properties = RELEVANT_MEMBER_PROPERTIES
    return _get_difference(before, after, attributes=properties)


def _get_difference(
    before: Member,
    after: Member,
    attributes: list[str],
) -> Properties:
    return {
        attribute: getattr(after, attribute)
        for attribute in attributes
        if getattr(before, attribute) != getattr(after, attribute)
    }
