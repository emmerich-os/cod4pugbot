import dataclasses
from typing import Any
from typing import Union

import discord

from cod4pugbot.domain.commands import Command


@dataclasses.dataclass(frozen=True)
class UpdateMember(Command):
    """Update member information in the database."""

    member: Union[discord.User, discord.Member]
    details_to_update: dict[str, Any]
