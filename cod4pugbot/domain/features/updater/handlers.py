import copy
from typing import Any

from . import commands
from cod4pugbot.domain import model
from cod4pugbot.service_layer import unit_of_work


def update_member_details(
    command: commands.UpdateMember, uow: unit_of_work.DatabaseUnitOfWork
) -> None:
    """Update member information in the database."""
    with uow:
        member = uow.members.get({"discord_id": command.member.id})
        updated_member = _create_updated_member(
            member, details=command.details_to_update
        )
        uow.members.update(updated_member)


def _create_updated_member(
    member: model.Member, details: dict[str, Any]
) -> model.Member:
    updated_member = copy.deepcopy(member)
    for key, value in details.items():
        setattr(updated_member, key, value)
    return updated_member
