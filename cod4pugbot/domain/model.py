import abc
import dataclasses
import datetime
from typing import Any
from typing import Optional
from typing import Union


@dataclasses.dataclass(eq=False)
class AbstractModel(abc.ABC):
    @property
    @abc.abstractmethod
    def _key(self) -> Union[Any, tuple[Any]]:
        """Return a ``tuple`` of attributes.

        These are used in the ``__eq__`` and ``__hash__`` methods.

        """
        return NotImplementedError

    @property
    def identifier(self) -> dict[str, Any]:
        """Identifier representing one or multiple database columns."""
        return {
            key: value
            for key, value in self.to_dict().items()
            if key in ALLOWED_IDENTIFIER_COLUMNS
        }

    def __eq__(self, other) -> bool:
        """Compare ``_key``s."""
        if isinstance(other, self.__class__):
            return self._key == other._key
        return NotImplemented

    def __hash__(self) -> int:
        """Create hash from ``_key``."""
        return hash(self._key)

    def to_dict(self, rename_keys: bool = False) -> dict[str, Any]:
        """Convert to dictionary.

        Parameters:
            rename_keys : bool, default False
                Whether to rename the keys of the data.
                Can be used for printing to a user.

        """
        instance_to_dict = dataclasses.asdict(self)
        if not rename_keys:
            return instance_to_dict
        return {
            (COLUMN_NAMES_MAP[key] if key in COLUMN_NAMES_MAP else key): value
            for key, value in instance_to_dict.items()
        }

    def to_markdown(self, rename_keys: bool = False) -> str:
        """Create multi-line markdown string.

        Parameters:
            rename_keys : bool, default False
                Whether to rename the keys of the data.
                Can be used for printing to a user.

        """
        newline = "\n"
        attributes = [
            f"{key}: {value}"
            for key, value in self.to_dict(rename_keys=rename_keys).items()
        ]
        return f"```HTTP\n" f"{f'{newline}'.join(attributes)}" f"\n```"


@dataclasses.dataclass(eq=False)
class Member(AbstractModel):
    discord_id: int
    name: str
    discriminator: int
    nick: str
    pb_guid: str
    active: bool
    created_at: datetime.datetime

    @property
    def _key(self) -> int:
        """Return discord ID as key."""
        return self.discord_id


@dataclasses.dataclass(eq=False)
class Stats(AbstractModel):
    discord_id: int
    elo: int
    pugs_played: int
    wins: int
    draws: int
    losses: int
    maps_played: int
    map_wins: int
    map_draws: int
    map_losses: int
    rounds_played: int
    round_wins: int
    round_losses: int
    score: int
    kills: int
    assists: int
    deaths: int
    impact_kills: int
    opening_kills: int
    opening_deaths: int
    opening_kills_converted: int
    opening_deaths_converted: int
    one_kills: int
    two_kills: int
    three_kills: int
    four_kills: int
    five_kills: int
    rounds_converted: int
    headshot_kills: int
    rifle_kills: int
    smg_kills: int
    sniper_kills: int
    pistol_kills: int
    shotgun_kills: int
    nade_kills: int
    knife_kills: int
    car_kills: int
    other: int
    suicides: int
    team_kills: int
    bomb_plants: int
    bomb_defuses: int

    @property
    def _key(self) -> int:
        """Return discord ID as key."""
        return self.discord_id


@dataclasses.dataclass(eq=False)
class PUGInfo(AbstractModel):
    id: int
    mode: str
    size: str
    maps: list[str]
    attack: list[int]
    defence: list[int]
    attack_score: int
    defence_score: int

    @property
    def _key(self) -> int:
        """Return discord ID as key."""
        return self.id


@dataclasses.dataclass(eq=False)
class Ban(AbstractModel):
    discord_id: int
    global_ban: bool
    pug_ban: bool
    reason: str
    duration: str
    created_at: datetime.datetime
    expires_at: Optional[datetime.datetime]

    @property
    def _key(self) -> int:
        """Return discord ID as key."""
        return self.discord_id


COLUMN_NAMES_MAP = {
    "discord_id": "Discord ID",
    "name": "Name",
    "nick": "Nick",
    "pb_guid": "PB GUID",
    "active": "Active",
    "mode": "Mode",
    "size": "Size",
    "maps": "Maps",
    "attack": "Attack",
    "defence": "Defence",
    "elo": "Elo",
    "rating": "Rating",
    "pugs_player": "PUGs played",
    "wins": "Wins",
    "draws": "Draws",
    "losses": "Losses",
    "win_rate": "Win rate",
    "maps_played": "Maps played",
    "map_wins": "Map wins",
    "map_draws": "Map draws",
    "map_losses": "Map losses",
    "map_win_rate": "Map win rate",
    "score_per_map": "Score per map",
    "kills_per_map": "Kills per map",
    "assists_per_map": "Assists per map",
    "deaths_per_map": "Deaths per map",
    "rounds_played": "Rounds played",
    "rounds_wins": "Round wins",
    "rounds_losses": "Round losses",
    "round_win_rate": "Round win rate",
    "score_per_round": "Score per round",
    "kills_per_round": "Kills per round",
    "assists_per_round": "Assists per round",
    "deaths_per_round": "Deaths per round",
    "score": "Score",
    "kills": "Kills",
    "assists": "Assists",
    "deaths": "Deaths",
    "kd_ratio": "K/D",
    "kd_difference": "K-D",
    "impact_kills": "Impact kills",
    "opening_kills": "Opening kills",
    "opening_deaths": "Opening deaths",
    "open_kills_converted": "Opening kills converted",
    "open_deaths_converted": "Opening deaths converted",
    "one_kills": "1K",
    "two_kills": "2K",
    "three_kills": "3K",
    "four_kills": "4K",
    "five_kills": "5K",
    "rounds_converted": "Rounds converted",
    "headshot_kills": "Headshot kills",
    "rifle_kills": "Rifle kills",
    "smg_kills": "SMG kills",
    "sniper_kills": "Sniper kills",
    "pistol_kills": "Pistol kills",
    "shotgun_kills": "Shotgun kills",
    "nade_kills": "Nade kills",
    "knife_kills": "Knife kills",
    "car_kills": "Car kills",
    "other": "Other",
    "suicides": "Suicides",
    "team_kills": "Team kills",
    "bomb_plants": "Bomb plants",
    "bomb_defuses": "Bomb defuses",
}

ALLOWED_IDENTIFIER_COLUMNS = [
    "id",
    "discord_id",
    "name",
    "discriminator",
]
