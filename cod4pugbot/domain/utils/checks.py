import discord
from discord.ext import commands


async def message_has_attachments(
    message: discord.Message, pb_guid: bool = False
) -> bool:
    """Check is command contains an attachment.

    Parameters:
        message : discord.Message
        pb_guid : bool, optional
            If `True`, message belongs to a register request.
            Defaults to `False`.

    """
    if not message.attachments:
        if pb_guid:
            await message.channel.send(
                f"Sorry {message.author.mention}, "
                f"make sure you attach a screenshot of your PB_GUID "
                f"(only exactly 1 attachment allowed!). "
                f"In order to do so, connect to an orginal 1.7 server "
                f"and type /pb_plist into the console. "
                f"Then screenshot the output of the console and attach "
                f"the image to your message."
            )
        else:
            await message.channel.send(
                f"Sorry {message.author.name}, "
                "you need to give screenshot(s) as attachment(s) to "
                "support your accusations."
            )
        return False
    elif pb_guid and len(message.attachments) != 1:
        await message.channel.send(
            f"Sorry {message.author.name}, you can only add 1 attachment."
        )
        return False
    return True


async def is_channel(
    bot: commands.Bot,
    channel: str,
    ctx: commands.Context = None,
    channel_id: int = None,
    category: str = None,
    send_message: bool = False,
) -> bool:
    """Check if command is executed in the desired channel.

    Parameters:
        bot : commands.Bot
            Discord bot.
        channel : str
            Channel name.
        ctx : commands.Context, optional
            Context of the message.
            Defaults to `None`.
        channel_id : int, optional
            Channel ID.
            Defaults to `None`.
        category : str, optional
            Category the channel belongs to.
            Defaults to `None`.
        send_message : bool, optional
            If `True`, send message to `ctx.message.channel`.
            Defaults to `False`.

    """
    if ctx is not None and channel_id is None:
        channel_id = ctx.message.channel.id

    if category is None and bot.channel_ids[channel] == channel_id:
        return True
    elif category is not None and bot.channel_ids[category][channel] == channel_id:
        return True
    if ctx is not None and send_message:
        await ctx.message.channel.send(
            f"Sorry {ctx.message.author.mention}, this command "
            f"is only available in the respective channel."
        )
    return False


async def message_has_mentions(message: discord.Message, count: int = 1) -> bool:
    """Check if message contains a mention."""
    if message.mentions and len(message.mentions) == count:
        return True
    await message.channel.send(
        f"Sorry, {message.author.mention}, "
        "you need to @-mention a player (exactly 1 player)."
    )
    return False
