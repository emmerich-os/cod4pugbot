from typing import Union

import discord

from cod4pugbot.domain import events
from cod4pugbot.service_layer import unit_of_work


def add_command_successful_event_to_uow(
    uow: unit_of_work.AbstractUnitOfWork,
    channel: discord.TextChannel,
    member: discord.Member,
    message: str,
) -> None:
    """Add an event to the UnitOfWork indicating a command was successful."""
    _add_event_to_uow(
        uow=uow,
        event=events.CommandSuccessful,
        channel=channel,
        member=member,
        message=message,
    )


def add_command_failed_event_to_uow(
    uow: unit_of_work.AbstractUnitOfWork,
    channel: discord.TextChannel,
    member: discord.Member,
    message: str,
) -> None:
    """Add an event to the UnitOfWork indicating a command has failed."""
    _add_event_to_uow(
        uow=uow,
        event=events.CommandFailed,
        channel=channel,
        member=member,
        message=message,
    )


def _add_event_to_uow(
    uow: unit_of_work.AbstractUnitOfWork,
    event: Union[type[events.CommandSuccessful], type[events.CommandFailed]],
    channel: discord.TextChannel,
    member: discord.Member,
    message: str,
) -> None:
    event_to_add = event(
        channel=channel,
        member=member,
        message=message,
    )
    uow.add_new_event(event_to_add)
