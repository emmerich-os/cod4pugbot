import logging

import discord

logger = logging.getLogger(__name__)


async def clear_channel_history(channel: discord.TextChannel) -> None:
    """Clear all messages in a channel's history."""
    async for message in channel.history():
        await message.delete()


async def add_emojis_as_reactions_to_message(
    message: discord.Message, *emojis: str
) -> None:
    """Add given emojis as reactions to a message"""
    for emoji in emojis:
        await message.add_reaction(emoji=emoji)


async def remove_emoji_reactions_from_message(
    message: discord.Message, emoji: str, *members: discord.Member
) -> None:
    """Add given emojis as reactions to a message"""
    for members in members:
        try:
            await message.remove_reaction(emoji=emoji, member=members)
        except Exception:
            logger.error("Removing of reaction failed", exc_info=True)
