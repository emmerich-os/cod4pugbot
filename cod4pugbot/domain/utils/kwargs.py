import json
import logging
import re
from typing import Optional

import discord
from discord.ext import commands


logger = logging.getLogger(__name__)


async def _get_pb_guid(
    ctx: commands.Context = None, content: str = None
) -> Optional[str]:
    """Get and check PB GUID.

    Parameters:
        ctx : commands.Context, optional
            Context of the message.
            Defaults to `None`.
        content : str, optional
            PB GUID.
            Defaults to `None`.

    Returns:
        pb_guid : str
            PB GUID.

    """
    if content is None:
        raise ValueError("No PB GUID given")

    if "pb_guid" in content.lower() and (
        search := re.findall(r"pb_guid.*?=.*?([A-z0-9]{8})", content)
    ):
        [pb_guid] = search
    elif search := re.findall(r"[A-z0-9]{8}", content):
        [pb_guid] = search
    else:
        if ctx is not None:
            await ctx.channel.send(
                f"Sorry {ctx.message.author.mention}, the PB GUID "
                "needs to have 8 characters consisting "
                "of A-Z, a-z and/or 0-9."
            )
        else:
            raise ValueError(
                "PB GUID needs to have 8 characters consisting of A-Z, a-z and/or 0-9."
            )
        return None

    return pb_guid


async def _get_discord_id(ctx: commands.Context, content: str) -> Optional[int]:
    """Get Discord ID.

    Parameters:
        ctx : commands.Context
            Context.
        content : str
            Content.

    Returns:
        int
            Discord ID.

    """
    if search := re.findall(r"[0-9]{18}", content):
        [discord_id] = search
        return int(discord_id)
    else:
        if ctx is not None:
            await ctx.channel.send(
                f"Sorry {ctx.message.author.mention}, the Discord ID "
                "needs to have 18 digits."
            )
        else:
            raise ValueError("Discord ID needs to have 18 digits.")
        return None


def _get_user_info(user) -> Optional[dict]:
    """Get user info.

    Parameters:
        user
            `discord.User` or `discord.Member`.

    Return:
        dict
            User information:
                * nick (if exists)
                * name
                * Discord ID

    """
    if not isinstance(user, (discord.User, discord.Member)):
        logger.debug("Not a discord.User %s (%s)", user, type(user))
        return None

    if hasattr(user, "nick"):
        info = {"nick": user.nick or user.name}
    else:
        info = {"nick": user.name}

    info = {
        **info,
        "name": f"{user.name}#{user.discriminator}",
        "discord_id": user.id,
    }

    return info


async def _get_kwargs_input(ctx: commands.Context, content: str) -> Optional[dict]:
    """Get keyword arguments from user input.

    Parameters:
        ctx : commands.Context
            Context.
        content : str
            User input.
            Must contain keyword arguments as `key=argument`, `key="argument"` etc.

    Return:
        dict
            Contains all keywords and their arguments.

    """
    pattern = re.compile(
        r"""(?:(?:(\w+)[\s]*=[\s]*[\"\']([\w\s]*)[\"\'])|(?:(\w+)[\s]*=[\s]*[\"\']?(\w+)))"""
    )
    kwarg_list = pattern.findall(content)
    if kwarg_list:
        return dict(filter(None, kwarg) for kwarg in kwarg_list)
    elif ctx is not None:
        await ctx.channel.send(
            f"Sorry {ctx.message.author.mention}, "
            "you need to give keyword arguments "
            """(e.g. nick="<nick>", name="<Discord name>"""
            """discord_id="<Discord ID>" and/or pb_guid="<PB GUID>")."""
        )
    else:
        raise ValueError("kwargs required")
    return None


async def get_kwargs(
    ctx: commands.Context = None,
    content: str = None,
    member: discord.Member = None,
    user: discord.User = None,
    author: bool = False,
    pb_guid: bool = False,
    discord_id: bool = False,
) -> dict:
    """Get kwargs from content of command.

    Parameters:
        ctx : commands.Context, optional
            Context of the message.
            Defaults to `None`.
        content : str, optional
            Content of the message.
            Defaults to `None`.
        member : discord.Member, optional
            Member whose information to get.
            Defaults to `None`.
        user : discord.User, optional
            User whose information to get.
            Defaults to `None`.
        author : bool, optional
            If `True`, get kwargs for message author.
            Defaults to `False`.
        pb_guid : bool, optional
            If `True`, PB GUID is mandatory.
            Defaults to `False`.
        discord_id : bool, optional
            If `True`, Discord ID is mandatory.
            Defaults to `False`.

    Returns:
        kwargs : dict
            Keyword arguments retrieved.

    """
    logger.debug("Now in cogs.utils.kwargs.get_kwargs")

    kwargs: dict = {}

    if user is not None:
        kwargs = _get_user_info(user)
    elif member is not None:
        kwargs = _get_user_info(member)
    elif author and ctx is not None:
        kwargs = _get_user_info(ctx.message.author)

    if pb_guid and content is not None:
        pb_guid_input = await _get_pb_guid(ctx=ctx, content=content)
        if pb_guid_input is not None:
            kwargs.setdefault("pb_guid", pb_guid_input)

    if discord_id and content is not None:
        discord_id_input = await _get_discord_id(ctx=ctx, content=content)
        if discord_id_input is not None:
            kwargs.setdefault("discord_id", discord_id_input)

    if content is not None and "=" in content:
        kwargs_input = await _get_kwargs_input(ctx=ctx, content=content)
        if kwargs_input is not None:
            if discord_id_arg := kwargs_input.pop("discord_id", False):
                kwargs["discord_id"] = int(discord_id_arg)
            if pb_guid_arg := kwargs_input.pop("pb_guid", False):
                kwargs["pb_guid"] = await _get_pb_guid(ctx=ctx, content=pb_guid_arg)
            kwargs.update(kwargs_input)
    elif content is not None and len(content.split()) != 1 and ctx is not None:
        await ctx.message.channel.send(
            f"Sorry {ctx.message.author.mention}, you need to give "
            f"keyword arguments "
            """(e.g. nick="<nick>", name="<Discord name>"""
            """discord_id="<Discord ID>" and/or pb_guid="<PB GUID>") """
            f"or only one argument."
        )

    if not kwargs:
        logger.debug("returning with None")
        return None

    logger.debug("%s", json.dumps(kwargs, indent=4, default=str))
    logger.debug("Returning from cogs.utils.kwargs.get_kwargs")
    return kwargs
