import logging

import discord
from discord.ext import commands

logger = logging.getLogger(__name__)


async def raw_reaction_to_reaction(
    bot: commands.Bot, raw_reaction: discord.RawReactionActionEvent
) -> tuple[discord.Member, discord.Reaction]:
    """On reaction to register messages in admin register channel.

    Parameters:
        bot : discord.Bot
            Discord bot instance.
        raw_reaction : discord.RawReactionActionEvent
            Reaction that was added/removed.

    Returns:
        member : discord.Member
            Member who added/removed the reaction.
        reaction : discord.Reaction
            Reaction which was added/removed.
            Has the following attributes:
                - message : discord.Message
                    Message to which the reaction was added/removed.
                - emoji : str
                    Emoji which was added/removed.
                - count : int
                    Count of that specific reaction.
                - me : bool
                    `True` if user is bot, `False` otherwise.

    """
    logger.debug("Converting raw reaction %s", raw_reaction)

    channel = bot.get_channel(raw_reaction.channel_id)
    message = await channel.fetch_message(raw_reaction.message_id)
    guild = channel.guild
    member = await guild.fetch_member(raw_reaction.user_id)

    emoji = raw_reaction.emoji
    count = None

    for i, reaction in enumerate(message.reactions):
        if reaction.emoji == raw_reaction.emoji.name:
            count = reaction.count
            emoji = reaction.emoji

    if count is None and raw_reaction.emoji.id is not None:
        count = 1
        emoji = await guild.fetch_emoji(raw_reaction.emoji.id)

    reaction = discord.Reaction(
        message=message,
        emoji=emoji,
        data={"count": count, "me": True if member == bot.user else False},
    )

    logger.debug("Converted to member %s and reaction %s", member, reaction)
    return member, reaction
