import logging
import os


def setup_logger(
    logging_level: int = logging.DEBUG,
    logfile: str = "./.cache/cod4pugbot.log",
):
    """Set up logging of errors to errors.log.

    Sets up a level `trace` to print messages to log.

    Parameters:
        logfile : str, optional
            File with the log output.
            Defaults to `"./utils/logging/logs/output.log`.

    """
    strfmt = (
        "%(levelname)s | "
        "%(asctime)s.%(msecs)03d - "
        "%(name)s.%(filename)s:%(lineno)s:%(funcName)s - "
        "%(message)s"
    )
    datefmt = "%Y-%m-%dT%H:%M:%S"
    logging.basicConfig(
        level=logging_level,
        format=strfmt,
        datefmt=datefmt,
    )
    logger = logging.getLogger()

    path = "/".join(logfile.split("/")[:-1])
    if not os.path.exists(path):
        os.makedirs(path)

    fh = logging.FileHandler(filename=logfile, encoding="utf-8", mode="w+")
    fh.setLevel(logging_level)
    fh.setFormatter(logging.Formatter(fmt=strfmt, datefmt=datefmt))
    logger.addHandler(fh)

    return logger
