from typing import Optional
from typing import Union

import discord

from cod4pugbot.domain import commands
from cod4pugbot.domain import events
from cod4pugbot.domain.features.administration import handlers as administration
from cod4pugbot.domain.features.contact_admins import handlers as contact_admins
from cod4pugbot.domain.features.report import handlers as report
from cod4pugbot.domain.features.updater import handlers as updater


async def respond_to_channel_for_invoked_command(
    event: Union[events.CommandSuccessful, events.CommandFailed],
) -> None:
    """Respond to a channel where a command that was successfully executed was invoked."""
    form_of_address = (
        f"Sorry {event.member.mention}"
        if isinstance(event, events.CommandFailed)
        else f"{event.member.mention}"
    )
    message = f"{form_of_address}, {event.message}"

    if event.embed is not None:
        await _send_message_to_channel(
            channel=event.channel,
            message=message,
            embed=event.embed,
            file=event.file,
            files=event.files,
        )
        return

    if isinstance(event, events.CommandSuccessful):
        color = discord.Color.green()
    elif isinstance(event, events.CommandFailed):
        color = discord.Color.red()
    else:
        color = None

    await _send_message_to_channel(
        channel=event.channel,
        message=None,
        embed=discord.Embed(
            description=message,
            color=color,
        ),
        file=event.file,
        files=event.files,
    )
    return


async def send_message_to_member(
    event: Union[
        events.MemberKicked,
        events.MemberGloballyBanned,
        events.MemberManuallyGloballyBanned,
        events.MemberPUGBanned,
        events.BanUpdated,
        events.BanLifted,
        events.BanLiftedManually,
        events.ReportSuccessful,
    ],
) -> None:
    """Send a PM to a member."""
    pre_sentence = f"{event.message}: " if hasattr(event, "message") else ""
    await event.member.send(f"{pre_sentence}{event.reason}")


async def _send_message_to_channel(
    channel: discord.TextChannel,
    message: Optional[str],
    embed: discord.Embed,
    file: discord.File,
    files: list[discord.File],
) -> None:
    """Send a message to a channel."""
    await channel.send(
        message,
        embed=embed,
        file=file,
        files=files,
    )


COMMAND_HANDLERS = {
    # contact feature
    commands.ContactAdmins: contact_admins.send_embed_to_admins,
    # administration feature
    commands.KickMember: administration.kick_member_from_guild,
    commands.GloballyBanMember: administration.ban_member,
    commands.PUGBanMember: administration.ban_member,
    commands.PostBanInformation: administration.post_ban_information,
    commands.UpdateBan: administration.update_ban_information,
    commands.LiftBan: administration.lift_ban,
    commands.WarnMember: administration.warn_member,
    commands.ViewBans: administration.send_all_bans_as_csv,
    commands.AddGlobalBanToDatabase: administration.add_global_ban_to_database,
    commands.RemoveBanFromDatabase: administration.remove_ban_from_database,
    commands.LiftExpiredBans: administration.lift_expired_bans,
    # updater feature
    commands.UpdateMember: updater.update_member_details,
    # report feature
    commands.ReportMember: report.report_member,
}

EVENT_HANDLERS = {
    events.CommandSuccessful: [respond_to_channel_for_invoked_command],
    events.CommandFailed: [respond_to_channel_for_invoked_command],
    # administration feature
    events.MemberKicked: [
        administration.send_embed_to_bans_channel,
        send_message_to_member,
    ],
    events.MemberGloballyBanned: [
        administration.send_embed_to_bans_channel,
        send_message_to_member,
    ],
    events.MemberPUGBanned: [
        administration.send_embed_to_bans_channel,
        send_message_to_member,
    ],
    events.BanUpdated: [administration.update_ban_embed, send_message_to_member],
    events.BanLifted: [
        administration.change_ban_embed_to_lifted,
        send_message_to_member,
    ],
    events.MemberManuallyGloballyBanned: [
        administration.send_embed_to_bans_channel,
        send_message_to_member,
    ],
    events.BanLiftedManually: [
        administration.change_ban_embed_to_lifted,
        send_message_to_member,
    ],
    events.ReportSuccessful: [report.delete_message],
}
