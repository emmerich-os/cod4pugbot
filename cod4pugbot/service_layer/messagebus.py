import functools
import inspect
import logging
from typing import Any
from typing import Callable
from typing import Union

from .unit_of_work import AbstractUnitOfWork
from cod4pugbot.domain import commands
from cod4pugbot.domain import events

logger = logging.getLogger(__name__)


Message = Union[commands.Command, events.Event]
Messages = list[Message]
Handler = Callable
Handlers = list[Handler]
Command = type[commands.Command]
Event = type[events.Event]
CommandHandlers = dict[Command, Handler]
EventHandlers = dict[Event, Handlers]


class MissingHandlerException(Exception):
    """The given command does not have a handler."""


class MessageBus:
    """Processes commands and events and passes them to the respective handler(s).

    Parameters:
        command_handlers : dict
        event_handlers : dict

    Commands are emitted by the entrypoint(s) (i.e. Cogs). Events are the response of
    handlers.

    """

    def __init__(
        self,
        uow: AbstractUnitOfWork,
        command_handlers: CommandHandlers,
        event_handlers: EventHandlers,
    ):
        """Initialize messagebus with Unit of Work and Handlers."""
        self.uow = uow
        self.command_handlers = command_handlers
        self.event_handlers = event_handlers
        self.queue: Messages = []

    async def handle(self, message: Message) -> None:
        """Handle a message."""
        logger.debug("Handling message %s", message)
        self.queue = [message]
        while self.queue:
            message_to_handle = self.queue.pop(0)
            self._handle_message(message_to_handle)

    @functools.singledispatchmethod
    async def _handle_message(self, message: Any) -> None:
        """Handle a given message."""
        raise NotImplementedError("Message not an event or command: %s", message)

    @_handle_message.register
    async def _handle_command(self, message: commands.Command) -> None:
        """Handle command with respective handler.

        Commands are not allowed to fail. Hence, any exception is logged
        and raised

        """
        logger.debug("Handling command %s", message)
        handler = self._get_command_handler(message)
        await self._handle_command_with_handler(message, handler)

    @_handle_message.register
    async def _handle_event(self, message: events.Event) -> None:
        """Handle event with respective handlers.

        Events are allowed to fail. Hence, any exception is logged but does
        not abort the procedure.

        """
        handlers = self._get_event_handlers(message)
        await self._handle_event_with_handlers(message, handlers)

    def _get_command_handler(self, command: commands.Command) -> Handler:
        try:
            return self.command_handlers[type(command)]
        except KeyError:
            raise MissingHandlerException(
                "Handler not defined for command %s", type(command)
            )

    def _get_event_handlers(self, event: events.Event) -> Handlers:
        try:
            return self.event_handlers[type(event)]
        except KeyError:
            raise MissingHandlerException("No handler(s) defined for event %s", event)

    async def _handle_command_with_handler(
        self, command: commands.Command, handler: Handler
    ) -> None:
        try:
            await self._handle_and_collect_new_events(command, handler)
        except Exception:
            logger.error(
                "Exception raised handling command %s with handler %s", command, handler
            )
            raise

    async def _handle_event_with_handlers(
        self, event: events.Event, handlers: Handlers
    ) -> None:
        for handler in handlers:
            try:
                logger.debug("Handling event %s with handler %s", event, handler)
                await self._handle_and_collect_new_events(event, handler)
            except Exception:
                logger.error(
                    "Exception raised handling event %s with handler %s",
                    event,
                    handler,
                    exc_info=True,
                )
                continue

    async def _handle_and_collect_new_events(
        self, message: Message, handler: Handler
    ) -> None:
        """Collect all recently published events."""
        handler_ = self._inject_uow(handler)
        if inspect.iscoroutinefunction(handler_):
            await handler_(message)
        else:
            handler_(message)
        self.queue.extend(self.uow.collect_new_events())

    def _inject_uow(self, handler: Handler) -> functools.partial:
        handler_signature = inspect.signature(handler)
        return (
            functools.partial(handler, uow=self.uow)
            if "uow" in handler_signature.parameters
            else handler
        )
