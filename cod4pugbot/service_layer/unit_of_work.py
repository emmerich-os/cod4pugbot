import abc
from typing import Iterator

from cod4pugbot.db import Client
from cod4pugbot.db import repository
from cod4pugbot.domain import events


class AbstractUnitOfWork(abc.ABC):
    """Unit of Work pattern."""

    def __init__(self):
        """Initialze the uow."""
        self.events: list[events.Event] = []

    def __enter__(self) -> "AbstractUnitOfWork":
        """Return the uow."""
        return self

    def __exit__(self, *args):
        """Do nothing."""
        pass

    def add_new_event(self, event: events.Event) -> None:
        """Add new event to collected events."""
        self.events.append(event)

    def add_new_events(self, events_list: list[events.Event]) -> None:
        """Add a list of events to collected events."""
        for event in events_list:
            self.add_new_event(event)

    def collect_new_events(self) -> Iterator[events.Event]:
        """Collect and return all new events."""
        while self.events:
            yield self.events.pop(0)


class UnitOfWork(AbstractUnitOfWork):
    """A simple unit of work."""


class DatabaseUnitOfWork(AbstractUnitOfWork):
    """Unit of work for the the database.

    Parameters:
        client : cod4pugbot.db.Client
            The client for interaction with the database.
        called_by_bot : bool, default False
            Whether repository is used by the bot.

    """

    members = repository.AbstractRepository
    bans = repository.AbstractRepository
    pugs = repository.AbstractRepository
    stats = repository.AbstractRepository

    def __init__(self, client: Client, called_by_bot: bool = False):
        """Initialize the uow without connecting yet."""
        super().__init__()
        self.client = client
        self.called_by_bot = called_by_bot

    def __enter__(self):
        """Initialze each repository."""
        self.members = repository.MembersRepository(
            client=self.client, called_by_bot=self.called_by_bot
        )
        self.bans = repository.BansRepository(
            client=self.client, called_by_bot=self.called_by_bot
        )
        self.pugs = repository.PUGsRepository(
            client=self.client, called_by_bot=self.called_by_bot
        )
        self.stats = repository.StatsRepository(
            client=self.client, called_by_bot=self.called_by_bot
        )
        return super().__enter__()
