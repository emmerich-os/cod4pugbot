from typing import Union

from .discord import Channel
from .discord import Member
from .discord import Role
from .discord import TextMessage
from .fakes import FakeDatabaseUnitOfWork
from cod4pugbot.domain.events import Event
from cod4pugbot.service_layer.unit_of_work import AbstractUnitOfWork


def received_message(entity: Union[Member, Channel]) -> bool:
    messages_received = entity.messages_received
    return messages_received and len(messages_received) == 1


def message_contains_embed(message: TextMessage) -> bool:
    return hasattr(message.kwargs, "embed") and message.kwargs.embed


def member_has_role(member: Member, role: Role) -> bool:
    return member.roles and any(
        isinstance(member_role, role) for member_role in member.roles
    )


def uow_has_events(uow: AbstractUnitOfWork, event: Event) -> bool:
    return any(isinstance(uow_event, event) for uow_event in uow.events)


def database_uow_called(uow: FakeDatabaseUnitOfWork) -> bool:
    return uow.instantiated


def iterables_contain_same_elements_in_order(
    left: Union[list, tuple, set], right: Union[list, tuple, set]
) -> bool:
    return all(l == r for l, r in zip(left, right))
