import dataclasses
from typing import Union

import pytz
from discord.ext import commands

from . import fakes
from cod4pugbot.cod4pugbot import COD4PUGBot
from cod4pugbot.service_layer import handlers
from cod4pugbot.service_layer import messagebus


TIMEZONE = pytz.timezone("UTC")


@dataclasses.dataclass
class Attachment:
    url: str = None


@dataclasses.dataclass
class Role:
    name: str = None

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self.name == other.name
        return NotImplemented


@dataclasses.dataclass
class GlobalBanRole(Role):
    name: str = "global_banned"


@dataclasses.dataclass
class PUGBanRole(Role):
    name: str = "pug_banned"


BAN_ROLES = {
    "global_banned": GlobalBanRole(),
    "pug_banned": PUGBanRole(),
}


@dataclasses.dataclass
class Bot(COD4PUGBot):
    _id: int = 1337
    _name: str = "test_bot"
    command_prefix: str = "!"
    has_global_ban_role: bool = True
    timezone: pytz.timezone = TIMEZONE
    database_uow: fakes.FakeDatabaseUnitOfWork = fakes.FakeDatabaseUnitOfWork(
        sql_client=None, called_by_bot=True
    )
    simple_uow: fakes.FakeSimpleUnitOfWork = fakes.FakeSimpleUnitOfWork()

    @property
    def db_info(self):
        return {
            "user": None,
            "password": None,
            "host": None,
            "port": None,
            "database": None,
        }

    @property
    def user(self):
        return Member(id=self._id, name=self._name)

    @property
    def channel_ids(self):
        return {
            "support": {"bans": "bans"},
            "admin_section": {"administration": "administration"},
        }

    @property
    def role_ids(self):
        return {
            "global_banned": GlobalBanRole.name if self.has_global_ban_role else None,
            "pug_banned": PUGBanRole.name,
        }

    def __eq__(self, other):
        if isinstance(other, Member):
            return self.user.id == other.id
        return NotImplemented

    def get_channel(self, *args, **kwargs) -> "Channel":
        return Channel()

    def _instantiate_sql_message_bus(
        self, called_by_bot: bool = False
    ) -> messagebus.MessageBus:
        """Instantiate a message bus with an SQL unit of work.

        Parameters:
            called_by_bot : bool, default False
                Whether following commands are intended to be called with bot rights.
                E.g. only the bot is allowed to force delete members.

        """
        uow = self.database_uow
        return messagebus.MessageBus(
            uow=uow,
            command_handlers=handlers.COMMAND_HANDLERS,
            event_handlers=handlers.EVENT_HANDLERS,
        )

    def _instantiate_simple_message_bus(self) -> messagebus.MessageBus:
        """Instantiate a message bus."""
        uow = self.simple_uow
        return messagebus.MessageBus(
            uow=uow,
            command_handlers=handlers.COMMAND_HANDLERS,
            event_handlers=handlers.EVENT_HANDLERS,
        )


@dataclasses.dataclass
class Guild:
    members: list["Member"] = dataclasses.field(default_factory=list)
    unbanned: list["Member"] = dataclasses.field(default_factory=list)

    def get_role(self, role_id: str) -> Role:
        return BAN_ROLES[role_id]

    async def unban(self, member, reason):
        self.unbanned.append(member)
        return

    async def fetch_member(self, discord_id: int) -> "Member":
        [member] = [member for member in self.members if member.id == discord_id]
        return member


@dataclasses.dataclass
class GuildPermissions:
    administrator: bool = None


@dataclasses.dataclass
class Member:
    id: int = None
    name: str = None
    nick: str = None
    discriminator: str = None
    guild: Guild = None
    guild_permissions: GuildPermissions = None
    messages_received: list[list[Union[list, dict]]] = dataclasses.field(
        default_factory=list
    )
    kicked: bool = False
    globally_banned: bool = False
    pug_banned: bool = False
    roles: list[Role] = dataclasses.field(default_factory=list)

    def __hash__(self) -> int:
        return hash(self.id)

    @property
    def mention(self) -> str:
        return f"@{self.nick}" if self.nick is not None else f"@{self.name}"

    async def send(self, *args, **kwargs):
        self.messages_received.append(TextMessage(args=args, kwargs=Kwargs(**kwargs)))

    async def kick(self, *args, **kwargs):
        self.kicked = True

    async def ban(self, *args, **kwargs):
        self.globally_banned = True

    async def unban(self, *args, **kwargs):
        self.globally_banned = False

    async def add_roles(self, *roles, reason: str):
        for role in roles:
            self.roles.append(role)
            if isinstance(role, GlobalBanRole):
                self.globally_banned = True
            elif isinstance(role, PUGBanRole):
                self.pug_banned = True

    async def remove_roles(self, *roles, reason: str):
        for role in roles:
            if role not in self.roles:
                continue
            self.roles.remove(role)
            if isinstance(role, GlobalBanRole):
                self.globally_banned = False
            elif isinstance(role, PUGBanRole):
                self.pug_banned = False


@dataclasses.dataclass
class Channel:
    id: Union[int, str] = None
    messages_received: list[list[Union[list, dict]]] = dataclasses.field(
        default_factory=list
    )
    guild: Guild = Guild()

    @property
    def mention(self) -> str:
        return f"@{self.id}"

    async def send(self, *args, **kwargs):
        self.messages_received.append(TextMessage(args=args, kwargs=Kwargs(**kwargs)))


@dataclasses.dataclass
class Kwargs:
    def __init__(self, **kwargs):
        self.__dict__.update(kwargs)

    def __repr__(self):
        return (
            f"{self.__class__.__name__}("
            f"{' '.join([f'{key}={value}' for key, value in self.__dict__.items()])}"
        )


@dataclasses.dataclass
class TextMessage:
    args: list
    kwargs: Kwargs


@dataclasses.dataclass
class Message:
    author: Member = None
    channel: Channel = None
    embeds: list = None
    reactions: list = None
    mentions: list[Member] = None
    attachments: list[Attachment] = dataclasses.field(default_factory=list)


@dataclasses.dataclass
class User:
    id: int = None
    name: str = None
    discriminator: str = None


@dataclasses.dataclass
class Users:
    users: list[User] = dataclasses.field(default_factory=list)

    async def flatten(self):
        return self.users

    async def remove(self, user: User) -> None:
        self.users.remove(user)


@dataclasses.dataclass
class Reaction:
    emoji: str = None
    message: Message = None
    count: int = None
    _users: "Users" = Users()

    def users(self) -> Users:
        return self._users

    async def remove(self, user: User) -> None:
        await self._users.remove(user)


@dataclasses.dataclass
class Context:
    channel: Channel = None
    message: Message = None
    content: str = None
    guild: Guild = None


async def invoke_cog_command(
    cog: commands.Cog, function_name: str, *args, **kwargs
) -> None:
    """Invoke a command, i.e. call the function of a Cog with given arguments."""
    command = getattr(cog, function_name)
    await command.callback(cog, *args, **kwargs)
