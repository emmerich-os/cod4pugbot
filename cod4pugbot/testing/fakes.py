from cod4pugbot.db import repository
from cod4pugbot.domain import model
from cod4pugbot.service_layer import unit_of_work


class FakeSimpleUnitOfWork(unit_of_work.AbstractUnitOfWork):
    pass


class FakeRepository(repository.AbstractRepository):
    database_model = model.AbstractModel

    def __init__(self, contains: list[dict] = None):
        self.contains = contains or []
        self.added = []
        self.fetched = []
        self.updated = []
        self.deleted = []
        self.called_by_bot = False

    def _add(self, entity) -> None:
        pass

    def _add_and_return_row_id(self, entity):
        return entity.identifier

    def _get(self, identifier):
        try:
            [entry] = [
                entry
                for entry in self.contains
                if all(
                    key in entry and entry[key] == value
                    for key, value in identifier.items()
                )
            ]
        except ValueError:
            raise repository.NotFoundInDatabaseError(
                "No entry in table %s with identifier(s) %s",
                identifier,
            )
        else:
            self.fetched.append(entry)
            return entry

    def _get_all(self):
        return self.contains

    def _update(self, entity, called_by_bot: bool):
        pass

    def _delete(self, entity, force: bool):
        pass


class FakeMembersRepository(FakeRepository):
    database_model = model.Member


class FakeBansRepository(FakeRepository):
    database_model = model.Ban


class FakePUGsRepository(FakeRepository):
    database_model = model.PUGInfo


class FakeStatsRepository(FakeRepository):
    database_model = model.Stats


class FakeDatabaseUnitOfWork(unit_of_work.AbstractUnitOfWork):
    existing_members = None
    existing_bans = None
    existing_pugs = None
    existing_stats = None
    instantiated: bool = False

    def __init__(self, sql_client, called_by_bot: bool = False):
        super().__init__()
        self.sql_client = sql_client
        self.called_by_bot = called_by_bot

    def __enter__(self):
        self.instantiated = True
        self.members = FakeMembersRepository(contains=self.existing_members)
        self.pugs = FakePUGsRepository(contains=self.existing_pugs)
        self.bans = FakeBansRepository(contains=self.existing_bans)
        self.stats = FakeRepository(contains=self.existing_stats)
        return super().__enter__()
