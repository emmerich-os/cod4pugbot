import datetime
import functools

from . import discord as testing
from cod4pugbot.domain import model

test_member = functools.partial(
    testing.Member,
    id=1,
    name="test_name",
    nick="test_nick",
    discriminator="1111",
    guild=testing.Guild(),
    guild_permissions=testing.GuildPermissions(administrator=False),
)

test_member_pug_banned = test_member(roles=[testing.PUGBanRole()])

test_member_global_banned_with_role = test_member(
    roles=[testing.GlobalBanRole()],
)

test_member_global_banned_from_guild = test_member(
    globally_banned=True,
)

test_admin = functools.partial(
    testing.Member,
    id=9,
    name="test_admin",
    nick="test_admin",
    discriminator="9999",
    guild=testing.Guild(),
    guild_permissions=testing.GuildPermissions(administrator=True),
)

test_database_member = functools.partial(
    model.Member,
    discord_id=test_member().id,
    name=test_member().name,
    nick=test_member().nick,
    discriminator=test_member().discriminator,
    pb_guid="abcdefgh",
    active=True,
    created_at=datetime.datetime(2020, 1, 1),
)

test_ban = functools.partial(
    model.Ban,
    discord_id=1,
    global_ban=False,
    pug_ban=False,
    reason="test reason",
    duration="1 day(s)",
    created_at=datetime.datetime(2020, 1, 1),
    expires_at=datetime.datetime(2020, 1, 2),
)

test_administration_channel = functools.partial(
    testing.Channel,
    id="administration",
)

test_support_bans_channel = functools.partial(
    testing.Channel,
    id="bans",
)
