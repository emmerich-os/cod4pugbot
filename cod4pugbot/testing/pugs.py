from .discord import Member
from .discord import User


class PUGPlayer(Member):
    def __eq__(self, other):
        if isinstance(other, (self.__class__, User)):
            return self.id == other.id
        return NotImplemented
