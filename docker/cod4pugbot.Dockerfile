# Building layer
FROM fabianemmi/python-poetry:3.9-1.1.7-slim as builder

# Copy files
ADD cod4pugbot /app/cod4pugbot
COPY poetry.lock pyproject.toml config.yml /app/
WORKDIR /app

# Install with poetry
RUN python -m venv /venv
RUN /venv/bin/python -m pip install --upgrade pip
RUN . /venv/bin/activate && poetry install --no-dev --no-root --no-interaction --no-ansi
RUN . /venv/bin/activate && poetry build
RUN . /venv/bin/activate && pip install /app/dist/*.whl

# Delete Python cache files
RUN find . | grep -E "(__pycache__|\.pyc|\.pyo$)" | xargs rm -rf
RUN cd /venv && find . | grep -E "(__pycache__|\.pyc|\.pyo$)" | xargs rm -rf

# Create entrypoint script
RUN echo ". /venv/bin/activate && cod4pugbot --config /app/config.yml" > /entrypoint.sh

# Execution layer
FROM python:3.9-slim

# Environment variables for the cod4pugbot package
ARG POSTGRES_PASSWORD="test_pw"
ARG DB_NAME="testdb"
ARG DB_USER="test_user"
ARG DB_USER_PASSWORD="test_pw"
ARG DB_HOST="localhost"
ARG DB_PORT=6280
ARG BOT_TOKEN="<bot token>"

ENV POSTGRES_PASSWORD ${POSTGRES_PASSWORD}
ENV DB_NAME ${DB_NAME}
ENV DB_USER ${DB_USER}
ENV DB_USER_PASSWORD ${DB_USER_PASSWORD}
ENV DB_HOST ${DB_HOST}
ENV DB_PORT ${DB_PORT}
ENV BOT_TOKEN ${BOT_TOKEN}

COPY --from=builder /venv /venv
COPY --from=builder /app/config.yml /app/config.yml
COPY --from=builder /entrypoint.sh /

ENTRYPOINT ["/bin/sh", "./entrypoint.sh"]
