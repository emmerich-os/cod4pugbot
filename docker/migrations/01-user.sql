-- psql --set=dbuser=<user> --set=dbuserpassword=<password> --set=dbname=<database name> -f user.sql
\set dbname `printenv DB_NAME`
\set dbuser `printenv DB_USER`
\set dbuserpassword `printenv DB_USER_PASSWORD`

CREATE USER :dbuser WITH PASSWORD :'dbuserpassword';

CREATE DATABASE :dbname WITH OWNER :dbuser;
