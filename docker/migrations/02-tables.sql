-- psql --set=dbname=<database name> -f tables.sql
\set dbname `printenv DB_NAME`

\c :dbname

CREATE TABLE IF NOT EXISTS members (
    discord_id	    BIGINT PRIMARY KEY          NOT NULL UNIQUE,
    name            TEXT                        NOT NULL,
    discriminator   CHAR(4)                     NOT NULL,
    nick	        TEXT                        NOT NULL,
    pb_guid         CHAR(8)                     NOT NULL UNIQUE,
    active          BOOLEAN DEFAULT TRUE        NOT NULL,
    created_at      TIMESTAMP                   DEFAULT CURRENT_TIMESTAMP,
    UNIQUE(name, discriminator)
);

CREATE TABLE IF NOT EXISTS bans (
    discord_id      BIGINT PRIMARY KEY      NOT NULL UNIQUE,
    global_ban	    BOOLEAN DEFAULT FALSE   NOT NULL,
    pug_ban	        BOOLEAN DEFAULT FALSE   NOT NULL,
    reason	        TEXT                    NOT NULL,
    duration	    TEXT                    NOT NULL,
    created_at      TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
    expires_at	    TIMESTAMP,
    CONSTRAINT fk_discord_id FOREIGN KEY(discord_id) REFERENCES members(discord_id) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS pugs (
    id            SERIAL PRIMARY KEY    NOT NULL UNIQUE,
    mode	      TEXT                  NOT NULL,
    size          TEXT                  NOT NULL,
    maps          TEXT[5],
    attack	      BIGINT[5],
    defence	      BIGINT[5],
    attack_score  INTEGER,
    defence_score INTEGER,
    created_at    TIMESTAMP             DEFAULT CURRENT_TIMESTAMP,
    finished_at   TIMESTAMP
);

CREATE TABLE IF NOT EXISTS stats_5v5 (
    discord_id                  BIGINT PRIMARY KEY      NOT NULL UNIQUE,
    elo	                        INTEGER DEFAULT 1000    NOT NULL,
    pugs_played                 INTEGER DEFAULT 0       NOT NULL,
    wins		                INTEGER DEFAULT 0		NOT NULL,
    draws	                    INTEGER DEFAULT 0		NOT NULL,
    losses	                    INTEGER DEFAULT 0		NOT NULL,
    maps_played	                INTEGER DEFAULT 0       NOT NULL,
    map_wins                    INTEGER DEFAULT 0       NOT NULL,
    map_draws                   INTEGER DEFAULT 0       NOT NULL,
    map_losses                  INTEGER DEFAULT 0       NOT NULL,
    rounds_played               INTEGER DEFAULT 0       NOT NULL,
    round_wins                  INTEGER DEFAULT 0       NOT NULL,
    round_losses                INTEGER DEFAULT 0       NOT NULL,
    score                       INTEGER DEFAULT 0       NOT NULL,
    kills	                    INTEGER DEFAULT 0       NOT NULL,
    assists	                    INTEGER DEFAULT 0       NOT NULL,
    deaths	                    INTEGER DEFAULT 0       NOT NULL,
    impact_kills                INTEGER DEFAULT 0       NOT NULL,
    opening_kills	            INTEGER DEFAULT 0       NOT NULL,
    opening_deaths              INTEGER DEFAULT 0       NOT NULL,
    opening_kills_converted     INTEGER DEFAULT 0       NOT NULL,
    opening_deaths_converted    INTEGER DEFAULT 0       NOT NULL,
    one_kills                   INTEGER DEFAULT 0       NOT NULL,
    two_kills                   INTEGER DEFAULT 0       NOT NULL,
    three_kills                 INTEGER DEFAULT 0       NOT NULL,
    four_kills                  INTEGER DEFAULT 0       NOT NULL,
    five_kills                  INTEGER DEFAULT 0       NOT NULL,
    rounds_converted            INTEGER DEFAULT 0       NOT NULL,
    headshot_kills              INTEGER DEFAULT 0       NOT NULL,
    rifle_kills	                INTEGER DEFAULT 0       NOT NULL,
    smg_kills	                INTEGER DEFAULT 0       NOT NULL,
    sniper_kills	            INTEGER DEFAULT 0       NOT NULL,
    pistol_kills	            INTEGER DEFAULT 0       NOT NULL,
    shotgun_kills	            INTEGER DEFAULT 0       NOT NULL,
    nade_kills	                INTEGER DEFAULT 0       NOT NULL,
    knife_kills                 INTEGER DEFAULT 0       NOT NULL,
    car_kills	                INTEGER DEFAULT 0       NOT NULL,
    other	                    INTEGER DEFAULT 0       NOT NULL,
    suicides	                INTEGER DEFAULT 0       NOT NULL,
    team_kills                  INTEGER DEFAULT 0       NOT NULL,
    bomb_plants	                INTEGER DEFAULT 0       NOT NULL,
    bomb_defuses	            INTEGER DEFAULT 0       NOT NULL,
    CONSTRAINT fk_discord_id FOREIGN KEY(discord_id) REFERENCES members(discord_id) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS stats_3v3 AS TABLE stats_5v5;
CREATE TABLE IF NOT EXISTS stats_2v2 AS TABLE stats_5v5;
CREATE TABLE IF NOT EXISTS stats_1v1 AS TABLE stats_5v5;
