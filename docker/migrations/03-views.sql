-- psql --set=dbname=<database name> -f views.sql
\set dbname `printenv DB_NAME`

\c :dbname

CREATE OR REPLACE VIEW expired_bans AS (
    SELECT
        b.discord_id,
        b.global_ban,
        b.pug_ban,
        b.reason,
        b.duration,
        b.created_at,
        b.expires_at
    FROM bans b
    WHERE b.expires_at <= now()
);

CREATE OR REPLACE FUNCTION calculate_rating(tbl regclass, OUT rating REAL)
    AS
    $func$
    DECLARE rating REAL;
    BEGIN
        EXECUTE format('
            SELECT
                (
                    (CAST(kills AS REAL) / CAST(NULLIF(rounds_played, 0) AS REAL))
                    /
                    (NULLIF(AVG(CAST(kills AS REAL)) OVER(PARTITION BY NULL), 0) / NULLIF(AVG(CAST(NULLIF(rounds_played, 0) AS REAL)) OVER(PARTITION BY NULL), 0))
                    + 0.7 * (
                            (((CAST(rounds_played AS REAL) - CAST(deaths AS REAL))) / CAST(NULLIF(rounds_played, 0) AS REAL))
                            / NULLIF(
                                (AVG(CAST(rounds_played AS REAL)) OVER(PARTITION BY NULL) - AVG(CAST(deaths AS REAL)) OVER(PARTITION BY NULL))
                                / NULLIF(AVG(CAST(rounds_played AS REAL)) OVER(PARTITION BY NULL), 0)
                            , 0)
                    )
                    + 1.0/2.7 * (
                        (
                            (
                                CAST(one_kills AS REAL)
                                + 4 * CAST(two_kills AS REAL)
                                + 9 * CAST(three_kills AS REAL)
                                + 16 * CAST(four_kills AS REAL)
                                + 25 * CAST(five_kills AS REAL)
                            )
                            / NULLIF(CAST(rounds_played AS REAL), 0)
                        )
                        /
                        (
                            NULLIF((
                                AVG(CAST(one_kills AS REAL)) OVER(PARTITION BY NULL)
                                + 4 * AVG(CAST(two_kills AS REAL)) OVER(PARTITION BY NULL)
                                + 9 * AVG(CAST(three_kills AS REAL)) OVER(PARTITION BY NULL)
                                + 16 * AVG(CAST(four_kills AS REAL)) OVER(PARTITION BY NULL)
                                + 25 * AVG(CAST(five_kills AS REAL)) OVER(PARTITION BY NULL)
                            ), 0)
                            / NULLIF(AVG(CAST(rounds_played AS REAL)) OVER(PARTITION BY NULL), 0)
                        )
                    )
                ) FROM %s
        ', tbl)
        INTO rating;
    END;
    $func$
    LANGUAGE plpgsql
;

CREATE OR REPLACE FUNCTION stats_print(tbl TEXT) -- or regclass instead of TEXT to avoid SQL injection
    RETURNS TABLE(
        discord_id BIGINT,
        nick TEXT,
        name TEXT,
        elo INTEGER,
        rating REAL,
        pugs_played INTEGER,
        wins INTEGER,
        draws INTEGER,
        losses INTEGER,
        win_rate REAL,
        maps_played INTEGER,
        map_wins INTEGER,
        map_draws INTEGER,
        map_losses INTEGER,
        map_win_rate REAL,
        score_per_map REAL,
        kills_per_map REAL,
        assists_per_map REAL,
        deaths_per_map REAL,
        rounds_played INTEGER,
        round_wins INTEGER,
        round_losses INTEGER,
        round_win_rate REAL,
        score_per_round REAL,
        kills_per_round REAL,
        assists_per_round REAL,
        deaths_per_round REAL,
        score INTEGER,
        kills INTEGER,
        assists INTEGER,
        deaths INTEGER,
        kd_ratio REAL,
        kd_difference INTEGER,
        impact_kills INTEGER,
        opening_kills INTEGER,
        opening_deaths INTEGER,
        opening_kills_converted INTEGER,
        opening_deaths_converted INTEGER,
        one_kills INTEGER,
        two_kills INTEGER,
        three_kills INTEGER,
        four_kills INTEGER,
        five_kills INTEGER,
        rounds_converted INTEGER,
        headshot_kills INTEGER,
        rifle_kills INTEGER,
        smg_kills INTEGER,
        sniper_kills INTEGER,
        pistol_kills INTEGER,
        shotgun_kills INTEGER,
        nade_kills INTEGER,
        knife_kills INTEGER,
        car_kills INTEGER,
        other INTEGER,
        suicides INTEGER,
        team_kills INTEGER,
        bomb_plants INTEGER,
        bomb_defuses INTEGER
    )
    AS
    $func$
    BEGIN
        RETURN QUERY
            EXECUTE format(E'
                SELECT
                    m.discord_id,
                    m.nick,
                    m.name,
                    s.elo,
                    calculate_rating(\'stats_%s\') rating,
                    s.pugs_played,
                    s.wins,
                    s.draws,
                    s.losses,
                    (CAST(s.wins AS REAL) / NULLIF(CAST(s.pugs_played AS REAL), 0)) win_rate,
                    s.maps_played,
                    s.map_wins,
                    s.map_draws,
                    s.map_losses,
                    (CAST(s.maps_played AS REAL) / NULLIF(CAST(s.map_wins AS REAL), 0)) map_win_rate,
                    (CAST(s.score AS REAL) / NULLIF(CAST(s.maps_played AS REAL), 0)) score_per_map,
                    (CAST(s.kills AS REAL) / NULLIF(CAST(s.maps_played AS REAL), 0)) kills_per_map,
                    (CAST(s.assists AS REAL) / NULLIF(CAST(s.maps_played AS REAL), 0)) assists_per_map,
                    (CAST(s.deaths AS REAL) / NULLIF(CAST(s.maps_played AS REAL), 0)) deaths_per_map,
                    s.rounds_played,
                    s.round_wins,
                    s.round_losses,
                    (CAST(s.rounds_played AS REAL) / NULLIF(CAST(s.round_wins AS REAL), 0)) round_win_rate,
                    (CAST(s.score AS REAL) / NULLIF(CAST(s.rounds_played AS REAL), 0)) score_per_round,
                    (CAST(s.kills AS REAL) / NULLIF(CAST(s.rounds_played AS REAL), 0)) kills_per_round,
                    (CAST(s.assists AS REAL) / NULLIF(CAST(s.rounds_played AS REAL), 0)) assists_per_round,
                    (CAST(s.deaths AS REAL) / NULLIF(CAST(s.rounds_played AS REAL), 0)) deaths_per_round,
                    s.score,
                    s.kills,
                    s.assists,
                    s.deaths,
                    (CAST(s.kills AS REAL) / NULLIF(CAST(s.deaths AS REAL), 0)) kd_ratio,
                    (s.kills - s.deaths) kd_difference,
                    s.impact_kills,
                    s.opening_kills,
                    s.opening_deaths,
                    s.opening_kills_converted,
                    s.opening_deaths_converted,
                    s.one_kills,
                    s.two_kills,
                    s.three_kills,
                    s.four_kills,
                    s.five_kills,
                    s.rounds_converted,
                    s.headshot_kills,
                    s.rifle_kills,
                    s.smg_kills,
                    s.sniper_kills,
                    s.pistol_kills,
                    s.shotgun_kills,
                    s.nade_kills,
                    s.knife_kills,
                    s.car_kills,
                    s.other,
                    s.suicides,
                    s.team_kills,
                    s.bomb_plants,
                    s.bomb_defuses
                FROM stats_%s AS s
                LEFT JOIN members AS m ON s.discord_id = m.discord_id
                ORDER BY m.nick
             ', tbl, tbl);
    END;
    $func$
    LANGUAGE plpgsql
;


CREATE OR REPLACE FUNCTION stats_print_partial(tbl TEXT) -- or regclass instead of TEXT to avoid SQL injection
    RETURNS TABLE(
        discord_id BIGINT,
        nick TEXT,
        name TEXT,
        elo INTEGER,
        rating REAL,
        pugs_played INTEGER,
        win_rate REAL,
        kd_ration REAL,
        kd_difference INTEGER
    )
    AS
    $func$
    BEGIN
        RETURN QUERY
            EXECUTE format(E'
                SELECT
                    m.discord_id,
                    m.nick,
                    m.name,
                    s.elo,
                    calculate_rating(\'stats_%s\') rating,
                    s.pugs_played,
                    (CAST(s.wins AS REAL) / CAST(s.pugs_played AS REAL)) win_rate,
                    (CAST(s.kills AS REAL) / CAST(s.deaths AS REAL)) kd_ratio,
                    (s.kills - s.deaths) kd_difference
                FROM stats_%s AS s
                LEFT JOIN members AS m USING (discord_id)
             ', tbl, tbl);
    END;
    $func$
    LANGUAGE plpgsql
;
