\set dbname `printenv DB_NAME`
\set dbuser `printenv DB_USER`

\c :dbname

GRANT ALL PRIVILEGES ON DATABASE :dbname TO :dbuser;
GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public to :dbuser;
GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA public to :dbuser;
GRANT ALL PRIVILEGES ON ALL FUNCTIONS IN SCHEMA public to :dbuser;
GRANT ALL PRIVILEGES ON SCHEMA public TO :dbuser;

CREATE OR REPLACE FUNCTION fn_grant_all_views(schema_name TEXT, role_name TEXT)
    RETURNS VOID
    AS
    $func$
    DECLARE view_name TEXT;
    BEGIN
      FOR view_name IN
        SELECT viewname FROM pg_views WHERE schemaname = schema_name
      LOOP
        EXECUTE format('GRANT ALL PRIVILEGES ON %s TO %s;', view_name, role_name);
      END LOOP;
    END;
    $func$
    LANGUAGE plpgsql
;

SELECT fn_grant_all_views('public', :'dbuser');
