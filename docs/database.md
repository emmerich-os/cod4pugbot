# Database architecture
The database consists of three tables:

1. members

   Contains all registered members and their necessary information.
   These are discord_id, nick, name, pb_guid and active (whether currently registered or not).

1. pugs

   All information about played PUGs are stored here.
   These are id (number), mode (BO5, BO3 etc.), size (5v5, 4v4 etc.), maps, attack and defence
   (players by their Discord ID), result.

1. stats_5v5/3v3/2v2/1v1

   Saves all stats about players that ever played a PUG on a server whose rcon password was known to
   the bot.
   For the exact columns check ``db/scripts/tables.sql``.

1. bans

   Saves all information about currently banned players.
   These are discord_id, where the ban is for pug or global, reason, duration, created, expires.


Each table's discord_id references the discord_id of the members table.


## Setup

1. Change to docker folder

   ```
   cd docker/
   ```

1. If running, stop and remove backend as well as the docker volume

   ```
   sudo docker stop cod4pugbot_backend
   sudo docker rm cod4pugbot_backend
   sudo docker volume rm cod4pugbot_data
   ```

1. Create an external docker volume

   ```
   sudo docker volume create cod4pugbot_data
   ```

1. Set environment variables

   ```
   source ../vars.env
   ```

1. Start container

   ```
   sudo -E docker-compose -f docker-compose.backend.yml up -d
   ```

1. After a few seconds, check if container is actually running since start up

   ```
   sudo docker ps -a
   ```
   **Note:** the container is set up as `restart: unless-stopped`.
   Hence, it gets restarted every time it crashes (unless explicitly stopped).
