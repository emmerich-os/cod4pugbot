# Testing

In order to test, do as follows:

1. Install `cod4pugbot` in editable mode

   ```
   pip install -e .
   ```

1. Install the dev requirements

   ```
   pip install -r requirements-dev.txt
   ```

1. Integration tests as well as unit tests need a database instance.
   For instructions on how to run the database, see `docs/database.md`.

1. Run the tests

   ```
   pytest
   ```

   **Note:** You can also run tests in spefific folders or individual tests

   ```
   pytest tests/unit/
   pytest tests/unit/db/
   pytest tests/unit/db/test_whatever.py
   pytest tests/unit/db/test_whatever.py::test_function
   pytest tests/unit/db/test_whatever.py::TestClass
   pytest tests/unit/db/test_whatever.py::TestClass::test_function
   etc.
   ```
