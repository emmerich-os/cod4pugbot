---

# COD4PUGBot
COD4PUGBot is a Discord bot designed for different tasks in a Discord server.
But most importantly it was designed to manage CoD4 PUGs of different kinds.
These can be enabled or disabled as the user desires.

The features include:

* different PUG modes (BO1, BO2, BO3 and/or BO5) and sizes (1v1, 2v2, 3v3 and/or 5v5):
    - add/remove player
    - player picking
    - map picking or random map choice
    - adding server information
        + see server information
        + scoreboard
        + killfeed
        + stats tracking
* user actions:
    - register
    - kick
    - ban (global and PUG)
* news posting
* moderation:
    - use of English
    - censoring of explicit language
* report system:
    - abusive behavior, cheating etc.
    - bot errors

---

## Configure COD4PUGBot
The `config.yml` file contains all data the bot needs to be provided with to run on your own Discord
server without causing any problems. It can be easily modified with any text editor program. Just
open it and change every setting as you wish. Be careful what values the options can have.

Detailed information about each parameter can be found in the `config_setup.md`.

If the bot is not provided with all necessary role, category and channel IDs, it will create everything which is missing automatically on connect if it has admin rights. If it does not, this will cause errors. The bot can also be manually initiated once it has connected to the Discord server and the admin rights have been assigned to it using the `init` command.

---

## How to run COD4PUGBot
Copy the files, then simply run the bot with `nohup python /path/to/test.py > output.log &`.
Here, `nohup` avoids the process to be stopped when closing the terminal.
The console output (errors, warnings etc.) will be written into `output.log`
The `&` causes the bot to run in the background.

---

## Bot prefix
The bot prefix can be set individually within the `config.yml`.
The default prefix is `!`.

---

## User features
There are different options for users, some of which are only available in certain channels.

### help
The `help` command returns the help table containing all commands and a short descriptipn of what they do.
Besides, it can give detailed help about every other available command.

#### Usage
```
!help
!help <command>
```

### register
The `register` command gives users the opportunity to register with their PunkBuster GUID (PB GUID).
PUGs are only available for registered users.
This command needs a PB GUID (8 characters, only consisting of A-Z, a-z and 0-9) and an attachment with a
screenshot of `/pb_plist` so that admins can verify it.
Whenever a user registers, the admins get a notification with all user details about the user that sent the request
(including the PB GUID and the screenshot), which they then can process.

#### Usage
```
!register <PB GUID> (+ attachment of /pb_plist)
```

### admin
The `admin` command gives users the opportunity to call admins whenever they need them.
A reason can be optionally included.

#### Usage
```
!admin
!admin <reason>
```

### report
The `report` command offers users the opportunity to report other users (e.g. in case of cheat abuse, abusive behavior etc.).
It requires the `@`-mentioning of the user that is to be reported, a reason and an attachment (e.g. screenshot as proof).

#### Usage
```
!report @user reason="<explanation>" (+ attachment with proof)
```

### report_error
The `report_error` command helps the admins and me as the developer to provide the best functionality of the bot.
Whenever a user thinks the bot did not behave as intended, they may report this to admins so they can fix the problem.
It requires the command as the user executed it and the response from the bot or an explanation of what the user thinks went wrong.

#### Usage
```
!report_error "command as user executed it" "description of what went wrong"
```

---

## PUG features
The COD4PUGBot was intentionally designed to manage PUGs. PUGs are always open, users can join and leave.
Whenever a PUG is full (e.g. 10/10 players in a 5v5 PUG), the bot opens a new temporary channel and the player
pick phase begins in which team captains can pick their team mates. When the pick phase finished, the map
elimination phase starts in which team captains can remove (or pick) maps. After the map elimination phase,
the PUG starts and the players can join the server. If a server IP is provided by the `server` command in
the temporary match channel (see below), the bot will post information from the game server (scoreboard, kills etc.)
in the corresponding PUG channel so that everyone outside the server can have an insight.
If the rcon password is given, stats of all players in the PUG will be tracked and saved.

The bot offers BO1, BO2, BO3 and/or BO5 PUGs in the sizes of 1v1, 2v2, 3v3 and/or 5v5.

### add
Players can add themselves to a PUG by reacting with :white_check_mark: to the respective PUG message.

### remove
Players can remove themselves from a PUG by removing the :white_check_mark: reaction.

### pick
Team captains can pick players by reacting with :white_check_mark: to the message with respective player.

### veto map
Team captains can veto a map by reacting with :no_entry: to the message with respective map name.

### pick map
In BO2, BO3 and BO5 PUGs, it is possible to pick desired maps from the mappool.
Team captains can pick a map by reacting with :white_check_mark: to the message with the respective map name.

### server
If the `server` command is executed in the temporary match channel, the bot will retrieve all information
from the server and post them in the temporary match channel in order to provide the information
to all other users of the Discord server. The command needs the server IP or the CoD4-like connect command.

If an rcon password is provided, player stats can be tracked using the PB GUIDs.

#### Usage
```
!server connect <IP>; password <password>
!server connect <IP>; password <password>; rcon <rcon password>
```

### ao
As soon as a user's status changes to AFK (idle) or offline, they will be removed from all open PUGs he joined. To avoid this, users can enable AFK/offline protection using the `ao` command.

#### Usage
```
!ao
```

### sub
To call a subsititute player for a PUG, any player in a running PUG can use the `sub` command. This has to be executed in the temporary PUG channel. The bot will then post a message in the `sub-needed` channel refering to the respective PUG.

#### Usage
```
!sub
```

### abort
Any user in a PUG can abort the PUG using the `abort` command. This will immediately abort the PUG and delete the PUG channel. It has to be posted into the respective temporary PUG channel.

#### Usage
```
!abort
```

### stats
Users can view their PUG stats with the `stats` command. Stats are distinguished in 1v1, 2v2, 3v3 and 5v5 stats. Users can only view one of these stats at a time, but they can also view stats from other (multiple) users.

#### Usage
```
!stats <type>
!stats @user <type>
```

Type can be:
- `1v1`
- `2v2`
- `3v3`
- `4v4`

This only shows a few stats. To see extended stats, the keyword `all` needs to be added to the command:
```
!stats <type> all
!stats @user <type> all
```
Users can also get the full stats table with all users as a `.csv` file by adding the keyword `table`:
```
!stats table
```

---

## Admin features
There are several features and commands which are only available to admins in order to manage the Discord server
itself and its users.

### Registering of users
Whenever a user uses `register`s, the information will be posted in a separate channel that is only visible for admins.
In this channel, admins can see which users tried to register and which PB GUID and screenshot they provided.
If an attempt gets accepted, the user will be added to the list of registered users, which can be read by admins any time.
If it gets rejected, the user receives a direct message explaining that it got rejected.
Optionally, admins can make the bot send a private message to the rejected user with the `message` command explaining why he got rejected.

#### Accept a register request
To accept a register request, amdins have to reaction with :white_check_mark: to the message of the bot in the admins-only request channel.
Alternatively, admins can manually register users using `register_member`, mentioning the player and giving his PB GUID (see below).
The bot will then put the user into the list of registered users and assign the `Registered` role.

An accepted register request can be undone any time by removing the :white_check_mark: reaction again.

#### Reject a register request
To reject a register request, admins have to react with :no_entry: to the register request message.

A rejected reqister request can be undone any time by removing the :no_entry: reaction again.

#### message
Admins can use the bot to send a private message a user with the `message` command. This is especially useful to give rejected user a reason why their register request got rejected.

##### Usage
```
!message @user <reason>
```

#### register_member
To manually register a member, admins can use the `register_member` command. The user to register has to be `@`-mentioned and the PB GUID passed to the bot.

##### Usage
```
!register_member @user pb_guid="<PB GUID>"
```

#### search_member
Admins can both see the list of registered users or check whether a user or PB GUID is already registered.
Usually, on register requests, the bot does this automatically.
When manually registering a player, the bot does this aswell.
But if an admin wants to search the list of registered players by name, nick or PB GUID, they may use the `search_member` command.

##### Usage
```
!search_member @user
!search member keyword="<argument>"
```

###### Possible keywords
Possible keywords to search for are:
- `nick` (name of the user in the Discord server)
- `name` (always the full Discord name, e.g. `COD4PUGBot#1234`)
- `discord_id` (Discord ID of the user)
- `pb_guid` (8-character PG GUID)

#### update_member
The information of registered members can be updated using the `update_member` command. Since the bot keeps track of name changes, this can only be used to update the PB GUID. The member to update can be refered to via `@`-mention, Discord name, Discord ID.

##### Usage
```
!update_member @user pb_guid="<PB GUID>"
!update_member keyword="<argument>" pb_guid="<PB GUID>"
```

###### Possible keywords
Possible keywords to search for are:
- `nick` (name of the user in the Discord server)
- `name` (always the full Discord name, e.g. `COD4PUGBot#1234`)
- `discord_id` (Discord ID of the user)

#### remove_member
Registered members can be manually removed (i.e. unregistered) using the `remove_member` command.

##### Usage
```
!remove_member @user
!remove_member keyword="<argument>"
```

###### Possible keywords
Possible keywords to search for are:
- `nick` (name of the user in the Discord server)
- `name` (always the full Discord name, e.g. `COD4PUGBot#1234`)
- `discord_id` (Discord ID of the user)
- `pb_guid` (8-character PG GUID)

#### members
Admins can get the full list of registered members as a `.csv` file using the `members` command. The bot will send the file via private message.

##### Usage
```
!members
```

### User administration
Users can be kicked, banned (globally and from PUGs) from the server using the bot. The bot sends a message with information about the action in the `bans` channel. If a user gets banned, the bot assigns the `GLOBAL BAN` or `PUG BAN` role to the member. (If the `GLOBAL BAN` role is not known to the bot, the user simply gets banned from the Discord server.) As soon as the ban has expires, the bot unassigns the given role.

#### warn
Users can be warned using the `warn` command. The bot then sends a private message to the user. As soon as the user has 3 warnings to his name, he will get temporarily banned for 1 week. This command requires an `@`-mention of the user and a reason for the warning.

##### Usage
```
!warn @user <reason>
```


#### kick
Users can be kicked from the server using the `kick` command.

#### permaban
Users can be permanently banned using the `permaban` command. This is a global ban. The ban length here is simply put to 999 years. The command requires an `@`-mention of the user and a reason for the ban.

##### Usage
```
!permaban @user <reason>
!permaban @user reason="<reason>"
```

#### tempban
Temporary bans are global bans. They require an `@`-mention of the user, a ban duration and a reason for the ban.

##### Usage
```
!tempban @user duration"<duration>" reason="<reason>"
```

###### Possible durations
The duration (almost always) requires a definition of the amount and the type of duration. Amount is always given in round numbers (1, 2, 3, ...) whereas the type can be:
- `h`/`hours`
- `d`/`days`
- `w`/`weeks`
- `m`/`months`
- `y`/`years`
- `p`/`permanent` (no amount needed)

###### Examples
- `duration="1d"`
- `duration="2weeks"`
- `duration="3 months"`
- `duration="p"`


#### pugban
To ban a user from playing PUGs admins can use the `pugban` command. It requires an `@`-mention of the user, a duration and a reason.

##### Usage
```
!pugban @user duration="<duration>" reason="<reason>"
```
See above (`tempban`) for possible arguments for duration `duration`.

#### search_ban
Ban details can be viewed with the `search_ban` command. This requires an `@`-mention of the user or user details to search for.

##### Usage
```
!search_ban @user
!search_ban keyword="<argument>"
```

###### Possible keywords
Possible keywords to search for are:
- `nick` (name of the user in the Discord server)
- `name` (always the full Discord name, e.g. `COD4PUGBot#1234`)
- `discord_id` (Discord ID of the user)

#### update ban
Ban duration, type and/or reason can be updated with the `update_ban` command. This requires either an `@`-mention of the user, his Discord name, or Discord ID and the details to update. Multiple details are possible

##### Usage
```
!update_ban @user keyword="<argument>" (keyword="<argument>" etc.)
!update_ban user_keyword="<user detail>" keyword="<argument>" ...
```

###### Possible user_keywords
Users can be referred to by:
- `name` (Discord name)
- `discord_id` (Discord ID)

###### Possible keywords
Possible keywords to use are:
- `duration` (see above (`tempban`) for possible arguments)
- `reason`
- `pug` (0 or 1)
- `global` (0 or 1)

The ban types (`pug` or `globabl`) cannot be set to 1 simultaneously. A user can either be globally **OR** PUG banned.

#### lift_ban
Usually, bans are being lifted automatically by the bot once they expired. But they can also be manually lifted using the `lift_ban` command. This requires a reference to the user as demonstrated above and a reason for the ban lift.

##### Usage
```
!lift_ban @user reason="<reason>""
!lift_ban user_keyword="<argument>" reason="<reason>"
```
See above (`update_ban`) for possible `user_keyword`s.

#### bans
The complete bans can be retrieved as a `.csv` file which will be sent via private message by the bot using the ` bans` command.

##### Usage
```
! bans
```

### (Un-)loading features (Cogs)
Features (Cogs in `discord.py` speech) can be unloaded and loaded (i.e. deactivated and activated). This can be done via commands.

#### load
Cogs can be loaded using the `load` command. This requires the name of the Cog.

##### Usage
```
!load <Cog name>
```
Cog names are:
- administration (warning, kicking, banning)
- contact_admin (users can contact admins via private message)
- errors (error report system)
- language (censoring of explicit language & check for correct language)
- pugs
- register
- report (user report system)

#### unload
Cogs can be unloaded using the `unload` command.

##### Usage
```
!unload <Cog name>
```
For Cog names see above. Other Cogs than the ones listed should **NEVER** be unloaded since they are mandatory for the bot to work properly!

---

## Moderation features

### Use of correct language
The bot can check all messages on the server whether they use the correct language of the Discord.

### Use of appropriate language
The bot can check whether any message contains inappropriate language and censor it.
The bot does this by deleting the original message and re-posting it as a quote with all inappropriate words censored.

---

## Future implementations
Currently, there are no future implementations of any features planned. I am always open for suggestions, though.

---

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

---

## Created by
Fabian 'fappyy' Emmerich

---

## License
Copyright :copyright: 2020 fappyy

This program is free software: you can redistribute it and/or modify
it.
This program is distributed in the hope that it will be useful,
but without any warranty, without even the implied warranty of
merchantability or fitness for a particular purpose.
