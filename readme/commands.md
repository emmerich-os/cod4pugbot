# COD4PUGBot commands
This document is supposted to be a short list of all available commands if all features (Cogs) are activated. For detailed usage information of each command, see the `README.md`.

## Commands

### Help
- `!help`: Show help message

### Contact Admins
- `!admin`: Contact admins.

### Errors
- `!report_error`: Report any command or bot errors to the admins.

### Extension Manager
- `!load`: Load a specific extension.
- `!unload`: Unload a specific extension.

### Initializer
- `!init`: Initialize all roles, categories and channels.

### Administration
- `!bans`: Get the bans as `.csv` table.
- `!kick`: Kick a user.
- `!lift_ban`: Lift ban of a user.
- `!permaban`: Permanently ban a user.
- `!pugban`: Temporarily ban a user from PUGs.
- `!search_ban`: Search ban entry.
- `!tempban`: Temporarily ban a user.
- `!update_ban`: Update ban of a user.
- `!warn`: Warn a user.

### PUG
- `!abort`: Abort a PUG.
- `!ao`: Enable/disable AFK/offline protection.
- `!server`: Add server to a PUG.
- `!stats`: Show PUG stats.
- `!sub`: Call a sub.

### Register
- `!members`: Get the members as `.csv`table.
- `!register`: Request to get registered in the Discord server.
- `!register_member`: Manually register a member with a PB GUID.
- `!remove_member`: Manually remove a registered member.
- `!search_member`: Search the members for a specific member.
- `!update_member`: Update given member in the members.

### Report system
- `!report`: Report a player to the admin team.
