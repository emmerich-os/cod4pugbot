# Bot config setup
The `config.yml` contains all necessary information the bot is required to know in order to run. Some of these details do not have to be set before running the bot. These are role, category and channel IDs. If these are not given, the bot will create these on its own and write them into the config. Everything else always has to be set manually.

This document is supposted to explain each and every entry in the config.

## bot_statuses
The `bot_statuses` are statuses shown under the bot's name on the Discord server. This can be a list of multiple status messages. These will be changed every 10 seconds.

## category_ids
The bot operates in different categories: `admin_section`, `bo1` (`bo2`, `bo3` and/or `bo5` if enabled), `running_pugs` and `support`.

### admin_section
The admin section contains the `administration`, `register-requests`, `report` and `errors` channel:
- `administration`: Warn, kick or ban users.
- `register-requests`: Register requests will be posted here.
- `report`: User reports will be posted here.
- `errors`: The bot will automatically post error messages here to provide the best maintenance.

### bo1, bo2, bo3 and bo5
These are the categories where the PUG channels with the open PUG messages will be posted. These can contain channels for `1v1`, `2v2`, `3v3` and/or `5v5`.

To have PUGs of different modes and sizes in the same category, these can basically set equal for every PUG mode (i.e. category ID of `bo1` can be the same as for `bo2` and so forth).

### running_pugs
In this category the temporary PUG channels of runnig PUGs will be created. They will also be automatically deleted by the bot as soon as a PUG is finished or once the PUG exceeds an expiration time equal `45 minutes x maximum number of maps to be played` (e.g. 45 minutes to BO1 and 135 minutes for BO3).

### support
To this category belong the `register`, `report` and `bans` channel:
- `register`: Users can request to get registered.
- `report`: Users can report other users.
- `bans`: Here the bot will post kick/ban messages containing all information.

## channel_ids
These are the channel IDs of each channel listed above. These can, of course, be equal for different channels, but this is not recommended. If any value is set to `null` and the respective feature is activated, the bot will create the channel itself upon connecting to the Discord server or when using the `init` command.

## command_prefix
This is the command prefix under which the bot recognizes commands. This can be any alphanumeric character or word.

## features
These are the features the bot provides. They can be either `true` (activated) or `false` (deactivated):
- `bot_status`: Cycle bot status.
- `contact_admin`: Users can contact admins.
- `inappropriate_language_filter`: Filter explicit language.
- `language_check`: Check for use of English.
- `news`: Post news to `news` channel.
- `private_welcome_message`: Private welcome message sent to every new user.
- `pug`: PUGs.
- `register`: User can request to get registered.
- `report_errors`: Bot errors will be reported in the `errors` channel.
- `report_members`: Members can report other members.

## messages
These are messages the bot sends at the respective event:
- `welcome_message`: Message sent to users who join the Discord server.

## pugs
All PUG options can be set here such as modes (`bo1` etc.), types (`1v1` etc.), `maps` for each size, `map_elimination` and `ready_up_phase_length`.
- `map_elimination`: If `true`, map elimination is enabled, `false` for random map choice.
- `modes`: PUG mode and size can be de-/activated by setting it to `true` or `false`.
- `maps_XvX`: Map pool for the respective match type.
- `ready_up_phase_length`: Length in seconds for the ready-up phase.

## role_ids
The IDs for the roles the bot needs to know to operate. These are the `admin`, `global_banned`, `head_admin`, `pug_banned`, `pug_captain`, `registered` and `unregistered` role:
- `admin`: Admin. Can be equal to the `head_admin` ID.
- `global_banned`: Globally banned users. If set to `null`, users will be banned from the Discord server instead of getting the role assigned.
- `head_admin`: Head admin. Can be equal to the `admin` ID.
- `pug_banned`: PUG banned users.
- `pug_captain`: Users to be preferred when randomly choosing team captains.
- `registered`: Registered members.
- `unregistered`: Unregistered members.

## token
The Discord bot token.
