class TestBot:
    def __init__(self):
        pass


class TestCog:
    def __init__(self):
        self.bot = TestBot()


class TestPUG:
    def __init__(self):
        self.cog = TestCog()
        self.mode = "bo1"
        self.size = "5v5"
        self.attack = {
            "captain": None,
            "players": [],
            "result": None,
        }
        self.defence = {
            "captain": None,
            "players": [],
            "result": None,
        }

    def end_pug(self, reported_by_scorebot):
        """Print when stats saved successfully."""
        print(f"Stats saved, PUG successfully ended by bot {reported_by_scorebot}")


if __name__ == "__main__":
    # pugs = TestPUG()
    # server = Server.connect(
    #     ip="95.179.243.231", port=20001, password="pug1", rcon_password="topsecret", pugs=pugs,
    # )
    # ticker = []
    #
    # import time
    #
    # total_time = 0
    # while total_time < 30 * 60:
    #     total_time += 9
    #     # time.sleep(9)
    #     server.refresh()
    #     pdb.set_trace()
    #     if server.prev_tick_info is not None:
    #         ticker = [*ticker, *server.prev_tick_info]
    #
    # pdb.set_trace()

    from cod4pugbot.domain.features.pugs.cod4.utils import PromodPlayer
    from cod4pugbot.logger import setup_logger
    from tests.unit.domain.features.pugs.cod4.data.killfeed_example import killfeed
    from cod4pugbot.domain.features.pugs.cod4.gameserver import Server

    # import pdb

    logger = setup_logger()

    pug = TestPUG()
    server = Server.connect(
        pug=pug,
        ip="54.38.219.139",
        port=28958,
        password=None,
        rcon_password=None,
    )
    player = PromodPlayer(info=["mG # denci", True, 8, 0, 5, False], team="attack")
    player.pb_guid = "12345678"
    player.ip = "1.1.1.1"
    server.stats_tracker.update_player(player)
    server.prev_tick_info = killfeed
    server.format_killfeed()

    # pdb.set_trace()
# connect 176.9.29.181:18354; password orxo; rcon login ddv5pize
