import dataclasses

import discord
import pytest
from discord.ext import commands


@dataclasses.dataclass(eq=False)
class User(discord.User):
    name = "user_name"
    discriminator = 1111
    id = 1


@dataclasses.dataclass(eq=False)
class Member(discord.Member):
    nick = "member_nick"
    name = "member_name"
    discriminator = 2222
    id = 2


@dataclasses.dataclass(eq=False)
class Author(discord.User):
    nick = "author_nick"
    name = "author_name"
    discriminator = 3333
    id = 3
    mention = f"@{nick}"


@dataclasses.dataclass(eq=False)
class Channel(discord.TextChannel):
    id = 4

    async def send(self, content):
        return


@dataclasses.dataclass(eq=False)
class Message(discord.Message):
    content = "test message"
    author = Author()
    channel = Channel()


@dataclasses.dataclass(eq=False)
class Context(commands.Context):
    message = Message()
    channel = Channel()


@pytest.fixture()
def user():
    return User()


@pytest.fixture()
def member():
    return Member()


@pytest.fixture()
def author():
    return Author()


@pytest.fixture()
def ctx():
    return Context()
