from contextlib import suppress

import discord
import pytest
from _pytest.fixtures import FixtureLookupError
from discord.ext import commands

import cod4pugbot.domain.utils


@pytest.mark.parametrize(
    "user_input,pb_guid,author,discord_id,expected",
    [
        ("abcd1234", True, False, False, {"pb_guid": "abcd1234"}),
        ('"abcd1234"', True, False, False, {"pb_guid": "abcd1234"}),
        ('pb_guid="abcd1234"', True, False, False, {"pb_guid": "abcd1234"}),
        ("pb_guid=abcd1234", True, False, False, {"pb_guid": "abcd1234"}),
        (
            "user",
            False,
            False,
            False,
            {"nick": "user_name", "name": "user_name#1111", "discord_id": 1},
        ),
        (
            "member",
            False,
            False,
            False,
            {"nick": "member_nick", "name": "member_name#2222", "discord_id": 2},
        ),
        (
            "ctx",
            False,
            True,
            False,
            {"nick": "author_nick", "name": "author_name#3333", "discord_id": 3},
        ),
        (
            (
                """!register <@!112345678901234567> """
                """discord_id=112345678901234567 """
                """pb_guid="abcd1234" """
                """reason='test reason' """
                """duration=4h asd"""
            ),
            False,
            False,
            False,
            {
                "discord_id": 112345678901234567,
                "pb_guid": "abcd1234",
                "reason": "test reason",
                "duration": "4h",
            },
        ),
        (
            """!register <@!112345678901234567> """,
            False,
            False,
            True,
            {"discord_id": 112345678901234567},
        ),
    ],
)
@pytest.mark.asyncio()
async def test_get_kwargs(
    request, ctx, user_input, pb_guid, author, discord_id, expected
):
    with suppress(FixtureLookupError):
        user_input = request.getfixturevalue(user_input)

    if isinstance(user_input, discord.User):
        result = await cod4pugbot.cogs.utils.get_kwargs(
            ctx=ctx,
            user=user_input,
            pb_guid=pb_guid,
        )
    elif isinstance(user_input, discord.Member):
        result = await cod4pugbot.cogs.utils.get_kwargs(
            ctx=ctx,
            member=user_input,
            pb_guid=pb_guid,
        )
    elif isinstance(user_input, commands.Context):
        result = await cod4pugbot.cogs.utils.get_kwargs(
            ctx=user_input,
            author=author,
            pb_guid=pb_guid,
        )
    else:
        result = await cod4pugbot.cogs.utils.get_kwargs(
            ctx=ctx,
            content=user_input,
            pb_guid=pb_guid,
            discord_id=discord_id,
        )

    assert result == expected
