import dataclasses
import datetime
import os

import discord
import numpy as np
import psycopg2
import pytest
import pytz

from cod4pugbot.db.client import SQLClient


@pytest.fixture(scope="session")
def db_info():
    """Read test DB information from environment variables."""
    return {
        "database": os.getenv("DB_NAME", default="test_database"),
        "user": os.getenv("DB_USER", default="test_user"),
        "password": os.getenv("DB_USER_PASSWORD", default="test_password"),
        "host": os.getenv("DB_HOST", default="127.0.0.1"),
        "port": int(os.getenv("DB_PORT", default=5432)),
    }


@pytest.fixture(scope="session")
def db_connection(db_info):
    """Return connection to a local PostgreSQL database.

    Used for encoding 'sql.Composed`to `str`.

    """
    return psycopg2.connect(**db_info)


@dataclasses.dataclass(eq=False)
class User(discord.User):
    id: int = None
    name: str = None
    discriminator: str = None

    def __eq__(self, other):
        """Compare name to other name or other."""
        if isinstance(other, discord.User):
            return self.name == other.name
        return self.name == other


@dataclasses.dataclass(eq=False)
class Bot:
    db_info: dict
    user: User = User(name="test_bot")
    channel_ids: dict = None
    timezone: pytz.timezone = pytz.timezone("UTC")

    async def fetch_channel(self, *args, **kwargs):
        return

    def get_channel(self, *args, **kwargs):
        return

    def __eq__(self, other):
        """Compare user to other name."""
        if isinstance(other, discord.User):
            return self.user == other.name
        return self.user == other


CHANNEL_IDS = {
    "admin_section": {
        "errors": "test_admin_error_channel_id",
        "register": "test_admin_register_channel_id",
        "report": "test_admin_report_channel_id",
        "administration": "test_admin_administration_channel_id",
    },
    "news": None,
    "pugs": {
        "bo1": {
            "1v1": "test_pug_bo1_1v1_channel_id",
            "2v2": "test_pug_bo1_2v2_channel_id",
            "3v3": "test_pug_bo1_3v3_channel_id",
            "5v5": "test_pug_bo1_5v5_channel_id",
        },
        "bo2": {
            "1v1": "test_pug_bo2_1v1_channel_id",
            "2v2": "test_pug_bo2_2v2_channel_id",
            "3v3": "test_pug_bo2_3v3_channel_id",
            "5v5": "test_pug_bo2_5v5_channel_id",
        },
        "bo3": {
            "1v1": "test_pug_bo3_1v1_channel_id",
            "2v2": "test_pug_bo3_2v2_channel_id",
            "3v3": "test_pug_bo3_3v3_channel_id",
            "5v5": "test_pug_bo3_5v5_channel_id",
        },
        "bo5": {
            "1v1": "test_pug_bo5_1v1_channel_id",
            "2v2": "test_pug_bo5_2v2_channel_id",
            "3v3": "test_pug_bo5_3v3_channel_id",
            "5v5": "test_pug_bo5_5v5_channel_id",
        },
        "sub": "test_pug_sub_channel_id",
    },
    "support": {
        "bans": "test_support_bans_channel_id",
        "register": "test_support_register_channel_id",
        "report": "test_support_report_channel_id",
    },
}


@pytest.fixture()
def bot(db_info):
    return Bot(db_info=db_info, channel_ids=CHANNEL_IDS)


@pytest.fixture(scope="session")
def objects():
    """Return all database objects created with the migration scripts in `docker/migrations/`."""
    return {
        "tables": [
            "members",
            "bans",
            "pugs",
            "stats_5v5",
            "stats_3v3",
            "stats_2v2",
            "stats_1v1",
        ],
        "views": ["members_print", "bans_print", "pugs_print"],
        "functions": [
            "stats_print",
            "stats_print_partial",
            "calculate_rating",
            "fn_grant_all_views",
        ],
    }


@pytest.fixture(scope="session")
def delete_query(objects):
    """Return a delete query to clear all tables defined in the `objects` fixture."""
    return "\n".join(f"DELETE FROM {table};" for table in objects["tables"][::-1])


@pytest.fixture(scope="session")
def _db_instance(db_info, delete_query):
    sql_client_ = SQLClient(**db_info)
    _exec_query(sql_client_, delete_query)
    yield
    _exec_query(sql_client_, delete_query)


@pytest.fixture(scope="session")
def client(_db_instance, db_info):
    """Return `cod4pugbot.db.SQLClient` with a connection to the DB.

    When called, all tables in the DB will be cleared.

    """
    return SQLClient(**db_info)


@pytest.fixture()
def _clean_db(client, delete_query):
    _exec_query(client, delete_query)


@pytest.fixture()
def _db_with_members(_clean_db, client, kwargs_member, kwargs_member_2):
    for member in [kwargs_member, kwargs_member_2]:
        client.insert(table="members", **member)


@pytest.fixture()
def _db_with_members_full(
    _clean_db,
    client,
    kwargs_member,
    kwargs_member_2,
    kwargs_ban,
    kwargs_pug,
    kwargs_stats,
    kwargs_stats_2,
):
    tables = ["members", "members", "bans", "pugs", "stats_5v5", "stats_5v5"]
    for table, member in zip(
        tables,
        [
            kwargs_member,
            kwargs_member_2,
            kwargs_ban,
            kwargs_pug,
            kwargs_stats,
            kwargs_stats_2,
        ],
    ):
        client.insert(table=table, **member)


@pytest.fixture()
def kwargs_member():
    return {
        "discord_id": 1,
        "name": "fappy",
        "discriminator": "0815",
        "nick": "fappyy",
        "pb_guid": "as2i9adf",
    }


@pytest.fixture()
def kwargs_member_changed():
    return {
        "discord_id": 1,
        "name": "fappy",
        "discriminator": "1111",
        "nick": "fappyy",
        "pb_guid": "as2i9adf",
    }


@pytest.fixture()
def kwargs_member_np_int():
    return {
        "discord_id": np.int64(1),
        "name": "fappy",
        "discriminator": "0815",
        "nick": "fappyy",
        "pb_guid": "as2i9adf",
    }


@pytest.fixture()
def kwargs_member_2():
    return {
        "discord_id": 2,
        "name": "fappy",
        "discriminator": "0816",
        "nick": "fappyyy",
        "pb_guid": "as2i9adg",
    }


@pytest.fixture()
def kwargs_ban():
    return {
        "discord_id": 1,
        "global_ban": True,
        "reason": "Test reason",
        "duration": "4h",
        "created_at": datetime.datetime(2020, 1, 1),
        "expires_at": datetime.datetime(2020, 1, 2),
    }


@pytest.fixture()
def kwargs_pug():
    return {
        "mode": "bo3",
        "size": "5v5",
        "maps": ["Backlot", "Crash", "Strike"],
        "attack": [1, 2, 3, 4, 5],
        "defence": [6, 7, 8, 9, 10],
        "attack_score": 1,
        "defence_score": 0,
    }


@pytest.fixture()
def kwargs_stats():
    return {
        "discord_id": 1,
    }


@pytest.fixture()
def kwargs_stats_np_int():
    return {
        "discord_id": np.int64(1),
    }


@pytest.fixture()
def kwargs_stats_2():
    return {
        "discord_id": 2,
    }


@pytest.fixture(
    params=[
        "discord_id",
        "name",
        "nick",
        "pb_guid",
        "attack",
        "defence",
        "maps",
        "mode",
        "size",
        "result",
    ]
)
def columns(request):
    return request.param


def _exec_query(sql_client: SQLClient, query: str):
    """Execute an SQL query.

    Parameters:
        sql_client : cod4pugbot.db.SQLClient
            SQL client with a connection to the DB.
        query : str
            SQL query.

    """
    queries = [f"{q};" for q in query.split(";") if q]

    with sql_client.connection.cursor() as cursor:
        for subquery in queries:
            cursor.execute(subquery)
