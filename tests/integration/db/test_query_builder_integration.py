import pytest

import cod4pugbot.db._query_builder


@pytest.mark.parametrize(
    "table, return_output, kwargs, expected",
    [
        (
            "test_table",
            False,
            {"test_column_1": "test_value_1"},
            (
                """INSERT INTO "test_table" """
                """("test_column_1") """
                """VALUES """
                """('test_value_1');"""
            ),
        ),
        (
            "test_table",
            False,
            {
                "test_column_1": "test_value_1",
                "test_column_2": "test_value_2",
            },
            (
                """INSERT INTO "test_table" """
                """("test_column_1", "test_column_2") """
                """VALUES """
                """('test_value_1', 'test_value_2');"""
            ),
        ),
        (
            "test_table",
            True,
            {"test_column_1": "test_value_1"},
            (
                """INSERT INTO "test_table" """
                """("test_column_1") """
                """VALUES """
                """('test_value_1') """
                """RETURNING *;"""
            ),
        ),
        (
            "test_table",
            False,
            {"test_column_1": 1},
            (
                """INSERT INTO "test_table" """
                """("test_column_1") """
                """VALUES """
                """(1);"""
            ),
        ),
    ],
)
def test_create_insert_query(db_connection, table, return_output, kwargs, expected):
    query = cod4pugbot.db._query_builder.create_insert_query(
        table, return_output=return_output, **kwargs
    )

    result = query.as_string(db_connection)

    assert result == expected


@pytest.mark.parametrize(
    "table, expected",
    [
        (
            "test_table",
            'SELECT * FROM "test_table" ',
        ),
    ],
)
def test_create_select_query(db_connection, table, expected):
    query = cod4pugbot.db._query_builder.create_select_query(table)

    result = query.as_string(db_connection)

    assert result == expected


@pytest.mark.parametrize(
    "table, values, expected",
    [
        (
            "test_table",
            {"test_column": 1},
            """UPDATE "test_table" SET  "test_column" = 1 """,
        ),
    ],
)
def test_create_update_query(db_connection, table, values, expected):
    query = cod4pugbot.db._query_builder.create_update_query(
        table=table,
        **values,
    )

    result = query.as_string(db_connection)

    assert result == expected


@pytest.mark.parametrize(
    "kwargs, expected",
    [
        (
            {"test_column": "test_value_1"},
            f""" WHERE """,
        ),
        (
            {"discord_id": "test_value_1"},
            f""" WHERE  "discord_id" = 'test_value_1' """,
        ),
        (
            {"discord_id": 1},
            f""" WHERE  "discord_id" = 1 """,
        ),
        (
            {"discord_id": None},
            f""" WHERE  "discord_id" IS NULL """,
        ),
        (
            {
                "discord_id": "test_value_1",
                "name": "test_value_2",
            },
            (
                """ WHERE  "discord_id" = 'test_value_1' """
                """ AND  "name" = 'test_value_2' """
            ),
        ),
        (
            {
                "discord_id": "test_value_1",
                "name": "test_value_2",
                "nick": "test_value_3",
            },
            (
                """ WHERE  "discord_id" = 'test_value_1' """
                """ AND  "name" = 'test_value_2' """
                """ AND  "nick" = 'test_value_3' """
            ),
        ),
    ],
)
def test_create_where_clause(db_connection, kwargs, expected):
    clause = cod4pugbot.db._query_builder.create_where_clause(**kwargs)

    result = clause.as_string(db_connection)

    assert result == expected
