import datetime
import functools

import pytest

from cod4pugbot.db import repository
from cod4pugbot.domain.model import Member

member_1 = functools.partial(
    Member,
    discord_id=1,
    name="fappy",
    discriminator="0815",
    nick="fappyy",
    pb_guid="as2i9adf",
    active=True,
    created_at=datetime.datetime(2020, 1, 1),
)
member_2 = functools.partial(
    Member,
    discord_id=2,
    name="fappy",
    discriminator="0816",
    nick="fappyyy",
    pb_guid="as2i9adg",
    active=True,
    created_at=datetime.datetime(2020, 1, 1),
)


@pytest.fixture()
def members_repository(client):
    return repository.MembersRepository(sql_client=client, called_by_bot=False)


class TestMembersRepository:
    @pytest.mark.parametrize("to_insert", [member_1(), member_2()])
    @pytest.mark.usefixtures("_clean_db")
    def test_add(self, members_repository, to_insert):
        expected = to_insert

        members_repository.add(to_insert)
        result = members_repository.get(expected.identifier)

        assert result == expected

    @pytest.mark.parametrize("to_insert", [member_1(), member_2()])
    @pytest.mark.usefixtures("_clean_db")
    def test_add_and_return_row_id(self, members_repository, to_insert):
        expected_row = to_insert

        result = members_repository.add_and_return_row_id(to_insert)
        result_row = members_repository.get(expected_row.identifier)

        assert isinstance(result, int)
        assert result_row == expected_row

    @pytest.mark.parametrize(
        ("identifiers", "expected"),
        [
            ({"name": "fappy", "discriminator": "0815"}, member_1()),
            ({"name": "fappy", "discriminator": "0816"}, member_2()),
            (member_1().identifier, member_1()),
            (member_2().identifier, member_2()),
        ],
    )
    @pytest.mark.usefixtures("_db_with_members")
    def test_get(self, members_repository, identifiers, expected):
        result = members_repository.get(identifiers)

        assert result == expected

    @pytest.mark.usefixtures("_db_with_members")
    def test_get_all(self, members_repository):
        expected = [member_1(), member_2()]

        result = members_repository.get_all()

        assert result == expected

    @pytest.mark.parametrize(
        "to_update",
        [
            member_1(nick="changed_nick", pb_guid="aaaaaaaa"),
        ],
    )
    @pytest.mark.usefixtures("_db_with_members")
    def test_update(self, members_repository, to_update):
        expected = to_update

        members_repository.update(to_update)
        result = members_repository.get(to_update.to_dict())

        assert result == expected

    @pytest.mark.parametrize(
        ("to_delete", "force"),
        [
            (member_1(), False),
            (member_2(), False),
            (member_1(), True),
            (member_2(), True),
        ],
    )
    @pytest.mark.usefixtures("_db_with_members")
    def test_delete(self, members_repository, to_delete, force):
        members_repository.delete(to_delete, force=force)
        result = [
            member for member in members_repository.get_all() if member == to_delete
        ]

        if not force:
            assert len(result) == 1
            assert to_delete in result
            assert not result[0].active
        else:
            assert not result
