import configparser
from contextlib import nullcontext as doesnotraise
from unittest import mock

import pytest

import cod4pugbot.db


class TestSQLClient:
    @pytest.mark.parametrize(
        "d,expected",
        [
            [{"a": {"b": "c"}}, ValueError()],
            [{"db_info": {"b": "c"}}, ValueError()],
            [{"db_info": {"user": "a"}}, ValueError()],
            [{"db_info": {"user": "a", "database": "b"}}, ValueError()],
            [{"db_info": {"user": "a", "database": "b", "host": "c"}}, ValueError()],
            [
                {"db_info": {"user": "a", "database": "b", "host": "c", "port": "d"}},
                ValueError(),
            ],
            [
                {
                    "db_info": {
                        "user": "a",
                        "database": "b",
                        "host": "c",
                        "port": "d",
                        "password": "e",
                    }
                },
                None,
            ],
        ],
    )
    def test_from_config(self, client, d, expected, monkeypatch):
        test_path = "test_path/test_config.ini"
        test_config = configparser.ConfigParser()
        for key, value in d.items():
            test_config[key] = value
        monkeypatch.setattr(
            configparser.ConfigParser,
            "read",
            read := mock.MagicMock(),
        )
        with mock.patch("configparser.ConfigParser", return_value=test_config):
            context = (
                pytest.raises(type(expected))
                if isinstance(expected, Exception)
                else doesnotraise()
            )

            with context:
                result = client.from_config(test_path)

                assert isinstance(result, cod4pugbot.db.client.SQLClient)
                read.assert_called_once_with(test_path)

    @pytest.mark.parametrize(
        "table,kwargs,insert,delete",
        [
            ("members", "kwargs_member", False, True),
            ("members", "kwargs_member", True, False),
            ("members", "kwargs_member_np_int", False, True),
            ("members", "kwargs_member_np_int", True, False),
        ],
    )
    @pytest.mark.usefixtures("_db_with_members_full")
    def test_set_active_status(self, request, client, table, kwargs, insert, delete):
        kwargs_dict = request.getfixturevalue(kwargs)
        assert client._set_active_status(
            table=table, insert=insert, delete=delete, **kwargs_dict
        )

        result = client.select(view=f"{table}_print", **kwargs_dict)

        if delete:
            assert not result[0]["active"]
        elif insert:
            assert result[0]["active"]

    @pytest.mark.parametrize(
        "table,kwargs",
        [
            ("bans", "kwargs_ban"),
            ("pugs", "kwargs_pug"),
            ("stats_5v5", "kwargs_stats"),
            ("stats_5v5", "kwargs_stats_2"),
            ("stats_5v5", "kwargs_stats_np_int"),
        ],
    )
    @pytest.mark.usefixtures("_db_with_members")
    def test_insert(self, request, client, table, kwargs):
        client.insert(table=table, **request.getfixturevalue(kwargs))

    @pytest.mark.parametrize(
        "view,subtable,kwargs",
        [
            ("members_print", None, "kwargs_member"),
            ("bans_print", None, "kwargs_ban"),
            ("pugs_print", None, "kwargs_pug"),
            ("stats_print", "5v5", "kwargs_stats"),
            ("stats_5v5", None, "kwargs_stats_np_int"),
        ],
    )
    @pytest.mark.usefixtures("_db_with_members_full")
    def test_select(self, request, client, view, subtable, kwargs):
        assert client.select(
            view=view, subtable=subtable, **request.getfixturevalue(kwargs)
        )

    @pytest.mark.parametrize(
        "table,view,before,after,called_by_bot_",
        [
            ("members", "members_print", "kwargs_member", "kwargs_member", True),
            (
                "members",
                "members_print",
                "kwargs_member",
                "kwargs_member_changed",
                True,
            ),
            ("bans", "bans_print", "kwargs_ban", "kwargs_ban", True),
            ("pugs", "pugs_print", "kwargs_pug", "kwargs_pug", True),
            ("stats_5v5", "stats_5v5", "kwargs_stats", "kwargs_stats", True),
            ("stats_5v5", "stats_5v5", "kwargs_stats", "kwargs_stats_np_int", True),
        ],
    )
    @pytest.mark.usefixtures("_db_with_members_full")
    def test_update(
        self,
        request,
        client,
        table,
        view,
        before,
        after,
        called_by_bot_,
    ):
        before = request.getfixturevalue(before)
        expected = request.getfixturevalue(after)
        with pytest.raises(Exception) if (
            not called_by_bot_ and table == "stats_5v5"
        ) else doesnotraise():
            client.update(
                table=table, where=before, called_by_bot=called_by_bot_, **expected
            )

            [result] = client.select(view=view, **expected)

            for key in result:
                if key in expected:
                    assert result[key] == expected[key]

    @pytest.mark.parametrize(
        "table,kwargs,expected,force",
        [
            ("members", "kwargs_member", True, False),
            ("members", "kwargs_member_np_int", True, False),
            ("members", "kwargs_member_2", True, False),
            ("members", "kwargs_member_2", True, True),
            ("bans", "kwargs_ban", True, False),
            ("pugs", "kwargs_pug", True, False),
            ("stats_5v5", "kwargs_stats", True, False),
            ("members", "kwargs_member", True, True),
        ],
    )
    @pytest.mark.usefixtures("_db_with_members_full")
    def test_delete(self, request, client, table, kwargs, expected, force):
        client.delete(table=table, force=force, **request.getfixturevalue(kwargs))
