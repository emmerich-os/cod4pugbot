import dataclasses

import pytest

import cod4pugbot.domain.features.pugs.cod4


@dataclasses.dataclass
class Bot:
    pass


@dataclasses.dataclass
class Cog:
    bot: Bot = Bot()


@dataclasses.dataclass
class PUG:
    cog: Cog = Cog()
    size: str = "5v5"


@pytest.fixture()
def stats_tracker(db_info):
    stats_tracker_ = cod4pugbot.cod4.utils.StatsTracker(pug=PUG())
    stats_tracker_.pug.cog.bot.db_info = db_info
    return stats_tracker_
