import dataclasses

import pytest

import cod4pugbot.domain.features.pugs.cod4


@dataclasses.dataclass(eq=False)
class Player(cod4pugbot.cod4.utils.PromodPlayer):
    name: str = None
    team: str = None
    pb_guid: str = None
    score: int = 0
    kills: int = 0
    assists: int = 0
    deaths: int = 0
    prev_score: int = 0
    prev_kills: int = 0
    prev_assists: int = 0
    prev_deaths: int = 0
    evaluated_kills: int = 0
    evaluated_assists: int = 0
    evaluated_deaths: int = 0
    evaluated_score: int = 0
    opening_kills: int = 0
    headshot_kills: int = 0
    opening_deaths: int = 0
    rifle_kills: int = 0
    smg_kills: int = 0
    sniper_kills: int = 0
    pistol_kills: int = 0
    shotgun_kills: int = 0
    nade_kills: int = 0
    suicides: int = 0
    team_kills: int = 0
    bomb_plants: int = 0
    bomb_defuses: int = 0
    other: int = 0
    rounds_played: int = 0
    impact_kills: int = 0
    round_wins: int = 0
    rounds_converted: int = 0
    opening_kills_converted: int = 0
    round_losses: int = 0
    opening_deaths_converted: int = 0
    one_kills: int = 0
    two_kills: int = 0
    three_kills: int = 0
    four_kils: int = 0
    five_kills: int = 0
    aliases: list = None


@dataclasses.dataclass(eq=False)
class Assistant:
    player: str


@dataclasses.dataclass(eq=False)
class BombExploded(cod4pugbot.cod4.scorebot.BombExploded):
    name: str = "bomb_exploded"


@dataclasses.dataclass(eq=False)
class DefusedBy(cod4pugbot.cod4.scorebot.DefusedBy):
    name: str = "defused_by"
    player: str = None


@dataclasses.dataclass(eq=False)
class Kill(cod4pugbot.cod4.scorebot.Kill):
    name: str = "kill"
    killer: str = None
    weapon: str = None
    killed: str = None
    headshot: bool = None
    assistant: str = None


@dataclasses.dataclass(eq=False)
class Kniferound(cod4pugbot.cod4.scorebot.KnifeRound):
    name: str = "knife_round"


@dataclasses.dataclass(eq=False)
class Map(cod4pugbot.cod4.scorebot.Map):
    name: str = "map"
    mapname: str = None
    gametype: str = None


@dataclasses.dataclass(eq=False)
class MapComplete(cod4pugbot.cod4.scorebot.MapComplete):
    name: str = "map_complete"
    attack_score: int = None
    defence_score: int = None


@dataclasses.dataclass(eq=False)
class PlantedBy(cod4pugbot.cod4.scorebot.PlantedBy):
    name: str = "planted_by"
    player: str = None


@dataclasses.dataclass(eq=False)
class RoundWinner(cod4pugbot.cod4.scorebot.RoundWinner):
    name: str = "round_winner"
    winner: str = None
    attack_score: int = None
    defence_score: int = None


@dataclasses.dataclass(eq=False)
class StartText(cod4pugbot.cod4.scorebot.StartText):
    name: str = "start_text"
    round: int = None


@pytest.fixture()
def player():
    return Player(
        name="test_player", aliases=["test_player"], team="attack", pb_guid="as2i9adf"
    )


@pytest.fixture()
def player_2():
    return Player(
        name="test_killed_player",
        aliases=["test_killed_player"],
        team="defence",
        pb_guid="as2i9adg",
    )


@pytest.fixture()
def player_2_same_team():
    return Player(
        name="test_killed_player_same_team",
        aliases=["test_killed_player_same_team"],
        team="attack",
        pb_guid="as2i9adg",
    )


STATS_DICT = {
    "assists": 0,
    "bomb_defuses": 0,
    "bomb_plants": 0,
    "car_kills": 0,
    "deaths": 0,
    "discord_id": 1,
    "draws": 0,
    "elo": 1000,
    "five_kills": 0,
    "four_kills": 0,
    "headshot_kills": 0,
    "impact_kills": 0,
    "kills": 0,
    "knife_kills": 0,
    "losses": 0,
    "map_draws": 0,
    "map_losses": 0,
    "map_wins": 0,
    "maps_played": 0,
    "nade_kills": 0,
    "one_kills": 0,
    "opening_deaths": 0,
    "opening_deaths_converted": 0,
    "opening_kills": 0,
    "opening_kills_converted": 0,
    "other": 0,
    "pistol_kills": 0,
    "pugs_played": 0,
    "rifle_kills": 0,
    "round_losses": 0,
    "round_wins": 0,
    "rounds_converted": 0,
    "rounds_played": 0,
    "score": 0,
    "shotgun_kills": 0,
    "smg_kills": 0,
    "sniper_kills": 0,
    "suicides": 0,
    "team_kills": 0,
    "three_kills": 0,
    "two_kills": 0,
    "wins": 0,
}


class TestStatsTracker:
    @pytest.mark.parametrize(
        "killfeed,map_winner,expected",
        [
            # 1. case: 1 kill of player not in stats tracker's players, round and map won
            (
                [
                    StartText(),
                    Kill(
                        killer="test_player",
                        killed="test_killed_player_2",
                        weapon="ak74u_mp",
                    ),
                    RoundWinner(winner="attack"),
                ],
                "attack",
                (
                    STATS_DICT
                    | {
                        "maps_played": 1,
                        "map_wins": 1,
                        "rounds_played": 1,
                        "round_wins": 1,
                    }
                ),
            ),
            # 2. case: 1 kill with ak, round and map not won,
            # thus no impact kills or kills converted
            (
                [
                    StartText(),
                    Kill(
                        killer="test_player",
                        killed="test_killed_player",
                        weapon="ak47_mp",
                    ),
                    RoundWinner(winner="defence"),
                ],
                "defence",
                (
                    STATS_DICT
                    | {
                        "kills": 1,
                        "score": 5,
                        "maps_played": 1,
                        "map_losses": 1,
                        "rounds_played": 1,
                        "round_losses": 1,
                        "rifle_kills": 1,
                        "one_kills": 1,
                        "opening_kills": 1,
                    }
                ),
            ),
            # 3. case: 1 kill with scope, 1 with smg, 1 with shotgun, 1 with pistol each in separate
            # round, rounds and map won,
            # thus impact kills and kills converted
            (
                [
                    StartText(),
                    Kill(
                        killer="test_player",
                        killed="test_killed_player",
                        weapon="m40a3_mp",
                    ),
                    RoundWinner(winner="attack"),
                    StartText(),
                    Kill(
                        killer="test_player",
                        killed="test_killed_player",
                        weapon="ak74u_mp",
                    ),
                    RoundWinner(winner="attack"),
                    StartText(),
                    Kill(
                        killer="test_player",
                        killed="test_killed_player",
                        weapon="winchester1200_mp",
                    ),
                    RoundWinner(winner="attack"),
                    StartText(),
                    Kill(
                        killer="test_player",
                        killed="test_killed_player",
                        weapon="deserteagle_mp",
                    ),
                    RoundWinner(winner="attack"),
                ],
                "attack",
                (
                    STATS_DICT
                    | {
                        "kills": 4,
                        "score": 20,
                        "maps_played": 1,
                        "map_wins": 1,
                        "rounds_played": 4,
                        "round_wins": 4,
                        "smg_kills": 1,
                        "sniper_kills": 1,
                        "shotgun_kills": 1,
                        "pistol_kills": 1,
                        "one_kills": 4,
                        "opening_kills": 4,
                        "opening_kills_converted": 4,
                        "impact_kills": 4,
                        "rounds_converted": 4,
                    }
                ),
            ),
            # 4. case: map tie, round lost, 1 headshot kill, 1 nade kill, 1 knife kill,
            # 1 car kill, 1 other (none), 1 suicide, 1 team kill, 1 bomb plant, 1 bomb defuse,
            # 1 assist, 1 death, opening death (also converted), and thereby 2, 3 and 1k
            (
                [
                    StartText(),
                    Kill(
                        killer="test_player",
                        killed="test_killed_player",
                        weapon="ak47_mp",
                        headshot=True,
                    ),
                    Kill(
                        killer="test_player",
                        killed="test_killed_player",
                        weapon="frag_grenade_mp",
                    ),
                    PlantedBy(player="test_player"),
                    DefusedBy(player="test_player"),
                    RoundWinner(winner="defence"),
                    StartText(),
                    Kill(
                        killer="test_player",
                        killed="test_killed_player",
                        weapon="knife_mp",
                    ),
                    Kill(
                        killer="test_player",
                        killed="test_killed_player",
                        weapon="destructible_car",
                    ),
                    Kill(
                        killer="test_player", killed="test_killed_player", weapon="none"
                    ),
                    Kill(
                        killer="test_player",
                        killed="test_killed_player_same_team",
                        weapon="none",
                    ),
                    RoundWinner(winner="defence"),
                    StartText(),
                    Kill(killer="test_player_2", killed="test_player", weapon="none"),
                    Kill(killer="test_player", killed="test_player", weapon="none"),
                    Kill(
                        killer="test_player_2",
                        killed="test_killed_player",
                        assistant=Assistant(player="test_player"),
                        weapon="none",
                    ),
                    RoundWinner(winner="defence"),
                ],
                "tie",
                (
                    STATS_DICT
                    | {
                        "assists": 1,
                        "bomb_defuses": 1,
                        "bomb_plants": 1,
                        "car_kills": 1,
                        "deaths": 1,
                        "headshot_kills": 1,
                        "kills": 5,
                        "knife_kills": 1,
                        "map_draws": 1,
                        "maps_played": 1,
                        "nade_kills": 1,
                        "opening_deaths": 1,
                        "opening_deaths_converted": 1,
                        "opening_kills": 2,
                        "other": 1,
                        "rifle_kills": 1,
                        "round_losses": 3,
                        "rounds_played": 3,
                        "score": 29,
                        "suicides": 1,
                        "team_kills": 1,
                        "three_kills": 1,
                        "two_kills": 1,
                    }
                ),
            ),
            # 5. case: 4k, 5k
            (
                [
                    StartText(),
                    Kill(
                        killer="test_player", killed="test_killed_player", weapon="none"
                    ),
                    Kill(
                        killer="test_player", killed="test_killed_player", weapon="none"
                    ),
                    Kill(
                        killer="test_player", killed="test_killed_player", weapon="none"
                    ),
                    Kill(
                        killer="test_player", killed="test_killed_player", weapon="none"
                    ),
                    RoundWinner(winner="attack"),
                    StartText(),
                    Kill(
                        killer="test_player", killed="test_killed_player", weapon="none"
                    ),
                    Kill(
                        killer="test_player", killed="test_killed_player", weapon="none"
                    ),
                    Kill(
                        killer="test_player", killed="test_killed_player", weapon="none"
                    ),
                    Kill(
                        killer="test_player", killed="test_killed_player", weapon="none"
                    ),
                    Kill(
                        killer="test_player", killed="test_killed_player", weapon="none"
                    ),
                    RoundWinner(winner="attack"),
                ],
                "attack",
                (
                    STATS_DICT
                    | {
                        "kills": 9,
                        "score": 45,
                        "maps_played": 1,
                        "map_wins": 1,
                        "rounds_played": 2,
                        "round_wins": 2,
                        "four_kills": 1,
                        "five_kills": 1,
                        "opening_kills": 2,
                        "opening_kills_converted": 2,
                        "impact_kills": 9,
                        "rounds_converted": 2,
                        "other": 9,
                    }
                ),
            ),
        ],
    )
    def test_save_stats_to_db(
        self,
        stats_tracker,
        _db_with_members_full,
        client,
        player,
        player_2,
        player_2_same_team,
        killfeed,
        map_winner,
        expected,
    ):
        stats_tracker.players = [
            player,
            player_2,
            player_2_same_team,
        ]

        stats_tracker.save_stats_to_db(killfeed=killfeed, map_winner=map_winner)

        result = client.select(view="stats_5v5", discord_id=1).iloc[0].to_dict()

        assert result == expected
