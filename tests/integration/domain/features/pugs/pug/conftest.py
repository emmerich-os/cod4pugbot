import dataclasses
from unittest import mock

import discord
import pytest
import pytz

import cod4pugbot.domain.features.pugs.pug


async def async_magic():
    return


mock.MagicMock.__await__ = lambda x: async_magic().__await__()


class Message(discord.Message):
    def __init__(self, content=None, embed=None, *args, **kwargs):
        self.id = 6
        self.embed = embed
        self.embeds = [embed]
        self.content = content
        self.args = [*args]
        self.kwargs = {**kwargs}
        self.emojis = []

    def __repr__(self):
        """Name when printing."""
        return (
            f"<{self.__class__.__name__} {self.id=} {self.embed=} {self.embeds=} {self.content=} "
            f"{self.args=} {self.kwargs=}>"
        )

    def __str__(self):
        """Name when formatted to string."""
        return (
            f"<{self.__class__.__name__} {self.id=} {self.embed=} {self.embeds=} {self.content=} "
            f"{self.args=} {self.kwargs=}>"
        )

    async def add_reaction(self, emoji):
        self.emojis.append(emoji)
        return emoji

    async def remove_reaction(self, emoji, member):
        self.emojis.remove(emoji)
        return emoji, member

    async def pin(self):
        return

    async def delete(self, *args, **kwargs):
        return

    async def edit(self, content=None, embed=None, *args, **kwargs):
        self.embed = embed
        self.embeds = [embed]
        self.content = content
        self.args = [*self.args, *args]
        self.kwargs = self.kwargs | kwargs


class History:
    async def __aiter__(self):
        """Return a message for `async for ... in discord.Channel.history()`."""
        yield Message(embed=None)


@dataclasses.dataclass(eq=False)
class Channel(discord.TextChannel):
    id = 5
    args: list
    kwargs: dict
    history = History

    async def send(self, *args, **kwargs):
        return Message(*args, **kwargs)

    async def delete(self, *args, **kwargs):
        return


@dataclasses.dataclass(eq=False)
class Bot(mock.MagicMock):
    user = "test_bot"
    timezone = pytz.timezone("UTC")
    tzinfos = {"CEST": "UTC+2"}
    timestring = "%d/%m/%Y %H:%M:%S %Z"
    channel_ids = {"pugs": {"bo1": {"1v1": 5}}}
    role_ids = {
        "pug_captain": "captain",
        "registered": "registered",
        "global_banned": "global_banned",
        "pug_banned": "pug_banned",
        "unregistered": "unregistered",
    }
    db_info = {"test_key": "test_value"}
    pugs = {
        "ready_up_phase_length": 60,
        "map_elimination": True,
        "maps_1v1": [
            "test_map_1",
            "test_map_2",
            "test_map_3",
            "test_map_4",
            "test_map_5",
        ],
    }
    category_ids = {}

    def get_channel(self, *args, **kwargs):
        args = [*args]
        kwargs = {**kwargs}
        return Channel(args, kwargs)


class Cog(mock.MagicMock):
    active_pugs = cod4pugbot.pug.ActivePUGs()
    bot = Bot()


@dataclasses.dataclass(eq=False)
class Guild(discord.Guild):
    id = 4

    def get_role(self, role_id):
        return role_id


@pytest.fixture()
def message():
    return Message()


@pytest.fixture()
def channel():
    return Channel([], {})


@pytest.fixture()
def cog():
    return Cog()


@pytest.fixture()
def guild():
    return Guild()


@pytest.fixture()
async def open_pug(monkeypatch, cog, guild):
    monkeypatch.setattr(
        cod4pugbot.pug.OpenPUG,
        "_create_pug_message",
        mock.MagicMock(),
    )

    with mock.patch("cod4pugbot.pugs.open.SQLClient") as mock_sqlclient:
        mock_sqlclient.return_value.insert.return_value = 1
        pug = await cod4pugbot.pug.OpenPUG.create(
            cog=cog,
            guild=guild,
            mode="bo1",
            size="1v1",
        )

    pug.messages["pugs"] = Message(
        embed=discord.Embed.from_dict(
            {
                "color": 29951,
                "description": (
                    "React with :white_check_mark: to enter PUG, remove to leave PUG. "
                    "If you go AFK or offline, you will be automatically removed from the PUG. "
                    "Send `!ao` into a different channel to enable AFK/offline protection."
                ),
                "fields": [
                    {
                        "inline": True,
                        "name": "Status",
                        "value": "```css\nOPEN\n```",
                    },
                    {"inline": True, "name": "Slots", "value": "```0/2```"},
                ],
                "footer": {"text": "Last updated: 01/01/2020 00:00 UTC"},
                "title": "1v1 PUG (BO1)",
                "type": "rich",
            }
        ),
    )

    return pug


class Player:
    def __init__(self, nick: str, name: str, roles: list = None):
        self.nick = nick
        self.name = name
        self.mention = f"@{nick}"
        self.roles = roles or []

    def __eq__(self, other):
        """Check if `other.nick` equals `self.nick`."""
        return self.nick == other.nick


@pytest.fixture()
async def pre_match_pug(cog, guild):
    with mock.patch("cod4pugbot.pugs.PreMatchPUG._create_match_channel"), mock.patch(
        "cod4pugbot.pugs.PreMatchPUG._create_match_message"
    ), mock.patch("cod4pugbot.pugs.PreMatchPUG._pick_captains"), mock.patch(
        "cod4pugbot.pugs.PreMatchPUG._remove_players_from_open_pugs"
    ):
        pug = await cod4pugbot.pug.PreMatchPUG.create(
            cog=cog,
            guild=guild,
            pug_id=2,
            mode="bo1",
            size="1v1",
            playerlist=[Player(nick="test_nick", name="test_name")],
        )

    return pug


@pytest.fixture(params=[True, False])
def map_elimination(request):
    return request.param
