from unittest import mock

import pytest

import cod4pugbot.db
import cod4pugbot.domain.features.pugs.pug


class Player:
    def __init__(self, nick: str, name: str):
        self.nick = nick
        self.name = name
        self.mention = f"@{nick}"

    def __eq__(self, other):
        """Check if `other.nick` equals `self.nick`."""
        return self.nick == other.nick


async def async_magic():
    return


mock.MagicMock.__await__ = lambda x: async_magic().__await__()


TIME = "2020-01-01 00:00"


def _pug_message_embed_dict(fields: list, ready_up: bool = False):
    return {
        "color": 29951 if not ready_up else 16741376,
        "description": (
            "React with :white_check_mark: to enter PUG, remove to leave PUG. "
            "If you go AFK or offline, you will be automatically removed from the PUG. "
            "Send `!ao` into a different channel to enable AFK/offline protection."
        ),
        "fields": [
            {
                "inline": True,
                "name": "Status",
                "value": "```css\nOPEN\n```"
                if not ready_up
                else "```ARM\nREADYUP\n```",
            },
            *fields,
        ],
        "footer": {"text": "Last updated: 01/01/2020 00:00 UTC"},
        "title": "1v1 PUG (BO1)",
        "type": "rich",
    }


def _ready_up_message(add: str = None):
    message = (
        """> **READY-UP phase started.**\n"""
        """> React with :white_check_mark: to __**ready up**__.\n"""
        """> React with :no_entry: to __**abort**__.\n"""
        """> Duration of ready-up phase is """
        """60 seconds.\n"""
    )
    if add is not None:
        message += add
    return message


class TestOpenPUG:
    @pytest.mark.asyncio()
    async def test_create(self, monkeypatch, cog, guild):
        monkeypatch.setattr(
            cod4pugbot.pug.OpenPUG,
            "_create_pug_message",
            _create_pug_message := mock.MagicMock(),
        )
        monkeypatch.setattr(
            cod4pugbot.pug.OpenPUG,
            "add_player",
            mock.MagicMock(),
        )

        with mock.patch("cod4pugbot.pugs.open.SQLClient") as mock_sqlclient:
            mock_sqlclient.return_value.insert.return_value = 1
            result = await cod4pugbot.pug.OpenPUG.create(
                cog=cog,
                guild=guild,
                mode="bo1",
                size="1v1",
            )

        assert isinstance(result, cod4pugbot.pug.OpenPUG)
        assert result.id == 1
        assert result.mode == "bo1"
        assert result.size == "1v1"
        assert cog.active_pugs.pugs.get(1) == result
        _create_pug_message.assert_called_once()

    @pytest.mark.parametrize(
        "member,playerlist,playerqueue,status",
        [
            (
                Player(nick="test_nick", name="test_name"),
                [],
                [],
                "open",
            ),
            (
                Player(nick="test_nick", name="test_name"),
                [Player(nick="test_nick", name="test_name")],
                [],
                "open",
            ),
            (
                Player(nick="test_nick", name="test_name"),
                [
                    Player(nick="test_nick_1", name="test_name_1"),
                ],
                [],
                "open",
            ),
            (
                Player(nick="test_nick", name="test_name"),
                [],
                [Player(nick="test_nick", name="test_name")],
                "readyup",
            ),
            (
                Player(nick="test_nick", name="test_name"),
                [
                    Player(nick="test_nick_1", name="test_name_1"),
                    Player(nick="test_nick_2", name="test_name_2"),
                ],
                [],
                "readyup",
            ),
            (
                Player(nick="test_nick", name="test_name"),
                [
                    Player(nick="test_nick_1", name="test_name_1"),
                    Player(nick="test_nick_2", name="test_name_2"),
                ],
                [Player(nick="test_nick_3", name="test_name_3")],
                "readyup",
            ),
            (
                Player(nick="test_nick", name="test_name"),
                [
                    Player(nick="test_nick_1", name="test_name_1"),
                    Player(nick="test_nick_2", name="test_name_2"),
                ],
                [
                    Player(nick="test_nick_3", name="test_name_3"),
                    Player(nick="test_nick_4", name="test_name_4"),
                ],
                "readyup",
            ),
            (
                Player(nick="test_nick", name="test_name"),
                [
                    Player(nick="test_nick_1", name="test_name_1"),
                    Player(nick="test_nick_2", name="test_name_2"),
                ],
                [
                    Player(nick="test_nick", name="test_name"),
                ],
                "readyup",
            ),
        ],
    )
    @pytest.mark.asyncio()
    async def test_add_player(
        self, monkeypatch, open_pug, member, playerlist, playerqueue, status
    ):
        monkeypatch.setattr(
            cod4pugbot.pug.OpenPUG,
            "_modify_pug_message",
            _modify_pug_message := mock.MagicMock(),
        )
        monkeypatch.setattr(
            cod4pugbot.pug.OpenPUG,
            "_start_ready_up_phase",
            _start_ready_up_phase := mock.MagicMock(),
        )
        open_pug.playerlist = playerlist.copy()
        open_pug.playerqueue = playerqueue.copy()
        open_pug.status = status

        await open_pug.add_player(member=member)

        if member in playerlist:
            assert member in open_pug.playerlist
            assert member not in open_pug.playerqueue
            _modify_pug_message.assert_not_called()
            _start_ready_up_phase.assert_not_called()
        elif member in playerqueue:
            if status == "open":
                assert member not in open_pug.playerqueue
                assert member in open_pug.playerlist
            elif status == "readyup":
                assert member in open_pug.playerqueue
                assert member not in open_pug.playerlist
                _modify_pug_message.assert_not_called()
                _start_ready_up_phase.assert_not_called()
        else:
            if len(playerlist) + 1 > 2:
                assert member not in open_pug.playerlist
                assert member in open_pug.playerqueue
            else:
                assert member in open_pug.playerlist
                assert member not in open_pug.playerqueue
            _modify_pug_message.assert_called_once()
            if status == "open" and len(playerlist) + 1 >= 2:
                _start_ready_up_phase.assert_called_once()

    @pytest.mark.parametrize(
        "member, playerlist,playerqueue",
        [
            (
                Player(nick="test_nick", name="test_name"),
                [],
                [],
            ),
            (
                Player(nick="test_nick", name="test_name"),
                [Player(nick="test_nick", name="test_name")],
                [],
            ),
            (
                Player(nick="test_nick", name="test_name"),
                [],
                [Player(nick="test_nick", name="test_name")],
            ),
        ],
    )
    @pytest.mark.asyncio()
    async def test_remove_player(
        self, monkeypatch, open_pug, member, playerlist, playerqueue
    ):
        monkeypatch.setattr(
            cod4pugbot.pug.OpenPUG,
            "_modify_pug_message",
            _modify_pug_message := mock.MagicMock(),
        )
        member = Player(nick="test_nick", name="test_name")
        open_pug.playerlist = playerlist.copy()
        open_pug.playerqueue = playerqueue.copy()

        await open_pug.remove_player(member=member)

        if member in playerlist or member in playerqueue:
            _modify_pug_message.assert_called_once()
            if member in playerlist:
                assert member not in open_pug.playerlist
            elif member in playerqueue:
                assert member not in open_pug.playerqueue
        else:
            _modify_pug_message.assert_not_called()

    @pytest.mark.parametrize(
        "members,aborted_by_bot,not_ready,ready,playerlist,playerqueue,expected",
        [
            (
                None,
                False,
                [Player(nick="test_nick_1", name="test_name_1")],
                [Player(nick="test_nick_2", name="test_name_2")],
                [
                    Player(nick="test_nick_1", name="test_name_1"),
                    Player(nick="test_nick_2", name="test_name_2"),
                ],
                [],
                "> READY-UP phase aborted. @test_nick_1 was not ready.",
            ),
            (
                None,
                False,
                [
                    Player(nick="test_nick_1", name="test_name_1"),
                    Player(nick="test_nick_2", name="test_name_2"),
                ],
                [],
                [
                    Player(nick="test_nick_1", name="test_name_1"),
                    Player(nick="test_nick_2", name="test_name_2"),
                ],
                [],
                "> READY-UP phase aborted. @test_nick_1, @test_nick_2 were not ready.",
            ),
            (
                [Player(nick="test_nick_1", name="test_name_1")],
                True,
                [],
                [],
                [
                    Player(nick="test_nick_1", name="test_name_1"),
                    Player(nick="test_nick_2", name="test_name_2"),
                ],
                [],
                "> READY-UP phase aborted. @test_nick_1 is already in a running PUG.",
            ),
            (
                [
                    Player(nick="test_nick_1", name="test_name_1"),
                    Player(nick="test_nick_2", name="test_name_2"),
                ],
                True,
                [],
                [],
                [
                    Player(nick="test_nick_1", name="test_name_1"),
                    Player(nick="test_nick_2", name="test_name_2"),
                ],
                [],
                (
                    "> READY-UP phase aborted. @test_nick_1, @test_nick_2 "
                    "are already in a running PUG."
                ),
            ),
        ],
    )
    @pytest.mark.asyncio()
    async def test_abort_ready_up_phase(
        self,
        monkeypatch,
        open_pug,
        message,
        members,
        aborted_by_bot,
        not_ready,
        ready,
        playerlist,
        playerqueue,
        expected,
    ):
        monkeypatch.setattr(
            cod4pugbot.pug.OpenPUG,
            "add_player",
            add_player := mock.MagicMock(),
        )
        monkeypatch.setattr(
            cod4pugbot.pug.OpenPUG,
            "remove_player",
            remove_player := mock.MagicMock(),
        )
        open_pug.not_ready = not_ready.copy()
        open_pug.ready = ready.copy()
        open_pug.playerlist = playerlist.copy()
        open_pug.playerqueue = playerqueue.copy()
        open_pug.messages["readyup"] = message

        await open_pug.abort_ready_up_phase(
            members=members,
            aborted_by_bot=aborted_by_bot,
        )
        result = open_pug.messages["readyup"].content

        assert result == expected
        if members is None:
            assert remove_player.call_count == len(not_ready)
        else:
            assert remove_player.call_count == len(members)
        assert not open_pug.ready
        assert open_pug.not_ready == playerlist
        assert add_player.call_count == len(playerqueue)

    @pytest.mark.parametrize(
        "member,playerlist,ready,not_ready",
        [
            (
                Player(nick="test_nick_1", name="test_name_1"),
                [
                    Player(nick="test_nick_1", name="test_name_1"),
                    Player(nick="test_nick_2", name="test_name_2"),
                ],
                [],
                [
                    Player(nick="test_nick_1", name="test_name_1"),
                    Player(nick="test_nick_2", name="test_name_2"),
                ],
            ),
            (
                Player(nick="test_nick_1", name="test_name_1"),
                [
                    Player(nick="test_nick_1", name="test_name_1"),
                    Player(nick="test_nick_2", name="test_name_2"),
                ],
                [Player(nick="test_nick_2", name="test_name_2")],
                [Player(nick="test_nick_1", name="test_name_1")],
            ),
            (
                Player(nick="test_nick_3", name="test_name_3"),
                [
                    Player(nick="test_nick_1", name="test_name_1"),
                    Player(nick="test_nick_2", name="test_name_2"),
                ],
                [],
                [
                    Player(nick="test_nick_1", name="test_name_1"),
                    Player(nick="test_nick_2", name="test_name_2"),
                ],
            ),
            (
                Player(nick="test_nick_1", name="test_name_1"),
                [
                    Player(nick="test_nick_1", name="test_name_1"),
                    Player(nick="test_nick_2", name="test_name_2"),
                ],
                [Player(nick="test_nick_1", name="test_name_1")],
                [Player(nick="test_nick_2", name="test_name_2")],
            ),
        ],
    )
    @pytest.mark.asyncio()
    async def test_ready_player(
        self, monkeypatch, open_pug, member, playerlist, ready, not_ready
    ):
        monkeypatch.setattr(
            cod4pugbot.pug.OpenPUG,
            "_start_pug",
            _start_pug := mock.MagicMock(),
        )
        monkeypatch.setattr(
            cod4pugbot.pug.OpenPUG,
            "_modify_ready_up_message",
            _modify_ready_up_message := mock.MagicMock(),
        )
        open_pug.playerlist = playerlist.copy()
        open_pug.ready = ready.copy()
        open_pug.not_ready = not_ready.copy()

        await open_pug.ready_player(member=member)

        if member in playerlist and member in not_ready:
            assert member in open_pug.ready
            assert member not in open_pug.not_ready
            assert member in open_pug.playerlist
            if member is not None and len(ready) == 1:
                _start_pug.assert_called_once()
            else:
                _modify_ready_up_message.assert_called_once()
        elif member in playerlist and member in ready:
            assert member in open_pug.playerlist
            assert member in open_pug.ready
            assert member not in open_pug.not_ready
        else:
            assert member not in open_pug.playerlist
            assert member not in open_pug.ready
            assert member not in open_pug.not_ready

    @pytest.mark.parametrize(
        "member,playerlist,ready,not_ready",
        [
            (
                Player(nick="test_nick_1", name="test_name_1"),
                [
                    Player(nick="test_nick_1", name="test_name_1"),
                    Player(nick="test_nick_2", name="test_name_2"),
                ],
                [Player(nick="test_nick_1", name="test_name_1")],
                [Player(nick="test_nick_2", name="test_name_2")],
            ),
            (
                Player(nick="test_nick_1", name="test_name_1"),
                [
                    Player(nick="test_nick_1", name="test_name_1"),
                    Player(nick="test_nick_2", name="test_name_2"),
                ],
                [Player(nick="test_nick_2", name="test_name_2")],
                [Player(nick="test_nick_1", name="test_name_1")],
            ),
            (
                Player(nick="test_nick_3", name="test_name_3"),
                [
                    Player(nick="test_nick_1", name="test_name_1"),
                    Player(nick="test_nick_2", name="test_name_2"),
                ],
                [],
                [
                    Player(nick="test_nick_1", name="test_name_1"),
                    Player(nick="test_nick_2", name="test_name_2"),
                ],
            ),
            (
                Player(nick="test_nick_2", name="test_name_2"),
                [
                    Player(nick="test_nick_1", name="test_name_1"),
                    Player(nick="test_nick_2", name="test_name_2"),
                ],
                [],
                [Player(nick="test_nick_2", name="test_name_2")],
            ),
        ],
    )
    @pytest.mark.asyncio()
    async def test_unready_player(
        self, monkeypatch, open_pug, member, playerlist, ready, not_ready
    ):
        monkeypatch.setattr(
            cod4pugbot.pug.OpenPUG,
            "_modify_ready_up_message",
            _modify_ready_up_message := mock.MagicMock(),
        )
        open_pug.playerlist = playerlist.copy()
        open_pug.ready = ready.copy()
        open_pug.not_ready = not_ready.copy()

        await open_pug.unready_player(member=member)

        if member in playerlist and member in ready:
            assert member in open_pug.not_ready
            assert member not in open_pug.ready
            assert member in open_pug.playerlist
            _modify_ready_up_message.assert_called_once()
        elif member in playerlist and member in not_ready:
            assert member in open_pug.playerlist
            assert member not in open_pug.ready
            assert member in open_pug.not_ready
        else:
            assert member not in open_pug.playerlist
            assert member not in open_pug.ready
            assert member not in open_pug.not_ready
