import asyncio
import dataclasses
import datetime
import itertools
from unittest import mock

import discord
import freezegun
import pytest
import pytz

import cod4pugbot.domain.features.pugs.pug


@dataclasses.dataclass(eq=False)
class Category(discord.CategoryChannel):
    name: str
    id: str

    def __eq__(self, other):
        """Check if `self.name` is equal to `other.name`."""
        return (
            self.name == other.name if isinstance(other, Player) else self.name == other
        )

    def __hash__(self):
        """Hash name."""
        return hash(self.name)


class Player:
    def __init__(self, nick: str, name: str, roles: list = None):
        self.nick = nick
        self.name = name
        self.mention = f"@{nick}"
        self.roles = roles or []

    def __eq__(self, other):
        """Check if `other.nick` equals `self.nick`."""
        return (
            self.nick == other.nick if isinstance(other, Player) else self.nick == other
        )

    def __hash__(self):
        """Hash nick."""
        return hash(self.nick)

    def __repr__(self):
        """Name when printing."""
        return f"<{self.__class__.__name__} {self.nick=} {self.name=} {self.roles=}>"

    def __str__(self):
        """Name when formatted to string."""
        return f"<{self.__class__.__name__} {self.nick=} {self.name=} {self.roles=}>"


@dataclasses.dataclass(eq=False)
class Reaction(discord.Reaction):
    message: dict
    emoji: str

    async def remove(self, user):
        return


@dataclasses.dataclass(eq=False)
class Message(discord.Message):
    id: int
    author: Player
    mentions: list = None
    content: str = None

    def __eq__(self, other):
        """Check if self.id is equal to other.id."""
        return self.id == other.id


@dataclasses.dataclass(eq=False)
class ReactionWithMessage(discord.Reaction):
    message: Message
    emoji: str

    async def remove(self, user):
        return


@dataclasses.dataclass(eq=False)
class PUG:
    status: str
    playerlist: list
    playerqueue: list


async def async_magic():
    return


mock.MagicMock.__await__ = lambda x: async_magic().__await__()

TIME = "2020-01-01 00:00"


class TestPreMatchPUG:
    @freezegun.freeze_time(TIME)
    @pytest.mark.parametrize(
        "pug_id,mode,size,playerlist",
        [
            (
                1,
                "bo1",
                "1v1",
                [
                    Player(
                        nick="test_nick_1",
                        name="test_name_1",
                        roles=["member", "captain"],
                    ),
                    Player(nick="test_nick_2", name="test_name_2", roles=["member"]),
                ],
            ),
        ],
    )
    @pytest.mark.asyncio()
    async def test_create(
        self, monkeypatch, cog, guild, pug_id, mode, size, playerlist
    ):
        monkeypatch.setattr(
            cod4pugbot.pug.PreMatchPUG,
            "_create_match_channel",
            _create_match_channel := mock.MagicMock(),
        )
        monkeypatch.setattr(
            cod4pugbot.pug.PreMatchPUG,
            "_remove_players_from_open_pugs",
            _remove_players_from_open_pugs := mock.MagicMock(),
        )
        monkeypatch.setattr(
            cod4pugbot.pug.PreMatchPUG,
            "_create_match_message",
            _create_match_message := mock.MagicMock(),
        )
        monkeypatch.setattr(
            cod4pugbot.pug.PreMatchPUG,
            "_pick_captains",
            _pick_captains := mock.MagicMock(),
        )
        result = await cod4pugbot.pug.PreMatchPUG.create(
            cog=cog,
            guild=guild,
            pug_id=pug_id,
            mode=mode,
            size=size,
            playerlist=playerlist,
        )

        assert isinstance(result, cod4pugbot.pug.PreMatchPUG)
        _create_match_channel.assert_called_once()
        _remove_players_from_open_pugs.assert_called_once()
        _create_match_message.assert_called_once()
        assert result.cog == cog
        assert result.guild == guild
        assert result.id == 1
        assert result.mode == mode
        assert result.size == size
        assert result.playerlist == playerlist
        assert result.captains_pool == playerlist[:-1]
        assert result.max_time == datetime.datetime(
            2020, 1, 1, 0, 15, tzinfo=pytz.timezone("UTC")
        )
        _pick_captains.assert_called_once()

    @pytest.mark.parametrize(
        "picker,delete_channel_delay,expected",
        [
            (
                Player(nick="test_nick", name="test_name"),
                60,
                (
                    """> PUG has expired.\n"""
                    """> @test_nick did not pick in time.\n"""
                    """> This channel will be deleted in 1 minute."""
                ),
            ),
            (
                Player(nick="test_nick_1", name="test_name_1"),
                120,
                (
                    """> PUG has expired.\n"""
                    """> @test_nick_1 did not pick in time.\n"""
                    """> This channel will be deleted in 2 minutes."""
                ),
            ),
        ],
    )
    @pytest.mark.asyncio()
    async def test_end_pug(
        self,
        monkeypatch,
        pre_match_pug,
        channel,
        picker,
        delete_channel_delay,
        expected,
    ):
        monkeypatch.setattr(
            cod4pugbot.pug.PreMatchPUG,
            "_delete_remaining_messages",
            _delete_remaining_messages := mock.MagicMock(),
        )
        monkeypatch.setattr(
            cod4pugbot.pug.ActivePUGs,
            "remove_pug",
            remove_pug := mock.MagicMock(),
        )
        monkeypatch.setattr(
            cod4pugbot.pug.PreMatchPUG,
            "_modify_match_message",
            _modify_match_message := mock.MagicMock(),
        )
        monkeypatch.setattr(
            channel,
            "send",
            send := mock.MagicMock(),
        )
        monkeypatch.setattr(
            asyncio,
            "sleep",
            sleep := mock.MagicMock(),
        )
        pre_match_pug.channel = channel
        pre_match_pug.picker = picker
        pre_match_pug.delete_channel_delay = delete_channel_delay

        await pre_match_pug.end_pug()

        _delete_remaining_messages.assert_called_once()
        remove_pug.assert_called_once_with(pug_id=2)
        send.assert_called_once()
        assert len(send.call_args_list) == 1
        args, _ = send.call_args_list[0]
        assert len(args) == 1
        assert args[0] == expected
        _modify_match_message.assert_called_once()
        sleep.assert_called_once_with(delete_channel_delay)

    @pytest.mark.parametrize(
        "phase,picker,captain,reaction,add_message_to_pug,playerlist",
        [
            # 1. case: wrong phase
            (
                "readyup",
                Player(nick="test_nick_1", name="test_name_1"),
                Player(nick="test_nick_1", name="test_name_1"),
                ReactionWithMessage(
                    message=Message(
                        id=1,
                        author=Player(nick="test_bot", name="test_bot"),
                        mentions=[Player(nick="test_nick_3", name="test_name_3")],
                    ),
                    emoji="\u2705",
                ),
                True,
                [Player(nick="test_nick_3", name="test_name_3")],
            ),
            # 2. case: captain not allowed to pick
            (
                "player pick",
                Player(nick="test_nick_1", name="test_name_1"),
                Player(nick="test_nick_2", name="test_name_2"),
                ReactionWithMessage(
                    message=Message(
                        id=1,
                        author=Player(nick="test_bot", name="test_bot"),
                        mentions=[Player(nick="test_nick_3", name="test_name_3")],
                    ),
                    emoji="\u2705",
                ),
                True,
                [Player(nick="test_nick_3", name="test_name_3")],
            ),
            # 2. case: captain not allowed to pick
            (
                "player pick",
                Player(nick="test_nick_2", name="test_name_2"),
                Player(nick="test_nick_1", name="test_name_1"),
                ReactionWithMessage(
                    message=Message(
                        id=1,
                        author=Player(nick="test_bot", name="test_bot"),
                        mentions=[Player(nick="test_nick_3", name="test_name_3")],
                    ),
                    emoji="\u2705",
                ),
                True,
                [Player(nick="test_nick_3", name="test_name_3")],
            ),
            # 3. case: message not a message from the Bot
            (
                "player pick",
                Player(nick="test_nick_1", name="test_name_1"),
                Player(nick="test_nick_1", name="test_name_1"),
                ReactionWithMessage(
                    message=Message(
                        id=1,
                        author=Player(nick="test_author", name="test_author"),
                        mentions=[Player(nick="test_nick_3", name="test_name_3")],
                    ),
                    emoji="\u2705",
                ),
                True,
                [Player(nick="test_nick_3", name="test_name_3")],
            ),
            # 4. case: wrong emoji
            (
                "player pick",
                Player(nick="test_nick_1", name="test_name_1"),
                Player(nick="test_nick_1", name="test_name_1"),
                ReactionWithMessage(
                    message=Message(
                        id=1,
                        author=Player(nick="test_author", name="test_author"),
                        mentions=[Player(nick="test_nick_3", name="test_name_3")],
                    ),
                    emoji="\u26d4",
                ),
                False,
                [Player(nick="test_nick_3", name="test_name_3")],
            ),
            # 6. case: message not in bot messages
            (
                "player pick",
                Player(nick="test_nick_1", name="test_name_1"),
                Player(nick="test_nick_1", name="test_name_1"),
                ReactionWithMessage(
                    message=Message(
                        id=1,
                        author=Player(nick="test_bot", name="test_bot"),
                        mentions=[Player(nick="test_nick_3", name="test_name_3")],
                    ),
                    emoji="\u2705",
                ),
                False,
                [Player(nick="test_nick_4", name="test_name_4")],
            ),
            # 7. case: player not in playerlist
            (
                "player pick",
                Player(nick="test_nick_1", name="test_name_1"),
                Player(nick="test_nick_1", name="test_name_1"),
                ReactionWithMessage(
                    message=Message(
                        id=1,
                        author=Player(nick="test_bot", name="test_bot"),
                        mentions=[Player(nick="test_nick_3", name="test_name_3")],
                    ),
                    emoji="\u2705",
                ),
                True,
                [Player(nick="test_nick_4", name="test_name_4")],
            ),
            (
                "player pick",
                Player(nick="test_nick_1", name="test_name_1"),
                Player(nick="test_nick_1", name="test_name_1"),
                ReactionWithMessage(
                    message=Message(
                        id=1,
                        author=Player(nick="test_bot", name="test_bot"),
                        mentions=[Player(nick="test_nick_3", name="test_name_3")],
                    ),
                    emoji="\u2705",
                ),
                True,
                [Player(nick="test_nick_3", name="test_name_3")],
            ),
            (
                "player pick",
                Player(nick="test_nick_1", name="test_name_1"),
                Player(nick="test_nick_1", name="test_name_1"),
                ReactionWithMessage(
                    message=Message(
                        id=1,
                        author=Player(nick="test_bot", name="test_bot"),
                        mentions=[Player(nick="test_nick_3", name="test_name_3")],
                    ),
                    emoji="\u2705",
                ),
                True,
                [
                    Player(nick="test_nick_3", name="test_name_3"),
                    Player(nick="test_nick_4", name="test_name_4"),
                ],
            ),
        ],
    )
    @pytest.mark.asyncio()
    async def test_pick_player(
        self,
        monkeypatch,
        pre_match_pug,
        phase,
        picker,
        captain,
        reaction,
        add_message_to_pug,
        playerlist,
        map_elimination,
    ):
        monkeypatch.setattr(
            cod4pugbot.pug.PreMatchPUG,
            "_add_player_to_team",
            _add_player_to_team := mock.MagicMock(),
        )
        monkeypatch.setattr(
            cod4pugbot.pug.PreMatchPUG,
            "_modify_match_message",
            _modify_match_message := mock.MagicMock(),
        )
        monkeypatch.setattr(
            cod4pugbot.pug.PreMatchPUG,
            "_start_map_elimination",
            _start_map_elimination := mock.MagicMock(),
        )
        monkeypatch.setattr(
            cod4pugbot.pug.PreMatchPUG,
            "_pick_random_maps",
            _pick_random_maps := mock.MagicMock(),
        )
        monkeypatch.setattr(
            cod4pugbot.pug.PreMatchPUG,
            "_start_pug",
            _start_pug := mock.MagicMock(),
        )
        captains = [
            Player(nick="test_nick_1", name="test_name_1"),
            Player(nick="test_nick_2", name="test_name_2"),
        ]
        pre_match_pug.attack = {"captain": captains[0]}
        pre_match_pug.defence = {"captain": captains[1]}
        pre_match_pug.picker_cycle = itertools.cycle(captains.copy())
        # iterate to test_nick_1 just like PreMatchPUG._start_player_pick_phase does
        next(pre_match_pug.picker_cycle)
        pre_match_pug.picker = picker
        pre_match_pug.playerlist = playerlist.copy()
        if add_message_to_pug:
            pre_match_pug.messages["players"]["test_player_message"] = reaction.message
        pre_match_pug.phase = phase
        pre_match_pug.map_elimination = map_elimination

        await pre_match_pug.pick_player(captain=captain, reaction=reaction)

        if (
            phase == "player pick"
            and captain in captains
            and captain == picker
            and add_message_to_pug
            and reaction.message.author == Player(nick="test_bot", name="test_bot")
            and reaction.emoji == "\u2705"
            and reaction.message.mentions
            and reaction.message.mentions[0] in playerlist
        ):
            assert pre_match_pug.picker == captains[1]

            if len(playerlist) == 1:
                _add_player_to_team.assert_called_with(
                    captain=captains[1], player=playerlist[0]
                )
                if map_elimination:
                    _modify_match_message.assert_called_once()
                    _start_map_elimination.assert_called_once()
                else:
                    _pick_random_maps.assert_called_once()
                    _start_pug.assert_called_once()
            else:
                _add_player_to_team.assert_called_with(
                    captain=captain,
                    player=reaction.message.mentions[0],
                )
                _modify_match_message.assert_called_once()
        else:
            _add_player_to_team.assert_not_called()
            _modify_match_message.assert_not_called()

    @pytest.mark.parametrize(
        (
            "phase,map_elimination_phase,map_elimination_cycle,picker,"
            "captain,reaction,add_message_to_pug"
        ),
        [
            # 1. case: wrong phase
            (
                "player pick",
                "pick",
                ("pick", "pick"),
                Player(nick="test_nick_1", name="test_name_1"),
                Player(nick="test_nick_1", name="test_name_1"),
                ReactionWithMessage(
                    message=Message(
                        id=1,
                        author=Player(nick="test_bot", name="test_bot"),
                        content="Backlot",
                    ),
                    emoji="\u2705",
                ),
                True,
            ),
            # 2. case: wrong map elimination phase
            (
                "map elimination",
                "veto",
                ("pick", "pick"),
                Player(nick="test_nick_1", name="test_name_1"),
                Player(nick="test_nick_1", name="test_name_1"),
                ReactionWithMessage(
                    message=Message(
                        id=1,
                        author=Player(nick="test_bot", name="test_bot"),
                        content="Backlot",
                    ),
                    emoji="\u2705",
                ),
                True,
            ),
            # 3. case: picker not captain
            (
                "map elimination",
                "pick",
                ("pick", "pick"),
                Player(nick="test_nick_2", name="test_name_2"),
                Player(nick="test_nick_1", name="test_name_1"),
                ReactionWithMessage(
                    message=Message(
                        id=1,
                        author=Player(nick="test_bot", name="test_bot"),
                        content="Backlot",
                    ),
                    emoji="\u2705",
                ),
                True,
            ),
            # 4. case: captain not picker
            (
                "map elimination",
                "pick",
                ("pick", "pick"),
                Player(nick="test_nick_1", name="test_name_1"),
                Player(nick="test_nick_2", name="test_name_2"),
                ReactionWithMessage(
                    message=Message(
                        id=1,
                        author=Player(nick="test_bot", name="test_bot"),
                        content="Backlot",
                    ),
                    emoji="\u2705",
                ),
                True,
            ),
            # 5. case: message author not bot
            (
                "map elimination",
                "pick",
                ("pick", "pick"),
                Player(nick="test_nick_1", name="test_name_1"),
                Player(nick="test_nick_1", name="test_name_1"),
                ReactionWithMessage(
                    message=Message(
                        id=1,
                        author=Player(nick="test_player", name="test_player"),
                        content="Backlot",
                    ),
                    emoji="\u2705",
                ),
                True,
            ),
            # 6. case: map not in map pool
            (
                "map elimination",
                "pick",
                ("pick", "pick"),
                Player(nick="test_nick_1", name="test_name_1"),
                Player(nick="test_nick_1", name="test_name_1"),
                ReactionWithMessage(
                    message=Message(
                        id=1,
                        author=Player(nick="test_player", name="test_player"),
                        content="Ambush",
                    ),
                    emoji="\u2705",
                ),
                True,
            ),
            # 7. case: wrong emoji
            (
                "map elimination",
                "pick",
                ("pick", "pick"),
                Player(nick="test_nick_1", name="test_name_1"),
                Player(nick="test_nick_1", name="test_name_1"),
                ReactionWithMessage(
                    message=Message(
                        id=1,
                        author=Player(nick="test_player", name="test_player"),
                        content="Ambush",
                    ),
                    emoji="\u26d4",
                ),
                True,
            ),
            # 8. case: message not in bot's messages
            (
                "map elimination",
                "pick",
                ("pick", "pick"),
                Player(nick="test_nick_1", name="test_name_1"),
                Player(nick="test_nick_1", name="test_name_1"),
                ReactionWithMessage(
                    message=Message(
                        id=1,
                        author=Player(nick="test_player", name="test_player"),
                        content="Ambush",
                    ),
                    emoji="\u2705",
                ),
                False,
            ),
            (
                "map elimination",
                "pick",
                ("pick", "pick"),
                Player(nick="test_nick_1", name="test_name_1"),
                Player(nick="test_nick_1", name="test_name_1"),
                ReactionWithMessage(
                    message=Message(
                        id=1,
                        author=Player(nick="test_bot", name="test_bot"),
                        content="Backlot",
                    ),
                    emoji="\u2705",
                ),
                True,
            ),
            (
                "map elimination",
                "pick",
                ("pick",),
                Player(nick="test_nick_1", name="test_name_1"),
                Player(nick="test_nick_1", name="test_name_1"),
                ReactionWithMessage(
                    message=Message(
                        id=1,
                        author=Player(nick="test_bot", name="test_bot"),
                        content="Backlot",
                    ),
                    emoji="\u2705",
                ),
                True,
            ),
        ],
    )
    @pytest.mark.asyncio()
    async def test_pick_map(
        self,
        monkeypatch,
        pre_match_pug,
        phase,
        map_elimination_phase,
        map_elimination_cycle,
        picker,
        captain,
        reaction,
        add_message_to_pug,
    ):
        monkeypatch.setattr(
            cod4pugbot.pug.PreMatchPUG,
            "_add_map_to_maps",
            _add_map_to_maps := mock.MagicMock(),
        )
        monkeypatch.setattr(
            cod4pugbot.pug.PreMatchPUG,
            "_start_pug",
            _start_pug := mock.MagicMock(),
        )
        map_pool = ["Backlot", "Crash", "Crossfire", "Citystreets", "Strike", "Vacant"]
        pre_match_pug.map_pool = map_pool.copy()
        captains = [
            Player(nick="test_nick_1", name="test_name_1"),
            Player(nick="test_nick_2", name="test_name_2"),
        ]
        pre_match_pug.attack = {"captain": captains[0]}
        pre_match_pug.defence = {"captain": captains[1]}
        pre_match_pug.picker_cycle = itertools.cycle(captains.copy())
        # iterate to test_nick_1 just like PreMatchPUG._start_player_pick_phase does
        next(pre_match_pug.picker_cycle)
        pre_match_pug.picker = picker
        pre_match_pug.map_elimination_cycle = iter(map_elimination_cycle)
        next(pre_match_pug.map_elimination_cycle)
        pre_match_pug.map_elimination_phase = map_elimination_phase
        if add_message_to_pug:
            pre_match_pug.messages["maps"][reaction.message.content] = reaction.message
        pre_match_pug.phase = phase

        await pre_match_pug.pick_map(captain=captain, reaction=reaction)

        if (
            phase == "map elimination"
            and (map_elimination_phase == "pick" or map_elimination_phase == "random")
            and captain in captains
            and captain == picker
            and add_message_to_pug
            and reaction.message.author == Player(nick="test_bot", name="test_bot")
            and reaction.message.content
            and reaction.message.content in map_pool
            and reaction.emoji == "\u2705"
        ):
            assert pre_match_pug.picker == captains[1]

            if pre_match_pug.map_elimination_phase != "random":
                if len(map_elimination_cycle) == 1:
                    _add_map_to_maps.assert_called_once_with(
                        map_to_add=reaction.message.content
                    )
                    _start_pug.assert_called_once()
                elif len(map_pool) - 1 == 1:
                    assert _add_map_to_maps.call_count == 2
                    _add_map_to_maps.assert_called_with(
                        map_to_add=reaction.message.content
                    )
                    _add_map_to_maps.assert_called_with(map_to_add=map_pool[0])
                    _start_pug.assert_called_once()
                else:
                    _add_map_to_maps.assert_called_once_with(
                        map_to_add=reaction.message.content
                    )
            else:
                assert _add_map_to_maps.call_count == 2
                _start_pug.assert_called_once()
        else:
            _add_map_to_maps.assert_not_called()
            _start_pug.assert_not_called()

    @pytest.mark.parametrize(
        (
            "phase,map_elimination_phase,map_elimination_cycle,picker,"
            "captain,reaction,add_message_to_pug"
        ),
        [
            # 1. case: wrong phase
            (
                "player pick",
                "pick",
                ("pick", "pick"),
                Player(nick="test_nick_1", name="test_name_1"),
                Player(nick="test_nick_1", name="test_name_1"),
                ReactionWithMessage(
                    message=Message(
                        id=1,
                        author=Player(nick="test_bot", name="test_bot"),
                        content="Backlot",
                    ),
                    emoji="\u26d4",
                ),
                True,
            ),
            # 2. case: wrong map elimination phase
            (
                "map elimination",
                "pick",
                ("veto", "pick"),
                Player(nick="test_nick_1", name="test_name_1"),
                Player(nick="test_nick_1", name="test_name_1"),
                ReactionWithMessage(
                    message=Message(
                        id=1,
                        author=Player(nick="test_bot", name="test_bot"),
                        content="Backlot",
                    ),
                    emoji="\u26d4",
                ),
                True,
            ),
            # 3. case: picker not captain
            (
                "map elimination",
                "veto",
                ("veto", "pick"),
                Player(nick="test_nick_2", name="test_name_2"),
                Player(nick="test_nick_1", name="test_name_1"),
                ReactionWithMessage(
                    message=Message(
                        id=1,
                        author=Player(nick="test_bot", name="test_bot"),
                        content="Backlot",
                    ),
                    emoji="\u26d4",
                ),
                True,
            ),
            # 4. case: captain not picker
            (
                "map elimination",
                "veto",
                ("veto", "pick"),
                Player(nick="test_nick_1", name="test_name_1"),
                Player(nick="test_nick_2", name="test_name_2"),
                ReactionWithMessage(
                    message=Message(
                        id=1,
                        author=Player(nick="test_bot", name="test_bot"),
                        content="Backlot",
                    ),
                    emoji="\u26d4",
                ),
                True,
            ),
            # 5. case: message author not bot
            (
                "map elimination",
                "veto",
                ("veto", "pick"),
                Player(nick="test_nick_1", name="test_name_1"),
                Player(nick="test_nick_1", name="test_name_1"),
                ReactionWithMessage(
                    message=Message(
                        id=1,
                        author=Player(nick="test_player", name="test_player"),
                        content="Backlot",
                    ),
                    emoji="\u26d4",
                ),
                True,
            ),
            # 6. case: map not in map pool
            (
                "map elimination",
                "veto",
                ("veto", "pick"),
                Player(nick="test_nick_1", name="test_name_1"),
                Player(nick="test_nick_1", name="test_name_1"),
                ReactionWithMessage(
                    message=Message(
                        id=1,
                        author=Player(nick="test_player", name="test_player"),
                        content="Ambush",
                    ),
                    emoji="\u26d4",
                ),
                True,
            ),
            # 7. case: wrong emoji
            (
                "map elimination",
                "veto",
                ("veto", "pick"),
                Player(nick="test_nick_1", name="test_name_1"),
                Player(nick="test_nick_1", name="test_name_1"),
                ReactionWithMessage(
                    message=Message(
                        id=1,
                        author=Player(nick="test_player", name="test_player"),
                        content="Ambush",
                    ),
                    emoji="\u2705",
                ),
                True,
            ),
            # 8. case: message not in bot's messages
            (
                "map elimination",
                "veto",
                ("veto", "pick"),
                Player(nick="test_nick_1", name="test_name_1"),
                Player(nick="test_nick_1", name="test_name_1"),
                ReactionWithMessage(
                    message=Message(
                        id=1,
                        author=Player(nick="test_player", name="test_player"),
                        content="Ambush",
                    ),
                    emoji="\u26d4",
                ),
                False,
            ),
            (
                "map elimination",
                "veto",
                ("veto", "pick"),
                Player(nick="test_nick_1", name="test_name_1"),
                Player(nick="test_nick_1", name="test_name_1"),
                ReactionWithMessage(
                    message=Message(
                        id=1,
                        author=Player(nick="test_bot", name="test_bot"),
                        content="Backlot",
                    ),
                    emoji="\u26d4",
                ),
                True,
            ),
        ],
    )
    @pytest.mark.asyncio()
    async def test_veto_map(
        self,
        monkeypatch,
        pre_match_pug,
        phase,
        map_elimination_phase,
        map_elimination_cycle,
        picker,
        captain,
        reaction,
        add_message_to_pug,
    ):
        monkeypatch.setattr(
            cod4pugbot.pug.PreMatchPUG,
            "_remove_map_from_map_pool",
            _remove_map_from_map_pool := mock.MagicMock(),
        )
        monkeypatch.setattr(
            cod4pugbot.pug.PreMatchPUG,
            "_add_map_to_maps",
            _add_map_to_maps := mock.MagicMock(),
        )
        monkeypatch.setattr(
            cod4pugbot.pug.PreMatchPUG,
            "_start_pug",
            _start_pug := mock.MagicMock(),
        )
        map_pool = ["Backlot", "Crash", "Crossfire", "Citystreets", "Strike", "Vacant"]
        pre_match_pug.map_pool = map_pool.copy()
        captains = [
            Player(nick="test_nick_1", name="test_name_1"),
            Player(nick="test_nick_2", name="test_name_2"),
        ]
        pre_match_pug.attack = {"captain": captains[0]}
        pre_match_pug.defence = {"captain": captains[1]}
        pre_match_pug.picker_cycle = itertools.cycle(captains.copy())
        # iterate to test_nick_1 just like PreMatchPUG._start_player_pick_phase does
        next(pre_match_pug.picker_cycle)
        pre_match_pug.picker = picker
        pre_match_pug.map_elimination_cycle = iter(map_elimination_cycle)
        next(pre_match_pug.map_elimination_cycle)
        pre_match_pug.map_elimination_phase = map_elimination_phase
        if add_message_to_pug:
            pre_match_pug.messages["maps"][reaction.message.content] = reaction.message
        pre_match_pug.phase = phase

        await pre_match_pug.veto_map(captain=captain, reaction=reaction)

        if (
            phase == "map elimination"
            and (map_elimination_phase == "veto" or map_elimination_phase == "random")
            and captain in captains
            and captain == picker
            and add_message_to_pug
            and reaction.message.author == Player(nick="test_bot", name="test_bot")
            and reaction.message.content
            and reaction.message.content in map_pool
            and reaction.emoji == "\u26d4"
        ):
            assert pre_match_pug.picker == captains[1]

            if len(map_elimination_cycle) == 1:
                _remove_map_from_map_pool.assert_called_once_with(
                    map_to_remove=reaction.message.content
                )
                _start_pug.assert_called_once()
            elif map_elimination_phase == "random":
                _remove_map_from_map_pool.assert_called_with(
                    map_to_remove=reaction.message.content
                )
                assert _add_map_to_maps.call_count == 2
                _start_pug.assert_called_once()
            elif len(map_pool) - 1 == 1:
                _remove_map_from_map_pool.assert_called_with(
                    map_to_remove=reaction.message.content
                )
                _add_map_to_maps.assert_called_with(map_to_add=map_pool[0])
                assert _add_map_to_maps.call_count == 2
                _start_pug.assert_called_once()
            else:
                _remove_map_from_map_pool.assert_called_once_with(
                    map_to_remove=reaction.message.content
                )
        else:
            _remove_map_from_map_pool.assert_not_called()
            _add_map_to_maps.assert_not_called()
            _start_pug.assert_not_called()
