import os

import pytest
import yaml

os.environ["3"] = "three"
os.environ["4"] = "4"
os.environ["5a"] = "5"
os.environ["5ca"] = "5"
os.environ["5cb"] = "5cb"


@pytest.fixture()
def config_dict():
    return {
        "test1": 1,
        "test2": "two",
        "test3": "${3}",
        "test4": "${4}",
        "test5": {
            "test5a": "${5a}",
            "test5b": "5b",
            "test5c": {
                "test5ca": "${5ca}",
                "test5cb": "${5cb}",
            },
        },
    }


@pytest.fixture()
def config_dict_2():
    return {
        "test1": 1,
        "test2": "two",
        "test3": "${3}",
        "test4": "${4}",
        "test5": {
            "test5a": "${5a}",
            "test5b": "5b",
            "test5c": {
                "test5ca": "${5ca}",
                "test5cb": "fivecb",
            },
        },
        "test6": {
            "test6a": "sixa",
        },
    }


@pytest.fixture()
def config_yml():
    return yaml.safe_load("./resources/test.yml")


@pytest.fixture(name="mock_get_env_vars", scope="session")
def fixture_mock_get_env_vars():
    def _mock_get_env_vars(config):
        return

    return _mock_get_env_vars


@pytest.fixture(name="mock_set_value", scope="session")
def fixture_mock_set_value():
    def _mock_set_value(d, keys, value):
        return

    return _mock_set_value
