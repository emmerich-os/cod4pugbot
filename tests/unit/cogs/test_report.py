import discord
import freezegun
import pytest
import pytz

import cod4pugbot.cogs
from cod4pugbot import testing


@pytest.fixture()
def report_embed():
    return discord.Embed.from_dict(
        {
            "color": 7500402,
            "fields": [
                {"inline": True, "name": "Reported member", "value": "test_reported"},
                {"inline": True, "name": "Reported by", "value": "test_reporter"},
                {"inline": False, "name": "Reason", "value": "```\ntest_reason\n```"},
                {
                    "inline": False,
                    "name": "Status",
                    "value": "```brainfuck\nSOME_STATUS\n```",
                },
            ],
            "thumbnail": {"url": "test_url"},
            "timestamp": "2020-01-01T00:00:00+00:00",
            "title": "MEMBER REPORTED",
            "type": "rich",
        },
    )


TIME = "2020-01-01 00:00"


@pytest.mark.parametrize(
    "reported_member,reported_by,reason,timezone,attachments,expected",
    [
        (
            testing.Member(name="test_reported"),
            testing.Member(name="test_reporter"),
            "test_reason",
            pytz.timezone("UTC"),
            [testing.Attachment("test_url")],
            {
                "color": 7500402,
                "fields": [
                    {
                        "inline": True,
                        "name": "Reported member",
                        "value": "test_reported",
                    },
                    {"inline": True, "name": "Reported by", "value": "test_reporter"},
                    {
                        "inline": False,
                        "name": "Reason",
                        "value": "```\ntest_reason\n```",
                    },
                    {
                        "inline": False,
                        "name": "Status",
                        "value": "```brainfuck\nPENDING\n```",
                    },
                ],
                "thumbnail": {"url": "test_url"},
                "timestamp": "2020-01-01T00:00:00+00:00",
                "title": "MEMBER REPORTED",
                "type": "rich",
            },
        ),
        (
            testing.Member(name="test_reported"),
            testing.Member(name="test_reporter"),
            "test_reason",
            pytz.timezone("UTC"),
            [testing.Attachment("test_url"), testing.Attachment("test_url_2")],
            {
                "color": 7500402,
                "fields": [
                    {
                        "inline": True,
                        "name": "Reported member",
                        "value": "test_reported",
                    },
                    {"inline": True, "name": "Reported by", "value": "test_reporter"},
                    {
                        "inline": False,
                        "name": "Reason",
                        "value": "```\ntest_reason\n```",
                    },
                    {
                        "inline": False,
                        "name": "Further attachments",
                        "value": "test_url_2",
                    },
                    {
                        "inline": False,
                        "name": "Status",
                        "value": "```brainfuck\nPENDING\n```",
                    },
                ],
                "thumbnail": {"url": "test_url"},
                "timestamp": "2020-01-01T00:00:00+00:00",
                "title": "MEMBER REPORTED",
                "type": "rich",
            },
        ),
        (
            testing.Member(name="test_reported"),
            testing.Member(name="test_reporter"),
            "test_reason",
            pytz.timezone("UTC"),
            [
                testing.Attachment("test_url"),
                testing.Attachment("test_url_2"),
                testing.Attachment("test_url_3"),
            ],
            {
                "color": 7500402,
                "fields": [
                    {
                        "inline": True,
                        "name": "Reported member",
                        "value": "test_reported",
                    },
                    {"inline": True, "name": "Reported by", "value": "test_reporter"},
                    {
                        "inline": False,
                        "name": "Reason",
                        "value": "```\ntest_reason\n```",
                    },
                    {
                        "inline": False,
                        "name": "Further attachments",
                        "value": "test_url_2\ntest_url_3",
                    },
                    {
                        "inline": False,
                        "name": "Status",
                        "value": "```brainfuck\nPENDING\n```",
                    },
                ],
                "thumbnail": {"url": "test_url"},
                "timestamp": "2020-01-01T00:00:00+00:00",
                "title": "MEMBER REPORTED",
                "type": "rich",
            },
        ),
    ],
)
@freezegun.freeze_time(TIME)
def test_create_report_embed(
    reported_member, reported_by, reason, timezone, attachments, expected
):
    result = cod4pugbot.domain.features.report.report._create_report_embed(
        reported_member=reported_member,
        reported_by=reported_by,
        reason=reason,
        attachments=attachments,
        timezone=timezone,
    ).to_dict()

    assert result == expected


@pytest.mark.parametrize(
    "set_reviewed,set_pending,expected",
    [
        (
            True,
            False,
            {
                "color": 65280,
                "fields": [
                    {
                        "inline": True,
                        "name": "Reported member",
                        "value": "test_reported",
                    },
                    {"inline": True, "name": "Reported by", "value": "test_reporter"},
                    {
                        "inline": False,
                        "name": "Reason",
                        "value": "```\ntest_reason\n```",
                    },
                    {
                        "inline": False,
                        "name": "Status",
                        "value": "```css\nREVIEWED\n```",
                    },
                ],
                "thumbnail": {"url": "test_url"},
                "timestamp": "2020-01-01T00:00:00+00:00",
                "title": "MEMBER REPORTED",
                "type": "rich",
            },
        ),
        (
            False,
            True,
            {
                "color": 7500402,
                "fields": [
                    {
                        "inline": True,
                        "name": "Reported member",
                        "value": "test_reported",
                    },
                    {"inline": True, "name": "Reported by", "value": "test_reporter"},
                    {
                        "inline": False,
                        "name": "Reason",
                        "value": "```\ntest_reason\n```",
                    },
                    {
                        "inline": False,
                        "name": "Status",
                        "value": "```brainfuck\nPENDING\n```",
                    },
                ],
                "thumbnail": {"url": "test_url"},
                "timestamp": "2020-01-01T00:00:00+00:00",
                "title": "MEMBER REPORTED",
                "type": "rich",
            },
        ),
    ],
)
def test_change_report_embed_status(report_embed, set_reviewed, set_pending, expected):
    result = cod4pugbot.domain.features.report.report._change_report_embed_status(
        report_embed,
        set_reviewed=set_reviewed,
        set_pending=set_pending,
    ).to_dict()

    assert result == expected


@pytest.mark.parametrize(
    "user,reaction,was_added,was_removed,expected",
    [
        # Test case: reaction added by bot, reaction does not have to be removed
        (
            "test_bot",
            testing.Reaction(),
            True,
            False,
            (False, False),
        ),
        # Test case: reaction added by user, but message author is not bot,
        # reaction does not have to be removed
        (
            "test_user",
            testing.Reaction(message=testing.Message(author="test_user")),
            True,
            False,
            (False, False),
        ),
        # Test case: reaction added by user, message author is bot, but message has no embeds,
        # reaction does not have to be removed
        (
            "test_user",
            testing.Reaction(message=testing.Message(author="test_bot", embeds=[])),
            True,
            False,
            (False, False),
        ),
        # Test case: reaction added by user, message author is bot, message has embeds,
        # reaction count is <= 1, but emoji is incorrect, thus reaction has to be removed
        (
            "test_user",
            testing.Reaction(
                message=testing.Message(author="test_bot", embeds=["test_embed"]),
                count=1,
                emoji="test_emoji",
            ),
            True,
            False,
            (False, True),
        ),
        # Test case: reaction added by user, message author is bot, message has embeds,
        # but reaction count is >= 2, thus reaction has to be removed
        (
            "test_user",
            testing.Reaction(
                message=testing.Message(author="test_bot", embeds=["test_embed"]),
                emoji="\u2705",
                count=3,
            ),
            True,
            False,
            (False, True),
        ),
        # Test case: reaction added by user, message author is bot, message has embeds,
        # reaction count is <= 1, emoji is correct, thus everything as desired
        (
            "test_user",
            testing.Reaction(
                message=testing.Message(author="test_bot", embeds=["test_embed"]),
                count=1,
                emoji="\u2705",
            ),
            True,
            False,
            (True, False),
        ),
        # Test case: reaction added by user, message author is bot, message has embeds,
        # but reaction count is >= 2, thus reaction has to be removed
        (
            "test_user",
            testing.Reaction(
                message=testing.Message(author="test_bot", embeds=["test_embed"]),
                emoji="\u2705",
                count=2,
            ),
            True,
            False,
            (True, False),
        ),
        # Test case: reaction removed by bot
        (
            "test_bot",
            testing.Reaction(),
            False,
            True,
            (False, False),
        ),
        # Test case: reaction removed by user, but message author is not bot,
        (
            "test_user",
            testing.Reaction(message=testing.Message(author="test_user")),
            False,
            True,
            (False, False),
        ),
        # Test case: reaction removed by user, message author is bot, but message has no embeds,
        (
            "test_user",
            testing.Reaction(message=testing.Message(author="test_bot", embeds=[])),
            False,
            True,
            (False, False),
        ),
        # Test case: reaction removed by user, message author is bot, message has embeds,
        # but emoji is incorrect
        (
            "test_user",
            testing.Reaction(
                message=testing.Message(author="test_bot", embeds=["test_embed"]),
                emoji="test_emoji",
            ),
            False,
            True,
            (False, False),
        ),
        # Test case: reaction removed by user, message author is bot, message has embeds,
        # emoji is correct, thus everything as desired
        (
            "test_user",
            testing.Reaction(
                message=testing.Message(author="test_bot", embeds=["test_embed"]),
                emoji="\u2705",
            ),
            False,
            True,
            (True, False),
        ),
    ],
)
def test_check_for_correct_user_and_reaction(
    bot,
    user,
    reaction,
    was_added,
    was_removed,
    expected,
):
    result = (
        cod4pugbot.domain.features.report.report._check_for_correct_user_and_reaction(
            user=user,
            reaction=reaction,
            bot=bot,
            was_added=was_added,
            was_removed=was_removed,
        )
    )

    assert result == expected
