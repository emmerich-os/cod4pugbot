import pytest

import cod4pugbot.cogs
from cod4pugbot.testing import Member
from cod4pugbot.testing import User


@pytest.mark.parametrize(
    "before,after,expected",
    [
        (Member(), Member(), False),
        (Member(name="test_name"), Member(name="test_name"), False),
        (Member(name="test_name"), Member(name="test_name_changed"), True),
        (Member(nick="test_nick"), Member(nick="test_nick"), False),
        (Member(nick="test_nick"), Member(nick="test_nick_changed"), True),
        (Member(discriminator="1111"), Member(discriminator="1111"), False),
        (Member(discriminator="1111"), Member(discriminator="2222"), True),
    ],
)
def test_check_member_details_change(before, after, expected):
    result = cod4pugbot.cogs.updater._check_member_details_change(before, after)

    assert result == expected


@pytest.mark.parametrize(
    "before,after,expected",
    [
        (User(), User(), False),
        (User(name="test_name"), User(name="test_name"), False),
        (User(name="test_name"), User(name="test_name_changed"), True),
        (User(discriminator="1111"), User(discriminator="1111"), False),
        (User(discriminator="1111"), User(discriminator="2222"), True),
    ],
)
def test_check_user_details_change(before, after, expected):
    result = cod4pugbot.cogs.updater._check_user_details_change(before, after)

    assert result == expected


@pytest.mark.parametrize(
    "before,after,expected",
    [
        ({"nick": "test_nick"}, {"nick": "test_nick"}, {}),
        (
            {"nick": "test_nick"},
            {"nick": "test_nick_changed"},
            {"nick": "test_nick_changed"},
        ),
        (
            {"name": "test_name"},
            {"name": "test_name_changed"},
            {"name": "test_name_changed"},
        ),
        (
            {"discriminator": "1111"},
            {"discriminator": "2222"},
            {"discriminator": "2222"},
        ),
    ],
)
def test_get_changed_details(before, after, expected):
    result = cod4pugbot.cogs.updater._get_changed_details(before, after)

    assert result == expected
