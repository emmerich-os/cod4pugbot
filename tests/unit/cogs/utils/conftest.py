import dataclasses
from unittest import mock

import discord
import pytest
from discord.ext import commands


@dataclasses.dataclass(eq=False)
class User(discord.User):
    name = "user_name"
    discriminator = 1111
    id = 1


@dataclasses.dataclass(eq=False)
class Member(discord.Member):
    nick = "member_nick"
    name = "member_name"
    discriminator = 2222
    id = 2
    _user = "member_name#2222"


@dataclasses.dataclass(eq=False)
class Author(discord.User):
    nick = "author_nick"
    name = "author_name"
    discriminator = 3333
    id = 3
    mention = f"@{nick}"


@dataclasses.dataclass(eq=False)
class Emoji(discord.Emoji):
    id = 0
    name = "smile"
    animated = "smile"


@dataclasses.dataclass(eq=False)
class Guild(discord.Guild):
    id = 4

    @staticmethod
    def get_member(user_id):
        return Member()

    @staticmethod
    async def fetch_member(user_id):
        return Member()

    @staticmethod
    async def fetch_emoji(emoji_id):
        return Emoji()


@dataclasses.dataclass(eq=False)
class Channel(discord.TextChannel):
    id = 5
    guild = Guild()

    async def send(self, content):
        return

    async def fetch_message(self, id):
        return Message()


@dataclasses.dataclass(eq=False)
class RawReaction(discord.RawReactionActionEvent):
    channel_id = 5
    message_id = 6
    user_id = 1
    emoji = Emoji()
    count = 4
    member = Member()


@dataclasses.dataclass(eq=False)
class Message(discord.Message):
    id = 6
    content = "test message"
    author = Author()
    channel = Channel()
    reactions = [RawReaction()]
    attachments = None


@dataclasses.dataclass(eq=False)
class Context(commands.Context):
    message = Message()
    channel = Channel()


class Bot(mock.MagicMock):
    channel_ids = {"test_category": {"test_channel": 5}, "test_channel": 5}

    @staticmethod
    def get_channel(id):
        return Channel()


@pytest.fixture()
def user():
    return User()


@pytest.fixture()
def member():
    return Member()


@pytest.fixture()
def author():
    return Author()


@pytest.fixture()
def channel():
    return Channel()


@pytest.fixture()
def raw_reaction():
    return RawReaction()


@pytest.fixture()
def reaction():
    return discord.Reaction(
        message="test_message",
        emoji=Emoji(),
        data=dict(
            count=4,
            me=False,
        ),
    )


@pytest.fixture()
def ctx():
    return Context()


@pytest.fixture()
def bot():
    return Bot()
