from unittest import mock

import pytest

import cod4pugbot.domain.utils


async def async_magic():
    return


mock.MagicMock.__await__ = lambda x: async_magic().__await__()


@pytest.mark.parametrize(
    "attachments,pb_guid,expected",
    [
        ([], False, False),
        ([1], False, True),
        ([1, 2], False, True),
        ([1, 2, 3], False, True),
        ([], True, False),
        ([1], True, True),
        ([1, 2], True, False),
        ([1, 2, 3], True, False),
    ],
)
@pytest.mark.asyncio()
async def test_has_attachments(monkeypatch, ctx, attachments, pb_guid, expected):
    monkeypatch.setattr(
        ctx.message.channel,
        "send",
        send := mock.MagicMock(),
    )
    ctx.message.attachments = attachments

    result = await cod4pugbot.cogs.utils.has_attachments(ctx=ctx, pb_guid=pb_guid)

    assert result == expected
    if not result:
        send.assert_called_once()


@pytest.mark.parametrize(
    "ctx_,channel,channel_id,category,send_message,expected",
    [
        ("ctx", "test_channel", 5, None, False, True),
        ("ctx", "test_channel", 4, None, False, False),
        (None, "test_channel", 5, "test_category", False, True),
        (None, "test_channel", 4, "test_category", False, False),
        ("ctx", "test_channel", None, "test_category", False, True),
        ("ctx", "test_channel", 4, None, True, False),
    ],
)
@pytest.mark.asyncio()
async def test_is_channel(
    monkeypatch,
    request,
    bot,
    ctx_,
    channel,
    channel_id,
    category,
    send_message,
    expected,
):
    if ctx_ is not None:
        ctx_ = request.getfixturevalue(ctx_)
        monkeypatch.setattr(
            ctx_.message.channel,
            "send",
            send := mock.MagicMock(),
        )

    result = await cod4pugbot.cogs.utils.is_channel(
        bot=bot,
        channel=channel,
        ctx=ctx_,
        channel_id=channel_id,
        category=category,
        send_message=send_message,
    )

    assert result == expected
    if ctx_ is not None and send_message:
        send.assert_called_once()


@pytest.mark.parametrize(
    "mentions,expected",
    [
        ([], False),
        ([1], True),
        ([1, 2], False),
        ([1, 2, 3], False),
    ],
)
@pytest.mark.asyncio()
async def test_has_mentions(monkeypatch, ctx, mentions, expected):
    monkeypatch.setattr(
        ctx.message.channel,
        "send",
        send := mock.MagicMock(),
    )
    ctx.message.mentions = mentions

    result = await cod4pugbot.cogs.utils.has_mentions(ctx=ctx)

    assert result == expected
    if not result:
        send.assert_called_once()
