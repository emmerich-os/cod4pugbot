from contextlib import nullcontext as doesnotraise
from contextlib import suppress
from unittest import mock

import discord
import pytest
from _pytest.fixtures import FixtureLookupError
from discord.ext import commands

import cod4pugbot.domain.utils


@pytest.mark.parametrize(
    "content,expected",
    [
        ("abcd1234", "abcd1234"),
        ('"abcd1234"', "abcd1234"),
        ('pb_guid="abcd1234"', "abcd1234"),
        ('pb_guid= "abcd1234"', "abcd1234"),
        ("pb_guid='abcd1234'", "abcd1234"),
        ("pb_guid= 'abcd1234'", "abcd1234"),
        ("pb_guid=abcd1234", "abcd1234"),
        ("pb_guid= abcd1234", "abcd1234"),
        ("abcd123", ValueError()),
        ('"abcd123"', ValueError()),
        ('pb_guid="abcd123"', ValueError()),
        ('pb_guid= "abcd123"', ValueError()),
        ("pb_guid='abcd123'", ValueError()),
        ("pb_guid= 'abcd123'", ValueError()),
        ("pb_guid=abcd123", ValueError()),
        ("pb_guid= abcd123", ValueError()),
        ("pb_guid= abcd12.4", ValueError()),
    ],
)
@pytest.mark.asyncio()
async def test_get_pb_guid(content, expected):
    context = (
        pytest.raises(type(expected))
        if isinstance(expected, Exception)
        else doesnotraise()
    )

    with context:
        result = await cod4pugbot.domain.utils.utils.kwargs._get_pb_guid(
            ctx=None, content=content
        )

        assert result == expected


@pytest.mark.parametrize(
    "content,expected",
    [
        ("112345678901234567", 112345678901234567),
        ('"112345678901234567"', 112345678901234567),
        ('discord_id="112345678901234567"', 112345678901234567),
        ('discord_id= "112345678901234567"', 112345678901234567),
        ("discord_id='112345678901234567'", 112345678901234567),
        ("discord_id= '112345678901234567'", 112345678901234567),
        ("discord_id=112345678901234567", 112345678901234567),
        ("discord_id= 112345678901234567", 112345678901234567),
        ("11234567890123456", ValueError()),
        ('"11234567890123456"', ValueError()),
        ('discord_id="11234567890123456"', ValueError()),
        ('discord_id= "11234567890123456"', ValueError()),
        ("discord_id='11234567890123456'", ValueError()),
        ("discord_id= '11234567890123456'", ValueError()),
        ("discord_id=11234567890123456", ValueError()),
        ("discord_id= 11234567890123456", ValueError()),
        ("discord_id= 11234567890123.567", ValueError()),
    ],
)
@pytest.mark.asyncio()
async def test_get_discord_id(content, expected):
    context = (
        pytest.raises(type(expected))
        if isinstance(expected, Exception)
        else doesnotraise()
    )

    with context:
        result = await cod4pugbot.domain.utils.utils.kwargs._get_discord_id(
            ctx=None, content=content
        )

        assert result == expected


@pytest.mark.parametrize(
    "user_,expected",
    [
        ("user", {"nick": "user_name", "name": "user_name#1111", "discord_id": 1}),
        (
            "member",
            {"nick": "member_nick", "name": "member_name#2222", "discord_id": 2},
        ),
        (
            "author",
            {"nick": "author_nick", "name": "author_name#3333", "discord_id": 3},
        ),
        ("ctx", None),
    ],
)
def test_get_user_info(request, user_, expected):
    user_ = request.getfixturevalue(user_)
    result = cod4pugbot.domain.utils.utils.kwargs._get_user_info(user_)

    assert result == expected


@pytest.mark.parametrize(
    "user_input,expected",
    [
        (
            (
                """discord_id=112345678901234567 """
                """pb_guid="abcd1234" """
                """reason='test reason' """
                """duration=4h asd"""
            ),
            {
                "discord_id": "112345678901234567",
                "pb_guid": "abcd1234",
                "reason": "test reason",
                "duration": "4h",
            },
        ),
        (
            (
                """test_key_1=test_value_1 """
                """test_key_2="test_value_2" """
                """test_key_3= "test_value_3" """
                """test_key_4 ="test_value_4" """
                """test_key_5 = "test_value_5" """
                """test_key_6='test_value_6' """
                """test_key_7= 'test_value_7' """
                """test_key_8 ='test_value_8' """
                """test_key_9 = 'test_value_9' """
                """test_key_10=test_value_10 """
                """test_key_11 = test_value_11 """
                """test_key_12= test_value_12 """
                """test_key_13 = test_value_13 """
                """test_key_14=test_value_14 asd """
                """test_key_15="test_value_15 """
                """test_key_16= "test_value_16 """
                """test_key_17 ="test_value_17 """
                """test_key_18 = "test_value_18 """
                """test_key_19=test_value_19" """
                """test_key_20= test_value_20" """
                """test_key_21 =test_value_21" """
                """test_key_22 = test_value_22" """
                """test_key_23='test_value_23" """
                """test_key_24= 'test_value_24" """
                """test_key_25 ='test_value_25" """
                """test_key_26 = 'test_value_26" """
                """test_key_27="test_value_27' """
                """test_key_28= "test_value_28' """
                """test_key_29 ="test_value_29' """
                """test_key_30 = "test_value_30' """
                """test_key_31='test_value_31 """
                """test_key_32= 'test_value_32 """
                """test_key_33 ='test_value_33 """
                """test_key_34 = 'test_value_34 """
                """test_key_35=test_value_35' """
                """test_key_36= test_value_36' """
                """test_key_37 =test_value_37' """
                """test_key_38 = test_value_38' """
                """test_key_39=  test_value_39 """
                """test_key_40  =test_value_40 """
                """test_key_41  =  test_value_41 """
                """test_key_42="test_value_42" asd """
                """test_key_43="test_value_43"asd """
                """test_key_44='test_value_44' asd """
                """test_key_45='test_value_45'asd """
                """test_key_46="test value 46" """
                """test_key_47='test value 47' """
            ),
            {
                "test_key_1": "test_value_1",
                "test_key_2": "test_value_2",
                "test_key_3": "test_value_3",
                "test_key_4": "test_value_4",
                "test_key_5": "test_value_5",
                "test_key_6": "test_value_6",
                "test_key_7": "test_value_7",
                "test_key_8": "test_value_8",
                "test_key_9": "test_value_9",
                "test_key_10": "test_value_10",
                "test_key_11": "test_value_11",
                "test_key_12": "test_value_12",
                "test_key_13": "test_value_13",
                "test_key_14": "test_value_14",
                "test_key_15": "test_value_15",
                "test_key_16": "test_value_16",
                "test_key_17": "test_value_17",
                "test_key_18": "test_value_18",
                "test_key_19": "test_value_19",
                "test_key_20": "test_value_20",
                "test_key_21": "test_value_21",
                "test_key_22": "test_value_22",
                "test_key_23": "test_value_23",
                "test_key_24": "test_value_24",
                "test_key_25": "test_value_25",
                "test_key_26": "test_value_26",
                "test_key_27": "test_value_27",
                "test_key_28": "test_value_28",
                "test_key_29": "test_value_29",
                "test_key_30": "test_value_30",
                "test_key_31": "test_value_31",
                "test_key_32": "test_value_32",
                "test_key_33": "test_value_33",
                "test_key_34": "test_value_34",
                "test_key_35": "test_value_35",
                "test_key_36": "test_value_36",
                "test_key_37": "test_value_37",
                "test_key_38": "test_value_38",
                "test_key_39": "test_value_39",
                "test_key_40": "test_value_40",
                "test_key_41": "test_value_41",
                "test_key_42": "test_value_42",
                "test_key_43": "test_value_43",
                "test_key_44": "test_value_44",
                "test_key_45": "test_value_45",
                "test_key_46": "test value 46",
                "test_key_47": "test value 47",
            },
        ),
    ],
)
@pytest.mark.asyncio()
async def test_kwargs_input(user_input, expected):
    result = await cod4pugbot.domain.utils.utils.kwargs._get_kwargs_input(
        ctx=None, content=user_input
    )

    assert result == expected


@pytest.mark.parametrize(
    "user_input,pb_guid,author,discord_id,expected",
    [
        ("abcd1234", True, False, False, {"pb_guid": "abcd1234"}),
        ('"abcd1234"', True, False, False, {"pb_guid": "abcd1234"}),
        ('pb_guid="abcd1234"', True, False, False, {"pb_guid": "abcd1234"}),
        ("pb_guid=abcd1234", True, False, False, {"pb_guid": "abcd1234"}),
        (
            "user",
            False,
            False,
            False,
            {"nick": "user_name", "name": "user_name#1111", "discord_id": 1},
        ),
        (
            "member",
            False,
            False,
            False,
            {"nick": "member_nick", "name": "member_name#2222", "discord_id": 2},
        ),
        (
            "ctx",
            False,
            True,
            False,
            {"nick": "author_nick", "name": "author_name#3333", "discord_id": 3},
        ),
        (
            (
                """!register <@!112345678901234567> """
                """discord_id=112345678901234567 """
                """pb_guid="abcd1234" """
                """reason='test reason' """
                """duration=4h asd"""
            ),
            False,
            False,
            False,
            {
                "discord_id": 112345678901234567,
                "pb_guid": "abcd1234",
                "reason": "test reason",
                "duration": "4h",
            },
        ),
        (
            """!register <@!112345678901234567> """,
            False,
            False,
            True,
            {"discord_id": 112345678901234567},
        ),
        (
            (
                """!register <@!112345678901234568> """
                """discord_id=112345678901234567 """
                """pb_guid="abcd1234" """
                """reason='test reason' """
                """duration=4h asd"""
            ),
            False,
            False,
            True,
            {
                "discord_id": 112345678901234567,
                "pb_guid": "abcd1234",
                "reason": "test reason",
                "duration": "4h",
            },
        ),
    ],
)
@pytest.mark.asyncio()
async def test_get_kwargs(
    request, ctx, user_input, pb_guid, author, discord_id, expected
):
    with mock.patch(
        "cod4pugbot.cogs.utils.kwargs._get_user_info",
        return_value=expected.copy(),
    ) and mock.patch(
        "cod4pugbot.cogs.utils.kwargs._get_pb_guid",
        return_value=expected.get("pb_guid"),
    ) and mock.patch(
        "cod4pugbot.cogs.utils.kwargs._get_discord_id",
        return_value=expected.get("discord_id"),
    ) and mock.patch(
        "cod4pugbot.cogs.utils.kwargs._get_kwargs_input",
        return_value=expected.copy(),
    ):
        with suppress(FixtureLookupError):
            user_input = request.getfixturevalue(user_input)

        if isinstance(user_input, discord.User):
            result = await cod4pugbot.cogs.utils.get_kwargs(
                ctx=ctx,
                user=user_input,
                pb_guid=pb_guid,
            )
        elif isinstance(user_input, discord.Member):
            result = await cod4pugbot.cogs.utils.get_kwargs(
                ctx=ctx,
                member=user_input,
                pb_guid=pb_guid,
            )
        elif isinstance(user_input, commands.Context):
            result = await cod4pugbot.cogs.utils.get_kwargs(
                ctx=user_input,
                author=author,
                pb_guid=pb_guid,
            )
        else:
            result = await cod4pugbot.cogs.utils.get_kwargs(
                ctx=ctx,
                content=user_input,
                pb_guid=pb_guid,
                discord_id=discord_id,
            )

        assert result == expected
