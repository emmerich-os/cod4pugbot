import discord
import pytest

import cod4pugbot.domain.utils


@pytest.mark.parametrize(
    "expected_user,expected_reaction",
    [
        (
            "member",
            "reaction",
        ),
    ],
)
@pytest.mark.asyncio()
async def test_raw_reaction_to_reaction(
    request,
    bot,
    raw_reaction,
    expected_user,
    expected_reaction,
):
    expected_user = request.getfixturevalue(expected_user)
    expected_reaction = request.getfixturevalue(expected_reaction)

    result_user, result_reaction = await cod4pugbot.cogs.utils.raw_reaction_to_reaction(
        bot=bot, raw_reaction=raw_reaction
    )

    assert isinstance(result_user, discord.Member)
    assert isinstance(result_reaction, discord.Reaction)
    assert result_user == expected_user
    assert result_reaction == expected_reaction
