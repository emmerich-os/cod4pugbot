import pytest
from psycopg2 import sql

import cod4pugbot.db._query_builder


@pytest.mark.parametrize(
    "table, return_output, kwargs, expected",
    [
        (
            "test_table",
            False,
            {"test_column_1": "test_value_1"},
            sql.Composed(
                [
                    sql.SQL("INSERT INTO "),
                    sql.Identifier("test_table"),
                    sql.SQL(" ("),
                    sql.Composed([sql.Identifier("test_column_1")]),
                    sql.SQL(") VALUES ("),
                    sql.Composed([sql.Literal("test_value_1")]),
                    sql.SQL(")"),
                    sql.SQL(";"),
                ]
            ),
        ),
        (
            "test_table",
            False,
            {
                "test_column_1": "test_value_1",
                "test_column_2": "test_value_2",
            },
            sql.Composed(
                [
                    sql.SQL("INSERT INTO "),
                    sql.Identifier("test_table"),
                    sql.SQL(" ("),
                    sql.Composed(
                        [
                            sql.Identifier("test_column_1"),
                            sql.SQL(", "),
                            sql.Identifier("test_column_2"),
                        ]
                    ),
                    sql.SQL(") VALUES ("),
                    sql.Composed(
                        [
                            sql.Literal("test_value_1"),
                            sql.SQL(", "),
                            sql.Literal("test_value_2"),
                        ]
                    ),
                    sql.SQL(")"),
                    sql.SQL(";"),
                ]
            ),
        ),
        (
            "test_table",
            True,
            {"test_column_1": "test_value_1"},
            sql.Composed(
                [
                    sql.SQL("INSERT INTO "),
                    sql.Identifier("test_table"),
                    sql.SQL(" ("),
                    sql.Composed([sql.Identifier("test_column_1")]),
                    sql.SQL(") VALUES ("),
                    sql.Composed([sql.Literal("test_value_1")]),
                    sql.SQL(")"),
                    sql.SQL(" RETURNING *"),
                    sql.SQL(";"),
                ]
            ),
        ),
    ],
)
def test_create_insert_query(table, return_output, kwargs, expected):
    result = cod4pugbot.db._query_builder.create_insert_query(
        table, return_output=return_output, **kwargs
    )

    assert result == expected


@pytest.mark.parametrize(
    "table, expected",
    [
        (
            "test_table",
            sql.Composed(
                [sql.SQL("SELECT * FROM "), sql.Identifier("test_table"), sql.SQL(" ")]
            ),
        ),
    ],
)
def test_create_select_query(table, expected):
    result = cod4pugbot.db._query_builder.create_select_query(table)

    assert result == expected


@pytest.mark.parametrize(
    "table, kwargs, expected",
    [
        (
            "test_table",
            {"test_column": "test_value"},
            sql.Composed(
                [
                    sql.SQL("UPDATE "),
                    sql.Identifier("test_table"),
                    sql.SQL(" SET "),
                    sql.SQL(" "),
                    sql.Identifier("test_column"),
                    sql.SQL(" = "),
                    sql.Literal("test_value"),
                    sql.SQL(" "),
                ]
            ),
        ),
        (
            "test_table",
            {"test_column_1": "test_value_1", "test_column_2": "test_value_2"},
            sql.Composed(
                [
                    sql.SQL("UPDATE "),
                    sql.Identifier("test_table"),
                    sql.SQL(" SET "),
                    sql.SQL(" "),
                    sql.Identifier("test_column_1"),
                    sql.SQL(" = "),
                    sql.Literal("test_value_1"),
                    sql.SQL(" "),
                    sql.SQL(", "),
                    sql.SQL(" "),
                    sql.Identifier("test_column_2"),
                    sql.SQL(" = "),
                    sql.Literal("test_value_2"),
                    sql.SQL(" "),
                ]
            ),
        ),
    ],
)
def test_create_update_query(table, kwargs, expected):
    result = cod4pugbot.db._query_builder.create_update_query(table=table, **kwargs)

    assert result == expected


@pytest.mark.parametrize(
    "table, expected",
    [
        (
            "test_table",
            sql.Composed(
                [
                    sql.SQL("DELETE FROM "),
                    sql.Identifier("test_table"),
                    sql.SQL(" "),
                ]
            ),
        ),
    ],
)
def test_create_delete_query(table, expected):
    result = cod4pugbot.db._query_builder.create_delete_query(table)

    assert result == expected


@pytest.mark.parametrize(
    "kwargs, expected",
    [
        (
            {"test_column_1": "test_value_1"},
            sql.Composed(
                [
                    sql.SQL(" WHERE "),
                    sql.SQL(" "),
                    sql.Identifier("test_column_1"),
                    sql.SQL(" "),
                    sql.SQL("="),
                    sql.SQL(" "),
                    sql.Literal("test_value_1"),
                    sql.SQL(" "),
                ]
            ),
        ),
        (
            {"test_column_1": None},
            sql.Composed(
                [
                    sql.SQL(" WHERE "),
                    sql.SQL(" "),
                    sql.Identifier("test_column_1"),
                    sql.SQL(" "),
                    sql.SQL("IS"),
                    sql.SQL(" "),
                    sql.Literal(None),
                    sql.SQL(" "),
                ]
            ),
        ),
        (
            {
                "test_column_1": "test_value_1",
                "test_column_2": "test_value_2",
            },
            sql.Composed(
                [
                    sql.SQL(" WHERE "),
                    sql.SQL(" "),
                    sql.Identifier("test_column_1"),
                    sql.SQL(" "),
                    sql.SQL("="),
                    sql.SQL(" "),
                    sql.Literal("test_value_1"),
                    sql.SQL(" "),
                    sql.SQL(" AND "),
                    sql.SQL(" "),
                    sql.Identifier("test_column_2"),
                    sql.SQL(" "),
                    sql.SQL("="),
                    sql.SQL(" "),
                    sql.Literal("test_value_2"),
                    sql.SQL(" "),
                ]
            ),
        ),
        (
            {
                "test_column_1": "test_value_1",
                "test_column_2": "test_value_2",
                "test_column_3": "test_value_3",
            },
            sql.Composed(
                [
                    sql.SQL(" WHERE "),
                    sql.SQL(" "),
                    sql.Identifier("test_column_1"),
                    sql.SQL(" "),
                    sql.SQL("="),
                    sql.SQL(" "),
                    sql.Literal("test_value_1"),
                    sql.SQL(" "),
                    sql.SQL(" AND "),
                    sql.SQL(" "),
                    sql.Identifier("test_column_2"),
                    sql.SQL(" "),
                    sql.SQL("="),
                    sql.SQL(" "),
                    sql.Literal("test_value_2"),
                    sql.SQL(" "),
                    sql.SQL(" AND "),
                    sql.SQL(" "),
                    sql.Identifier("test_column_3"),
                    sql.SQL(" "),
                    sql.SQL("="),
                    sql.SQL(" "),
                    sql.Literal("test_value_3"),
                    sql.SQL(" "),
                ]
            ),
        ),
    ],
)
def test_create_where_clause(kwargs, expected):
    result = cod4pugbot.db._query_builder.create_where_clause(**kwargs)

    assert result == expected
