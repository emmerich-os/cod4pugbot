import pytest

from cod4pugbot import testing


@pytest.fixture()
def simple_uow():
    return testing.FakeSimpleUnitOfWork()


@pytest.fixture()
def database_uow():
    return testing.FakeDatabaseUnitOfWork(sql_client=None, called_by_bot=True)


@pytest.fixture()
def bot(simple_uow, database_uow):
    return testing.Bot(simple_uow=simple_uow, database_uow=database_uow)
