import datetime

import pytest

from cod4pugbot import testing
from cod4pugbot.domain.events import CommandFailed
from cod4pugbot.domain.events import CommandSuccessful
from cod4pugbot.domain.features.administration import commands
from cod4pugbot.domain.features.administration import handlers


@pytest.mark.parametrize(
    ("contains", "command"),
    [
        (
            [],
            commands.KickMember(
                channel=testing.Channel(),
                member=testing.test_member(),
                reason="test_reason",
                admin=testing.test_admin(),
                support_bans_channel=testing.Channel(),
            ),
        ),
    ],
)
@pytest.mark.asyncio()
async def test_kick_member_from_guild(simple_uow, contains, command):
    await handlers.kick_member_from_guild(command, uow=simple_uow)

    member = command.member
    assert member.kicked


@pytest.mark.parametrize(
    ("existing_bans", "command", "ban_expected"),
    [
        (
            [],
            commands.GloballyBanMember(
                channel=testing.Channel(),
                member=testing.test_member(),
                reason="test reason",
                duration="1h",
                admin=testing.test_admin(),
                created_at=datetime.datetime(2020, 1, 1),
                support_bans_channel=testing.Channel(),
                role=None,
            ),
            True,
        ),
        (
            [],
            commands.GloballyBanMember(
                channel=testing.Channel(),
                member=testing.test_member(),
                reason="test reason",
                duration="1h",
                admin=testing.test_admin(),
                created_at=datetime.datetime(2020, 1, 1),
                support_bans_channel=testing.Channel(),
                role=testing.GlobalBanRole(),
            ),
            True,
        ),
        (
            [testing.test_ban().to_dict()],
            commands.GloballyBanMember(
                channel=testing.Channel(),
                member=testing.test_member(),
                reason="test reason",
                duration="1h",
                admin=testing.test_admin(),
                created_at=datetime.datetime(2020, 1, 1),
                support_bans_channel=testing.Channel(),
                role=testing.GlobalBanRole(),
            ),
            False,
        ),
    ],
)
@pytest.mark.asyncio()
async def test_ban_member(database_uow, existing_bans, command, ban_expected):
    database_uow.existing_bans = existing_bans

    await handlers.ban_member(command, uow=database_uow)

    member = command.member
    role = command.role
    if ban_expected:
        assert member.globally_banned
        if role is not None:
            assert testing.member_has_role(member, type(role))
        assert testing.uow_has_events(database_uow, CommandSuccessful)
    else:
        assert not member.globally_banned
        if role is not None:
            assert not testing.member_has_role(member, type(role))
        assert testing.uow_has_events(database_uow, CommandFailed)
