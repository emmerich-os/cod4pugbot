import pytest

from cod4pugbot import testing
from cod4pugbot.domain.features.administration import events
from cod4pugbot.domain.features.administration import handlers


@pytest.fixture()
def kwargs():
    return {
        "channel": testing.Channel(),
        "member": testing.test_member(),
        "reason": "test reason",
        "admin": testing.test_admin(),
        "support_bans_channel": testing.Channel(),
        "ban": testing.test_ban(),
    }


@pytest.mark.parametrize(
    "event",
    [
        events.MemberKicked,
        events.MemberPUGBanned,
        events.MemberGloballyBanned,
    ],
)
@pytest.mark.asyncio()
async def test_send_embed_to_admins(event, kwargs):
    if event == events.MemberKicked:
        kwargs.pop("ban")
    event = event(**kwargs)

    await handlers.send_embed_to_bans_channel(event)

    bans_channel = event.support_bans_channel
    assert testing.received_message(bans_channel)
    [message] = bans_channel.messages_received
    assert testing.message_contains_embed(message)
