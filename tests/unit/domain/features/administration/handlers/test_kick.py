import pytest

from cod4pugbot import testing
from cod4pugbot.domain.events import CommandSuccessful
from cod4pugbot.domain.features.administration import commands
from cod4pugbot.domain.features.administration import handlers


@pytest.mark.parametrize(
    ("contains", "command", "expected"),
    [
        (
            [],
            commands.KickMember(
                channel=testing.Channel(),
                member=testing.test_member(),
                reason="test_reason",
                admin=testing.test_admin(),
                support_bans_channel=testing.Channel(),
            ),
            CommandSuccessful,
        ),
    ],
)
@pytest.mark.asyncio()
async def test_kick_member_from_guild(simple_uow, contains, command, expected):
    simple_uow.existing_bans = contains

    await handlers.kick_member_from_guild(command, uow=simple_uow)

    member = command.member
    assert member.kicked
    assert testing.uow_has_events(simple_uow, expected)
