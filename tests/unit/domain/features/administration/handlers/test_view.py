import pytest

from cod4pugbot import testing
from cod4pugbot.domain.events import CommandFailed
from cod4pugbot.domain.events import CommandSuccessful
from cod4pugbot.domain.features.administration import commands
from cod4pugbot.domain.features.administration import handlers


@pytest.mark.parametrize(
    ("contains", "command", "expected"),
    [
        (
            [],
            commands.ViewBans(
                channel=testing.Channel(),
                admin=testing.test_admin(),
            ),
            CommandFailed,
        ),
        (
            [testing.test_ban().to_dict()],
            commands.ViewBans(
                channel=testing.Channel(),
                admin=testing.test_admin(),
            ),
            CommandSuccessful,
        ),
    ],
)
@pytest.mark.asyncio()
async def test_send_all_bans_as_csv(database_uow, contains, command, expected):
    database_uow.existing_bans = contains

    await handlers.send_all_bans_as_csv(command, uow=database_uow)

    assert testing.uow_has_events(database_uow, expected)
