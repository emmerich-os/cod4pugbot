import pytest

from cod4pugbot import testing
from cod4pugbot.domain.events import CommandSuccessful
from cod4pugbot.domain.features.administration import commands
from cod4pugbot.domain.features.administration import handlers


@pytest.mark.parametrize(
    ("command", "expected"),
    [
        (
            commands.WarnMember(
                channel=testing.Channel(),
                admin=testing.test_admin(),
                member=testing.test_member(),
                reason="test reason",
            ),
            CommandSuccessful,
        ),
    ],
)
@pytest.mark.asyncio()
async def test_warn_member(simple_uow, command, expected):
    await handlers.warn_member(command, uow=simple_uow)

    assert testing.uow_has_events(simple_uow, expected)
