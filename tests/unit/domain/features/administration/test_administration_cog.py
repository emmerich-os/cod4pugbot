import datetime

import freezegun
import pytest

import cod4pugbot.domain.features.administration.cog as administration
from cod4pugbot import testing

GET_SUPPORT_BANS_CHANNEL_METHOD = "_get_support_bans_channel"


@pytest.fixture()
def cog(bot):
    return administration.Administration(bot=bot)


class TestAdministration:
    @pytest.mark.parametrize(
        ("ctx", "member", "content", "expected"),
        [
            # Test case: command invoked correctly
            (
                testing.Context(
                    channel=testing.test_administration_channel(),
                    message=testing.Message(
                        author=testing.test_admin(),
                        channel=testing.test_administration_channel(),
                        mentions=[testing.test_member()],
                    ),
                ),
                testing.test_member(),
                "test reason",
                True,
            ),
            # Test case: command invoked in incorrect channel
            (
                testing.Context(
                    channel=testing.Channel(id="incorrect_channel"),
                    message=testing.Message(
                        author=testing.test_admin(),
                        channel=testing.Channel(id="incorrect_channel"),
                        mentions=[testing.test_member()],
                    ),
                ),
                testing.test_member(),
                "test reason",
                False,
            ),
        ],
    )
    @pytest.mark.asyncio()
    async def test_kick_member(self, cog, ctx, member, content, expected):
        await testing.invoke_cog_command(
            cog, "kick_member", ctx=ctx, member=member, content=content
        )

        if expected:
            assert member.kicked
            command_channel = ctx.channel
            support_bans_channel = cog.support_bans_channel
            assert testing.received_message(command_channel)
            assert testing.received_message(support_bans_channel)
            [message] = support_bans_channel.messages_received
            assert testing.message_contains_embed(message)
        else:
            assert not member.kicked

    @pytest.mark.parametrize(
        (
            "existing_bans",
            "global_ban_role_exists",
            "ctx",
            "member",
            "content",
            "expected_duration",
        ),
        [
            # Test case: command invoked correctly, duration is 1 day
            (
                [],
                True,
                testing.Context(
                    channel=testing.test_administration_channel(),
                    guild=testing.Guild(),
                    message=testing.Message(
                        author=testing.test_admin(),
                        channel=testing.test_administration_channel(),
                        mentions=[testing.test_member()],
                    ),
                ),
                testing.test_member(),
                """duration="1d" reason="test reason" """,
                datetime.datetime(2020, 1, 2, tzinfo=testing.TIMEZONE),
            ),
            # Test case: command invoked correctly, member already banned
            (
                [testing.test_ban().to_dict()],
                True,
                testing.Context(
                    channel=testing.test_administration_channel(),
                    guild=testing.Guild(),
                    message=testing.Message(
                        author=testing.test_admin(),
                        channel=testing.test_administration_channel(),
                        mentions=[testing.test_member()],
                    ),
                ),
                testing.test_member(),
                """duration="1d" reason="test reason" """,
                None,
            ),
            # Test case: command invoked in incorrect channel
            (
                [],
                True,
                testing.Context(
                    channel=testing.Channel(id="incorrect_channel"),
                    message=testing.Message(
                        author=testing.test_admin(),
                        channel=testing.Channel(id="incorrect_channel"),
                        mentions=[testing.test_member()],
                    ),
                ),
                testing.test_member(),
                "test reason",
                None,
            ),
        ],
    )
    @pytest.mark.asyncio()
    @freezegun.freeze_time("2020-01-01")
    async def test_global_ban_member(
        self,
        cog,
        existing_bans,
        global_ban_role_exists,
        ctx,
        member,
        content,
        expected_duration,
    ):
        cog.bot.database_uow.existing_bans = existing_bans
        cog.bot.has_global_ban_role = global_ban_role_exists

        await testing.invoke_cog_command(
            cog, "global_ban_member", ctx=ctx, member=member, content=content
        )

        if expected_duration is not None:
            assert member.globally_banned
            if global_ban_role_exists:
                assert testing.member_has_role(member, testing.GlobalBanRole)

            ban = cog.bot.database_uow.bans.added[-1]
            assert ban.expires_at == expected_duration

            command_channel = ctx.channel
            support_bans_channel = cog.support_bans_channel
            assert testing.received_message(command_channel)
            assert testing.received_message(support_bans_channel)
            [message] = support_bans_channel.messages_received
            assert testing.message_contains_embed(message)
        else:
            assert not member.globally_banned

    @pytest.mark.parametrize(
        ("existing_bans", "ctx", "member", "content", "expected_duration"),
        [
            # Test case: command invoked correctly, duration is 1 day
            (
                [],
                testing.Context(
                    channel=testing.test_administration_channel(),
                    guild=testing.Guild(),
                    message=testing.Message(
                        author=testing.test_admin(),
                        channel=testing.test_administration_channel(),
                        mentions=[testing.test_member()],
                    ),
                ),
                testing.test_member(),
                """duration="1d" reason="test reason" """,
                datetime.datetime(2020, 1, 2, tzinfo=testing.TIMEZONE),
            ),
            # Test case: command invoked correctly, member already banned
            (
                [testing.test_ban().to_dict()],
                testing.Context(
                    channel=testing.test_administration_channel(),
                    guild=testing.Guild(),
                    message=testing.Message(
                        author=testing.test_admin(),
                        channel=testing.test_administration_channel(),
                        mentions=[testing.test_member()],
                    ),
                ),
                testing.test_member(),
                """duration="1d" reason="test reason" """,
                None,
            ),
            # Test case: command invoked in incorrect channel
            (
                [],
                testing.Context(
                    channel=testing.Channel(id="incorrect_channel"),
                    message=testing.Message(
                        author=testing.test_admin(),
                        channel=testing.Channel(id="incorrect_channel"),
                        mentions=[testing.test_member()],
                    ),
                ),
                testing.test_member(),
                "test reason",
                None,
            ),
        ],
    )
    @pytest.mark.asyncio()
    @freezegun.freeze_time("2020-01-01")
    async def test_pug_ban_member(
        self, cog, existing_bans, ctx, member, content, expected_duration
    ):
        cog.bot.database_uow.existing_bans = existing_bans

        await testing.invoke_cog_command(
            cog, "pug_ban_member", ctx=ctx, member=member, content=content
        )

        if expected_duration is not None:
            assert member.pug_banned
            assert testing.member_has_role(member, testing.PUGBanRole)

            ban = cog.bot.database_uow.bans.added[-1]
            assert ban.expires_at == expected_duration

            command_channel = ctx.channel
            support_bans_channel = cog.support_bans_channel
            assert testing.received_message(command_channel)
            assert testing.received_message(support_bans_channel)
            [message] = support_bans_channel.messages_received
            assert testing.message_contains_embed(message)
        else:
            assert not member.pug_banned

    @pytest.mark.parametrize(
        ("ctx", "content", "match_expected"),
        [
            # Test case: mention given, matching ban found
            (
                testing.Context(
                    channel=testing.test_administration_channel(),
                    message=testing.Message(
                        author=testing.test_admin(),
                        channel=testing.test_administration_channel(),
                        mentions=[testing.test_member()],
                    ),
                ),
                None,
                True,
            ),
            # Test case: discord ID given, matching ban found
            (
                testing.Context(
                    channel=testing.test_administration_channel(),
                    message=testing.Message(
                        author=testing.test_admin(),
                        channel=testing.test_administration_channel(),
                    ),
                ),
                f"""discord_id={testing.test_member().id}""",
                True,
            ),
            # Test case: name given, matching ban found
            (
                testing.Context(
                    channel=testing.test_administration_channel(),
                    message=testing.Message(
                        author=testing.test_admin(),
                        channel=testing.test_administration_channel(),
                    ),
                ),
                f"""name="{testing.test_member().name}" """,
                True,
            ),
            # Test case: nick given, matching ban found
            (
                testing.Context(
                    channel=testing.test_administration_channel(),
                    message=testing.Message(
                        author=testing.test_admin(),
                        channel=testing.test_administration_channel(),
                    ),
                ),
                f"""nick="{testing.test_member().nick}" """,
                True,
            ),
            # Test case: no matching ban for member
            (
                testing.Context(
                    channel=testing.test_administration_channel(),
                    message=testing.Message(
                        author=testing.test_admin(),
                        channel=testing.test_administration_channel(),
                        mentions=[testing.test_admin()],
                    ),
                ),
                None,
                False,
            ),
        ],
    )
    @pytest.mark.asyncio()
    async def test_search_ban(self, cog, ctx, content, match_expected):
        cog.bot.database_uow.existing_members = [
            testing.test_database_member().to_dict()
        ]
        cog.bot.database_uow.existing_bans = [testing.test_ban().to_dict()]

        await testing.invoke_cog_command(cog, "search_ban", ctx=ctx, content=content)

        channel = ctx.channel
        assert testing.received_message(channel)
        [message] = channel.messages_received
        assert testing.message_contains_embed(message)
        content = message.kwargs.embed.description
        if match_expected:
            assert ", matching ban found" in content
        else:
            assert ", no matching ban found" in content

    @pytest.mark.parametrize(
        (
            "global_ban_role_exists",
            "ctx",
            "content",
            "expected",
            "expected_ban_type_changed",
            "expected_ban_role",
        ),
        [
            # Test case: Everything correct, member mentioned
            (
                False,
                testing.Context(
                    guild=testing.Guild(),
                    channel=testing.test_administration_channel(),
                    message=testing.Message(
                        author=testing.test_admin(),
                        channel=testing.test_administration_channel(),
                        mentions=[testing.test_member()],
                    ),
                ),
                'reason="test reason changed"',
                {"reason": "test reason changed"},
                False,
                None,
            ),
            # Test case: Everything correct, discord ID given
            (
                False,
                testing.Context(
                    guild=testing.Guild(),
                    channel=testing.test_administration_channel(),
                    message=testing.Message(
                        author=testing.test_admin(),
                        channel=testing.test_administration_channel(),
                        mentions=[],
                    ),
                ),
                'discord_id=1 reason="test reason changed"',
                {"reason": "test reason changed"},
                False,
                None,
            ),
            # Test case: Everything correct, member mentioned and globally banned,
            # global ban role does not exist
            (
                False,
                testing.Context(
                    guild=testing.Guild(),
                    channel=testing.test_administration_channel(),
                    message=testing.Message(
                        author=testing.test_admin(),
                        channel=testing.test_administration_channel(),
                        mentions=[testing.test_member()],
                    ),
                ),
                'global=1 reason="test reason changed"',
                {"global_ban": True, "reason": "test reason changed"},
                True,
                None,
            ),
            # Test case: Everything correct, member mentioned and globally banned
            # global ban role exists
            (
                True,
                testing.Context(
                    guild=testing.Guild(),
                    channel=testing.test_administration_channel(),
                    message=testing.Message(
                        author=testing.test_admin(),
                        channel=testing.test_administration_channel(),
                        mentions=[testing.test_member()],
                    ),
                ),
                'global=1 reason="test reason changed"',
                {"global_ban": True, "reason": "test reason changed"},
                True,
                testing.GlobalBanRole,
            ),
            # Test case: Everything correct, member mentioned and pugs banned
            (
                False,
                testing.Context(
                    guild=testing.Guild(),
                    channel=testing.test_administration_channel(),
                    message=testing.Message(
                        author=testing.test_admin(),
                        channel=testing.test_administration_channel(),
                        mentions=[testing.test_member()],
                    ),
                ),
                'pugs=1 reason="test reason changed"',
                {"pug_ban": True, "reason": "test reason changed"},
                True,
                testing.PUGBanRole,
            ),
            # Test case: Incorrect channel
            (
                False,
                testing.Context(
                    guild=testing.Guild(),
                    channel=testing.Channel(id="incorrect channel"),
                    message=testing.Message(
                        author=testing.test_admin(),
                        channel=testing.Channel(id="incorrect channel"),
                    ),
                ),
                None,
                None,
                False,
                None,
            ),
        ],
    )
    @pytest.mark.asyncio()
    async def test_update_ban(
        self,
        global_ban_role_exists,
        cog,
        ctx,
        content,
        expected,
        expected_ban_type_changed,
        expected_ban_role,
    ):
        cog.bot.has_global_ban_role = global_ban_role_exists
        cog.bot.database_uow.existing_members = [
            testing.test_database_member().to_dict()
        ]
        cog.bot.database_uow.existing_bans = [testing.test_ban().to_dict()]

        await testing.invoke_cog_command(cog, "update_ban", ctx=ctx, content=content)

        if expected is not None:
            assert cog.bot.database_uow.bans.updated
            [result] = cog.bot.database_uow.bans.updated

            for key, value in expected.items():
                assert getattr(result, key) == value

            if expected_ban_type_changed:
                [member] = ctx.message.mentions
                if expected_ban_role is not None:
                    assert testing.member_has_role(member, expected_ban_role)
                else:
                    assert member.globally_banned
        else:
            assert not testing.database_uow_called(cog.bot.database_uow)

    @pytest.mark.parametrize(
        ("global_ban_role_exists", "ctx", "content", "expected"),
        [
            (
                False,
                testing.Context(
                    guild=testing.Guild(),
                    channel=testing.test_administration_channel(
                        guild=testing.Guild(members=[testing.test_member()])
                    ),
                    message=testing.Message(
                        author=testing.test_admin(),
                        channel=testing.test_administration_channel(),
                        mentions=[testing.test_member_pug_banned],
                    ),
                ),
                """reason="test reason" """,
                True,
            ),
            (
                False,
                testing.Context(
                    guild=testing.Guild(),
                    channel=testing.test_administration_channel(
                        guild=testing.Guild(members=[testing.test_member()])
                    ),
                    message=testing.Message(
                        author=testing.test_admin(),
                        channel=testing.test_administration_channel(),
                    ),
                ),
                """name="test_name" reason="test reason" """,
                True,
            ),
        ],
    )
    @pytest.mark.asyncio()
    async def test_lift_ban(
        self,
        cog,
        global_ban_role_exists,
        ctx,
        content,
        expected,
    ):
        cog.bot.has_global_ban_role = global_ban_role_exists
        cog.bot.database_uow.existing_members = [
            testing.test_database_member().to_dict()
        ]
        cog.bot.database_uow.existing_bans = [testing.test_ban().to_dict()]

        await testing.invoke_cog_command(cog, "lift_ban", ctx=ctx, content=content)
        result = cog.bot.database_uow.bans.deleted

        if expected:
            if ctx.message.mentions:
                [member] = ctx.message.mentions

                assert not member.roles
            assert result
        else:
            if ctx.message.mentions:
                [member] = ctx.message.mentions

                assert not member.roles
            assert not result
