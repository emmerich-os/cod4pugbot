import pytest

import cod4pugbot.domain.features.contact_admins.cog as contact_admins
from cod4pugbot import testing


@pytest.fixture()
def cog(bot):
    return contact_admins.ContactAdmin(bot=bot)


class TestContactAdmin:
    @pytest.mark.parametrize(
        ("ctx", "content", "expected"),
        [
            # Test case: no admins in guild
            (
                testing.Context(
                    channel=testing.Channel(),
                    message=testing.Message(
                        author=testing.test_member(), channel=testing.Channel()
                    ),
                    content="test_message",
                    guild=testing.Guild(
                        members=[
                            testing.Member(
                                name="test_random_member",
                                guild_permissions=testing.GuildPermissions(
                                    administrator=False
                                ),
                            ),
                        ]
                    ),
                ),
                "test_message",
                False,
            ),
            # Test case: 1 admin in guild
            (
                testing.Context(
                    channel=testing.Channel(),
                    message=testing.Message(
                        author=testing.test_member(), channel=testing.Channel()
                    ),
                    content="test_message",
                    guild=testing.Guild(
                        members=[
                            testing.test_admin(),
                            testing.Member(
                                name="test_random_member",
                                guild_permissions=testing.GuildPermissions(
                                    administrator=False
                                ),
                            ),
                        ]
                    ),
                ),
                "test_message",
                True,
            ),
        ],
    )
    @pytest.mark.asyncio()
    async def test_send_embed_to_admins(self, cog, ctx, content, expected):
        await testing.invoke_cog_command(cog, "contact_admin", ctx=ctx, content=content)

        channel = ctx.channel
        if expected:
            admin = ctx.guild.members[0]
            assert testing.received_message(admin)
            assert testing.received_message(channel)
        else:
            assert testing.received_message(channel)
