import pytest

from cod4pugbot import testing
from cod4pugbot.domain.events import CommandFailed
from cod4pugbot.domain.events import CommandSuccessful
from cod4pugbot.domain.features.contact_admins import commands
from cod4pugbot.domain.features.contact_admins import handlers


@pytest.mark.parametrize(
    ("command", "contact_expected"),
    [
        (
            commands.ContactAdmins(
                member=testing.test_member(),
                channel=testing.Channel(),
                message=testing.Message(
                    author=testing.test_member(), channel=testing.Channel()
                ),
                content="test_message",
                admins=[testing.test_admin()],
            ),
            True,
        ),
        (
            commands.ContactAdmins(
                member=testing.test_member(),
                channel=testing.Channel(),
                message=testing.Message(
                    author=testing.test_member(), channel=testing.Channel()
                ),
                content="test_message",
                admins=[],
            ),
            False,
        ),
    ],
)
@pytest.mark.asyncio()
async def test_send_embed_to_admins(simple_uow, command, contact_expected):
    await handlers.send_embed_to_admins(command=command, uow=simple_uow)

    if contact_expected:
        [admin] = command.admins
        assert testing.received_message(admin)
        [message] = admin.messages_received
        assert testing.message_contains_embed(message)
        assert testing.uow_has_events(simple_uow, CommandSuccessful)
    else:
        assert testing.uow_has_events(simple_uow, CommandFailed)
