from unittest import mock

import pytest

import cod4pugbot.domain.features.pugs.cod4


@pytest.fixture()
def server():
    with mock.patch("socket.socket"), mock.patch(
        "cod4pugbot.cod4.Server._get_details", return_value=(None, None)
    ):
        return cod4pugbot.cod4.Server.connect(
            pug="test_pug",
            ip="test_ip",
            port=1,
        )
