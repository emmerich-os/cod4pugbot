INFO = [
    "ÿÿÿÿinfoResponse",
    "\\game\\mods/txc-pm\\voice\\0\\mod\\1\\hw\\1\\od\\1\\hc\\1\\ki\\1\\ff\\0\\pswrd\\0\\shortversion\\x18\\build\\1004\\pure\\1\\gametype\\sd\\sv_maxclients\\24\\g_humanplayers\\9\\clients\\9\\mapname\\mp_strike\\type\\0\\hostname\\Toxic Gamers ^2|^7 Promod^2L^7ive220 S^3&^7D - Round: 6/20\\protocol\\6",
]
STATUS = [
    "ÿÿÿÿstatusResponse",
    "\\type\\0\\fs_game\\mods/txc-pm\\sv_maxclients\\24\\version\\CoD4 X - linux-i386 build 1004 May 26 2020\\shortversion\\-\\build\\1004\\branch\\basedonoldstable\\revision\\ecfad090406a58c2a458a43e8a62297720767e5e\\_CoD4 X Site\\http://cod4x.me\\protocol\\18\\sv_privateClients\\0\\sv_hostname\\Toxic Gamers ^2|^7 Promod^2L^7ive220 S^3&^7D - Round: 6/20\\sv_minPing\\0\\sv_maxPing\\0\\sv_disableClientConsole\\0\\sv_voice\\0\\g_mapStartTime\\Mon Nov  2 20:03:46 2020\\uptime\\15 days\\g_gametype\\sd\\mapname\\mp_strike\\sv_maxRate\\25000\\sv_floodprotect\\4\\sv_pure\\1\\gamename\\Call of Duty 4\\g_compassShowEnemies\\0",
    '0 46 "[KKC]SANAPUN"',
    '0 53 "[KKC]RAMbo"',
    '0 38 "elena kitic"',
    '35 38 "variable.handi`"',
    '10 60 "nosound"',
    '20 194 "F e r n a n d o."',
    '10 25 "Clegan!"',
    '45 25 "DAVIDDANTHEZ"',
    '5 38 "S|T|E|4|L|T|H"',
    "",
]

INFO_SHORT = [
    "ÿÿÿÿinfoResponse",
    "\\test_key_1\\test_value_1\\test_key_2\\test_value_2",
]
STATUS_SHORT = [
    "ÿÿÿÿstatusResponse",
    "\\test_key_1\\test_value_1\\test_key_2\\test_value_2",
    '0 999 "test_player_1"',
    '5 999 "test_player_2"',
    "",
]

STATUS_DICT = {
    "test_key_1": "test_value_1",
    "test_key_2": "test_value_2",
    "players": ['0 999 "test_player_1"', '5 999 "test_player_2"'],
}

PROMOD_SCORE_1 = "1\x01test_player_1\x011\x0113\x013\x017\x010"
PROMOD_SCORE_2 = "2\x01test_player_1\x011\x0113\x013\x017\x010\x01test_player_2\x010\x01133\x017\x010\x011"
