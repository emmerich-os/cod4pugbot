import dataclasses
from unittest import mock

import freezegun
import pytest
import pytz

from cod4pugbot.domain.features.pugs import cod4
from cod4pugbot.domain.features.pugs import pug
from cod4pugbot.domain.features.pugs.cod4 import gameserver
from cod4pugbot.domain.features.pugs.cod4 import scorebot
from cod4pugbot.domain.features.pugs.cod4 import utils
from tests.unit.domain.features.pugs.cod4.data import response_examples


@dataclasses.dataclass
class Bot:
    timezone: pytz.timezone = pytz.timezone("UTC")


@dataclasses.dataclass
class Cog:
    bot: Bot = Bot()


@dataclasses.dataclass(eq=False)
class PUG(pug.RunningPUG):
    mode: str = None
    attack: dict = None
    defence: dict = None
    cog: Cog = Cog()


@dataclasses.dataclass(eq=False)
class Response:
    country: str
    city: str
    region: str


@dataclasses.dataclass(eq=False)
class Player(utils.players.Player):
    name: str
    ping: int
    score: int


@dataclasses.dataclass(eq=False)
class PromodPlayer(utils.players.PromodPlayer):
    team: str
    name: str
    alive: bool
    kills: int
    assists: int
    deaths: int
    bombcarrier: bool
    score: int
    ping: int = None
    pb_guid: str = None
    ip: str = None


@dataclasses.dataclass(eq=False)
class PromodTeam(utils.teams.PromodTeam):
    name: str = None
    score: int = None
    players: list = None

    def __eq__(self, other) -> bool:
        """Compare to `Player`.

        Returns:
            `True` if other `PromodTeam` is the same, `False` otherwise.
            `True` if team name is equal to other, `False` otherwise.

        """
        if self.players and isinstance(other, PromodTeam):
            return self.players == other.players
        return (
            self.name == other.name
            if isinstance(other, PromodTeam)
            else self.name == other
        )


@dataclasses.dataclass(eq=False)
class BombExploded(scorebot.BombExploded):
    name: str = None


@dataclasses.dataclass(eq=False)
class Captured(scorebot.Captured):
    name: str = None
    player: str = None


@dataclasses.dataclass(eq=False)
class DefusedBy(scorebot.DefusedBy):
    name: str = None
    player: str = None


@dataclasses.dataclass(eq=False)
class DroppedBomb(scorebot.DroppedBomb):
    name: str = None
    player: str = None


@dataclasses.dataclass(eq=False)
class HQCaptured(scorebot.HQCaptured):
    name: str = None
    player: str = None


@dataclasses.dataclass(eq=False)
class HQDestroyed(scorebot.HQDestroyed):
    name: str = None
    player: str = None


@dataclasses.dataclass(eq=False)
class Kill(scorebot.Kill):
    name: str = None
    killer: str = None
    weapon: str = None
    killed: str = None
    headshot: bool = None
    assistant: str = None


@dataclasses.dataclass(eq=False)
class Kniferound(scorebot.KnifeRound):
    name: str = None


@dataclasses.dataclass(eq=False)
class Map(scorebot.Map):
    name: str = None
    mapname: str = None
    gametype: str = None


@dataclasses.dataclass(eq=False)
class MapComplete(scorebot.MapComplete):
    name: str = None
    attack_score: int = None
    defence_score: int = None


@dataclasses.dataclass(eq=False)
class PickupBomb(scorebot.PickupBomb):
    name: str = None
    player: str = None


@dataclasses.dataclass(eq=False)
class PlantedBy(scorebot.PlantedBy):
    name: str = None
    player: str = None


@dataclasses.dataclass(eq=False)
class RdyText(scorebot.RdyText):
    name: str = None


@dataclasses.dataclass(eq=False)
class RoundWinner(scorebot.RoundWinner):
    name: str = None
    winner: str = None
    attack_score: int = None
    defence_score: int = None


@dataclasses.dataclass(eq=False)
class StartText(scorebot.StartText):
    name: str = None
    round: int = None


@dataclasses.dataclass(eq=False)
class TimeoutCalled(scorebot.TimeoutCalled):
    name: str = None
    team: str = None
    player: str = None


@dataclasses.dataclass(eq=False)
class TimeoutCancelled(scorebot.TimeoutCancelled):
    name: str = None
    team: str = None
    player: str = None


DEFAULT_COUNTER = {
    "maps": 0,
    "rounds": 0,
    "attack": {"maps": 0, "rounds": 0},
    "defence": {"maps": 0, "rounds": 0},
}


class TestServer:
    @pytest.mark.parametrize(
        "info,status",
        [
            (None, None),
        ],
    )
    def test_connect(self, monkeypatch, info, status):
        monkeypatch.setattr(
            gameserver.Server,
            "_get_details",
            _get_details := mock.MagicMock(return_value=(info, status)),
        )
        with mock.patch("socket.socket"):
            result = cod4.Server.connect(
                pug="test_pug",
                ip="test_ip",
                port=1,
            )

        assert isinstance(result, cod4.Server)
        _get_details.assert_called_once()

    @pytest.mark.parametrize(
        "location,expected",
        [
            (
                Response(
                    country="test_country", city="test_city", region="test_region"
                ),
                {
                    "country": "test_country",
                    "city": "test_city",
                    "region": "test_region",
                },
            )
        ],
    )
    def test_get_location(self, monkeypatch, server, location, expected):
        monkeypatch.setattr(
            gameserver.DbIpCity,
            "get",
            get := mock.MagicMock(return_value=location),
        )

        server._get_location()

        get.assert_called_once_with("test_ip", api_key="free")
        assert server.location == expected

    @pytest.mark.parametrize(
        "rcon_password,responses,expected",
        [
            (
                None,
                (None, None, None),
                (None, None, None),
            ),
            (
                "test_rcon",
                (None, None, None),
                (None, None, None),
            ),
            (
                None,
                (response_examples.INFO_SHORT, response_examples.STATUS_SHORT, None),
                (
                    {"test_key_1": "test_value_1", "test_key_2": "test_value_2"},
                    {
                        "test_key_1": "test_value_1",
                        "test_key_2": "test_value_2",
                        "players": [
                            '0 999 "test_player_1"',
                            '5 999 "test_player_2"',
                        ],
                    },
                    None,
                ),
            ),
        ],
    )
    def test_send_requests(
        self, monkeypatch, server, rcon_password, responses, expected
    ):
        _responses = iter(responses)

        def return_responses(*args, **kwargs):
            return next(_responses)

        monkeypatch.setattr(
            server,
            "_send_request",
            _send_request := mock.MagicMock(side_effect=return_responses),
        )
        server.rcon_password = rcon_password

        result = server._send_requests(server=None)

        assert _send_request.call_count == 3 if rcon_password is not None else 2
        assert result == expected

    @pytest.mark.parametrize(
        "info,status,expected",
        [
            ({}, {}, (None, None, None, None)),
            ({"clients": "1"}, {}, (1, None, None, None)),
            ({"mapname": "test_map"}, {}, (None, "test_map", None, None)),
            ({}, {"mapname": "test_map"}, (None, "test_map", None, None)),
            (
                {"mapname": "test_map"},
                {"mapname": "test_map"},
                (None, "test_map", None, None),
            ),
            ({"gametype": "test_gametype"}, {}, (None, None, "test_gametype", None)),
            ({}, {"g_gametype": "test_gametype"}, (None, None, "test_gametype", None)),
            (
                {"gametype": "test_gametype"},
                {"g_gametype": "test_gametype"},
                (None, None, "test_gametype", None),
            ),
            ({}, {"__promod_mode": "test_mode"}, (None, None, None, "test_mode")),
        ],
    )
    def test_update_basic_details(self, server, info, status, expected):
        server._update_basic_details(info=info, status=status)
        result = (server.clients, server.map, server.gametype, server.promod_mode)

        assert result == expected

    @pytest.mark.parametrize(
        "status,expected",
        [
            # 1. case: no info
            ({}, (None, None, None)),
            # 2. case: status with player infos
            (
                response_examples.STATUS_DICT,
                (
                    [
                        Player(name="test_player_2", ping=999, score=5),
                        Player(name="test_player_1", ping=999, score=0),
                    ],
                    None,
                    None,
                ),
            ),
            # 3. case: promod score stats about attack with 1 player
            (
                {"__promod_attack_score": response_examples.PROMOD_SCORE_1},
                (
                    None,
                    PromodTeam(
                        name="attack",
                        score=1,
                        players=[
                            PromodPlayer(
                                team="attack",
                                name="test_player_1",
                                alive=True,
                                kills=13,
                                assists=3,
                                deaths=7,
                                bombcarrier=False,
                                score=13 * 5 + 3 * 3,
                            )
                        ],
                    ),
                    None,
                ),
            ),
            # 4. case: promod score stats about defence with 1 player
            (
                {"__promod_defence_score": response_examples.PROMOD_SCORE_1},
                (
                    None,
                    None,
                    PromodTeam(
                        name="defence",
                        score=1,
                        players=[
                            PromodPlayer(
                                team="defence",
                                name="test_player_1",
                                alive=True,
                                kills=13,
                                assists=3,
                                deaths=7,
                                bombcarrier=False,
                                score=13 * 5 + 3 * 3,
                            )
                        ],
                    ),
                ),
            ),
            # 5. case: promod score stats about attack with 2 players
            (
                {"__promod_attack_score": response_examples.PROMOD_SCORE_2},
                (
                    None,
                    PromodTeam(
                        name="attack",
                        score=2,
                        players=[
                            PromodPlayer(
                                team="attack",
                                name="test_player_2",
                                alive=False,
                                kills=133,
                                assists=7,
                                deaths=0,
                                bombcarrier=True,
                                score=133 * 5 + 7 * 3,
                            ),
                            PromodPlayer(
                                team="attack",
                                name="test_player_1",
                                alive=True,
                                kills=13,
                                assists=3,
                                deaths=7,
                                bombcarrier=False,
                                score=13 * 5 + 3 * 3,
                            ),
                        ],
                    ),
                    None,
                ),
            ),
        ],
    )
    def test_update_players_and_teams(self, server, status, expected):
        result = server._update_players_and_teams(status)

        assert result[0] == expected[0]
        if expected[1] is not None:
            assert result[1].name == expected[1].name
            assert result[1].score == expected[1].score
            assert result[1].players == expected[1].players
            for res, exp in zip(result[1].players, expected[1].players):
                assert res.team == exp.team
                assert res.name == exp.name
                assert res.alive == exp.alive
                assert res.kills == exp.kills
                assert res.assists == exp.assists
                assert res.deaths == exp.deaths
                assert res.bombcarrier == exp.bombcarrier
                assert res.score == exp.score
        else:
            assert expected[1] is None
        if expected[2] is not None:
            assert result[2].name == expected[2].name
            assert result[2].score == expected[2].score
            assert result[2].players == expected[2].players
            for res, exp in zip(result[2].players, expected[2].players):
                assert res.team == exp.team
                assert res.name == exp.name
                assert res.alive == exp.alive
                assert res.kills == exp.kills
                assert res.assists == exp.assists
                assert res.deaths == exp.deaths
                assert res.bombcarrier == exp.bombcarrier
                assert res.score == exp.score
        else:
            assert expected[2] is None

    @pytest.mark.parametrize(
        "status,expected",
        [({"g_mapStartTime": "Mon Jan 1 00:00:00 2020"}, 1)],
    )
    @freezegun.freeze_time("2020-01-01 00:01")
    def test_update_map_uptime(self, server, status, expected):
        result = server._update_map_uptime(status)

        assert result == expected

    @pytest.mark.parametrize(
        "players,attack,defence",
        [
            (
                [
                    Player(name="test_player_1", ping=999, score=0),
                ],
                PromodTeam(
                    name="attack",
                    score=0,
                    players=None,
                ),
                PromodTeam(
                    name="defence",
                    score=1,
                    players=None,
                ),
            ),
            (
                [
                    Player(name="test_player_1", ping=999, score=0),
                ],
                PromodTeam(
                    name="attack",
                    score=0,
                    players=[],
                ),
                PromodTeam(
                    name="defence",
                    score=1,
                    players=[],
                ),
            ),
        ],
    )
    def test_update_player_info(self, monkeypatch, server, players, attack, defence):
        monkeypatch.setattr(
            utils.teams.PromodTeam,
            "add_info",
            add_info := mock.MagicMock(),
        )
        server.players = players.copy()
        server.attack = attack
        server.defence = defence

        server._update_player_info()

        if (
            attack is not None
            and defence is not None
            and attack.players is not None
            and defence.players is not None
        ):
            assert add_info.call_count == len(players) * 2
            add_info.assert_has_calls(
                [
                    mock.call(name=player.name, score=player.score, ping=player.ping)
                    for player in players
                ]
            )
        else:
            add_info.assert_not_called()

    @pytest.mark.parametrize(
        "pb_plist,expected",
        [
            (None, None),
            ([0, 1], None),
            # pb plist first and second last elements not required
            # 1. case: 3 players
            (
                [
                    0,
                    1,
                    '"test_name_1" 12345678901234567890123456789011 1.1.1.1',
                    '"test_name_2" 12345678901234567890123456789012 1.1.1.2',
                    '"test_name_3" 12345678901234567890123456789013 1.1.1.3',
                    "3 Players",
                    -1,
                ],
                [
                    ("test_name_1", "56789011", "1.1.1.1"),
                    ("test_name_1", "56789011", "1.1.1.1"),
                    ("test_name_2", "56789012", "1.1.1.2"),
                    ("test_name_2", "56789012", "1.1.1.2"),
                    ("test_name_3", "56789013", "1.1.1.3"),
                    ("test_name_3", "56789013", "1.1.1.3"),
                ],
            ),
            # 2. case: wrong name
            (
                [
                    0,
                    1,
                    "test_name_1 12345678901234567890123456789012 1.1.1.1",
                    "1 Player",
                    -1,
                ],
                None,
            ),
            # 3. case: wrong GUID
            (
                [
                    0,
                    1,
                    '"test_name_1" 1234567890123456789012345678901 1.1.1.1',
                    "1 Player",
                    -1,
                ],
                None,
            ),
            # 3. case: wrong ip
            (
                [
                    0,
                    1,
                    '"test_name_1" 12345678901234567890123456789012 1.1.1',
                    "1 Player",
                    -1,
                ],
                None,
            ),
        ],
    )
    def test_update_player_pb_info(self, monkeypatch, server, pb_plist, expected):
        monkeypatch.setattr(
            utils.teams.PromodTeam,
            "add_info",
            add_info := mock.MagicMock(return_value="test_player"),
        )
        monkeypatch.setattr(
            utils.stats_tracker.StatsTracker,
            "update_player",
            update_player := mock.MagicMock(),
        )
        server.attack = PromodTeam(
            name="attack",
            score=0,
            players=[],
        )
        server.defence = PromodTeam(
            name="defence",
            score=1,
            players=[],
        )

        server._update_player_pb_info(pb_plist)

        if expected is not None:
            assert add_info.call_count == len(pb_plist[2:-2]) * 2
            assert update_player.call_count == len(pb_plist[2:-2]) * 2
            add_info.assert_has_calls(
                [
                    mock.call(name=name, pb_guid=pb_guid, ip=ip)
                    for name, pb_guid, ip in expected
                ]
            )
            update_player.assert_has_calls(
                [mock.call("test_player") for _ in range(len(expected))]
            )
        else:
            add_info.assert_not_called()
            update_player.assert_not_called()

    @pytest.mark.parametrize(
        "prev_tick,status,new_tick,expected",
        [
            ("1", None, "1", None),
            ("1", {}, "1", None),
            ("1", {"__promod_ticker": "1"}, "1", None),
            ("1", {"__promod_ticker": "2"}, "2", None),
            (
                "1",
                {"__promod_ticker": "2\x01round_winner\x01attack\x011\x010"},
                "2",
                [
                    RoundWinner(
                        name="round_winner",
                        winner="attack",
                        attack_score=1,
                        defence_score=0,
                    )
                ],
            ),
            (
                "1",
                {"__promod_ticker": "2\x01map_complete\x01attack\x011\x01defence\x010"},
                "2",
                [MapComplete(name="map_complete", attack_score=1, defence_score=0)],
            ),
            (
                "1",
                {"__promod_ticker": "2\x01knife_round"},
                "2",
                [Kniferound(name="knife_round")],
            ),
            (
                "1",
                {"__promod_ticker": "2\x011st_half_started\x011"},
                "2",
                [StartText(name="1st_half_started", round=1)],
            ),
            (
                "1",
                {"__promod_ticker": "2\x01map\x01test_map\x01test_gametype"},
                "2",
                [Map(name="map")],
            ),
            (
                "1",
                {
                    "__promod_ticker": "2\x01kill\x01test_killer\x01test_weapon\x01test_killed\x010"
                },
                "2",
                [
                    Kill(
                        name="kill",
                        killer="test_killer",
                        weapon="test_weapon",
                        killed="test_killed",
                        headshot=False,
                    )
                ],
            ),
            (
                "1",
                {"__promod_ticker": "2\x011st_half_ready_up"},
                "2",
                [RdyText(name="1st_half_ready_up")],
            ),
            (
                "1",
                {"__promod_ticker": "2\x01timeout_cancelled\x01attack\x01test_player"},
                "2",
                [
                    TimeoutCancelled(
                        name="timeout_cancelled", team="attack", player="test_player"
                    )
                ],
            ),
            (
                "1",
                {"__promod_ticker": "2\x01timeout_called\x01attack\x01test_player"},
                "2",
                [
                    TimeoutCalled(
                        name="timeout_called", team="attack", player="test_player"
                    )
                ],
            ),
            (
                "1",
                {"__promod_ticker": "2\x01captured\x01test_player"},
                "2",
                [Captured(name="captured", player="test_player")],
            ),
            (
                "1",
                {"__promod_ticker": "2\x01hq_captured\x01test_player"},
                "2",
                [HQCaptured(name="hq_captured", player="test_player")],
            ),
            (
                "1",
                {"__promod_ticker": "2\x01hq_destroyed\x01test_player"},
                "2",
                [HQDestroyed(name="hq_destroyed", player="test_player")],
            ),
            (
                "1",
                {"__promod_ticker": "2\x01pickup_bomb\x01test_player"},
                "2",
                [PickupBomb(name="pickup_bomb", player="test_player")],
            ),
            (
                "1",
                {"__promod_ticker": "2\x01dropped_bomb\x01test_player"},
                "2",
                [DroppedBomb(name="dropped_bomb", player="test_player")],
            ),
            (
                "1",
                {"__promod_ticker": "2\x01defused_by\x01test_player"},
                "2",
                [DefusedBy(name="defused_by", player="test_player")],
            ),
            (
                "1",
                {"__promod_ticker": "2\x01bomb_exploded"},
                "2",
                [BombExploded(name="bomb_exploded")],
            ),
        ],
    )
    def test_update_and_convert_promod_ticker(
        self, server, prev_tick, status, new_tick, expected
    ):
        server.prev_tick = prev_tick

        result = server._update_and_convert_promod_ticker(status)

        assert server.prev_tick == new_tick
        assert result == expected

    @pytest.mark.parametrize(
        "map_winner,attack_score,defence_score",
        [("attack", 13, 9)],
    )
    def test_save_stats(
        self, monkeypatch, server, map_winner, attack_score, defence_score
    ):
        monkeypatch.setattr(
            utils.stats_tracker.StatsTracker,
            "save_stats_to_db",
            save_stats_to_db := mock.MagicMock(),
        )
        server.killfeed = ["test_event"]

        server._save_stats(
            map_winner=map_winner,
            attack_score=attack_score,
            defence_score=defence_score,
        )

        save_stats_to_db.assert_called_once_with(
            killfeed=["test_event"],
            map_winner=map_winner,
            attack_score=attack_score,
            defence_score=defence_score,
        )
        assert not server.killfeed

    @pytest.mark.parametrize(
        (
            "pug_mode,promod_mode,maps,start_winner_check,event,expected_return,"
            "expected_counter,expected_result,start_winner_check_expected"
        ),
        [
            # 1. case: invalid event, winner check False (remains False), promod_mode unkknown
            (
                "bo1",
                None,
                None,
                False,
                Kill(),
                (False, False),
                DEFAULT_COUNTER.copy(),
                (None, None),
                False,
            ),
            # 2. case: invalid event, winner check True (remains True), promod_mode unkknown
            (
                "bo1",
                None,
                None,
                True,
                Kill(),
                (False, False),
                DEFAULT_COUNTER.copy(),
                (None, None),
                True,
            ),
            # 3. case: invalid event, winner check False (remains False), promod_mode unknown
            (
                "bo1",
                None,
                None,
                False,
                Kill(),
                (False, False),
                DEFAULT_COUNTER.copy(),
                (None, None),
                False,
            ),
            # 4. case: invalid event, winner check True (remains True), promod_mode known
            (
                "bo1",
                "mr12",
                None,
                True,
                Kill(),
                (False, False),
                DEFAULT_COUNTER.copy(),
                (None, None),
                True,
            ),
            # 5. case: Start event (round started), winner check False (remains False)
            (
                "bo1",
                "mr12",
                None,
                False,
                StartText(name="round_start"),
                (False, False),
                DEFAULT_COUNTER.copy(),
                (None, None),
                False,
            ),
            # 5. case: 1st hast started, winner check False (turns to True)
            (
                "bo1",
                "mr12",
                None,
                False,
                StartText(name="1st_half_started"),
                (False, False),
                DEFAULT_COUNTER.copy(),
                (None, None),
                True,
            ),
            # 6. case: round won by attack (but not map): round counters get incremented
            (
                "bo1",
                "mr12",
                None,
                True,
                RoundWinner(winner="attack", attack_score=1, defence_score=1),
                (False, False),
                (
                    DEFAULT_COUNTER.copy()
                    | {"rounds": 1, "attack": {"maps": 0, "rounds": 1}}
                ),
                (None, None),
                True,
            ),
            # 7. case: round won by defence (but not map): round counters get incremented
            (
                "bo1",
                "mr12",
                None,
                True,
                RoundWinner(winner="defence", attack_score=1, defence_score=1),
                (False, False),
                (
                    DEFAULT_COUNTER.copy()
                    | {"rounds": 1, "defence": {"maps": 0, "rounds": 1}}
                ),
                (None, None),
                True,
            ),
            # 8. case: bo1, round and map (i.e. match) won by attack: map and round counter
            # gets incremented, winner check turns False
            (
                "bo1",
                "mr12",
                None,
                True,
                RoundWinner(winner="attack", attack_score=13, defence_score=0),
                (True, True),
                (
                    DEFAULT_COUNTER.copy()
                    | {"maps": 1, "rounds": 1, "attack": {"maps": 1, "rounds": 1}}
                ),
                ("win", "loss"),
                False,
            ),
            # 9. case: bo1, round and map (i.e. match) won by defence: map and round counter
            # gets incremented, winner check turns False
            (
                "bo1",
                "mr12",
                None,
                True,
                RoundWinner(winner="defence", attack_score=0, defence_score=13),
                (True, True),
                (
                    DEFAULT_COUNTER.copy()
                    | {"maps": 1, "rounds": 1, "defence": {"maps": 1, "rounds": 1}}
                ),
                ("loss", "win"),
                False,
            ),
            # 10. case: bo1, attack won round, but map is draw: map and round counter gets
            # incremented, winner check turns False
            (
                "bo1",
                "mr12",
                None,
                True,
                RoundWinner(winner="attack", attack_score=12, defence_score=12),
                (True, True),
                (
                    DEFAULT_COUNTER.copy()
                    | {"maps": 1, "rounds": 1, "attack": {"maps": 0, "rounds": 1}}
                ),
                ("draw", "draw"),
                False,
            ),
            # 11. case: bo1, defence won round, but map is draw: map and round counter
            # gets incremented, winner check turns False
            (
                "bo1",
                "mr12",
                None,
                True,
                RoundWinner(winner="defence", attack_score=12, defence_score=12),
                (True, True),
                (
                    DEFAULT_COUNTER.copy()
                    | {"maps": 1, "rounds": 1, "defence": {"maps": 0, "rounds": 1}}
                ),
                ("draw", "draw"),
                False,
            ),
            # 12. case: bo3, attack won round and map (but not match): map and round counter get
            # incremented, winner check turns False
            (
                "bo3",
                "mr12",
                None,
                True,
                RoundWinner(winner="attack", attack_score=13, defence_score=0),
                (True, False),
                (
                    DEFAULT_COUNTER.copy()
                    | {"maps": 1, "rounds": 1, "attack": {"maps": 1, "rounds": 1}}
                ),
                (None, None),
                False,
            ),
            # 13. case: bo3, defence won round and map (but not match): map and round counter get
            # incremented, winner check turns False
            (
                "bo3",
                "mr12",
                None,
                True,
                RoundWinner(winner="defence", attack_score=0, defence_score=13),
                (True, False),
                (
                    DEFAULT_COUNTER.copy()
                    | {"maps": 1, "rounds": 1, "defence": {"maps": 1, "rounds": 1}}
                ),
                (None, None),
                False,
            ),
            # 14. case: bo3, attack won round, map, and match: map and round counter get
            # incremented, winner check turns False
            (
                "bo3",
                "mr12",
                ("attack", 1),
                True,
                RoundWinner(winner="attack", attack_score=13, defence_score=1),
                (True, True),
                (
                    DEFAULT_COUNTER.copy()
                    | {"maps": 1, "rounds": 1, "attack": {"maps": 2, "rounds": 1}}
                ),
                ("win", "loss"),
                False,
            ),
            # 15. case: bo3, defence won round, map, and match: map and round counter get
            # incremented, winner check turns False
            (
                "bo3",
                "mr12",
                ("defence", 1),
                True,
                RoundWinner(winner="defence", attack_score=1, defence_score=13),
                (True, True),
                (
                    DEFAULT_COUNTER.copy()
                    | {"maps": 1, "rounds": 1, "defence": {"maps": 2, "rounds": 1}}
                ),
                ("loss", "win"),
                False,
            ),
            # 16. case: bo3, attack won round, map is draw: map and round counter get
            # incremented, winner check turnse False
            (
                "bo3",
                "mr12",
                None,
                True,
                RoundWinner(winner="attack", attack_score=12, defence_score=12),
                (True, False),
                (
                    DEFAULT_COUNTER.copy()
                    | {"rounds": 1, "attack": {"maps": 0, "rounds": 1}}
                ),
                (None, None),
                False,
            ),
            # 17. case: bo3, defence won round, map is draw: map and round counter get
            # incremented, winner check turnse False
            (
                "bo3",
                "mr12",
                None,
                True,
                RoundWinner(winner="defence", attack_score=12, defence_score=12),
                (True, False),
                (
                    DEFAULT_COUNTER.copy()
                    | {"rounds": 1, "defence": {"maps": 0, "rounds": 1}}
                ),
                (None, None),
                False,
            ),
        ],
    )
    def test_check_event_for_winner(
        self,
        server,
        pug_mode,
        promod_mode,
        maps,
        start_winner_check,
        event,
        expected_return,
        expected_counter,
        expected_result,
        start_winner_check_expected,
    ):
        server.pug = PUG(mode=pug_mode)
        server.pug.attack = {"result": None}
        server.pug.defence = {"result": None}
        server.promod_mode = promod_mode
        if maps is not None:
            server.counter[maps[0]]["maps"] = maps[1]
        server.start_winner_check = start_winner_check

        result = server._check_event_for_winner(event)

        assert result == expected_return
        assert server.counter == expected_counter
        assert server.pug.attack["result"] == expected_result[0]
        assert server.pug.defence["result"] == expected_result[1]
        assert server.start_winner_check == start_winner_check_expected

    @pytest.mark.parametrize(
        "events,save_stats,end_pug,expected",
        [
            # 1. case: 2 events, no winner
            (
                [Kniferound(name="knife_round"), Kill(name="kill")],
                False,
                False,
                [Kniferound(name="knife_round"), Kill(name="kill")],
            ),
            # 2. case: 2 events, 1st event winner
            (
                [
                    RoundWinner(
                        name="round_winner",
                        winner="attack",
                        attack_score=1,
                        defence_score=0,
                    ),
                    Kill(name="kill"),
                ],
                True,
                True,
                [
                    RoundWinner(
                        name="round_winner",
                        winner="attack",
                        attack_score=1,
                        defence_score=0,
                    )
                ],
            ),
        ],
    )
    def test_add_to_killfeed_and_check_for_winner(
        self, monkeypatch, server, events, save_stats, end_pug, expected
    ):
        server.pug = PUG()
        monkeypatch.setattr(
            cod4.Server,
            "_check_event_for_winner",
            _check_event_for_winner := mock.MagicMock(
                return_value=(save_stats, end_pug)
            ),
        )
        monkeypatch.setattr(
            cod4.Server,
            "_save_stats",
            _save_stats := mock.MagicMock(),
        )
        monkeypatch.setattr(
            server.pug,
            "end_pug",
            _end_pug := mock.MagicMock(),
        )

        server._add_to_killfeed_and_check_for_winner(events)

        assert server.killfeed == expected
        _check_event_for_winner.assert_has_calls(
            [mock.call(event=event) for event in expected]
        )
        if save_stats:
            _save_stats.assert_called_with(
                map_winner=expected[0].winner,
                attack_score=expected[0].attack_score,
                defence_score=expected[0].defence_score,
            )
        if end_pug:
            _end_pug.assert_called_once_with(reported_by_scorebot=True)

    def test_get_details(self, monkeypatch, server):
        monkeypatch.setattr(
            cod4.Server,
            "_send_requests",
            _send_requests := mock.MagicMock(
                return_value=("test_info", "test_status", "test_pb_plist")
            ),
        )
        monkeypatch.setattr(
            cod4.Server,
            "_update_basic_details",
            _update_basic_details := mock.MagicMock(),
        )
        monkeypatch.setattr(
            cod4.Server,
            "_update_players_and_teams",
            _update_players_and_teams := mock.MagicMock(
                return_value=("test_players", "test_attack", "test_defence")
            ),
        )
        monkeypatch.setattr(
            cod4.Server,
            "_update_map_uptime",
            _update_map_uptime := mock.MagicMock(return_value="test_map_uptime"),
        )
        monkeypatch.setattr(
            cod4.Server,
            "_update_player_info",
            _update_player_info := mock.MagicMock(),
        )
        monkeypatch.setattr(
            cod4.Server,
            "_update_player_pb_info",
            _update_player_pb_info := mock.MagicMock(),
        )
        monkeypatch.setattr(
            cod4.Server,
            "_update_and_convert_promod_ticker",
            _update_promod_ticker := mock.MagicMock(return_value="test_prev_tick_info"),
        )
        monkeypatch.setattr(
            cod4.Server,
            "_add_to_killfeed_and_check_for_winner",
            _add_to_killfeed_and_check_for_winner := mock.MagicMock(),
        )

        result = server._get_details("test_server")

        _send_requests.assert_called_once_with(server="test_server")
        _update_basic_details.assert_called_once_with(
            info="test_info", status="test_status"
        )
        _update_players_and_teams.assert_called_once_with(status="test_status")
        assert server.players == "test_players"
        assert server.attack == "test_attack"
        assert server.defence == "test_defence"
        _update_map_uptime.assert_called_once_with(status="test_status")
        assert server.map_uptime == "test_map_uptime"
        _update_player_info.assert_called_once()
        _update_player_pb_info.assert_called_once_with(pb_plist="test_pb_plist")
        _update_promod_ticker.assert_called_once_with(status="test_status")
        assert server.current_killfeed == "test_prev_tick_info"
        _add_to_killfeed_and_check_for_winner.assert_called_once_with(
            events="test_prev_tick_info"
        )
        assert result == ("test_info", "test_status")

    def test_refresh(self):
        pass

    @pytest.mark.parametrize(
        "players,attack,defence,to_message,expected",
        [
            # 1. case: no players
            (None, None, None, False, []),
            # 2. case: 1 player, no promod players
            (
                [Player(name="0123456789123456", score=42, ping=999)],
                None,
                None,
                False,
                [
                    "Name             Score Ping",
                    "---------------------------",
                    "012345678912345     42  999",
                ],
            ),
            # 3. case: 2 players, no promod players
            (
                [
                    Player(name="0123456789123456", score=42, ping=999),
                    Player(name="test_player", score=5, ping=42),
                ],
                None,
                None,
                False,
                [
                    "Name             Score Ping",
                    "---------------------------",
                    "012345678912345     42  999",
                    "test_player          5   42",
                ],
            ),
            # 4. case: 1 alive player in attack, 0 in defence
            (
                None,
                PromodTeam(
                    name="attack",
                    score=13,
                    players=[
                        PromodPlayer(
                            team="attack",
                            name="0123456789123456",
                            alive=True,
                            kills=13,
                            assists=3,
                            deaths=7,
                            bombcarrier=False,
                            score=42,
                            ping=999,
                        ),
                    ],
                ),
                PromodTeam(name="defence", score=1, players=[]),
                False,
                [
                    "Attack 13 - 1 Defence\n",
                    "\tAttack (1)",
                    "      Name             Score Kills Assists Deaths Ping",
                    "------------------------------------------------------",
                    "      012345678912345     42    13       3      7  999",
                    "\n",
                    "\tDefence (0)",
                    "      Name             Score Kills Assists Deaths Ping",
                    "------------------------------------------------------",
                    "\n",
                ],
            ),
            # 5. case: 1 alive player in defence, 0 in attack
            (
                None,
                PromodTeam(
                    name="attack",
                    score=13,
                    players=[],
                ),
                PromodTeam(
                    name="defence",
                    score=1,
                    players=[
                        PromodPlayer(
                            team="defence",
                            name="0123456789123456",
                            alive=True,
                            kills=1,
                            assists=33,
                            deaths=77,
                            bombcarrier=False,
                            score=42,
                            ping=42,
                        ),
                    ],
                ),
                False,
                [
                    "Attack 13 - 1 Defence\n",
                    "\tAttack (0)",
                    "      Name             Score Kills Assists Deaths Ping",
                    "------------------------------------------------------",
                    "\n",
                    "\tDefence (1)",
                    "      Name             Score Kills Assists Deaths Ping",
                    "------------------------------------------------------",
                    "      012345678912345     42     1      33     77   42",
                    "\n",
                ],
            ),
            # 6. case: 1 dead player in attack, 1 player in defence carrying bomb
            (
                None,
                PromodTeam(
                    name="attack",
                    score=13,
                    players=[
                        PromodPlayer(
                            team="attack",
                            name="test_player_1",
                            alive=False,
                            kills=1,
                            assists=1,
                            deaths=1,
                            bombcarrier=False,
                            score=8,
                            ping=42,
                        ),
                    ],
                ),
                PromodTeam(
                    name="defence",
                    score=1,
                    players=[
                        PromodPlayer(
                            team="defence",
                            name="test_player_2",
                            alive=True,
                            kills=2,
                            assists=2,
                            deaths=2,
                            bombcarrier=True,
                            score=16,
                            ping=9,
                        ),
                    ],
                ),
                False,
                [
                    "Attack 13 - 1 Defence\n",
                    "\tAttack (1)",
                    "      Name             Score Kills Assists Deaths Ping",
                    "------------------------------------------------------",
                    " DEAD test_player_1        8     1       1      1   42",
                    "\n",
                    "\tDefence (1)",
                    "      Name             Score Kills Assists Deaths Ping",
                    "------------------------------------------------------",
                    " BOMB test_player_2       16     2       2      2    9",
                    "\n",
                ],
            ),
            # 6. case: to message, 1 dead + 1 alive player in attack,
            # 1 player in defence carrying bomb
            (
                None,
                PromodTeam(
                    name="attack",
                    score=13,
                    players=[
                        PromodPlayer(
                            team="attack",
                            name="test_player_1",
                            alive=False,
                            kills=1,
                            assists=1,
                            deaths=1,
                            bombcarrier=False,
                            score=8,
                            ping=42,
                        ),
                        PromodPlayer(
                            team="attack",
                            name="test_player_3",
                            alive=True,
                            kills=3,
                            assists=3,
                            deaths=3,
                            bombcarrier=False,
                            score=124,
                            ping=42,
                        ),
                    ],
                ),
                PromodTeam(
                    name="defence",
                    score=1,
                    players=[
                        PromodPlayer(
                            team="defence",
                            name="test_player_2",
                            alive=True,
                            kills=2,
                            assists=2,
                            deaths=2,
                            bombcarrier=True,
                            score=16,
                            ping=42,
                        ),
                    ],
                ),
                True,
                [
                    "<:marines:694918527777243136> 13 - 1 <:arabs:694918527299223573>\n",
                    "<:marines:694918527777243136> Attack (2)",
                    "    Name             Score Kills Assists Deaths Ping",
                    "----------------------------------------------------",
                    " <:dead:694918527727173642> test_player_1        8     1       1      1   42",
                    "  test_player_3      124     3       3      3   42",
                    "\n",
                    "<:arabs:694918527299223573> Defence (1)",
                    "    Name             Score Kills Assists Deaths Ping",
                    "----------------------------------------------------",
                    (
                        " <:explosives:694918527169331382> test_player_2"
                        "       16     2       2      2   42"
                    ),
                    "\n",
                ],
            ),
        ],
    )
    def test_format_scoreboard(
        self,
        server,
        players,
        attack,
        defence,
        to_message,
        expected,
    ):
        server.attack = attack
        server.defence = defence
        if players is not None:
            server.players = players.copy()

        result = server.format_scoreboard(to_message=to_message)

        assert result == expected

    @pytest.mark.parametrize(
        (
            "promod_version,promod_mode,location,clients,slots,map,"
            "players,attack,defence,add_scoreboard,expected"
        ),
        [
            # 1. case: promod_version
            (
                "test_version",
                None,
                None,
                None,
                None,
                None,
                None,
                None,
                None,
                False,
                {
                    "color": 65428,
                    "description": "test_version",
                    "fields": [{"inline": True, "name": "IP", "value": "test_ip:1"}],
                    "footer": {"text": "Last updated: 00:00 UTC"},
                    "title": "test_name",
                    "type": "rich",
                },
            ),
            # 2. case: location
            (
                None,
                None,
                {"country": "DE"},
                None,
                None,
                None,
                None,
                None,
                None,
                False,
                {
                    "color": 65428,
                    "fields": [
                        {"inline": True, "name": "IP", "value": "test_ip:1"},
                        {"inline": True, "name": "Location", "value": "DE"},
                    ],
                    "footer": {"text": "Last updated: 00:00 UTC"},
                    "title": "test_name",
                    "type": "rich",
                },
            ),
            # 3. case: clients
            (
                None,
                None,
                None,
                12,
                None,
                None,
                None,
                None,
                None,
                False,
                {
                    "color": 65428,
                    "fields": [
                        {"inline": True, "name": "IP", "value": "test_ip:1"},
                        {"inline": True, "name": "Players", "value": "12"},
                    ],
                    "footer": {"text": "Last updated: 00:00 UTC"},
                    "title": "test_name",
                    "type": "rich",
                },
            ),
            # 4. case: clients and slots
            (
                None,
                None,
                None,
                12,
                24,
                None,
                None,
                None,
                None,
                False,
                {
                    "color": 65428,
                    "fields": [
                        {"inline": True, "name": "IP", "value": "test_ip:1"},
                        {"inline": True, "name": "Players", "value": "12/24"},
                    ],
                    "footer": {"text": "Last updated: 00:00 UTC"},
                    "title": "test_name",
                    "type": "rich",
                },
            ),
            # 5. case: map
            (
                None,
                None,
                None,
                None,
                None,
                "test_map",
                None,
                None,
                None,
                False,
                {
                    "color": 65428,
                    "fields": [
                        {"inline": True, "name": "IP", "value": "test_ip:1"},
                        {"inline": True, "name": "Map", "value": "test_map"},
                    ],
                    "footer": {"text": "Last updated: 00:00 UTC"},
                    "title": "test_name",
                    "type": "rich",
                },
            ),
            # 6. case: attack, defence, and promod_mode, thus round and score
            (
                None,
                "mr12",
                None,
                None,
                None,
                None,
                None,
                PromodTeam(name="attack", score=11),
                PromodTeam(name="defence", score=1),
                False,
                {
                    "color": 65428,
                    "fields": [
                        {"inline": True, "name": "IP", "value": "test_ip:1"},
                        {"inline": True, "name": "Round", "value": "12/24"},
                        {
                            "inline": True,
                            "name": "Score",
                            "value": (
                                "<:marines:694918527777243136> 11 - "
                                "1 <:arabs:694918527299223573>"
                            ),
                        },
                    ],
                    "footer": {"text": "Last updated: 00:00 UTC"},
                    "title": "test_name",
                    "type": "rich",
                },
            ),
            # 7. case: players and add_scoreboard
            (
                None,
                None,
                None,
                None,
                None,
                None,
                ["test_player"],
                None,
                None,
                True,
                {
                    "color": 65428,
                    "fields": [
                        {"inline": True, "name": "IP", "value": "test_ip:1"},
                        {
                            "inline": False,
                            "name": "Scoreboard",
                            "value": "```\ntest_scoreboard\n```",
                        },
                    ],
                    "footer": {"text": "Last updated: 00:00 UTC"},
                    "title": "test_name",
                    "type": "rich",
                },
            ),
            # 8. case: attack and defence and add_scoreboard
            (
                None,
                None,
                None,
                None,
                None,
                None,
                None,
                PromodTeam(score=1),
                PromodTeam(score=1),
                True,
                {
                    "color": 65428,
                    "fields": [
                        {"inline": True, "name": "IP", "value": "test_ip:1"},
                        {
                            "inline": True,
                            "name": "Score",
                            "value": (
                                "<:marines:694918527777243136> 1 - "
                                "1 <:arabs:694918527299223573>"
                            ),
                        },
                        {
                            "inline": False,
                            "name": "Scoreboard",
                            "value": "```\ntest_scoreboard\n```",
                        },
                    ],
                    "footer": {"text": "Last updated: 00:00 UTC"},
                    "title": "test_name",
                    "type": "rich",
                },
            ),
        ],
    )
    def test_to_embed(
        self,
        monkeypatch,
        server,
        promod_version,
        promod_mode,
        location,
        clients,
        slots,
        map,
        players,
        attack,
        defence,
        add_scoreboard,
        expected,
    ):
        monkeypatch.setattr(
            cod4.Server,
            "format_scoreboard",
            format_scoreboard := mock.MagicMock(return_value=["test_scoreboard"]),
        )
        server.name = "test_name"
        server.pug = PUG()
        server.promod_version = promod_version
        server.promod_mode = promod_mode
        server.location = location
        server._clients = clients
        server.slots = slots
        server.map = map
        if players is not None:
            server.players = players.copy()
        server.attack = attack
        server.defence = defence

        with freezegun.freeze_time("2020-01-01 00:00"):
            result = server.to_embed(add_scoreboard=add_scoreboard).to_dict()

        if add_scoreboard and (
            players is not None or (attack is not None and defence is not None)
        ):
            format_scoreboard.assert_called_once()
        assert result == expected
