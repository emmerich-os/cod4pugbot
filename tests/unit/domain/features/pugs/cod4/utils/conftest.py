import dataclasses

import pytest

import cod4pugbot.domain.features.pugs.cod4


@dataclasses.dataclass
class PUG:
    pass


@pytest.fixture()
def stats_tracker():
    return cod4pugbot.cod4.utils.StatsTracker(pug=PUG())


@pytest.fixture(params=[0, 1, 2, 3, 4, 5])
def kills(request):
    return request.param


@pytest.fixture()
def stats():
    return {
        "score": 0,
        "kills": 0,
        "assists": 0,
        "deaths": 0,
        "maps_played": 0,
        "map_wins": 0,
        "map_losses": 0,
        "map_draws": 0,
        "other": 0,
    }
