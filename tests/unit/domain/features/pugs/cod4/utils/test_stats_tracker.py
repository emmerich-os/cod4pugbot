import dataclasses
from unittest import mock

import pytest

from cod4pugbot.domain.features.pugs.cod4 import scorebot
from cod4pugbot.domain.features.pugs.cod4 import utils


@dataclasses.dataclass(eq=False)
class IncompletePlayer:
    kills: int = 0


@dataclasses.dataclass(eq=False)
class Player(utils.PromodPlayer):
    name: str = None
    team: str = None
    pb_guid: str = None
    score: int = 0
    kills: int = 0
    assists: int = 0
    deaths: int = 0
    prev_score: int = 0
    prev_kills: int = 0
    prev_assists: int = 0
    prev_deaths: int = 0
    evaluated_kills: int = 0
    evaluated_assists: int = 0
    evaluated_deaths: int = 0
    evaluated_score: int = 0
    opening_kills: int = 0
    headshot_kills: int = 0
    opening_deaths: int = 0
    rifle_kills: int = 0
    smg_kills: int = 0
    sniper_kills: int = 0
    pistol_kills: int = 0
    shotgun_kills: int = 0
    nade_kills: int = 0
    suicides: int = 0
    team_kills: int = 0
    bomb_plants: int = 0
    bomb_defuses: int = 0
    other: int = 0
    rounds_played: int = 0
    impact_kills: int = 0
    round_wins: int = 0
    rounds_converted: int = 0
    opening_kills_converted: int = 0
    round_losses: int = 0
    opening_deaths_converted: int = 0
    one_kills: int = 0
    two_kills: int = 0
    three_kills: int = 0
    four_kils: int = 0
    five_kills: int = 0
    aliases: list = None


@dataclasses.dataclass(eq=False)
class Assistant:
    player: str

    def __eq__(self, other):
        """Check if name equals other.name if Assistant."""
        if isinstance(other, Assistant):
            return self.player == other.player
        elif isinstance(other, Player):
            return self.player == other.name
        return self.name == other


@dataclasses.dataclass(eq=False)
class DefusedBy(scorebot.DefusedBy):
    name: str = "defused_by"
    player: str = None


@dataclasses.dataclass(eq=False)
class Kill(scorebot.Kill):
    name: str = "kill"
    killer: str = None
    weapon: str = None
    killed: str = None
    headshot: bool = None
    assistant: Assistant = None


@dataclasses.dataclass(eq=False)
class PlantedBy(scorebot.PlantedBy):
    name: str = "planted_by"
    player: str = None


@dataclasses.dataclass(eq=False)
class RoundWinner(scorebot.RoundWinner):
    name: str = "round_winner"
    winner: str = None
    attack_score: int = None
    defence_score: int = None


@dataclasses.dataclass(eq=False)
class StartText(scorebot.StartText):
    name: str = "kill"
    round: int = None


class TestStatsTracker:
    @pytest.mark.parametrize(
        "player,variable,value,expected_key,expected_value",
        [
            # 1. case: var not an attribute, thus set to 1
            (IncompletePlayer(), "assists", 1, "assists", 1),
            # 2. case: 0 kills, add 1
            (IncompletePlayer(), "kills", 1, "kills", 1),
            # 3. case: 0 kills, add 2
            (IncompletePlayer(kills=0), "kills", 2, "kills", 2),
            # 4. case: 1 kill, add 3
            (IncompletePlayer(kills=1), "kills", 3, "kills", 4),
        ],
    )
    def test_add_value_to_player(
        self, stats_tracker, player, variable, value, expected_key, expected_value
    ):
        stats_tracker._add_value_to_player(player, varname=variable, value=value)

        assert vars(player)[expected_key] == expected_value

    @pytest.mark.parametrize(
        "old,new,expected",
        [
            (
                Player(
                    name="test_player",
                    aliases=["test_player"],
                    team="attack",
                    kills=2,
                    prev_kills=1,
                ),
                Player(
                    name="test_player",
                    team="attack",
                    kills=4,
                ),
                Player(kills=4),
            ),
        ],
    )
    def test_update_player_score(self, stats_tracker, old, new, expected):
        stats_tracker._update_player_score(old, player_new=new)

        for key in ["score", "kills", "assists", "deaths"]:
            assert vars(old)[key] == vars(expected)[key]

    @pytest.mark.parametrize(
        "player,players,expected",
        [
            # 1. case: player not in players, gets added
            (Player(pb_guid="test_pb_guid"), [], True),
            # 2. case: player in players
            (Player(pb_guid="test_pb_guid"), [Player(pb_guid="test_pb_guid")], False),
        ],
    )
    def test_add_player(self, stats_tracker, player, players, expected):
        stats_tracker.players = players.copy()

        result = stats_tracker._add_player(player)

        assert result == expected
        assert any(
            player.pb_guid == player_.pb_guid for player_ in stats_tracker.players
        )
        for detail in ["score", "kills", "assists", "deaths"]:
            assert (
                vars(stats_tracker.players[0])[f"prev_{detail}"] == vars(player)[detail]
            )

    @pytest.mark.parametrize(
        "killfeed,expected",
        [
            # 1. case: no RoundWinner event, so no full round tracked
            ([Kill()], []),
            # 2. case: 1 round with a single kill tracked
            ([Kill(), RoundWinner()], [[Kill(), RoundWinner()]]),
            # 3. case: 2 rounds with 1 kill each tracked
            (
                [Kill(), RoundWinner(), StartText(), Kill(), RoundWinner()],
                [
                    [Kill(), RoundWinner()],
                    [StartText(), Kill(), RoundWinner()],
                ],
            ),
            # 4. case: 3 rounds tracked with Kill, Defuse, Plant and Kill, Plant, and Defuse
            (
                [
                    Kill(),
                    RoundWinner(),
                    StartText(),
                    DefusedBy(),
                    RoundWinner(),
                    StartText(),
                    PlantedBy(),
                    RoundWinner(),
                    StartText(),
                    Kill(),
                    PlantedBy(),
                    DefusedBy(),
                    RoundWinner(),
                ],
                [
                    [Kill(), RoundWinner()],
                    [StartText(), DefusedBy(), RoundWinner()],
                    [StartText(), PlantedBy(), RoundWinner()],
                    [StartText(), Kill(), PlantedBy(), DefusedBy(), RoundWinner()],
                ],
            ),
        ],
    )
    def test_format_killfeed_to_rounds(self, stats_tracker, killfeed, expected):
        result = stats_tracker._format_killfeed_to_rounds(killfeed)

        assert result == expected

    @pytest.mark.parametrize(
        "player,kill,first_kill,expected,expected_player",
        [
            # 1. case: 1 kill, not opening kill
            (
                Player(name="test_player", aliases=["test_player"], team="attack"),
                Kill(
                    killer="test_player",
                    killed="test_dead_player",
                    weapon="ak47_mp",
                ),
                False,
                (1, False, False),
                Player(
                    name="test_player",
                    aliases=["test_player"],
                    evaluated_kills=1,
                    evaluated_score=5,
                    rifle_kills=1,
                    team="attack",
                ),
            ),
            # 2. case: 1 kill, opening kill
            (
                Player(name="test_player", aliases=["test_player"], team="attack"),
                Kill(
                    killer="test_player",
                    killed="test_dead_player",
                    weapon="ak47_mp",
                ),
                True,
                (1, True, False),
                Player(
                    name="test_player",
                    aliases=["test_player"],
                    evaluated_kills=1,
                    evaluated_score=5,
                    opening_kills=1,
                    rifle_kills=1,
                    team="attack",
                ),
            ),
            # 3. case: 0 kill, 1 assist
            (
                Player(name="test_player", aliases=["test_player"], team="attack"),
                Kill(
                    killer="test_player_2",
                    killed="test_dead_player",
                    assistant=Assistant(player="test_player"),
                ),
                False,
                (0, False, False),
                Player(
                    name="test_player",
                    aliases=["test_player"],
                    evaluated_assists=1,
                    evaluated_score=3,
                    team="attack",
                ),
            ),
            # 4. case: 0 kill, 1 death, not opening death
            (
                Player(name="test_player", aliases=["test_player"], team="attack"),
                Kill(
                    killer="test_player_2",
                    killed="test_player",
                    weapon="ak47_mp",
                ),
                False,
                (0, False, False),
                Player(
                    name="test_player",
                    aliases=["test_player"],
                    evaluated_deaths=1,
                    team="attack",
                ),
            ),
            # 5. case: 0 kill, 1 death, opening death
            (
                Player(name="test_player", aliases=["test_player"], team="attack"),
                Kill(
                    killer="test_player_2",
                    killed="test_player",
                    weapon="ak47_mp",
                ),
                True,
                (0, False, True),
                Player(
                    name="test_player",
                    aliases=["test_player"],
                    evaluated_deaths=1,
                    opening_deaths=1,
                    team="attack",
                ),
            ),
            # 6. case: 1 suicide
            (
                Player(name="test_player", aliases=["test_player"], team="attack"),
                Kill(
                    killer="test_player",
                    killed="test_player",
                    weapon="none",
                ),
                False,
                (0, False, False),
                Player(
                    name="test_player",
                    aliases=["test_player"],
                    suicides=1,
                    team="attack",
                ),
            ),
            # 7. case: 1 team kill
            (
                Player(name="test_player", aliases=["test_player"], team="defence"),
                Kill(
                    killer="test_player",
                    killed="test_dead_player",
                    weapon="none",
                ),
                False,
                (0, False, False),
                Player(
                    name="test_player",
                    aliases=["test_player"],
                    team_kills=1,
                    evaluated_score=-5,
                    team="defence",
                ),
            ),
            # 8. case: 1 headshot kill with undefined weapon
            (
                Player(name="test_player", aliases=["test_player"], team="attack"),
                Kill(
                    killer="test_player",
                    killed="test_dead_player",
                    weapon="none",
                    headshot=True,
                ),
                False,
                (1, False, False),
                Player(
                    name="test_player",
                    aliases=["test_player"],
                    evaluated_kills=1,
                    other=1,
                    headshot_kills=1,
                    evaluated_score=5,
                    team="attack",
                ),
            ),
        ],
    )
    def test_evaluate_kill_event(
        self,
        stats_tracker,
        player,
        kill,
        first_kill,
        expected,
        expected_player,
    ):
        stats_tracker.players = [
            player,
            Player(aliases=["test_dead_player"], team="defence"),
        ]

        result = stats_tracker._evaluate_kill_event(
            player=player,
            event=kill,
            n_kills=0,
            first_kill=first_kill,
        )

        assert result == expected
        assert vars(player) == vars(expected_player)

    @pytest.mark.parametrize(
        "player,defused_by,expected_player",
        [
            # 1. case: wrong event
            (
                Player(),
                PlantedBy(player="test_player"),
                Player(),
            ),
            # 2. case: player defuser
            (
                Player(name="test_player", aliases=["test_player"]),
                DefusedBy(player="test_player"),
                Player(
                    name="test_player",
                    aliases=["test_player"],
                    bomb_defuses=1,
                    evaluated_score=3,
                ),
            ),
            # 3. case: player not defuser
            (
                Player(name="test_player", aliases=["test_player"]),
                DefusedBy(player="test_player_2"),
                Player(name="test_player", aliases=["test_player"]),
            ),
        ],
    )
    def test_evaluate_defused_by_event(
        self, stats_tracker, player, defused_by, expected_player
    ):
        stats_tracker._evaluate_defused_by_event(player=player, event=defused_by)

        assert vars(player) == vars(expected_player)

    @pytest.mark.parametrize(
        "player,planted_by,expected_player",
        [
            # 1. case: wrong event
            (
                Player(),
                DefusedBy(player="test_player"),
                Player(),
            ),
            # 2. case: player planter
            (
                Player(name="test_player", aliases=["test_player"]),
                PlantedBy(player="test_player"),
                Player(
                    name="test_player",
                    aliases=["test_player"],
                    bomb_plants=1,
                    evaluated_score=3,
                ),
            ),
            # 3. case: player not planter
            (
                Player(name="test_player", aliases=["test_player"]),
                PlantedBy(player="test_player_2"),
                Player(name="test_player", aliases=["test_player"]),
            ),
        ],
    )
    def test_evaluate_planted_by_event(
        self, stats_tracker, player, planted_by, expected_player
    ):
        stats_tracker._evaluate_planted_by_event(player=player, event=planted_by)

        assert vars(player) == vars(expected_player)

    @pytest.mark.parametrize(
        "player,round_winner,n_kills,opening_kill,opening_death,expected_player",
        [
            # 1. case: round loss, 0 kills
            (
                Player(team="defence"),
                RoundWinner(winner="attack"),
                0,
                False,
                False,
                Player(team="defence", rounds_played=1, round_losses=1),
            ),
            # 2. case: round loss, opening death, 2 kills
            (
                Player(team="defence"),
                RoundWinner(winner="attack"),
                2,
                False,
                True,
                Player(
                    team="defence",
                    rounds_played=1,
                    round_losses=1,
                    opening_deaths_converted=1,
                ),
            ),
            # 3. case: round win, 0 kills
            (
                Player(team="attack"),
                RoundWinner(winner="attack"),
                0,
                False,
                False,
                Player(team="attack", rounds_played=1, round_wins=1),
            ),
            # 4. case: round win, 0 kills, but opening kill falsely `True`
            (
                Player(team="attack"),
                RoundWinner(winner="attack"),
                0,
                True,
                False,
                Player(team="attack", rounds_played=1, round_wins=1),
            ),
            # 5. case: round win, 1 kills
            (
                Player(team="attack"),
                RoundWinner(winner="attack"),
                1,
                False,
                False,
                Player(
                    team="attack",
                    rounds_played=1,
                    round_wins=1,
                    impact_kills=1,
                    rounds_converted=1,
                ),
            ),
            # 6. case: round win, 1 kill, opening kill
            (
                Player(team="attack"),
                RoundWinner(winner="attack"),
                1,
                True,
                False,
                Player(
                    team="attack",
                    rounds_played=1,
                    round_wins=1,
                    impact_kills=1,
                    opening_kills_converted=1,
                    rounds_converted=1,
                ),
            ),
            # 6. case: round win, multiple kills
            (
                Player(team="attack"),
                RoundWinner(winner="attack"),
                3,
                False,
                False,
                Player(
                    team="attack",
                    rounds_played=1,
                    round_wins=1,
                    impact_kills=3,
                    rounds_converted=1,
                ),
            ),
            # 7. case: round win, multiple kills, opening kill
            (
                Player(team="attack"),
                RoundWinner(winner="attack"),
                3,
                True,
                False,
                Player(
                    team="attack",
                    rounds_played=1,
                    round_wins=1,
                    impact_kills=3,
                    opening_kills_converted=1,
                    rounds_converted=1,
                ),
            ),
        ],
    )
    def test_evaluate_round_winner_event(
        self,
        stats_tracker,
        player,
        round_winner,
        n_kills,
        opening_kill,
        opening_death,
        expected_player,
    ):
        stats_tracker._evaluate_round_winner_event(
            player=player,
            event=round_winner,
            n_kills=n_kills,
            opening_kill=opening_kill,
            opening_death=opening_death,
        )

        assert vars(player) == vars(expected_player)

    def test_add_multi_kill_to_player(self, stats_tracker, kills):
        player = Player()
        kill_map = {1: "one", 2: "two", 3: "three", 4: "four", 5: "five"}
        expected = Player()
        if kills in kill_map:
            vars(expected)[f"{kill_map[kills]}_kills"] = 1

        stats_tracker._add_multi_kill_to_player(player, n_kills=kills)

        assert vars(player) == vars(expected)

    @pytest.mark.parametrize(
        "rounds",
        [
            [[Kill(), Kill()], [Kill(), PlantedBy(), DefusedBy()]],
            [[Kill(), PlantedBy(), DefusedBy()], [Kill(), Kill()]],
        ],
    )
    def test_evaluate_killfeed(self, monkeypatch, stats_tracker, rounds):
        return_values = [
            [(1, True, True)] + [(1, False, False) for _ in round_[1:]]
            for round_ in rounds
        ]
        return_values = [
            return_val for return_value in return_values for return_val in return_value
        ]
        kill_evaluation_return = iter(return_values)

        def yield_kill_evaluation_return(*args, **kwargs):
            return next(kill_evaluation_return)

        monkeypatch.setattr(
            utils.StatsTracker,
            "_evaluate_kill_event",
            _evaluate_kill_event := mock.MagicMock(
                side_effect=yield_kill_evaluation_return
            ),
        )
        monkeypatch.setattr(
            utils.StatsTracker,
            "_evaluate_planted_by_event",
            _evaluate_planted_by_event := mock.MagicMock(),
        )
        monkeypatch.setattr(
            utils.StatsTracker,
            "_evaluate_defused_by_event",
            _evaluate_defused_by_event := mock.MagicMock(),
        )
        monkeypatch.setattr(
            utils.StatsTracker,
            "_evaluate_round_winner_event",
            _evaluate_round_winner_event := mock.MagicMock(),
        )
        monkeypatch.setattr(
            utils.StatsTracker,
            "_add_multi_kill_to_player",
            _add_multi_kill_to_player := mock.MagicMock(),
        )
        player = Player()
        stats_tracker.players = [player]

        stats_tracker._evaluate_killfeed(rounds)

        rounds_flattened = [event for round_ in rounds for event in round_]
        assert _evaluate_kill_event.call_count == len(rounds_flattened)
        assert _evaluate_planted_by_event.call_count == len(rounds_flattened)
        assert _evaluate_defused_by_event.call_count == len(rounds_flattened)
        _evaluate_round_winner_event.assert_has_calls(
            [
                mock.call(
                    player=player,
                    event=round_[-1],
                    n_kills=1,
                    opening_kill=True,
                    opening_death=True,
                )
                for round_ in rounds
            ]
        )
        _add_multi_kill_to_player.assert_has_calls(
            [mock.call(player, n_kills=1) for _ in range(len(rounds))]
        )

    @pytest.mark.parametrize(
        "player,map_winner,expected",
        [
            # 1. case: 1 map won
            (
                Player(team="attack"),
                "attack",
                {
                    "score": 0,
                    "kills": 0,
                    "assists": 0,
                    "deaths": 0,
                    "maps_played": 1,
                    "map_wins": 1,
                    "map_losses": 0,
                    "map_draws": 0,
                    "other": 0,
                },
            ),
            # 2. case: 1 map lost
            (
                Player(team="attack"),
                "defence",
                {
                    "score": 0,
                    "kills": 0,
                    "assists": 0,
                    "deaths": 0,
                    "maps_played": 1,
                    "map_wins": 0,
                    "map_losses": 1,
                    "map_draws": 0,
                    "other": 0,
                },
            ),
            # 3. case: 1 map draw
            (
                Player(team="attack"),
                "tie",
                {
                    "score": 0,
                    "kills": 0,
                    "assists": 0,
                    "deaths": 0,
                    "maps_played": 1,
                    "map_wins": 0,
                    "map_losses": 0,
                    "map_draws": 1,
                    "other": 0,
                },
            ),
            # 4. case: other var of player in stats to be incremented by 2
            (
                Player(team="attack", other=2),
                "attack",
                {
                    "score": 0,
                    "kills": 0,
                    "assists": 0,
                    "deaths": 0,
                    "maps_played": 1,
                    "map_wins": 1,
                    "map_losses": 0,
                    "map_draws": 0,
                    "other": 2,
                },
            ),
            # 5. case: evaluated score same or less than player scorebot's score
            (
                Player(team="attack", evaluated_score=5, score=5),
                "attack",
                {
                    "score": 5,
                    "kills": 0,
                    "assists": 0,
                    "deaths": 0,
                    "maps_played": 1,
                    "map_wins": 1,
                    "map_losses": 0,
                    "map_draws": 0,
                    "other": 0,
                },
            ),
            # 6. case: evaluated score greater than player scorebot's score
            (
                Player(team="attack", evaluated_score=10, score=5),
                "attack",
                {
                    "score": 10,
                    "kills": 0,
                    "assists": 0,
                    "deaths": 0,
                    "maps_played": 1,
                    "map_wins": 1,
                    "map_losses": 0,
                    "map_draws": 0,
                    "other": 0,
                },
            ),
            # 7. case: evaluated kills same or less than player scorebot's kills
            (
                Player(team="attack", evaluated_kills=2, kills=2),
                "attack",
                {
                    "score": 0,
                    "kills": 2,
                    "assists": 0,
                    "deaths": 0,
                    "maps_played": 1,
                    "map_wins": 1,
                    "map_losses": 0,
                    "map_draws": 0,
                    "other": 0,
                },
            ),
            # 8. case: evaluated kills greater than player scorebot's kills
            (
                Player(team="attack", evaluated_kills=4, kills=2),
                "attack",
                {
                    "score": 0,
                    "kills": 4,
                    "assists": 0,
                    "deaths": 0,
                    "maps_played": 1,
                    "map_wins": 1,
                    "map_losses": 0,
                    "map_draws": 0,
                    "other": 0,
                },
            ),
            # 9. case: evaluated assists same or less than player scorebot's assists
            (
                Player(team="attack", evaluated_assists=3, assists=3),
                "attack",
                {
                    "score": 0,
                    "kills": 0,
                    "assists": 3,
                    "deaths": 0,
                    "maps_played": 1,
                    "map_wins": 1,
                    "map_losses": 0,
                    "map_draws": 0,
                    "other": 0,
                },
            ),
            # 10. case: evaluated assists greater than player scorebot's assists
            (
                Player(team="attack", evaluated_assists=6, assists=3),
                "attack",
                {
                    "score": 0,
                    "kills": 0,
                    "assists": 6,
                    "deaths": 0,
                    "maps_played": 1,
                    "map_wins": 1,
                    "map_losses": 0,
                    "map_draws": 0,
                    "other": 0,
                },
            ),
            # 11. case: evaluated deaths same or less than player scorebot's deaths
            (
                Player(team="attack", evaluated_deaths=4, deaths=4),
                "attack",
                {
                    "score": 0,
                    "kills": 0,
                    "assists": 0,
                    "deaths": 4,
                    "maps_played": 1,
                    "map_wins": 1,
                    "map_losses": 0,
                    "map_draws": 0,
                    "other": 0,
                },
            ),
            # 12. case: evaluated deaths greater than player scorebot's deaths
            (
                Player(team="attack", evaluated_deaths=8, deaths=4),
                "attack",
                {
                    "score": 0,
                    "kills": 0,
                    "assists": 0,
                    "deaths": 8,
                    "maps_played": 1,
                    "map_wins": 1,
                    "map_losses": 0,
                    "map_draws": 0,
                    "other": 0,
                },
            ),
        ],
    )
    def test_update_stats(self, stats_tracker, stats, player, map_winner, expected):
        result = stats_tracker._update_stats(
            stats, player=player, map_winner=map_winner
        )

        assert result == expected

    @pytest.mark.parametrize(
        "player,added,expected_update",
        [
            (Player(), True, False),
            (Player(), False, False),
            (Player(pb_guid="test_pb_guid"), False, True),
        ],
    )
    def test_update_player(
        self, monkeypatch, stats_tracker, player, added, expected_update
    ):
        monkeypatch.setattr(
            utils.StatsTracker,
            "_add_player",
            _add_player := mock.MagicMock(return_value=added),
        )
        monkeypatch.setattr(
            utils.StatsTracker,
            "_update_player_score",
            _update_player_score := mock.MagicMock(),
        )
        player_ = Player(pb_guid="test_pb_guid")
        stats_tracker.playerlist = [player_]

        stats_tracker.update_player(player)

        _add_player.assert_called_once_with(player)
        if added and expected_update:
            _update_player_score.assert_called_once_with(player_, player_new=player)
