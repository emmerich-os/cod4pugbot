import pytest

from cod4pugbot.domain.features.pugs import pug


@pytest.fixture()
def active_pugs():
    return pug.ActivePUGs()


class TestActivePUGs:
    @pytest.mark.parametrize(
        "pug_id,pug,pugs",
        [
            (1, "test_pug_1", {}),
            (1, "test_pug_1", {1: "test_pug_1"}),
            (1, "test_pug_1", {1: "test_pug_2"}),
        ],
    )
    @pytest.mark.asyncio()
    def test_add_pug(self, active_pugs, pug_id, pug, pugs):
        active_pugs.pugs = pugs.copy()

        result = active_pugs.add_pug(pug_id, pug)

        if pug in pugs.values() or not pugs:
            assert result == pug
            assert active_pugs.pugs.get(pug_id) == pug
        else:
            assert result == pugs[1]
            assert active_pugs.pugs.get(pug_id) != pug

    @pytest.mark.parametrize(
        "pug_id,pugs",
        [
            (1, {1: "test_pug"}),
            (1, {}),
        ],
    )
    @pytest.mark.asyncio()
    def test_remove_pug(self, active_pugs, pug_id, pugs):
        active_pugs.pugs = pugs.copy()

        result = active_pugs.remove_pug(pug_id)

        assert not active_pugs.pugs.get(pug_id)
        if pugs:
            assert result == pugs[1]
        else:
            assert result is None
