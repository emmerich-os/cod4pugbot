import pytest

from cod4pugbot import testing
from cod4pugbot.domain.features.pugs.pug import _messages as pug_messages
from cod4pugbot.domain.features.pugs.pug import _modes as modes
from cod4pugbot.domain.features.pugs.pug import _sizes as sizes
from cod4pugbot.domain.features.pugs.pug import _statuses as statuses
from cod4pugbot.domain.features.pugs.pug import base_pug


class AnyPUG(base_pug.PUG):
    def __init__(
        self,
        mode: modes.Modes = modes.Modes.BO1,
        size: sizes.Sizes = sizes.Sizes.ONE,
        status: statuses.Statuses = statuses.Statuses.OPEN,
    ):
        self.mode = mode
        self.size = size
        self.status = status


class AnyDiscordPUG(base_pug.DiscordPUG):
    def __init__(
        self,
        pug: base_pug.PUG,
        channel: testing.Channel = None,
        messages: pug_messages.Messages = None,
    ):
        self.pug = pug
        self.channel = channel or testing.Channel()
        self.messages = messages or pug_messages.Messages()
        self.messages.pug = testing.discord.Message()


@pytest.fixture()
def pug() -> base_pug.PUG:
    return AnyPUG()


@pytest.fixture()
def discord_pug(pug) -> base_pug.DiscordPUG:
    return AnyDiscordPUG(pug=pug)


class TestPUG:
    def test_max_players(self, pug):
        assert pug.max_players == 2


class TestDiscordPUG:
    def test_size(self, discord_pug, pug):
        assert discord_pug.size == pug.size

    def test_mode(self, discord_pug, pug):
        assert discord_pug.mode == pug.mode

    @pytest.mark.parametrize(
        ("member", "emoji", "removal_expected"),
        [
            (testing.PUGPlayer(id=1), "\u2705", True),
            (testing.PUGPlayer(id=2), "\u2705", False),
            (testing.PUGPlayer(id=1), "\u26d4", True),
            (testing.PUGPlayer(id=2), "\u26d4", False),
        ],
    )
    @pytest.mark.asyncio()
    async def test_remove_reaction(self, discord_pug, member, emoji, removal_expected):
        # Add dummy user to message reactions.
        users = [testing.User(id=1)]
        discord_pug.messages.pug.reactions = [
            testing.Reaction(emoji=emoji, _users=testing.Users(users=users))
        ]

        await discord_pug.remove_reaction(member=member, emoji=emoji)
        result = discord_pug.messages.pug.reactions[0].users().users

        if removal_expected:
            assert not result
            assert testing.received_message(discord_pug.channel)
        else:
            assert result
            assert not testing.received_message(discord_pug.channel)
