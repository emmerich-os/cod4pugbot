import datetime
from unittest import mock

import freezegun
import pytest
import pytz

import cod4pugbot.db
from cod4pugbot import testing
from cod4pugbot.domain.features.pugs.pug import _modes as modes
from cod4pugbot.domain.features.pugs.pug import _sizes as sizes
from cod4pugbot.domain.features.pugs.pug import open


TIME = "2020-01-01 00:00"

PLAYER_1 = testing.PUGPlayer(id=1)
PLAYER_2 = testing.PUGPlayer(id=2)
PLAYER_3 = testing.PUGPlayer(id=3)


@pytest.fixture()
def pug() -> open.OpenPUG:
    return open.OpenPUG(
        pug_id=1,
        size=sizes.Sizes.ONE,
        mode=modes.Modes.BO1,
        ready_up_phase_length=60,
    )


@pytest.fixture()
def pug_full(pug) -> open.OpenPUG:
    pug.add_player(PLAYER_1)
    pug.add_player(PLAYER_2)

    assert pug.is_full
    assert pug.ready_up_started

    return pug


class TestOpenPUG:
    @pytest.mark.parametrize(
        ("players", "expected_players", "expected_queue"),
        [
            # Test case: add new player
            ([PLAYER_1], [PLAYER_1], []),
            # Test case: add second player
            (
                [PLAYER_1, PLAYER_2],
                [PLAYER_1, PLAYER_2],
                [],
            ),
            # Test case: 3rd player will be added to queue because PUG is full
            (
                [
                    PLAYER_1,
                    PLAYER_2,
                    PLAYER_3,
                ],
                [PLAYER_1, PLAYER_2],
                [PLAYER_3],
            ),
            # Test case: add player twice
            (
                [PLAYER_1, PLAYER_1],
                [PLAYER_1],
                [],
            ),
            # Test case: add player twice to queue
            (
                [
                    PLAYER_1,
                    PLAYER_2,
                    PLAYER_3,
                    PLAYER_3,
                ],
                [PLAYER_1, PLAYER_2],
                [PLAYER_3],
            ),
        ],
    )
    def test_add_player(self, pug, players, expected_players, expected_queue):
        for player in players:
            pug.add_player(player)

        assert testing.iterables_contain_same_elements_in_order(
            pug.players, expected_players
        )
        assert testing.iterables_contain_same_elements_in_order(
            pug.queue, expected_queue
        )

    def test_ready_phase_starts_when_full(self, pug):
        players = [PLAYER_1, PLAYER_2]

        for player in players:
            pug.add_player(player)

        assert pug.is_full
        assert pug.ready_up_started

    @pytest.mark.parametrize(
        ("contains", "player", "expected_players", "expected_queue"),
        [
            # Test case: remove non-existing player
            ([], PLAYER_1, [], []),
            # Test case: remove player
            ([PLAYER_1], PLAYER_1, [], []),
            (
                [PLAYER_1, PLAYER_2],
                PLAYER_1,
                [PLAYER_2],
                [],
            ),
            # Test case: remove player from queue
            (
                [
                    PLAYER_1,
                    PLAYER_2,
                    PLAYER_3,
                ],
                PLAYER_1,
                [],
                [],
            ),
        ],
    )
    def test_remove_player(
        self, pug, contains, player, expected_players, expected_queue
    ):
        for p in contains:
            pug.add_player(p)

        pug.remove_player(player)

        assert testing.iterables_contain_same_elements_in_order(
            pug._players, expected_players
        )
        assert testing.iterables_contain_same_elements_in_order(
            pug._queue, expected_queue
        )

    def test_ready_up_phase_aborted_when_player_removed(self, pug_full):
        player = PLAYER_1
        pug_full.remove_player(player)

        assert not pug_full.is_full
        assert pug_full.is_open

    @pytest.mark.parametrize(
        ("players", "expect_pug_start"),
        [
            ([PLAYER_1], False),
            ([PLAYER_1, PLAYER_2], True),
        ],
    )
    def test_ready_player(self, pug_full, players, expect_pug_start):
        for player in players:
            pug_full.ready_player(player)

            assert player in pug_full.ready_players
            assert player not in pug_full.unready_players

        if expect_pug_start:
            assert pug_full.all_players_ready

    @pytest.mark.parametrize(
        "players",
        [
            [PLAYER_1],
            [PLAYER_1, PLAYER_2],
        ],
    )
    def test_unready_player(self, pug_full, players):
        for player in players:
            pug_full.unready_player(player)

            assert player not in pug_full.ready_players
            assert player in pug_full.unready_players

    @pytest.mark.parametrize(
        ("queue", "players_to_remove"),
        [
            ([], [PLAYER_1]),
        ],
    )
    def test_abort_ready_up_phase(self, pug_full, queue, players_to_remove):
        for player in queue:
            pug_full.add_player(player)

        pug_full.abort_ready_up_phase(players_to_remove)

        assert pug_full.is_open
        assert all(player not in pug_full.players for player in players_to_remove)
        assert all(player in pug_full.players for player in queue)


class Player:
    def __init__(self, nick: str, name: str):
        self.nick = nick
        self.name = name
        self.mention = f"@{nick}"

    def __eq__(self, other):
        """Check if `other.nick` equals `self.nick`."""
        return self.nick == other.nick

    def __repr__(self):
        """Name when printing."""
        return self.__str__()

    def __str__(self):
        """Name when formatted to string."""
        return f"<{self.__class__.__name__} {self.nick=} {self.name=}>"


async def async_magic():
    return


mock.MagicMock.__await__ = lambda x: async_magic().__await__()


def _pug_message_embed_dict(fields: list, ready_up: bool = False):
    return {
        "color": 29951 if not ready_up else 16741376,
        "description": (
            "React with :white_check_mark: to enter PUG, remove to leave PUG. "
            "If you go AFK or offline, you will be automatically removed from the PUG. "
            "Send `!ao` into a different channel to enable AFK/offline protection."
        ),
        "fields": [
            {
                "inline": True,
                "name": "Status",
                "value": "```css\nOPEN\n```"
                if not ready_up
                else "```ARM\nREADYUP\n```",
            },
            *fields,
        ],
        "footer": {"text": "Last updated: 01/01/2020 00:00 UTC"},
        "title": "1v1 PUG (BO1)",
        "type": "rich",
    }


def _ready_up_message(add: str = None):
    message = (
        """> **READY-UP phase started.**\n"""
        """> React with :white_check_mark: to __**ready up**__.\n"""
        """> React with :no_entry: to __**abort**__.\n"""
        """> Duration of ready-up phase is """
        """60 seconds.\n"""
    )
    if add is not None:
        message += add
    return message


class TestOpenDiscordPUG:
    @pytest.mark.asyncio()
    async def test_create(self, monkeypatch, cog, guild):
        monkeypatch.setattr(
            cod4pugbot.pug.OpenPUG,
            "_create_pug_message",
            _create_pug_message := mock.MagicMock(),
        )
        monkeypatch.setattr(
            cod4pugbot.pug.OpenPUG,
            "add_player",
            mock.MagicMock(),
        )

        with mock.patch("cod4pugbot.pugs.open.SQLClient") as mock_sqlclient:
            mock_sqlclient.return_value.insert.return_value = 1
            result = await cod4pugbot.pug.OpenPUG.create(
                cog=cog,
                guild=guild,
                mode="bo1",
                size="1v1",
            )

        assert isinstance(result, cod4pugbot.pug.OpenPUG)
        assert result.cog == cog
        assert result.guild == guild
        assert result.id == 1
        assert result.mode == "bo1"
        assert result.size == "1v1"
        assert cog.active_pugs.pugs.get(1) == result
        _create_pug_message.assert_called_once()

    @freezegun.freeze_time(TIME)
    @pytest.mark.parametrize(
        "playerqueue,expected",
        [
            (
                None,
                _pug_message_embed_dict(
                    fields=[
                        {"inline": True, "name": "Slots", "value": "```0/2```"},
                    ]
                ),
            ),
            (
                [
                    Player(nick="test_nick_1", name="test_name_1"),
                ],
                _pug_message_embed_dict(
                    fields=[
                        {"inline": True, "name": "Slots", "value": "```0/2```"},
                    ]
                ),
            ),
            (
                [
                    Player(nick="test_nick_1", name="test_name_1"),
                    Player(nick="test_nick_2", name="test_name_2"),
                ],
                _pug_message_embed_dict(
                    fields=[
                        {"inline": True, "name": "Slots", "value": "```0/2```"},
                    ]
                ),
            ),
            (
                [
                    Player(nick="test_nick_1", name="test_name_1"),
                    Player(nick="test_nick_2", name="test_name_2"),
                    Player(nick="test_nick_3", name="test_name_3"),
                ],
                _pug_message_embed_dict(
                    fields=[
                        {"inline": True, "name": "Slots", "value": "```0/2```"},
                    ]
                ),
            ),
        ],
    )
    @pytest.mark.asyncio()
    async def test_create_pug_message(
        self, monkeypatch, cog, guild, playerqueue, expected
    ):
        monkeypatch.setattr(
            cod4pugbot.pug.OpenPUG,
            "add_player",
            add_player := mock.MagicMock(),
        )

        with mock.patch("cod4pugbot.pugs.open.SQLClient") as mock_sqlclient:
            mock_sqlclient.return_value.insert.return_value = 1
            open_pug = await cod4pugbot.pug.OpenPUG.create(
                cog=cog,
                guild=guild,
                mode="bo1",
                size="1v1",
                playerqueue=playerqueue,
            )
        result = open_pug.messages["pugs"].embed.to_dict()

        assert result == expected
        if playerqueue:
            assert add_player.call_count == len(playerqueue)

    @freezegun.freeze_time(TIME)
    @pytest.mark.parametrize(
        "playerlist,playerqueue,change_status,expected",
        [
            (
                [],
                [],
                False,
                _pug_message_embed_dict(
                    fields=[
                        {"inline": True, "name": "Slots", "value": "```0/2```"},
                    ]
                ),
            ),
            (
                [],
                [],
                True,
                _pug_message_embed_dict(
                    fields=[
                        {"inline": True, "name": "Slots", "value": "```0/2```"},
                    ],
                    ready_up=True,
                ),
            ),
            (
                [
                    Player(nick="test_nick_1", name="test_name_1"),
                ],
                [],
                False,
                _pug_message_embed_dict(
                    fields=[
                        {"inline": True, "name": "Slots", "value": "```1/2```"},
                        {
                            "inline": False,
                            "name": "Players",
                            "value": "```\ntest_nick_1\n```",
                        },
                    ]
                ),
            ),
            (
                [
                    Player(nick="test_nick_1", name="test_name_1"),
                    Player(nick="test_nick_2", name="test_name_2"),
                ],
                [],
                False,
                _pug_message_embed_dict(
                    fields=[
                        {"inline": True, "name": "Slots", "value": "```2/2```"},
                        {
                            "inline": False,
                            "name": "Players",
                            "value": "```\ntest_nick_1\ntest_nick_2\n```",
                        },
                    ]
                ),
            ),
            (
                [
                    Player(nick="test_nick_1", name="test_name_1"),
                    Player(nick="test_nick_2", name="test_name_2"),
                ],
                [Player(nick="test_nick_3", name="test_name_3")],
                False,
                _pug_message_embed_dict(
                    fields=[
                        {"inline": True, "name": "Slots", "value": "```2/2```"},
                        {
                            "inline": False,
                            "name": "Players",
                            "value": "```\ntest_nick_1\ntest_nick_2\n```",
                        },
                        {
                            "inline": False,
                            "name": "Waiting",
                            "value": "```\ntest_nick_3\n```",
                        },
                    ]
                ),
            ),
            (
                [
                    Player(nick="test_nick_1", name="test_name_1"),
                    Player(nick="test_nick_2", name="test_name_2"),
                ],
                [
                    Player(nick="test_nick_3", name="test_name_3"),
                    Player(nick="test_nick_4", name="test_name_4"),
                ],
                False,
                _pug_message_embed_dict(
                    fields=[
                        {"inline": True, "name": "Slots", "value": "```2/2```"},
                        {
                            "inline": False,
                            "name": "Players",
                            "value": "```\ntest_nick_1\ntest_nick_2\n```",
                        },
                        {
                            "inline": False,
                            "name": "Waiting",
                            "value": "```\ntest_nick_3\ntest_nick_4\n```",
                        },
                    ]
                ),
            ),
            (
                [
                    Player(nick="test_nick_1", name="test_name_1"),
                    Player(nick="test_nick_2", name="test_name_2"),
                ],
                [
                    Player(nick="test_nick_3", name="test_name_3"),
                    Player(nick="test_nick_4", name="test_name_4"),
                    Player(nick="test_nick_5", name="test_name_5"),
                ],
                False,
                _pug_message_embed_dict(
                    fields=[
                        {"inline": True, "name": "Slots", "value": "```2/2```"},
                        {
                            "inline": False,
                            "name": "Players",
                            "value": "```\ntest_nick_1\ntest_nick_2\n```",
                        },
                        {
                            "inline": False,
                            "name": "Waiting",
                            "value": "```\ntest_nick_3\ntest_nick_4\ntest_nick_5\n```",
                        },
                    ]
                ),
            ),
        ],
    )
    @pytest.mark.asyncio()
    async def test_modify_pug_message(
        self, open_pug, playerlist, playerqueue, change_status, expected
    ):
        open_pug.playerlist = playerlist
        open_pug.playerqueue = playerqueue

        if change_status:
            open_pug.status = "readyup"

        await open_pug._modify_pug_message()
        result = open_pug.messages["pugs"].embed.to_dict()

        assert result == expected

    @pytest.mark.parametrize(
        "playerlist,expected",
        [
            (
                [
                    Player(nick="test_nick_1", name="test_name_1"),
                ],
                _ready_up_message(add="""> Waiting for: @test_nick_1"""),
            ),
            (
                [
                    Player(nick="test_nick_1", name="test_name_1"),
                    Player(nick="test_nick_2", name="test_name_2"),
                ],
                _ready_up_message(add="""> Waiting for: @test_nick_1, @test_nick_2"""),
            ),
            (
                [
                    Player(nick="test_nick_1", name="test_name_1"),
                    Player(nick="test_nick_2", name="test_name_2"),
                    Player(nick="test_nick_3", name="test_name_3"),
                ],
                _ready_up_message(
                    add="""> Waiting for: @test_nick_1, @test_nick_2, @test_nick_3"""
                ),
            ),
        ],
    )
    @pytest.mark.asyncio()
    async def test_start_ready_up_phase(
        self, monkeypatch, open_pug, playerlist, expected
    ):
        monkeypatch.setattr(
            cod4pugbot.pug.OpenPUG,
            "_modify_pug_message",
            _modify_pug_message := mock.MagicMock(),
        )
        open_pug.playerlist = playerlist.copy()

        with freezegun.freeze_time(TIME):
            await open_pug._start_ready_up_phase()
        result = open_pug.messages["readyup"].content

        assert open_pug.ready_up_phase_end == datetime.datetime(
            2020, 1, 1, 0, 1, tzinfo=pytz.timezone("UTC")
        )
        assert open_pug.status == "readyup"
        assert result == expected
        assert open_pug.not_ready == playerlist
        assert not open_pug.ready
        _modify_pug_message.assert_called_once()

    @pytest.mark.parametrize(
        "playerlist,expected",
        [
            (
                [
                    Player(nick="test_nick_1", name="test_name_1"),
                ],
                _ready_up_message(add="""> Waiting for: @test_nick_1"""),
            ),
            (
                [
                    Player(nick="test_nick_1", name="test_name_1"),
                    Player(nick="test_nick_2", name="test_name_2"),
                ],
                _ready_up_message(add="""> Waiting for: @test_nick_1, @test_nick_2"""),
            ),
            (
                [
                    Player(nick="test_nick_1", name="test_name_1"),
                    Player(nick="test_nick_2", name="test_name_2"),
                    Player(nick="test_nick_3", name="test_name_3"),
                ],
                _ready_up_message(
                    add="""> Waiting for: @test_nick_1, @test_nick_2, @test_nick_3"""
                ),
            ),
        ],
    )
    @pytest.mark.asyncio()
    async def test_modify_ready_up_message(
        self, open_pug, message, playerlist, expected
    ):
        open_pug.messages["readyup"] = message
        open_pug.ready_up_base_message = _ready_up_message()
        open_pug.not_ready = playerlist.copy()

        await open_pug._modify_ready_up_message()
        result = open_pug.messages["readyup"].content

        assert result == expected

    @pytest.mark.asyncio()
    async def test_start_pug(self, monkeypatch, open_pug):
        monkeypatch.setattr(
            cod4pugbot.pug.OpenPUG,
            "create",
            open_pug_create := mock.MagicMock(),
        )
        monkeypatch.setattr(
            cod4pugbot.pug.PreMatchPUG,
            "create",
            pre_match_pug_create := mock.MagicMock(),
        )

        await open_pug._start_pug()

        open_pug_create.assert_called_once()
        pre_match_pug_create.assert_called_once()

    @pytest.mark.parametrize(
        "member,playerlist,playerqueue,status",
        [
            (
                Player(nick="test_nick", name="test_name"),
                [],
                [],
                "open",
            ),
            (
                Player(nick="test_nick", name="test_name"),
                [Player(nick="test_nick", name="test_name")],
                [],
                "open",
            ),
            (
                Player(nick="test_nick", name="test_name"),
                [
                    Player(nick="test_nick_1", name="test_name_1"),
                ],
                [],
                "open",
            ),
            (
                Player(nick="test_nick", name="test_name"),
                [],
                [Player(nick="test_nick", name="test_name")],
                "readyup",
            ),
            (
                Player(nick="test_nick", name="test_name"),
                [
                    Player(nick="test_nick_1", name="test_name_1"),
                    Player(nick="test_nick_2", name="test_name_2"),
                ],
                [],
                "readyup",
            ),
            (
                Player(nick="test_nick", name="test_name"),
                [
                    Player(nick="test_nick_1", name="test_name_1"),
                    Player(nick="test_nick_2", name="test_name_2"),
                ],
                [Player(nick="test_nick_3", name="test_name_3")],
                "readyup",
            ),
            (
                Player(nick="test_nick", name="test_name"),
                [
                    Player(nick="test_nick_1", name="test_name_1"),
                    Player(nick="test_nick_2", name="test_name_2"),
                ],
                [
                    Player(nick="test_nick_3", name="test_name_3"),
                    Player(nick="test_nick_4", name="test_name_4"),
                ],
                "readyup",
            ),
            (
                Player(nick="test_nick", name="test_name"),
                [
                    Player(nick="test_nick_1", name="test_name_1"),
                    Player(nick="test_nick_2", name="test_name_2"),
                ],
                [
                    Player(nick="test_nick", name="test_name"),
                ],
                "readyup",
            ),
        ],
    )
    @pytest.mark.asyncio()
    async def test_add_player(
        self, monkeypatch, open_pug, member, playerlist, playerqueue, status
    ):
        monkeypatch.setattr(
            cod4pugbot.pug.OpenPUG,
            "_modify_pug_message",
            _modify_pug_message := mock.MagicMock(),
        )
        monkeypatch.setattr(
            cod4pugbot.pug.OpenPUG,
            "_start_ready_up_phase",
            _start_ready_up_phase := mock.MagicMock(),
        )
        open_pug.playerlist = playerlist.copy()
        open_pug.playerqueue = playerqueue.copy()
        open_pug.status = status

        await open_pug.add_player(member=member)

        if member in playerlist:
            assert member in open_pug.playerlist
            assert member not in open_pug.playerqueue
            _modify_pug_message.assert_not_called()
            _start_ready_up_phase.assert_not_called()
        elif member in playerqueue:
            if status == "open":
                assert member not in open_pug.playerqueue
                assert member in open_pug.playerlist
            elif status == "readyup":
                assert member in open_pug.playerqueue
                assert member not in open_pug.playerlist
                _modify_pug_message.assert_not_called()
                _start_ready_up_phase.assert_not_called()
        else:
            if len(playerlist) + 1 > 2:
                assert member not in open_pug.playerlist
                assert member in open_pug.playerqueue
            else:
                assert member in open_pug.playerlist
                assert member not in open_pug.playerqueue
            _modify_pug_message.assert_called_once()
            if status == "open" and len(playerlist) + 1 >= 2:
                _start_ready_up_phase.assert_called_once()

    @pytest.mark.parametrize(
        "member, playerlist,playerqueue",
        [
            (
                Player(nick="test_nick", name="test_name"),
                [],
                [],
            ),
            (
                Player(nick="test_nick", name="test_name"),
                [Player(nick="test_nick", name="test_name")],
                [],
            ),
            (
                Player(nick="test_nick", name="test_name"),
                [],
                [Player(nick="test_nick", name="test_name")],
            ),
        ],
    )
    @pytest.mark.asyncio()
    async def test_remove_player(
        self, monkeypatch, open_pug, member, playerlist, playerqueue
    ):
        monkeypatch.setattr(
            cod4pugbot.pug.OpenPUG,
            "_modify_pug_message",
            _modify_pug_message := mock.MagicMock(),
        )
        member = Player(nick="test_nick", name="test_name")
        open_pug.playerlist = playerlist.copy()
        open_pug.playerqueue = playerqueue.copy()

        await open_pug.remove_player(member=member)

        if member in playerlist or member in playerqueue:
            _modify_pug_message.assert_called_once()
            if member in playerlist:
                assert member not in open_pug.playerlist
            elif member in playerqueue:
                assert member not in open_pug.playerqueue
        else:
            _modify_pug_message.assert_not_called()

    @pytest.mark.parametrize(
        "members,aborted_by_bot,not_ready,ready,playerlist,playerqueue,expected",
        [
            (
                None,
                False,
                [Player(nick="test_nick_1", name="test_name_1")],
                [Player(nick="test_nick_2", name="test_name_2")],
                [
                    Player(nick="test_nick_1", name="test_name_1"),
                    Player(nick="test_nick_2", name="test_name_2"),
                ],
                [],
                "> READY-UP phase aborted. @test_nick_1 was not ready.",
            ),
            (
                None,
                False,
                [
                    Player(nick="test_nick_1", name="test_name_1"),
                    Player(nick="test_nick_2", name="test_name_2"),
                ],
                [],
                [
                    Player(nick="test_nick_1", name="test_name_1"),
                    Player(nick="test_nick_2", name="test_name_2"),
                ],
                [],
                "> READY-UP phase aborted. @test_nick_1, @test_nick_2 were not ready.",
            ),
            (
                [Player(nick="test_nick_1", name="test_name_1")],
                True,
                [],
                [],
                [
                    Player(nick="test_nick_1", name="test_name_1"),
                    Player(nick="test_nick_2", name="test_name_2"),
                ],
                [],
                "> READY-UP phase aborted. @test_nick_1 is already in a running PUG.",
            ),
            (
                [
                    Player(nick="test_nick_1", name="test_name_1"),
                    Player(nick="test_nick_2", name="test_name_2"),
                ],
                True,
                [],
                [],
                [
                    Player(nick="test_nick_1", name="test_name_1"),
                    Player(nick="test_nick_2", name="test_name_2"),
                ],
                [],
                (
                    "> READY-UP phase aborted. @test_nick_1, @test_nick_2 "
                    "are already in a running PUG."
                ),
            ),
        ],
    )
    @pytest.mark.asyncio()
    async def test_abort_ready_up_phase(
        self,
        monkeypatch,
        open_pug,
        message,
        members,
        aborted_by_bot,
        not_ready,
        ready,
        playerlist,
        playerqueue,
        expected,
    ):
        monkeypatch.setattr(
            cod4pugbot.pug.OpenPUG,
            "add_player",
            add_player := mock.MagicMock(),
        )
        monkeypatch.setattr(
            cod4pugbot.pug.OpenPUG,
            "remove_player",
            remove_player := mock.MagicMock(),
        )
        open_pug.not_ready = not_ready.copy()
        open_pug.ready = ready.copy()
        open_pug.playerlist = playerlist.copy()
        open_pug.playerqueue = playerqueue.copy()
        open_pug.messages["readyup"] = message

        await open_pug.abort_ready_up_phase(
            members=members,
            aborted_by_bot=aborted_by_bot,
        )
        result = open_pug.messages["readyup"].content

        assert result == expected
        if members is None:
            assert remove_player.call_count == len(not_ready)
        else:
            assert remove_player.call_count == len(members)
        assert not open_pug.ready
        assert open_pug.not_ready == playerlist
        assert add_player.call_count == len(playerqueue)

    @pytest.mark.parametrize(
        "member,playerlist,ready,not_ready",
        [
            (
                Player(nick="test_nick_1", name="test_name_1"),
                [
                    Player(nick="test_nick_1", name="test_name_1"),
                    Player(nick="test_nick_2", name="test_name_2"),
                ],
                [],
                [
                    Player(nick="test_nick_1", name="test_name_1"),
                    Player(nick="test_nick_2", name="test_name_2"),
                ],
            ),
            (
                Player(nick="test_nick_1", name="test_name_1"),
                [
                    Player(nick="test_nick_1", name="test_name_1"),
                    Player(nick="test_nick_2", name="test_name_2"),
                ],
                [Player(nick="test_nick_2", name="test_name_2")],
                [Player(nick="test_nick_1", name="test_name_1")],
            ),
            (
                Player(nick="test_nick_3", name="test_name_3"),
                [
                    Player(nick="test_nick_1", name="test_name_1"),
                    Player(nick="test_nick_2", name="test_name_2"),
                ],
                [],
                [
                    Player(nick="test_nick_1", name="test_name_1"),
                    Player(nick="test_nick_2", name="test_name_2"),
                ],
            ),
            (
                Player(nick="test_nick_1", name="test_name_1"),
                [
                    Player(nick="test_nick_1", name="test_name_1"),
                    Player(nick="test_nick_2", name="test_name_2"),
                ],
                [Player(nick="test_nick_1", name="test_name_1")],
                [Player(nick="test_nick_2", name="test_name_2")],
            ),
        ],
    )
    @pytest.mark.asyncio()
    async def test_ready_player(
        self, monkeypatch, open_pug, member, playerlist, ready, not_ready
    ):
        monkeypatch.setattr(
            cod4pugbot.pug.OpenPUG,
            "_start_pug",
            _start_pug := mock.MagicMock(),
        )
        monkeypatch.setattr(
            cod4pugbot.pug.OpenPUG,
            "_modify_ready_up_message",
            _modify_ready_up_message := mock.MagicMock(),
        )
        open_pug.playerlist = playerlist.copy()
        open_pug.ready = ready.copy()
        open_pug.not_ready = not_ready.copy()

        await open_pug.ready_player(member=member)

        if member in playerlist and member in not_ready:
            assert member in open_pug.ready
            assert member not in open_pug.not_ready
            assert member in open_pug.playerlist
            if member is not None and len(ready) == 1:
                _start_pug.assert_called_once()
            else:
                _modify_ready_up_message.assert_called_once()
        elif member in playerlist and member in ready:
            assert member in open_pug.playerlist
            assert member in open_pug.ready
            assert member not in open_pug.not_ready
        else:
            assert member not in open_pug.playerlist
            assert member not in open_pug.ready
            assert member not in open_pug.not_ready

    @pytest.mark.parametrize(
        "member,playerlist,ready,not_ready",
        [
            (
                Player(nick="test_nick_1", name="test_name_1"),
                [
                    Player(nick="test_nick_1", name="test_name_1"),
                    Player(nick="test_nick_2", name="test_name_2"),
                ],
                [Player(nick="test_nick_1", name="test_name_1")],
                [Player(nick="test_nick_2", name="test_name_2")],
            ),
            (
                Player(nick="test_nick_1", name="test_name_1"),
                [
                    Player(nick="test_nick_1", name="test_name_1"),
                    Player(nick="test_nick_2", name="test_name_2"),
                ],
                [Player(nick="test_nick_2", name="test_name_2")],
                [Player(nick="test_nick_1", name="test_name_1")],
            ),
            (
                Player(nick="test_nick_3", name="test_name_3"),
                [
                    Player(nick="test_nick_1", name="test_name_1"),
                    Player(nick="test_nick_2", name="test_name_2"),
                ],
                [],
                [
                    Player(nick="test_nick_1", name="test_name_1"),
                    Player(nick="test_nick_2", name="test_name_2"),
                ],
            ),
            (
                Player(nick="test_nick_2", name="test_name_2"),
                [
                    Player(nick="test_nick_1", name="test_name_1"),
                    Player(nick="test_nick_2", name="test_name_2"),
                ],
                [],
                [Player(nick="test_nick_2", name="test_name_2")],
            ),
        ],
    )
    @pytest.mark.asyncio()
    async def test_unready_player(
        self, monkeypatch, open_pug, member, playerlist, ready, not_ready
    ):
        monkeypatch.setattr(
            cod4pugbot.pug.OpenPUG,
            "_modify_ready_up_message",
            _modify_ready_up_message := mock.MagicMock(),
        )
        open_pug.playerlist = playerlist.copy()
        open_pug.ready = ready.copy()
        open_pug.not_ready = not_ready.copy()

        await open_pug.unready_player(member=member)

        if member in playerlist and member in ready:
            assert member in open_pug.not_ready
            assert member not in open_pug.ready
            assert member in open_pug.playerlist
            _modify_ready_up_message.assert_called_once()
        elif member in playerlist and member in not_ready:
            assert member in open_pug.playerlist
            assert member not in open_pug.ready
            assert member in open_pug.not_ready
        else:
            assert member not in open_pug.playerlist
            assert member not in open_pug.ready
            assert member not in open_pug.not_ready
