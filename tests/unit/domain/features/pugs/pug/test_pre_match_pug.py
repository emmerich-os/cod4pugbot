import asyncio
import dataclasses
import datetime
from itertools import cycle
from unittest import mock

import discord
import freezegun
import pytest
import pytz

import cod4pugbot.domain.features.pugs.pug


@dataclasses.dataclass(eq=False)
class Category(discord.CategoryChannel):
    name: str
    id: str

    def __eq__(self, other):
        """Check if `self.name` is equal to `other.name`."""
        return (
            self.name == other.name if isinstance(other, Player) else self.name == other
        )

    def __hash__(self):
        """Hash name."""
        return hash(self.name)


class Player:
    def __init__(self, nick: str, name: str, roles: list = None):
        self.nick = nick
        self.name = name
        self.mention = f"@{nick}"
        self.roles = roles or []

    def __eq__(self, other):
        """Check if `other.nick` equals `self.nick`."""
        return (
            self.nick == other.nick if isinstance(other, Player) else self.nick == other
        )

    def __hash__(self):
        """Hash nick."""
        return hash(self.nick)

    def __repr__(self):
        """Name when printing."""
        return f"<{self.__class__.__name__} {self.nick=} {self.name=} {self.roles=}>"

    def __str__(self):
        """Name when formatted to string."""
        return f"<{self.__class__.__name__} {self.nick=} {self.name=} {self.roles=}>"


@dataclasses.dataclass(eq=False)
class Reaction(discord.Reaction):
    message: dict
    emoji: str

    async def remove(self, user):
        return


@dataclasses.dataclass(eq=False)
class Message(discord.Message):
    id: int
    author: Player
    mentions: list = None
    content: str = None

    def __eq__(self, other):
        """Check if self.id is equal to other.id."""
        return self.id == other.id


@dataclasses.dataclass(eq=False)
class ReactionWithMessage(discord.Reaction):
    message: Message
    emoji: str

    async def remove(self, user):
        return


@dataclasses.dataclass(eq=False)
class PUG:
    status: str
    playerlist: list
    playerqueue: list


async def async_magic():
    return


mock.MagicMock.__await__ = lambda x: async_magic().__await__()

TIME = "2020-01-01 00:00"


class TestPreMatchPUG:
    @freezegun.freeze_time(TIME)
    @pytest.mark.parametrize(
        "pug_id,mode,size,playerlist",
        [
            (
                1,
                "bo1",
                "1v1",
                [
                    Player(
                        nick="test_nick_1",
                        name="test_name_1",
                        roles=["member", "captain"],
                    ),
                    Player(nick="test_nick_2", name="test_name_2", roles=["member"]),
                ],
            ),
        ],
    )
    @pytest.mark.asyncio()
    async def test_create(
        self, monkeypatch, cog, guild, pug_id, mode, size, playerlist
    ):
        monkeypatch.setattr(
            cod4pugbot.pug.PreMatchPUG,
            "_create_match_channel",
            _create_match_channel := mock.MagicMock(),
        )
        monkeypatch.setattr(
            cod4pugbot.pug.PreMatchPUG,
            "_remove_players_from_open_pugs",
            _remove_players_from_open_pugs := mock.MagicMock(),
        )
        monkeypatch.setattr(
            cod4pugbot.pug.PreMatchPUG,
            "_create_match_message",
            _create_match_message := mock.MagicMock(),
        )
        monkeypatch.setattr(
            cod4pugbot.pug.PreMatchPUG,
            "_pick_captains",
            _pick_captains := mock.MagicMock(),
        )
        result = await cod4pugbot.pug.PreMatchPUG.create(
            cog=cog,
            guild=guild,
            pug_id=pug_id,
            mode=mode,
            size=size,
            playerlist=playerlist,
        )

        assert isinstance(result, cod4pugbot.pug.PreMatchPUG)
        _create_match_channel.assert_called_once()
        _remove_players_from_open_pugs.assert_called_once()
        _create_match_message.assert_called_once()
        assert result.cog == cog
        assert result.guild == guild
        assert result.id == 1
        assert result.mode == mode
        assert result.size == size
        assert result.playerlist == playerlist
        assert result.captains_pool == playerlist[:-1]
        assert result.max_time == datetime.datetime(
            2020, 1, 1, 0, 15, tzinfo=pytz.timezone("UTC")
        )
        _pick_captains.assert_called_once()

    @pytest.mark.parametrize(
        "open_pug_status,open_pug_playerlist,playerlist,expected",
        [
            (
                "open",
                [
                    Player(nick="test_nick_1", name="test_name_1"),
                    Player(nick="test_nick_2", name="test_name_2"),
                ],
                [
                    Player(nick="test_nick_3", name="test_name_3"),
                    Player(nick="test_nick_4", name="test_name_4"),
                ],
                0,
            ),
            (
                "open",
                [
                    Player(nick="test_nick_1", name="test_name_1"),
                    Player(nick="test_nick_2", name="test_name_2"),
                ],
                [
                    Player(nick="test_nick_1", name="test_name_1"),
                    Player(nick="test_nick_3", name="test_name_3"),
                ],
                1,
            ),
            (
                "open",
                [
                    Player(nick="test_nick_1", name="test_name_1"),
                    Player(nick="test_nick_2", name="test_name_2"),
                ],
                [
                    Player(nick="test_nick_1", name="test_name_1"),
                    Player(nick="test_nick_2", name="test_name_2"),
                ],
                2,
            ),
            (
                "readyup",
                [
                    Player(nick="test_nick_1", name="test_name_1"),
                    Player(nick="test_nick_2", name="test_name_2"),
                ],
                [
                    Player(nick="test_nick_3", name="test_name_3"),
                    Player(nick="test_nick_4", name="test_name_4"),
                ],
                0,
            ),
            (
                "readyup",
                [
                    Player(nick="test_nick_1", name="test_name_1"),
                    Player(nick="test_nick_2", name="test_name_2"),
                ],
                [
                    Player(nick="test_nick_1", name="test_name_1"),
                    Player(nick="test_nick_3", name="test_name_3"),
                ],
                1,
            ),
            (
                "readyup",
                [
                    Player(nick="test_nick_1", name="test_name_1"),
                    Player(nick="test_nick_2", name="test_name_2"),
                ],
                [
                    Player(nick="test_nick_1", name="test_name_1"),
                    Player(nick="test_nick_2", name="test_name_2"),
                ],
                2,
            ),
        ],
    )
    @pytest.mark.asyncio()
    async def test_remove_players_from_open_pugs(
        self,
        monkeypatch,
        open_pug,
        open_pug_status,
        open_pug_playerlist,
        pre_match_pug,
        playerlist,
        expected,
    ):
        monkeypatch.setattr(
            cod4pugbot.pug.OpenPUG, "remove_player", remove_player := mock.MagicMock()
        )
        monkeypatch.setattr(
            cod4pugbot.domain.features.pugs.pug.base_pug.PUG,
            "remove_reaction",
            remove_reaction := mock.MagicMock(),
        )
        monkeypatch.setattr(
            cod4pugbot.pug.OpenPUG,
            "abort_ready_up_phase",
            abort_ready_up_phase := mock.MagicMock(),
        )
        open_pug.status = open_pug_status
        open_pug.playerlist = open_pug_playerlist.copy()
        pre_match_pug.cog.active_pugs.pugs = {1: open_pug}
        pre_match_pug.playerlist = playerlist.copy()

        await pre_match_pug._remove_players_from_open_pugs()

        assert remove_player.call_count == expected
        assert remove_reaction.call_count == expected
        if open_pug_status == "readyup" and expected > 0:
            abort_ready_up_phase.assert_called_once()

    @pytest.mark.parametrize(
        "playerlist,expected",
        [
            (
                [
                    Player(nick="test_nick_1", name="test_name_1", roles=["captain"]),
                    Player(nick="test_nick_2", name="test_name_2", roles=["captain"]),
                    Player(nick="test_nick_3", name="test_name_3", roles=[]),
                    Player(nick="test_nick_4", name="test_name_4", roles=[]),
                ],
                [
                    Player(nick="test_nick_1", name="test_name_1"),
                    Player(nick="test_nick_2", name="test_name_2"),
                ],
            ),
            (
                [
                    Player(nick="test_nick_1", name="test_name_1", roles=["captain"]),
                    Player(nick="test_nick_2", name="test_name_2", roles=[]),
                    Player(nick="test_nick_3", name="test_name_3", roles=["captain"]),
                    Player(nick="test_nick_4", name="test_name_4", roles=[]),
                ],
                [
                    Player(nick="test_nick_1", name="test_name_1"),
                    Player(nick="test_nick_3", name="test_name_3"),
                ],
            ),
            (
                [
                    Player(nick="test_nick_1", name="test_name_1", roles=["captain"]),
                    Player(nick="test_nick_2", name="test_name_2", roles=[]),
                    Player(nick="test_nick_3", name="test_name_3", roles=[]),
                    Player(nick="test_nick_4", name="test_name_4", roles=["captain"]),
                ],
                [
                    Player(nick="test_nick_1", name="test_name_1"),
                    Player(nick="test_nick_4", name="test_name_4"),
                ],
            ),
            (
                [
                    Player(nick="test_nick_1", name="test_name_1", roles=[]),
                    Player(nick="test_nick_2", name="test_name_2", roles=["captain"]),
                    Player(nick="test_nick_3", name="test_name_3", roles=["captain"]),
                    Player(nick="test_nick_4", name="test_name_4", roles=[]),
                ],
                [
                    Player(nick="test_nick_2", name="test_name_2"),
                    Player(nick="test_nick_3", name="test_name_3"),
                ],
            ),
            (
                [
                    Player(nick="test_nick_1", name="test_name_1", roles=[]),
                    Player(nick="test_nick_2", name="test_name_2", roles=["captain"]),
                    Player(nick="test_nick_3", name="test_name_3", roles=[]),
                    Player(nick="test_nick_4", name="test_name_4", roles=["captain"]),
                ],
                [
                    Player(nick="test_nick_2", name="test_name_2"),
                    Player(nick="test_nick_4", name="test_name_4"),
                ],
            ),
            (
                [
                    Player(nick="test_nick_1", name="test_name_1", roles=[]),
                    Player(nick="test_nick_2", name="test_name_2", roles=[]),
                    Player(nick="test_nick_3", name="test_name_3", roles=["captain"]),
                    Player(nick="test_nick_4", name="test_name_4", roles=["captain"]),
                ],
                [
                    Player(nick="test_nick_3", name="test_name_3"),
                    Player(nick="test_nick_4", name="test_name_4"),
                ],
            ),
            (
                [
                    Player(nick="test_nick_1", name="test_name_1", roles=["captain"]),
                    Player(nick="test_nick_2", name="test_name_2", roles=[]),
                    Player(nick="test_nick_3", name="test_name_3", roles=[]),
                    Player(nick="test_nick_4", name="test_name_4", roles=[]),
                ],
                [Player(nick="test_nick_1", name="test_name_1")],
            ),
            (
                [
                    Player(nick="test_nick_1", name="test_name_1", roles=[]),
                    Player(nick="test_nick_2", name="test_name_2", roles=["captain"]),
                    Player(nick="test_nick_3", name="test_name_3", roles=[]),
                    Player(nick="test_nick_4", name="test_name_4", roles=[]),
                ],
                [Player(nick="test_nick_2", name="test_name_2")],
            ),
            (
                [
                    Player(nick="test_nick_1", name="test_name_1", roles=[]),
                    Player(nick="test_nick_2", name="test_name_2", roles=[]),
                    Player(nick="test_nick_3", name="test_name_3", roles=["captain"]),
                    Player(nick="test_nick_4", name="test_name_4", roles=[]),
                ],
                [Player(nick="test_nick_3", name="test_name_3")],
            ),
            (
                [
                    Player(nick="test_nick_1", name="test_name_1", roles=[]),
                    Player(nick="test_nick_2", name="test_name_2", roles=[]),
                    Player(nick="test_nick_3", name="test_name_3", roles=[]),
                    Player(nick="test_nick_4", name="test_name_4", roles=["captain"]),
                ],
                [Player(nick="test_nick_4", name="test_name_4")],
            ),
            (
                [
                    Player(nick="test_nick_1", name="test_name_1", roles=[]),
                    Player(nick="test_nick_2", name="test_name_2", roles=[]),
                    Player(nick="test_nick_3", name="test_name_3", roles=[]),
                    Player(nick="test_nick_4", name="test_name_4", roles=[]),
                ],
                [],
            ),
        ],
    )
    def test_pick_captains(self, pre_match_pug, playerlist, expected):
        pre_match_pug.playerlist = playerlist.copy()
        pre_match_pug.captains_pool = [
            player for player in playerlist if "captain" in player.roles
        ] or playerlist.copy()

        pre_match_pug._pick_captains()
        result = [pre_match_pug.teams[0]["captain"], pre_match_pug.teams[1]["captain"]]

        assert len(pre_match_pug.playerlist) == 2
        if len(expected) == 2:
            for player in expected:
                assert player in result
        elif len(expected) == 1:
            assert expected[0] in result
        assert len(result) == 2

    @pytest.mark.parametrize(
        "categories",
        [
            ([]),
            ([Category(name="RUNNING PUGS", id="test_id")]),
        ],
    )
    @pytest.mark.asyncio()
    async def test_create_match_channel(self, monkeypatch, pre_match_pug, categories):
        async def mock_create_category_channel(*args, **kwargs):
            return Category(name="RUNNING PUGS", id="test_id")

        async def mock_create_text_channel(*args, **kwargs):
            return "test_channel"

        monkeypatch.setattr(
            discord.Guild,
            "categories",
            mock.MagicMock(return_value=categories),
        )
        monkeypatch.setattr(
            discord.Guild,
            "create_category_channel",
            create_category_channel := mock.MagicMock(
                side_effect=mock_create_category_channel
            ),
        )
        monkeypatch.setattr(
            discord.CategoryChannel,
            "create_text_channel",
            create_text_channel := mock.MagicMock(side_effect=mock_create_text_channel),
        )

        with mock.patch("cod4pugbot.config.save_to_config"):
            await pre_match_pug._create_match_channel()

        if categories:
            create_category_channel.assert_called_once()
            assert categories[0].id in pre_match_pug.cog.bot.category_ids.values()
        create_text_channel.assert_called_once()
        assert pre_match_pug.channel == "test_channel"

    @pytest.mark.parametrize(
        "attack,defence,phase,expected",
        [
            (
                [Player(nick="test_nick_1", name="test_name_1")],
                [Player(nick="test_nick_2", name="test_name_2")],
                "match",
                (
                    """\n"""
                    """\t\t**MATCH PHASE STARTED**\n\n"""
                    """> att_icon **-** @test_nick_1\n\n"""
                    """\t\t**VERSUS**\n\n"""
                    """> def_icon **-** @test_nick_2\n"""
                ),
            ),
            (
                [
                    Player(nick="test_nick_1", name="test_name_1"),
                    Player(nick="test_nick_3", name="test_name_3"),
                ],
                [
                    Player(nick="test_nick_2", name="test_name_2"),
                    Player(nick="test_nick_4", name="test_name_4"),
                ],
                "player pick",
                (
                    """\n"""
                    """\t\t**PLAYER PICK PHASE STARTED**\n\n"""
                    """> att_icon **-** @test_nick_1, @test_nick_3\n\n"""
                    """\t\t**VERSUS**\n\n"""
                    """> def_icon **-** @test_nick_2, @test_nick_4\n"""
                ),
            ),
        ],
    )
    @pytest.mark.asyncio()
    async def test_create_match_message(
        self, monkeypatch, pre_match_pug, channel, attack, defence, phase, expected
    ):
        monkeypatch.setattr(
            cod4pugbot.pug.PreMatchPUG,
            "_start_player_pick_phase",
            _start_player_pick_phase := mock.MagicMock(),
        )
        pre_match_pug.attack = {"icon": "att_icon", "players": attack}
        pre_match_pug.defence = {"icon": "def_icon", "players": defence}
        pre_match_pug.channel = channel
        pre_match_pug.phase = phase

        await pre_match_pug._create_match_message()
        result = pre_match_pug.messages["match"].content

        assert result == expected
        assert pre_match_pug.match_base_message == expected
        _start_player_pick_phase.assert_called_once()

    @pytest.mark.parametrize(
        "picker,phase,map_elimination_phase,expected",
        [
            (
                None,
                "match",
                None,
                "",
            ),
            (
                Player(nick="test_nick_1", name="test_name_1"),
                "player pick",
                None,
                (
                    """\n@test_nick_1, it's your turn to """
                    """__**pick**__ a player!\n"""
                    """React with \u2705 to a message to pick the respective player."""
                ),
            ),
            (
                Player(nick="test_nick_2", name="test_name_2"),
                "map elimination",
                "pick",
                (
                    """\n@test_nick_2, it's your turn to """
                    """__**pick**__ a map!\n"""
                    """React with \u2705 to a message to pick the respective map."""
                ),
            ),
            (
                Player(nick="test_nick_3", name="test_name_3"),
                "map elimination",
                "veto",
                (
                    """\n@test_nick_3, it's your turn to """
                    """__**veto**__ a map!\n"""
                    """React with \u26d4 to a message to veto the respective map."""
                ),
            ),
        ],
    )
    @pytest.mark.asyncio()
    async def test_modify_match_message(
        self,
        monkeypatch,
        pre_match_pug,
        channel,
        message,
        picker,
        phase,
        map_elimination_phase,
        expected,
    ):
        monkeypatch.setattr(
            cod4pugbot.pug.PreMatchPUG,
            "match_base_message",
            match_base_message := mock.MagicMock(return_value="test "),
        )
        match_base_message.__add__ = lambda self, other: "test " + other
        if phase != "match":
            expected = f"test {expected}"
        message.content = ""
        pre_match_pug.messages = {"match": message}
        pre_match_pug.channel = channel
        pre_match_pug.phase = phase
        pre_match_pug.map_elimination_phase = map_elimination_phase
        pre_match_pug.picker = picker

        await pre_match_pug._modify_match_message()
        result = pre_match_pug.messages["match"].content

        assert result == expected

    @pytest.mark.parametrize(
        "pickers,playerlist,size",
        [
            (
                [
                    Player(nick="test_nick_1", name="test_name_1"),
                    Player(nick="test_nick_2", name="test_name_2"),
                ],
                [],
                "1v1",
            ),
            (
                [
                    Player(nick="test_nick_1", name="test_name_1"),
                    Player(nick="test_nick_2", name="test_name_2"),
                ],
                [
                    Player(nick="test_nick_3", name="test_name_3"),
                    Player(nick="test_nick_4", name="test_name_4"),
                ],
                "2v2",
            ),
            (
                [
                    Player(nick="test_nick_1", name="test_name_1"),
                    Player(nick="test_nick_2", name="test_name_2"),
                ],
                [
                    Player(nick="test_nick_3", name="test_name_3"),
                    Player(nick="test_nick_4", name="test_name_4"),
                    Player(nick="test_nick_5", name="test_name_5"),
                    Player(nick="test_nick_6", name="test_name_6"),
                ],
                "3v3",
            ),
            (
                [
                    Player(nick="test_nick_1", name="test_name_1"),
                    Player(nick="test_nick_2", name="test_name_2"),
                ],
                [
                    Player(nick="test_nick_3", name="test_name_3"),
                    Player(nick="test_nick_4", name="test_name_4"),
                    Player(nick="test_nick_5", name="test_name_5"),
                    Player(nick="test_nick_6", name="test_name_6"),
                    Player(nick="test_nick_7", name="test_name_7"),
                    Player(nick="test_nick_8", name="test_name_8"),
                    Player(nick="test_nick_9", name="test_name_9"),
                    Player(nick="test_nick_10", name="test_name_10"),
                ],
                "5v5",
            ),
        ],
    )
    @pytest.mark.asyncio()
    async def test_start_player_pick_phase(
        self, monkeypatch, pre_match_pug, channel, pickers, playerlist, size
    ):
        monkeypatch.setattr(
            cod4pugbot.pug.PreMatchPUG,
            "_start_map_elimination",
            _start_map_elimination := mock.MagicMock(),
        )
        monkeypatch.setattr(
            cod4pugbot.pug.PreMatchPUG,
            "_modify_match_message",
            _modify_match_message := mock.MagicMock(),
        )
        pre_match_pug.channel = channel
        pre_match_pug.pickers = pickers
        pre_match_pug.playerlist = playerlist.copy()
        pre_match_pug.size = size

        await pre_match_pug._start_player_pick_phase()

        if len(playerlist + pickers) == 2:
            _start_map_elimination.assert_called_once()
        else:
            assert pre_match_pug.phase == "player pick"
            for player in playerlist:
                assert player in pre_match_pug.messages["players"]
                assert isinstance(
                    pre_match_pug.messages["players"][player], discord.Message
                )
                assert "\u2705" in pre_match_pug.messages["players"][player].emojis
            _modify_match_message.assert_called_once()

    @pytest.mark.parametrize(
        "mode,expected", [("bo1", 1), ("bo2", 2), ("bo3", 3), ("bo5", 5)]
    )
    @pytest.mark.asyncio()
    async def test_pick_random_maps(self, pre_match_pug, mode, expected):
        map_pool = ["Backlot", "Crash", "Crossfire", "Citystreets", "Strike", "Vacant"]
        pre_match_pug.map_pool = map_pool.copy()
        pre_match_pug.mode = mode

        await pre_match_pug._pick_random_maps()

        assert len(pre_match_pug.maps) == expected
        assert len(pre_match_pug.map_pool) == len(map_pool) - expected

    @pytest.mark.parametrize("map_elimination", [False, True])
    @pytest.mark.asyncio()
    async def test_start_map_elimination(
        self, monkeypatch, pre_match_pug, channel, map_elimination
    ):
        monkeypatch.setattr(
            cod4pugbot.pug.PreMatchPUG,
            "_modify_match_message",
            _modify_match_message := mock.MagicMock(),
        )
        monkeypatch.setattr(
            cod4pugbot.pug.PreMatchPUG,
            "_pick_random_maps",
            _pick_random_maps := mock.MagicMock(),
        )
        monkeypatch.setattr(
            cod4pugbot.pug.PreMatchPUG,
            "_start_pug",
            _start_pug := mock.MagicMock(),
        )
        pre_match_pug.map_elimination = map_elimination
        map_pool = ["Backlot", "Crash", "Crossfire", "Citystreets", "Strike", "Vacant"]
        pre_match_pug.map_pool = map_pool.copy()
        pre_match_pug.channel = channel

        await pre_match_pug._start_map_elimination()

        assert pre_match_pug.phase == "map elimination"
        if map_elimination:
            for map_ in map_pool:
                assert map_ in pre_match_pug.messages["maps"]
                assert isinstance(pre_match_pug.messages["maps"][map_], discord.Message)
                assert pre_match_pug.messages["maps"][map_].content == f"> {map_}"
                assert "\u2705" in pre_match_pug.messages["maps"][map_].emojis
                assert "\u26d4" in pre_match_pug.messages["maps"][map_].emojis
            _modify_match_message.assert_called_once()
        else:
            _pick_random_maps.assert_called_once()
            _start_pug.assert_called_once()

    @pytest.mark.parametrize(
        "messages,expected",
        [
            (
                {
                    "test_key": Message(
                        id=1,
                        author=Player(nick="test_player", name="test_player"),
                    ),
                },
                {},
            ),
            (
                {
                    "match": Message(
                        id=1,
                        author=Player(nick="test_player", name="test_player"),
                    ),
                },
                {
                    "match": Message(
                        id=1,
                        author=Player(nick="test_player", name="test_player"),
                    ),
                },
            ),
            (
                {
                    "test_key": Message(
                        id=1,
                        author=Player(nick="test_player", name="test_player"),
                    ),
                    "match": Message(
                        id=1,
                        author=Player(nick="test_player", name="test_player"),
                    ),
                },
                {
                    "match": Message(
                        id=1,
                        author=Player(nick="test_player", name="test_player"),
                    ),
                },
            ),
            (
                {
                    "test_key": Message(
                        id=1,
                        author=Player(nick="test_player", name="test_player"),
                    ),
                    "test_key_2": {
                        "test_key_3": Message(
                            id=1,
                            author=Player(nick="test_player", name="test_player"),
                        ),
                    },
                },
                {},
            ),
            (
                {
                    "test_key": Message(
                        id=1,
                        author=Player(nick="test_player", name="test_player"),
                    ),
                    "test_key_2": {
                        "test_key_3": Message(
                            id=1,
                            author=Player(nick="test_player", name="test_player"),
                        ),
                        "match": Message(
                            id=1,
                            author=Player(nick="test_player", name="test_player"),
                        ),
                    },
                },
                {
                    "test_key_2": {
                        "match": Message(
                            id=1,
                            author=Player(nick="test_player", name="test_player"),
                        ),
                    }
                },
            ),
        ],
    )
    @pytest.mark.asyncio()
    async def test_delete_remaining_messages(
        self, monkeypatch, pre_match_pug, messages, expected
    ):
        monkeypatch.setattr(
            discord.Message,
            "delete",
            mock.MagicMock(),
        )
        pre_match_pug.messages = messages.copy()

        await pre_match_pug._delete_remaining_messages()

        assert pre_match_pug.messages == expected

    @pytest.mark.asyncio()
    async def test_start_pug(self, monkeypatch, pre_match_pug, channel, message):
        monkeypatch.setattr(
            cod4pugbot.pug.PreMatchPUG,
            "_modify_match_message",
            _modify_match_message := mock.MagicMock(),
        )
        monkeypatch.setattr(
            cod4pugbot.pug.PreMatchPUG,
            "_delete_remaining_messages",
            _delete_remaining_messages := mock.MagicMock(),
        )
        monkeypatch.setattr(
            cod4pugbot.pug.RunningPUG,
            "create",
            create := mock.MagicMock(),
        )
        pre_match_pug.channel = channel
        pre_match_pug.message = message

        await pre_match_pug._start_pug()

        _modify_match_message.assert_called_once()
        _delete_remaining_messages.assert_called_once()
        create.assert_called_once()

    @pytest.mark.parametrize(
        "captain,player",
        [
            (
                Player(nick="test_nick_1", name="test_name_1"),
                Player(nick="test_nick_3", name="test_name_3"),
            ),
            (
                Player(nick="test_nick_2", name="test_name_2"),
                Player(nick="test_nick_3", name="test_name_3"),
            ),
            (
                Player(nick="test_nick_3", name="test_name_3"),
                Player(nick="test_nick_4", name="test_name_4"),
            ),
        ],
    )
    @pytest.mark.asyncio()
    async def test_add_player_to_team(
        self, monkeypatch, pre_match_pug, message, captain, player
    ):
        monkeypatch.setattr(
            cod4pugbot.pug.PreMatchPUG,
            "_modify_match_message",
            _modify_match_message := mock.MagicMock(),
        )
        captains = [
            Player(nick="test_nick_1", name="test_name_1"),
            Player(nick="test_nick_2", name="test_name_2"),
        ]
        pre_match_pug.attack = {"captain": captains[0], "players": []}
        pre_match_pug.defence = {"captain": captains[1], "players": []}
        pre_match_pug.messages["players"][player] = message
        pre_match_pug.playerlist = [player]

        await pre_match_pug._add_player_to_team(captain=captain, player=player)

        if captain in captains:
            if captain == captains[0]:
                assert player in pre_match_pug.attack["players"]
            elif captain == captains[1]:
                assert player in pre_match_pug.defence["players"]
            assert player not in pre_match_pug.playerlist
            assert player not in pre_match_pug.messages["players"]
            _modify_match_message.assert_called_once()
        else:
            assert player not in pre_match_pug.attack["players"]
            assert player in pre_match_pug.playerlist
            assert player in pre_match_pug.messages["players"]
            _modify_match_message.assert_not_called()

    @pytest.mark.parametrize(
        "map_picked",
        ["Backlot", "Crash", "Crossfire", "Citystreets", "Strike", "Vacant", "Ambush"],
    )
    @pytest.mark.asyncio()
    async def test_add_map_to_maps(
        self, monkeypatch, pre_match_pug, message, map_picked
    ):
        monkeypatch.setattr(
            cod4pugbot.pug.PreMatchPUG,
            "_modify_match_message",
            _modify_match_message := mock.MagicMock(),
        )
        map_pool = ["Backlot", "Crash", "Crossfire", "Citystreets", "Strike", "Vacant"]
        pre_match_pug.map_pool = map_pool.copy()
        if map_picked in map_pool:
            pre_match_pug.messages["maps"][map_picked] = message

        await pre_match_pug._add_map_to_maps(map_to_add=map_picked)

        if map_picked in map_pool:
            assert map_picked in pre_match_pug.maps
            _modify_match_message.assert_called_once()
        else:
            assert map_picked not in pre_match_pug.maps
            _modify_match_message.assert_not_called()
        assert map_picked not in pre_match_pug.map_pool
        assert map_picked not in pre_match_pug.messages["maps"]

    @pytest.mark.parametrize(
        "map_vetoed",
        ["Backlot", "Crash", "Crossfire", "Citystreets", "Strike", "Vacant", "Ambush"],
    )
    @pytest.mark.asyncio()
    async def test_remove_map_from_map_pool(
        self, monkeypatch, pre_match_pug, message, map_vetoed
    ):
        monkeypatch.setattr(
            cod4pugbot.pug.PreMatchPUG,
            "_modify_match_message",
            _modify_match_message := mock.MagicMock(),
        )
        map_pool = ["Backlot", "Crash", "Crossfire", "Citystreets", "Strike", "Vacant"]
        pre_match_pug.map_pool = map_pool.copy()
        if map_vetoed in map_pool:
            pre_match_pug.messages["maps"][map_vetoed] = message

        await pre_match_pug._remove_map_from_map_pool(map_to_remove=map_vetoed)

        if map_vetoed in map_pool:
            _modify_match_message.assert_called_once()
        else:
            _modify_match_message.assert_not_called()
        assert map_vetoed not in pre_match_pug.maps
        assert map_vetoed not in pre_match_pug.map_pool
        assert map_vetoed not in pre_match_pug.messages["maps"]

    @pytest.mark.parametrize(
        "picker,delete_channel_delay,expected",
        [
            (
                Player(nick="test_nick", name="test_name"),
                60,
                (
                    """> PUG has expired.\n"""
                    """> @test_nick did not pick in time.\n"""
                    """> This channel will be deleted in 1 minute."""
                ),
            ),
            (
                Player(nick="test_nick_1", name="test_name_1"),
                120,
                (
                    """> PUG has expired.\n"""
                    """> @test_nick_1 did not pick in time.\n"""
                    """> This channel will be deleted in 2 minutes."""
                ),
            ),
        ],
    )
    @pytest.mark.asyncio()
    async def test_end_pug(
        self,
        monkeypatch,
        pre_match_pug,
        channel,
        picker,
        delete_channel_delay,
        expected,
    ):
        monkeypatch.setattr(
            cod4pugbot.pug.PreMatchPUG,
            "_delete_remaining_messages",
            _delete_remaining_messages := mock.MagicMock(),
        )
        monkeypatch.setattr(
            cod4pugbot.pug.ActivePUGs,
            "remove_pug",
            remove_pug := mock.MagicMock(),
        )
        monkeypatch.setattr(
            cod4pugbot.pug.PreMatchPUG,
            "_modify_match_message",
            _modify_match_message := mock.MagicMock(),
        )
        monkeypatch.setattr(
            channel,
            "send",
            send := mock.MagicMock(),
        )
        monkeypatch.setattr(
            asyncio,
            "sleep",
            sleep := mock.MagicMock(),
        )
        pre_match_pug.channel = channel
        pre_match_pug.picker = picker
        pre_match_pug.delete_channel_delay = delete_channel_delay

        await pre_match_pug.end_pug()

        _delete_remaining_messages.assert_called_once()
        remove_pug.assert_called_once_with(pug_id=2)
        send.assert_called_once_with(expected)
        _modify_match_message.assert_called_once()
        sleep.assert_called_once_with(delete_channel_delay)

    @pytest.mark.parametrize(
        "phase,picker,captain,reaction,add_message_to_pug,playerlist",
        [
            # 1. case: wrong phase
            (
                "readyup",
                Player(nick="test_nick_1", name="test_name_1"),
                Player(nick="test_nick_1", name="test_name_1"),
                ReactionWithMessage(
                    message=Message(
                        id=1,
                        author=Player(nick="test_bot", name="test_bot"),
                        mentions=[Player(nick="test_nick_3", name="test_name_3")],
                    ),
                    emoji="\u2705",
                ),
                True,
                [Player(nick="test_nick_3", name="test_name_3")],
            ),
            # 2. case: captain not allowed to pick
            (
                "player pick",
                Player(nick="test_nick_1", name="test_name_1"),
                Player(nick="test_nick_2", name="test_name_2"),
                ReactionWithMessage(
                    message=Message(
                        id=1,
                        author=Player(nick="test_bot", name="test_bot"),
                        mentions=[Player(nick="test_nick_3", name="test_name_3")],
                    ),
                    emoji="\u2705",
                ),
                True,
                [Player(nick="test_nick_3", name="test_name_3")],
            ),
            # 2. case: captain not allowed to pick
            (
                "player pick",
                Player(nick="test_nick_2", name="test_name_2"),
                Player(nick="test_nick_1", name="test_name_1"),
                ReactionWithMessage(
                    message=Message(
                        id=1,
                        author=Player(nick="test_bot", name="test_bot"),
                        mentions=[Player(nick="test_nick_3", name="test_name_3")],
                    ),
                    emoji="\u2705",
                ),
                True,
                [Player(nick="test_nick_3", name="test_name_3")],
            ),
            # 3. case: message not a message from the Bot
            (
                "player pick",
                Player(nick="test_nick_1", name="test_name_1"),
                Player(nick="test_nick_1", name="test_name_1"),
                ReactionWithMessage(
                    message=Message(
                        id=1,
                        author=Player(nick="test_author", name="test_author"),
                        mentions=[Player(nick="test_nick_3", name="test_name_3")],
                    ),
                    emoji="\u2705",
                ),
                True,
                [Player(nick="test_nick_3", name="test_name_3")],
            ),
            # 4. case: wrong emoji
            (
                "player pick",
                Player(nick="test_nick_1", name="test_name_1"),
                Player(nick="test_nick_1", name="test_name_1"),
                ReactionWithMessage(
                    message=Message(
                        id=1,
                        author=Player(nick="test_author", name="test_author"),
                        mentions=[Player(nick="test_nick_3", name="test_name_3")],
                    ),
                    emoji="\u26d4",
                ),
                False,
                [Player(nick="test_nick_3", name="test_name_3")],
            ),
            # 6. case: message not in bot messages
            (
                "player pick",
                Player(nick="test_nick_1", name="test_name_1"),
                Player(nick="test_nick_1", name="test_name_1"),
                ReactionWithMessage(
                    message=Message(
                        id=1,
                        author=Player(nick="test_bot", name="test_bot"),
                        mentions=[Player(nick="test_nick_3", name="test_name_3")],
                    ),
                    emoji="\u2705",
                ),
                False,
                [Player(nick="test_nick_4", name="test_name_4")],
            ),
            # 7. case: player not in playerlist
            (
                "player pick",
                Player(nick="test_nick_1", name="test_name_1"),
                Player(nick="test_nick_1", name="test_name_1"),
                ReactionWithMessage(
                    message=Message(
                        id=1,
                        author=Player(nick="test_bot", name="test_bot"),
                        mentions=[Player(nick="test_nick_3", name="test_name_3")],
                    ),
                    emoji="\u2705",
                ),
                True,
                [Player(nick="test_nick_4", name="test_name_4")],
            ),
            (
                "player pick",
                Player(nick="test_nick_1", name="test_name_1"),
                Player(nick="test_nick_1", name="test_name_1"),
                ReactionWithMessage(
                    message=Message(
                        id=1,
                        author=Player(nick="test_bot", name="test_bot"),
                        mentions=[Player(nick="test_nick_3", name="test_name_3")],
                    ),
                    emoji="\u2705",
                ),
                True,
                [Player(nick="test_nick_3", name="test_name_3")],
            ),
            (
                "player pick",
                Player(nick="test_nick_1", name="test_name_1"),
                Player(nick="test_nick_1", name="test_name_1"),
                ReactionWithMessage(
                    message=Message(
                        id=1,
                        author=Player(nick="test_bot", name="test_bot"),
                        mentions=[Player(nick="test_nick_3", name="test_name_3")],
                    ),
                    emoji="\u2705",
                ),
                True,
                [
                    Player(nick="test_nick_3", name="test_name_3"),
                    Player(nick="test_nick_4", name="test_name_4"),
                ],
            ),
        ],
    )
    @pytest.mark.asyncio()
    async def test_pick_player(
        self,
        monkeypatch,
        pre_match_pug,
        phase,
        picker,
        captain,
        reaction,
        add_message_to_pug,
        playerlist,
        map_elimination,
    ):
        monkeypatch.setattr(
            cod4pugbot.pug.PreMatchPUG,
            "_add_player_to_team",
            _add_player_to_team := mock.MagicMock(),
        )
        monkeypatch.setattr(
            cod4pugbot.pug.PreMatchPUG,
            "_modify_match_message",
            _modify_match_message := mock.MagicMock(),
        )
        monkeypatch.setattr(
            cod4pugbot.pug.PreMatchPUG,
            "_start_map_elimination",
            _start_map_elimination := mock.MagicMock(),
        )
        monkeypatch.setattr(
            cod4pugbot.pug.PreMatchPUG,
            "_pick_random_maps",
            _pick_random_maps := mock.MagicMock(),
        )
        monkeypatch.setattr(
            cod4pugbot.pug.PreMatchPUG,
            "_start_pug",
            _start_pug := mock.MagicMock(),
        )
        captains = [
            Player(nick="test_nick_1", name="test_name_1"),
            Player(nick="test_nick_2", name="test_name_2"),
        ]
        pre_match_pug.attack = {"captain": captains[0]}
        pre_match_pug.defence = {"captain": captains[1]}
        pre_match_pug.picker_cycle = cycle(captains.copy())
        # iterate to test_nick_1 just like PreMatchPUG._start_player_pick_phase does
        next(pre_match_pug.picker_cycle)
        pre_match_pug.picker = picker
        pre_match_pug.playerlist = playerlist.copy()
        if add_message_to_pug:
            pre_match_pug.messages["players"]["test_player_message"] = reaction.message
        pre_match_pug.phase = phase
        pre_match_pug.map_elimination = map_elimination

        await pre_match_pug.pick_player(captain=captain, reaction=reaction)

        if (
            phase == "player pick"
            and captain in captains
            and captain == picker
            and add_message_to_pug
            and reaction.message.author == Player(nick="test_bot", name="test_bot")
            and reaction.emoji == "\u2705"
            and reaction.message.mentions
            and reaction.message.mentions[0] in playerlist
        ):
            assert pre_match_pug.picker == captains[1]

            if len(playerlist) == 1:
                _add_player_to_team.assert_called_with(
                    captain=captains[1], player=playerlist[0]
                )
                if map_elimination:
                    _modify_match_message.assert_called_once()
                    _start_map_elimination.assert_called_once()
                else:
                    _pick_random_maps.assert_called_once()
                    _start_pug.assert_called_once()
            else:
                _add_player_to_team.assert_called_with(
                    captain=captain,
                    player=reaction.message.mentions[0],
                )
                _modify_match_message.assert_called_once()
        else:
            _add_player_to_team.assert_not_called()
            _modify_match_message.assert_not_called()

    @pytest.mark.parametrize(
        (
            "phase,map_elimination_phase,map_elimination_cycle,picker,"
            "captain,reaction,add_message_to_pug"
        ),
        [
            # 1. case: wrong phase
            (
                "player pick",
                "pick",
                ("pick", "pick"),
                Player(nick="test_nick_1", name="test_name_1"),
                Player(nick="test_nick_1", name="test_name_1"),
                ReactionWithMessage(
                    message=Message(
                        id=1,
                        author=Player(nick="test_bot", name="test_bot"),
                        content="Backlot",
                    ),
                    emoji="\u2705",
                ),
                True,
            ),
            # 2. case: wrong map elimination phase
            (
                "map elimination",
                "veto",
                ("pick", "pick"),
                Player(nick="test_nick_1", name="test_name_1"),
                Player(nick="test_nick_1", name="test_name_1"),
                ReactionWithMessage(
                    message=Message(
                        id=1,
                        author=Player(nick="test_bot", name="test_bot"),
                        content="Backlot",
                    ),
                    emoji="\u2705",
                ),
                True,
            ),
            # 3. case: picker not captain
            (
                "map elimination",
                "pick",
                ("pick", "pick"),
                Player(nick="test_nick_2", name="test_name_2"),
                Player(nick="test_nick_1", name="test_name_1"),
                ReactionWithMessage(
                    message=Message(
                        id=1,
                        author=Player(nick="test_bot", name="test_bot"),
                        content="Backlot",
                    ),
                    emoji="\u2705",
                ),
                True,
            ),
            # 4. case: captain not picker
            (
                "map elimination",
                "pick",
                ("pick", "pick"),
                Player(nick="test_nick_1", name="test_name_1"),
                Player(nick="test_nick_2", name="test_name_2"),
                ReactionWithMessage(
                    message=Message(
                        id=1,
                        author=Player(nick="test_bot", name="test_bot"),
                        content="Backlot",
                    ),
                    emoji="\u2705",
                ),
                True,
            ),
            # 5. case: message author not bot
            (
                "map elimination",
                "pick",
                ("pick", "pick"),
                Player(nick="test_nick_1", name="test_name_1"),
                Player(nick="test_nick_1", name="test_name_1"),
                ReactionWithMessage(
                    message=Message(
                        id=1,
                        author=Player(nick="test_player", name="test_player"),
                        content="Backlot",
                    ),
                    emoji="\u2705",
                ),
                True,
            ),
            # 6. case: map not in map pool
            (
                "map elimination",
                "pick",
                ("pick", "pick"),
                Player(nick="test_nick_1", name="test_name_1"),
                Player(nick="test_nick_1", name="test_name_1"),
                ReactionWithMessage(
                    message=Message(
                        id=1,
                        author=Player(nick="test_player", name="test_player"),
                        content="Ambush",
                    ),
                    emoji="\u2705",
                ),
                True,
            ),
            # 7. case: wrong emoji
            (
                "map elimination",
                "pick",
                ("pick", "pick"),
                Player(nick="test_nick_1", name="test_name_1"),
                Player(nick="test_nick_1", name="test_name_1"),
                ReactionWithMessage(
                    message=Message(
                        id=1,
                        author=Player(nick="test_player", name="test_player"),
                        content="Ambush",
                    ),
                    emoji="\u26d4",
                ),
                True,
            ),
            # 8. case: message not in bot's messages
            (
                "map elimination",
                "pick",
                ("pick", "pick"),
                Player(nick="test_nick_1", name="test_name_1"),
                Player(nick="test_nick_1", name="test_name_1"),
                ReactionWithMessage(
                    message=Message(
                        id=1,
                        author=Player(nick="test_player", name="test_player"),
                        content="Ambush",
                    ),
                    emoji="\u2705",
                ),
                False,
            ),
            (
                "map elimination",
                "pick",
                ("pick", "pick"),
                Player(nick="test_nick_1", name="test_name_1"),
                Player(nick="test_nick_1", name="test_name_1"),
                ReactionWithMessage(
                    message=Message(
                        id=1,
                        author=Player(nick="test_bot", name="test_bot"),
                        content="Backlot",
                    ),
                    emoji="\u2705",
                ),
                True,
            ),
            (
                "map elimination",
                "pick",
                ("pick",),
                Player(nick="test_nick_1", name="test_name_1"),
                Player(nick="test_nick_1", name="test_name_1"),
                ReactionWithMessage(
                    message=Message(
                        id=1,
                        author=Player(nick="test_bot", name="test_bot"),
                        content="Backlot",
                    ),
                    emoji="\u2705",
                ),
                True,
            ),
        ],
    )
    @pytest.mark.asyncio()
    async def test_pick_map(
        self,
        monkeypatch,
        pre_match_pug,
        phase,
        map_elimination_phase,
        map_elimination_cycle,
        picker,
        captain,
        reaction,
        add_message_to_pug,
    ):
        monkeypatch.setattr(
            cod4pugbot.pug.PreMatchPUG,
            "_add_map_to_maps",
            _add_map_to_maps := mock.MagicMock(),
        )
        monkeypatch.setattr(
            cod4pugbot.pug.PreMatchPUG,
            "_start_pug",
            _start_pug := mock.MagicMock(),
        )
        map_pool = ["Backlot", "Crash", "Crossfire", "Citystreets", "Strike", "Vacant"]
        pre_match_pug.map_pool = map_pool.copy()
        captains = [
            Player(nick="test_nick_1", name="test_name_1"),
            Player(nick="test_nick_2", name="test_name_2"),
        ]
        pre_match_pug.attack = {"captain": captains[0]}
        pre_match_pug.defence = {"captain": captains[1]}
        pre_match_pug.picker_cycle = cycle(captains.copy())
        # iterate to test_nick_1 just like PreMatchPUG._start_player_pick_phase does
        next(pre_match_pug.picker_cycle)
        pre_match_pug.picker = picker
        pre_match_pug.map_elimination_cycle = iter(map_elimination_cycle)
        next(pre_match_pug.map_elimination_cycle)
        pre_match_pug.map_elimination_phase = map_elimination_phase
        if add_message_to_pug:
            pre_match_pug.messages["maps"][reaction.message.content] = reaction.message
        pre_match_pug.phase = phase

        await pre_match_pug.pick_map(captain=captain, reaction=reaction)

        if (
            phase == "map elimination"
            and (map_elimination_phase == "pick" or map_elimination_phase == "random")
            and captain in captains
            and captain == picker
            and add_message_to_pug
            and reaction.message.author == Player(nick="test_bot", name="test_bot")
            and reaction.message.content
            and reaction.message.content in map_pool
            and reaction.emoji == "\u2705"
        ):
            assert pre_match_pug.picker == captains[1]

            if pre_match_pug.map_elimination_phase != "random":
                if len(map_elimination_cycle) == 1:
                    _add_map_to_maps.assert_called_once_with(
                        map_to_add=reaction.message.content
                    )
                    _start_pug.assert_called_once()
                elif len(map_pool) - 1 == 1:
                    assert _add_map_to_maps.call_count == 2
                    _add_map_to_maps.assert_called_with(
                        map_to_add=reaction.message.content
                    )
                    _add_map_to_maps.assert_called_with(map_to_add=map_pool[0])
                    _start_pug.assert_called_once()
                else:
                    _add_map_to_maps.assert_called_once_with(
                        map_to_add=reaction.message.content
                    )
            else:
                assert _add_map_to_maps.call_count == 2
                _start_pug.assert_called_once()
        else:
            _add_map_to_maps.assert_not_called()
            _start_pug.assert_not_called()

    @pytest.mark.parametrize(
        (
            "phase,map_elimination_phase,map_elimination_cycle,picker,"
            "captain,reaction,add_message_to_pug"
        ),
        [
            # 1. case: wrong phase
            (
                "player pick",
                "pick",
                ("pick", "pick"),
                Player(nick="test_nick_1", name="test_name_1"),
                Player(nick="test_nick_1", name="test_name_1"),
                ReactionWithMessage(
                    message=Message(
                        id=1,
                        author=Player(nick="test_bot", name="test_bot"),
                        content="Backlot",
                    ),
                    emoji="\u26d4",
                ),
                True,
            ),
            # 2. case: wrong map elimination phase
            (
                "map elimination",
                "pick",
                ("veto", "pick"),
                Player(nick="test_nick_1", name="test_name_1"),
                Player(nick="test_nick_1", name="test_name_1"),
                ReactionWithMessage(
                    message=Message(
                        id=1,
                        author=Player(nick="test_bot", name="test_bot"),
                        content="Backlot",
                    ),
                    emoji="\u26d4",
                ),
                True,
            ),
            # 3. case: picker not captain
            (
                "map elimination",
                "veto",
                ("veto", "pick"),
                Player(nick="test_nick_2", name="test_name_2"),
                Player(nick="test_nick_1", name="test_name_1"),
                ReactionWithMessage(
                    message=Message(
                        id=1,
                        author=Player(nick="test_bot", name="test_bot"),
                        content="Backlot",
                    ),
                    emoji="\u26d4",
                ),
                True,
            ),
            # 4. case: captain not picker
            (
                "map elimination",
                "veto",
                ("veto", "pick"),
                Player(nick="test_nick_1", name="test_name_1"),
                Player(nick="test_nick_2", name="test_name_2"),
                ReactionWithMessage(
                    message=Message(
                        id=1,
                        author=Player(nick="test_bot", name="test_bot"),
                        content="Backlot",
                    ),
                    emoji="\u26d4",
                ),
                True,
            ),
            # 5. case: message author not bot
            (
                "map elimination",
                "veto",
                ("veto", "pick"),
                Player(nick="test_nick_1", name="test_name_1"),
                Player(nick="test_nick_1", name="test_name_1"),
                ReactionWithMessage(
                    message=Message(
                        id=1,
                        author=Player(nick="test_player", name="test_player"),
                        content="Backlot",
                    ),
                    emoji="\u26d4",
                ),
                True,
            ),
            # 6. case: map not in map pool
            (
                "map elimination",
                "veto",
                ("veto", "pick"),
                Player(nick="test_nick_1", name="test_name_1"),
                Player(nick="test_nick_1", name="test_name_1"),
                ReactionWithMessage(
                    message=Message(
                        id=1,
                        author=Player(nick="test_player", name="test_player"),
                        content="Ambush",
                    ),
                    emoji="\u26d4",
                ),
                True,
            ),
            # 7. case: wrong emoji
            (
                "map elimination",
                "veto",
                ("veto", "pick"),
                Player(nick="test_nick_1", name="test_name_1"),
                Player(nick="test_nick_1", name="test_name_1"),
                ReactionWithMessage(
                    message=Message(
                        id=1,
                        author=Player(nick="test_player", name="test_player"),
                        content="Ambush",
                    ),
                    emoji="\u2705",
                ),
                True,
            ),
            # 8. case: message not in bot's messages
            (
                "map elimination",
                "veto",
                ("veto", "pick"),
                Player(nick="test_nick_1", name="test_name_1"),
                Player(nick="test_nick_1", name="test_name_1"),
                ReactionWithMessage(
                    message=Message(
                        id=1,
                        author=Player(nick="test_player", name="test_player"),
                        content="Ambush",
                    ),
                    emoji="\u26d4",
                ),
                False,
            ),
            (
                "map elimination",
                "veto",
                ("veto", "pick"),
                Player(nick="test_nick_1", name="test_name_1"),
                Player(nick="test_nick_1", name="test_name_1"),
                ReactionWithMessage(
                    message=Message(
                        id=1,
                        author=Player(nick="test_bot", name="test_bot"),
                        content="Backlot",
                    ),
                    emoji="\u26d4",
                ),
                True,
            ),
        ],
    )
    @pytest.mark.asyncio()
    async def test_veto_map(
        self,
        monkeypatch,
        pre_match_pug,
        phase,
        map_elimination_phase,
        map_elimination_cycle,
        picker,
        captain,
        reaction,
        add_message_to_pug,
    ):
        monkeypatch.setattr(
            cod4pugbot.pug.PreMatchPUG,
            "_remove_map_from_map_pool",
            _remove_map_from_map_pool := mock.MagicMock(),
        )
        monkeypatch.setattr(
            cod4pugbot.pug.PreMatchPUG,
            "_add_map_to_maps",
            _add_map_to_maps := mock.MagicMock(),
        )
        monkeypatch.setattr(
            cod4pugbot.pug.PreMatchPUG,
            "_start_pug",
            _start_pug := mock.MagicMock(),
        )
        map_pool = ["Backlot", "Crash", "Crossfire", "Citystreets", "Strike", "Vacant"]
        pre_match_pug.map_pool = map_pool.copy()
        captains = [
            Player(nick="test_nick_1", name="test_name_1"),
            Player(nick="test_nick_2", name="test_name_2"),
        ]
        pre_match_pug.attack = {"captain": captains[0]}
        pre_match_pug.defence = {"captain": captains[1]}
        pre_match_pug.picker_cycle = cycle(captains.copy())
        # iterate to test_nick_1 just like PreMatchPUG._start_player_pick_phase does
        next(pre_match_pug.picker_cycle)
        pre_match_pug.picker = picker
        pre_match_pug.map_elimination_cycle = iter(map_elimination_cycle)
        next(pre_match_pug.map_elimination_cycle)
        pre_match_pug.map_elimination_phase = map_elimination_phase
        if add_message_to_pug:
            pre_match_pug.messages["maps"][reaction.message.content] = reaction.message
        pre_match_pug.phase = phase

        await pre_match_pug.veto_map(captain=captain, reaction=reaction)

        if (
            phase == "map elimination"
            and (map_elimination_phase == "veto" or map_elimination_phase == "random")
            and captain in captains
            and captain == picker
            and add_message_to_pug
            and reaction.message.author == Player(nick="test_bot", name="test_bot")
            and reaction.message.content
            and reaction.message.content in map_pool
            and reaction.emoji == "\u26d4"
        ):
            assert pre_match_pug.picker == captains[1]

            if len(map_elimination_cycle) == 1:
                _remove_map_from_map_pool.assert_called_once_with(
                    map_to_remove=reaction.message.content
                )
                _start_pug.assert_called_once()
            elif map_elimination_phase == "random":
                _remove_map_from_map_pool.assert_called_with(
                    map_to_remove=reaction.message.content
                )
                assert _add_map_to_maps.call_count == 2
                _start_pug.assert_called_once()
            elif len(map_pool) - 1 == 1:
                _remove_map_from_map_pool.assert_called_with(
                    map_to_remove=reaction.message.content
                )
                _add_map_to_maps.assert_called_with(map_to_add=map_pool[0])
                assert _add_map_to_maps.call_count == 2
                _start_pug.assert_called_once()
            else:
                _remove_map_from_map_pool.assert_called_once_with(
                    map_to_remove=reaction.message.content
                )
        else:
            _remove_map_from_map_pool.assert_not_called()
            _add_map_to_maps.assert_not_called()
            _start_pug.assert_not_called()
