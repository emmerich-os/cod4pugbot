import asyncio
import dataclasses
import datetime
import itertools
from unittest import mock

import async_timeout as at
import discord
import freezegun
import pytest
import pytz

from cod4pugbot.domain import utils
from cod4pugbot.domain.features.pugs import cod4
from cod4pugbot.domain.features.pugs import pug


async def async_magic():
    return


mock.MagicMock.__await__ = lambda x: async_magic().__await__()

TIME = "2020-01-01 00:00"


class Player:
    def __init__(
        self,
        nick: str = None,
        name: str = None,
        roles: list = None,
        id: int = None,
        has_stats: bool = False,
    ):
        self.nick = nick
        self.name = name
        self.mention = f"@{nick}"
        self.roles = roles or []
        self.id = id
        self.has_stats = has_stats

    def __eq__(self, other):
        """Check if `other.nick` equals `self.nick`."""
        return (
            self.nick == other.nick if isinstance(other, Player) else self.nick == other
        )

    def __hash__(self):
        """Hash nick."""
        return hash(self.nick)

    def __repr__(self):
        """Name when printing."""
        return f"<{self.__class__.__name__} {self.nick=} {self.name=} {self.roles=}>"

    def __str__(self):
        """Name when formatted to string."""
        return f"<{self.__class__.__name__} {self.nick=} {self.name=} {self.roles=}>"


@dataclasses.dataclass(eq=False)
class Stats:
    players: list = None

    def save_stats_to_db(self):
        return


@dataclasses.dataclass(eq=False)
class Server:
    ip: str = None
    port: int = None
    password: str = None
    players: list = None
    attack: list = None
    defence: list = None
    stats: object = None
    current_killfeed: list = None

    @property
    def ip_port(self):
        return f"{self.ip}:{self.port}"

    @property
    def promod_players(self):
        """Return list of Promod players."""
        return (
            self.attack["players"] + self.defence["players"]
            if self.attack is not None and self.defence is not None
            else None
        )

    @staticmethod
    def to_embed():
        return "embed"

    @staticmethod
    def format_scoreboard():
        return

    @property
    def teams(self):
        if self.attack is not None and self.defence is not None:
            return [self.attack, self.defence]

    def refresh(self):
        return


@dataclasses.dataclass(eq=False)
class _TestPUG:
    server: Server = None
    channel: discord.TextChannel = None
    status: str = None


@dataclasses.dataclass(eq=False)
class Event:
    name: str = None

    @property
    def message(self):
        return self.name

    def __eq__(self, other):
        """Return if name is equal to other."""
        return (
            self.name == other.name if isinstance(other, Event) else self.name == other
        )


@dataclasses.dataclass(eq=False)
class Reaction(discord.Reaction):
    message: dict
    emoji: str

    async def remove(self, user):
        return


@dataclasses.dataclass(eq=False)
class Message(discord.Message):
    id: int
    author: Player
    mentions: list = None
    content: str = None

    def __eq__(self, other):
        """Check if self.id is equal to other.id."""
        return self.id == other.id


class TestRunningPUG:
    @pytest.mark.parametrize(
        "pug_id,mode,size,attack,defence,maps,messages",
        [
            (
                1,
                "bo1",
                "1v1",
                {
                    "players": [Player(nick="test_nick_1", name="test_name_1")],
                },
                {"players": [Player(nick="test_nick_2", name="test_name_2")]},
                ["Backlot"],
                {},
            )
        ],
    )
    @pytest.mark.asyncio()
    async def test_create(
        self,
        monkeypatch,
        cog,
        guild,
        pug_id,
        mode,
        size,
        attack,
        defence,
        maps,
        channel,
        messages,
    ):
        monkeypatch.setattr(
            pug.RunningPUG,
            "_modify_match_message",
            _modify_match_message := mock.MagicMock(),
        )
        monkeypatch.setattr(
            pug.RunningPUG,
            "_post_score_report_message",
            _post_score_report_message := mock.MagicMock(),
        )
        monkeypatch.setattr(
            pug.RunningPUG,
            "_modify_scoreboard_message",
            _modify_scoreboard_message := mock.MagicMock(),
        )
        monkeypatch.setattr(
            pug.RunningPUG,
            "_modify_killfeed_message",
            _modify_killfeed_message := mock.MagicMock(),
        )

        with mock.patch("pugs.running_pug.SQLClient"), freezegun.freeze_time(TIME):
            result = await pug.RunningPUG.create(
                cog=cog,
                guild=guild,
                pug_id=pug_id,
                mode=mode,
                size=size,
                attack=attack,
                defence=defence,
                maps=maps,
                channel=channel,
                messages=messages,
            )

        assert isinstance(result, pug.RunningPUG)
        assert result.cog.active_pugs.pugs[1] == result
        assert result.max_time == datetime.datetime(
            2020, 1, 1, 0, 45, tzinfo=pytz.timezone("UTC")
        )
        _modify_match_message.assert_called_once_with(init=True)
        _post_score_report_message.assert_called_once()
        _modify_scoreboard_message.assert_called_once_with(init=True)
        _modify_killfeed_message.assert_called_once_with(init=True)

    @pytest.mark.parametrize(
        "init,attack,defence,maps,server,expected",
        [
            # 1. case: 1 player per team, 1 map, no server
            (
                False,
                [Player(nick="test_nick_1", name="test_name_1")],
                [Player(nick="test_nick_2", name="test_name_2")],
                ["Backlot"],
                None,
                (
                    """\n"""
                    """\t\t**MATCH STARTED**\n\n"""
                    """> att_icon **-** @test_nick_1\n\n"""
                    """\t\t**VERSUS**\n\n"""
                    """> def_icon **-** @test_nick_2\n\n"""
                    """Map: Backlot\n\n"""
                    """You may now connect to the server.\n"""
                    """Use `!server connect <IP>; password <password>` to add a server.\n"""
                    """If you include the rcon (`!server connect <IP>; """
                    """password <password>; rcon <rcon password>`) and the server has Promod """
                    """scorebot and PunkBuster enabled, stats will be tracked.\n"""
                    """Your message will be deleted immediately to prevent others from stealing """
                    """the rcon password.\n"""
                ),
            ),
            # 2. case: 2 players per team, 1 map, no server
            (
                False,
                [
                    Player(nick="test_nick_1", name="test_name_1"),
                    Player(nick="test_nick_3", name="test_name_3"),
                ],
                [
                    Player(nick="test_nick_2", name="test_name_2"),
                    Player(nick="test_nick_4", name="test_name_4"),
                ],
                ["Backlot"],
                None,
                (
                    """\n"""
                    """\t\t**MATCH STARTED**\n\n"""
                    """> att_icon **-** @test_nick_1, @test_nick_3\n\n"""
                    """\t\t**VERSUS**\n\n"""
                    """> def_icon **-** @test_nick_2, @test_nick_4\n\n"""
                    """Map: Backlot\n\n"""
                    """You may now connect to the server.\n"""
                    """Use `!server connect <IP>; password <password>` to add a server.\n"""
                    """If you include the rcon (`!server connect <IP>; """
                    """password <password>; rcon <rcon password>`) and the server has Promod """
                    """scorebot and PunkBuster enabled, stats will be tracked.\n"""
                    """Your message will be deleted immediately to prevent others from stealing """
                    """the rcon password.\n"""
                ),
            ),
            # 3. case: 1 player per team, 2 maps, no server
            (
                False,
                [Player(nick="test_nick_1", name="test_name_1")],
                [Player(nick="test_nick_2", name="test_name_2")],
                ["Backlot", "Crash"],
                None,
                (
                    """\n"""
                    """\t\t**MATCH STARTED**\n\n"""
                    """> att_icon **-** @test_nick_1\n\n"""
                    """\t\t**VERSUS**\n\n"""
                    """> def_icon **-** @test_nick_2\n\n"""
                    """Maps (in order): Backlot, Crash\n\n"""
                    """You may now connect to the server.\n"""
                    """Use `!server connect <IP>; password <password>` to add a server.\n"""
                    """If you include the rcon (`!server connect <IP>; """
                    """password <password>; rcon <rcon password>`) and the server has Promod """
                    """scorebot and PunkBuster enabled, stats will be tracked.\n"""
                    """Your message will be deleted immediately to prevent others from stealing """
                    """the rcon password.\n"""
                ),
            ),
            # 4. case: 1 player per team, 1 map, server without password
            (
                False,
                [Player(nick="test_nick_1", name="test_name_1")],
                [Player(nick="test_nick_2", name="test_name_2")],
                ["Backlot"],
                Server(ip="1.1.1.1", port=11111),
                (
                    """\n"""
                    """\t\t**MATCH STARTED**\n\n"""
                    """> att_icon **-** @test_nick_1\n\n"""
                    """\t\t**VERSUS**\n\n"""
                    """> def_icon **-** @test_nick_2\n\n"""
                    """Map: Backlot\n\n"""
                    """You may now connect to the server.\n"""
                    """\nServer: ```connect 1.1.1.1:11111```"""
                ),
            ),
            # 5. case: 1 player per team, 1 map, server with password
            (
                False,
                [Player(nick="test_nick_1", name="test_name_1")],
                [Player(nick="test_nick_2", name="test_name_2")],
                ["Backlot"],
                Server(ip="1.1.1.1", port=11111, password="test_pw"),
                (
                    """\n"""
                    """\t\t**MATCH STARTED**\n\n"""
                    """> att_icon **-** @test_nick_1\n\n"""
                    """\t\t**VERSUS**\n\n"""
                    """> def_icon **-** @test_nick_2\n\n"""
                    """Map: Backlot\n\n"""
                    """You may now connect to the server.\n"""
                    """\nServer: ```connect 1.1.1.1:11111; password test_pw```"""
                ),
            ),
            # 6. case: init is True
            (
                True,
                [Player(nick="test_nick_1", name="test_name_1")],
                [Player(nick="test_nick_2", name="test_name_2")],
                ["Backlot"],
                None,
                (
                    """\n"""
                    """\t\t**MATCH STARTED**\n\n"""
                    """> att_icon **-** @test_nick_1\n\n"""
                    """\t\t**VERSUS**\n\n"""
                    """> def_icon **-** @test_nick_2\n\n"""
                    """Map: Backlot\n\n"""
                    """You may now connect to the server.\n"""
                    """Use `!server connect <IP>; password <password>` to add a server.\n"""
                    """If you include the rcon (`!server connect <IP>; """
                    """password <password>; rcon <rcon password>`) and the server has Promod """
                    """scorebot and PunkBuster enabled, stats will be tracked.\n"""
                    """Your message will be deleted immediately to prevent others from stealing """
                    """the rcon password.\n"""
                ),
            ),
        ],
    )
    @pytest.mark.asyncio()
    async def test_modify_match_message(
        self,
        monkeypatch,
        running_pug,
        init,
        attack,
        defence,
        maps,
        server,
        message,
        expected,
    ):
        monkeypatch.setattr(
            discord.TextChannel,
            "set_permissions",
            set_permissions := mock.MagicMock(),
        )
        running_pug.attack = {"icon": "att_icon", "players": attack.copy()}
        running_pug.defence = {"icon": "def_icon", "players": defence.copy()}
        running_pug.maps = maps.copy()
        running_pug.server = server
        running_pug.messages = {"match": message}

        await running_pug._modify_match_message(init=init)
        result = running_pug.messages["match"].content

        assert result == expected
        if server is not None:
            assert running_pug.messages["match"].embed
        if init:
            set_permissions.call_count == len(attack) + len(defence)  # noqa: B015

    @pytest.mark.parametrize(
        "init,server,expected",
        [
            # 1. case: init message
            (
                True,
                None,
                (
                    """The scoreboard will be posted once a server was added """
                    """and players joined the server."""
                ),
            ),
            # 2. case: server has players
            (False, Server(players=[]), """> test_scoreboard\n> test_scores"""),
            # 3. case: server has teams
            (
                False,
                Server(attack={"players": []}, defence={"players": []}),
                """> test_scoreboard\n> test_scores""",
            ),
        ],
    )
    @pytest.mark.asyncio()
    async def test_modify_scoreboard_message(
        self, monkeypatch, running_pug, init, server, message, expected
    ):
        monkeypatch.setattr(
            Server,
            "format_scoreboard",
            format_scoreboard := mock.MagicMock(
                return_value=["test_scoreboard", "test_scores"]
            ),
        )
        running_pug.server = server
        running_pug.messages["scoreboard"] = message

        await running_pug._modify_scoreboard_message(init=init)
        result = running_pug.messages["scoreboard"].content

        assert result == expected
        if server is not None:
            format_scoreboard.assert_called_with(to_message=True)

    @pytest.mark.parametrize(
        "init,killfeed,expected",
        [
            # 1. case: init message
            (
                True,
                None,
                """If the server has the Promod scorebot enabled, the killfeed will be posted.""",
            ),
            # 2. case: one killfeed message
            (False, [Event("test_kill_1")], "> test_kill_1"),
            # 3. case: multiple killfeed messages
            (
                False,
                [Event("test_kill_1"), Event("test_kill_2")],
                "> test_kill_1\n> test_kill_2",
            ),
        ],
    )
    @pytest.mark.asyncio()
    async def test_modify_killfeed_message(
        self, running_pug, init, killfeed, message, expected
    ):
        running_pug.killfeed = killfeed
        running_pug.messages["killfeed"] = message

        await running_pug._modify_killfeed_message(init=init)
        result = running_pug.messages["killfeed"].content

        assert result == expected

    @pytest.mark.parametrize(
        "mode, expected",
        [
            (
                "bo1",
                (
                    """> @test_nick_1, @test_nick_2:\n"""
                    """> React with \u26d4 to this message after the """
                    """ match to report your loss.\n"""
                    """> React with \U0001F538 to report a draw."""
                ),
            ),
            (
                "bo2",
                (
                    """> @test_nick_1, @test_nick_2:\n"""
                    """> React with \u26d4 to this message after the """
                    """ match to report your loss.\n"""
                    """> React with \U0001F538 to report a draw."""
                ),
            ),
            (
                "bo3",
                (
                    """> @test_nick_1, @test_nick_2:\n"""
                    """> React with \u26d4 to this message after the match to report your loss."""
                ),
            ),
            (
                "bo5",
                (
                    """> @test_nick_1, @test_nick_2:\n"""
                    """> React with \u26d4 to this message after the match to report your loss."""
                ),
            ),
        ],
    )
    @pytest.mark.asyncio()
    async def test_post_score_report_message(self, running_pug, mode, expected):
        running_pug.attack = {"captain": Player(nick="test_nick_1", name="test_name_1")}
        running_pug.defence = {
            "captain": Player(nick="test_nick_2", name="test_name_2")
        }
        running_pug.mode = mode

        await running_pug._post_score_report_message()
        result = running_pug.messages["score report"].content

        assert result == expected
        if mode == "bo1" or mode == "bo2":
            assert running_pug.messages["score report"].emojis == [
                "\u26d4",
                "\U0001F538",
            ]
        else:
            assert running_pug.messages["score report"].emojis == ["\u26d4"]

    @pytest.mark.parametrize(
        "attack,defence,attack_stats,defence_stats,expected_attack,expected_defence",
        [
            (
                [Player(has_stats=False)],
                [Player(has_stats=False)],
                [None],
                [None],
                1000,
                1000,
            ),
            (
                [Player(has_stats=True)],
                [Player(has_stats=False)],
                [{"pugs_played": 0, "elo": 1100}],
                [None],
                1100,
                1000,
            ),
            (
                [Player(has_stats=False)],
                [Player(has_stats=True)],
                [None],
                [{"pugs_played": 0, "elo": 1100}],
                1000,
                1100,
            ),
            (
                [Player(has_stats=True)],
                [Player(has_stats=True)],
                [{"pugs_played": 0, "elo": 1000}],
                [{"pugs_played": 0, "elo": 1000}],
                1000,
                1000,
            ),
            (
                [Player(has_stats=True)],
                [Player(has_stats=True)],
                [{"pugs_played": 0, "elo": 1100}],
                [{"pugs_played": 0, "elo": 1000}],
                1100,
                1000,
            ),
            (
                [Player(has_stats=True)],
                [Player(has_stats=True)],
                [{"pugs_played": 0, "elo": 1000}],
                [{"pugs_played": 0, "elo": 1100}],
                1000,
                1100,
            ),
            (
                [Player(has_stats=True)],
                [Player(has_stats=True)],
                [{"pugs_played": 0, "elo": 1100}],
                [{"pugs_played": 0, "elo": 1100}],
                1100,
                1100,
            ),
            (
                [Player(has_stats=True), Player(has_stats=True)],
                [Player(has_stats=True), Player(has_stats=True)],
                [{"pugs_played": 0, "elo": 1100}, {"pugs_played": 0, "elo": 1300}],
                [{"pugs_played": 0, "elo": 1200}, {"pugs_played": 0, "elo": 1400}],
                1200,
                1300,
            ),
        ],
    )
    def test_calculate_team_elo(
        self,
        monkeypatch,
        running_pug,
        attack,
        defence,
        attack_stats,
        defence_stats,
        expected_attack,
        expected_defence,
    ):
        stats_cycle = itertools.cycle(attack_stats + defence_stats)

        def cycle_stats(*args, **kwargs):
            for _ in attack_stats + defence_stats:
                return next(stats_cycle)

        monkeypatch.setattr(
            utils,
            "get_kwargs",
            mock.MagicMock(),
        )
        monkeypatch.setattr(
            pug.running_pug.SQLClient,
            "__init__",
            mock.MagicMock(return_value=None),
        )
        monkeypatch.setattr(
            pug.running_pug.SQLClient,
            "insert",
            insert := mock.MagicMock(),
        )
        monkeypatch.setattr(
            pug.running_pug.SQLClient,
            "select",
            select := mock.MagicMock(side_effect=cycle_stats),
        )
        monkeypatch.setattr(
            pug.running_pug.SQLClient,
            "update",
            update := mock.MagicMock(),
        )
        running_pug.attack = {"players": attack.copy()}
        running_pug.defence = {"players": defence.copy()}

        running_pug._calculate_team_elo()

        insert_call_count_expected = 0
        update_call_count_expected = 0
        for player in attack + defence:
            if not player.has_stats:
                insert_call_count_expected += 1
            else:
                update_call_count_expected += 1

        assert running_pug.attack["elo"] == expected_attack
        assert running_pug.defence["elo"] == expected_defence
        assert insert.call_count == insert_call_count_expected
        assert update.call_count == update_call_count_expected
        assert select.call_count == len(attack) + len(defence)

    @pytest.mark.parametrize(
        "team1_elo,team2_elo,expected",
        [
            (1000, 1000, (16, -16, 0)),
            (1000, 1100, (20, -12, 10)),
            (1000, 1200, (24, -8, 12)),
            (1200, 1200, (16, -16, 0)),
            (1100, 1000, (12, -20, -10)),
            (1200, 1000, (8, -24, -12)),
        ],
    )
    def test_calculate_elo_change(self, running_pug, team1_elo, team2_elo, expected):
        running_pug.elo_k = 32

        result = running_pug._calculate_elo_change(
            team1={"elo": team1_elo}, team2={"elo": team2_elo}
        )

        assert result == expected

    @pytest.mark.parametrize(
        "attack,defence",
        [
            (
                {
                    "captain": "test_captain_1",
                    "elo": 0,
                    "result": "win",
                    "players": [Player(id=1)],
                },
                {
                    "captain": "test_captain_2",
                    "elo": 0,
                    "result": "loss",
                    "players": [Player(id=2)],
                },
            ),
            (
                {
                    "captain": "test_captain_1",
                    "elo": 0,
                    "result": "loss",
                    "players": [Player(id=1)],
                },
                {
                    "captain": "test_captain_2",
                    "elo": 0,
                    "result": "win",
                    "players": [Player(id=2)],
                },
            ),
            (
                {
                    "captain": "test_captain_1",
                    "elo": 0,
                    "result": "draw",
                    "players": [Player(id=1)],
                },
                {
                    "captain": "test_captain_2",
                    "elo": 0,
                    "result": "draw",
                    "players": [Player(id=2)],
                },
            ),
        ],
    )
    def test_calculate_elo(self, monkeypatch, running_pug, attack, defence):
        stats = iter([{"wins": 0, "losses": 0, "draws": 0, "elo": 0} for _ in range(2)])

        def cycle_stats(*args, **kwargs):
            return next(stats)

        monkeypatch.setattr(
            pug.running_pug.SQLClient,
            "__init__",
            mock.MagicMock(return_value=None),
        )
        monkeypatch.setattr(
            pug.running_pug.SQLClient,
            "select",
            select := mock.MagicMock(side_effect=cycle_stats),
        )
        monkeypatch.setattr(
            pug.RunningPUG,
            "_calculate_elo_change",
            _calculate_elo_change := mock.MagicMock(return_value=(2, -3, 1)),
        )
        monkeypatch.setattr(
            pug.RunningPUG,
            "_calculate_team_elo",
            _calculate_team_elo := mock.MagicMock(),
        )
        running_pug.attack = attack
        running_pug.defence = defence

        running_pug._calculate_elo()

        _calculate_team_elo.assert_called_once()
        assert _calculate_elo_change.call_count == 2
        assert select.call_count == len(attack["players"] + defence["players"])
        for i, stat in enumerate(stats):
            if i < len(attack["players"]):
                if attack["result"] == "win":
                    assert stat["wins"] == 1
                    assert stat["elo"] == 2
                elif attack["result"] == "loss":
                    assert stat["loss"] == 1
                    assert stat["elo"] == -3
                elif attack["result"] == "draw":
                    assert stat["draw"] == 1
                    assert stat["elo"] == 1
            else:
                if defence["result"] == "win":
                    assert stat["wins"] == 1
                    assert stat["elo"] == 2
                elif defence["result"] == "loss":
                    assert stat["loss"] == 1
                    assert stat["elo"] == -3
                elif defence["result"] == "draw":
                    assert stat["draw"] == 1
                    assert stat["elo"] == 1

    @pytest.mark.parametrize(
        "server,captain,team,reported_by_scorebot,delete_delay,expected",
        [
            (
                None,
                None,
                None,
                False,
                60,
                (
                    """> PUG has expired.\n"""
                    """> No result have been reported.\n"""
                    """> This channel will be deleted in 60 seconds."""
                ),
            ),
            (
                None,
                None,
                None,
                False,
                1,
                (
                    """> PUG has expired.\n"""
                    """> No result have been reported.\n"""
                    """> This channel will be deleted in 1 second."""
                ),
            ),
            (
                None,
                Player(nick="test_nick", name="test_name"),
                {"result": "win"},
                False,
                60,
                (
                    """> PUG has finished.\n"""
                    """> @test_nick has reported a win.\n"""
                    """> This channel will be deleted in 60 seconds."""
                ),
            ),
            (
                Server(stats=Stats(players=[])),
                Player(nick="test_nick", name="test_name"),
                {"result": "win"},
                False,
                60,
                (
                    """> PUG has finished.\n"""
                    """> @test_nick has reported a win.\n"""
                    """> This channel will be deleted in 60 seconds."""
                ),
            ),
            (
                Server(stats=Stats(players=["test_player"])),
                Player(nick="test_nick", name="test_name"),
                {"result": "win"},
                False,
                60,
                (
                    """> PUG has finished.\n"""
                    """> @test_nick has reported a win.\n"""
                    """> This channel will be deleted in 60 seconds."""
                ),
            ),
            (
                None,
                None,
                None,
                True,
                60,
                (
                    """> PUG has finished.\n"""
                    """> Promod scorebot has reported a win for Attack and a loss for Defence.\n"""
                    """> This channel will be deleted in 60 seconds."""
                ),
            ),
        ],
    )
    @pytest.mark.asyncio()
    async def test_end_pug(
        self,
        monkeypatch,
        running_pug,
        message,
        server,
        captain,
        team,
        reported_by_scorebot,
        delete_delay,
        expected,
    ):
        monkeypatch.setattr(
            running_pug.cog.active_pugs,
            "remove_pug",
            remove_pug := mock.MagicMock(),
        )
        monkeypatch.setattr(
            pug.RunningPUG,
            "_calculate_elo",
            _calculate_elo := mock.MagicMock(),
        )
        monkeypatch.setattr(
            Stats,
            "save_stats_to_db",
            save_stats_to_db := mock.MagicMock(),
        )
        monkeypatch.setattr(
            running_pug.channel,
            "send",
            send := mock.MagicMock(),
        )
        monkeypatch.setattr(
            asyncio,
            "sleep",
            sleep := mock.MagicMock(),
        )
        monkeypatch.setattr(
            running_pug.channel,
            "delete",
            delete := mock.MagicMock(),
        )
        running_pug.delete_channel_delay = delete_delay
        running_pug.messages["score report"] = message
        running_pug.attack = {"result": "win"}
        running_pug.defence = {"result": "loss"}
        running_pug.server = server

        await running_pug.end_pug(
            captain=captain, team=team, reported_by_scorebot=reported_by_scorebot
        )

        remove_pug.assert_called_once_with(pug_id=running_pug.id)
        if reported_by_scorebot:
            _calculate_elo.assert_called_once()
        elif captain is not None:
            _calculate_elo.assert_called_once()
            if server is not None and server.stats.players:
                save_stats_to_db.assert_called_once()
            else:
                save_stats_to_db.assert_not_called()
        else:
            _calculate_elo.assert_not_called()
        send.assert_called_once_with(expected)
        sleep.assert_called_once_with(delete_delay)
        delete.assert_called_once()

    @pytest.mark.parametrize(
        "delete_delay,expected",
        [
            (
                60,
                """> PUG was aborted.\n> This channel will be deleted in 60 seconds.""",
            ),
            (
                1,
                """> PUG was aborted.\n> This channel will be deleted in 1 second.""",
            ),
        ],
    )
    @pytest.mark.asyncio()
    async def test_abort_pug(self, monkeypatch, running_pug, delete_delay, expected):
        monkeypatch.setattr(
            running_pug.cog.active_pugs,
            "remove_pug",
            remove_pug := mock.MagicMock(),
        )
        monkeypatch.setattr(
            pug.running_pug.SQLClient,
            "__init__",
            mock.MagicMock(return_value=None),
        )
        monkeypatch.setattr(
            pug.running_pug.SQLClient,
            "delete",
            delete_stats := mock.MagicMock(),
        )
        monkeypatch.setattr(
            running_pug.channel,
            "send",
            send := mock.MagicMock(),
        )
        monkeypatch.setattr(
            asyncio,
            "sleep",
            sleep := mock.MagicMock(),
        )
        monkeypatch.setattr(
            running_pug.channel,
            "delete",
            delete_channel := mock.MagicMock(),
        )
        running_pug.delete_channel_delay = delete_delay

        await running_pug.abort_pug()

        remove_pug.assert_called_once_with(pug_id=running_pug.id)
        delete_stats.assert_called_once_with(table="pugs", pug_id=running_pug.id)
        send.assert_called_once_with(expected)
        sleep.assert_called_once_with(delete_delay)
        delete_channel.assert_called_once()

    @pytest.mark.parametrize(
        "content,ip,port,password,rcon,add_pug,expected",
        [
            (
                "connect 1.1.1:1",
                "1.1.1",
                1,
                None,
                None,
                False,
                (
                    "Sorry @test_nick, "
                    "the connect command is invalid! I could not find any IP and port in there."
                ),
            ),
            (
                "connect 1.1.1.1:1",
                "1.1.1.1",
                1,
                None,
                None,
                True,
                """Sorry @test_nick, this server is currently in use by PUG #1.""",
            ),
            (
                "1.1.1.1:1",
                "1.1.1.1",
                1,
                None,
                None,
                False,
                None,
            ),
            (
                "111.111.111.111:11111",
                "111.111.111.111",
                11111,
                None,
                None,
                False,
                None,
            ),
            (
                "connect 1.1.1.1:1",
                "1.1.1.1",
                1,
                None,
                None,
                False,
                None,
            ),
            (
                "connect 111.111.111.111:11111",
                "111.111.111.111",
                11111,
                None,
                None,
                False,
                None,
            ),
            (
                "connect 1.1.1.1:1; password test_pw",
                "1.1.1.1",
                1,
                "test_pw",
                None,
                False,
                None,
            ),
            (
                "connect 1.1.1.1:1;   password    test_pw",
                "1.1.1.1",
                1,
                "test_pw",
                None,
                False,
                None,
            ),
            (
                "connect 1.1.1.1:1; rcon test_rcon",
                "1.1.1.1",
                1,
                None,
                "test_rcon",
                False,
                None,
            ),
            (
                "connect 1.1.1.1:1;   rcon   test_rcon",
                "1.1.1.1",
                1,
                None,
                "test_rcon",
                False,
                None,
            ),
            (
                "connect 1.1.1.1:1; password test_pw; rcon test_rcon",
                "1.1.1.1",
                1,
                "test_pw",
                "test_rcon",
                False,
                None,
            ),
            (
                "connect 1.1.1.1:1; password test_pw;   rcon   test_rcon",
                "1.1.1.1",
                1,
                "test_pw",
                "test_rcon",
                False,
                None,
            ),
        ],
    )
    @pytest.mark.asyncio()
    async def test_add_server(
        self,
        monkeypatch,
        running_pug,
        channel,
        ctx,
        content,
        ip,
        port,
        password,
        rcon,
        add_pug,
        expected,
    ):
        monkeypatch.setattr(
            ctx.channel,
            "send",
            send := mock.MagicMock(),
        )
        monkeypatch.setattr(
            cod4.Server,
            "connect",
            connect := mock.MagicMock(),
        )
        monkeypatch.setattr(
            pug.RunningPUG,
            "_modify_match_message",
            _modify_match_message := mock.MagicMock(),
        )
        ctx.message.author = Player(nick="test_nick", name="test_name")
        monkeypatch.setattr(
            discord.TextChannel,
            "mention",
            "#1",
        )
        if add_pug:
            test_pug = _TestPUG(
                server=Server(ip="1.1.1.1", port=1), channel=channel, status="running"
            )
            running_pug.cog.active_pugs.pugs = {"test_pug": test_pug}
        else:
            running_pug.cog.active_pugs.pugs = {}

        await running_pug.add_server(ctx=ctx, content=content)

        if expected is None:
            connect.assert_called_once_with(
                ip=ip, port=port, password=password, rcon_password=rcon, pug=running_pug
            )
            _modify_match_message.assert_called_once()
        else:
            send.assert_called_once_with(expected)
            connect.assert_not_called()
            _modify_match_message.assert_not_called()

    @pytest.mark.parametrize(
        "server,expected",
        [
            (
                None,
                None,
            ),
            (
                Server(),
                None,
            ),
            (
                Server(players=[]),
                None,
            ),
            (
                Server(players=[1]),
                None,
            ),
            (
                Server(attack={"players": [1]}, defence={"players": [2]}),
                None,
            ),
            (
                Server(current_killfeed=[Event(name="test_event_1")]),
                [Event(name="test_event_1")],
            ),
            (
                Server(
                    current_killfeed=[
                        Event(name="test_event_1"),
                        Event(name="1st_half_ready_up"),
                        Event(name="test_event_2"),
                    ]
                ),
                [Event(name="1st_half_ready_up"), Event(name="test_event_2")],
            ),
            (
                Server(
                    current_killfeed=[
                        Event(name="test_event_1"),
                        Event(name="2nd_half_ready_up"),
                        Event(name="test_event_2"),
                    ]
                ),
                [Event(name="2nd_half_ready_up"), Event(name="test_event_2")],
            ),
            (
                Server(
                    current_killfeed=[
                        Event(name="test_event_1"),
                        Event(name="timeout_ready_up"),
                        Event(name="test_event_2"),
                    ]
                ),
                [Event(name="timeout_ready_up"), Event(name="test_event_2")],
            ),
            (
                Server(
                    current_killfeed=[
                        Event(name="test_event_1"),
                        Event(name="1st_half_started"),
                        Event(name="test_event_2"),
                    ]
                ),
                [Event(name="1st_half_started"), Event(name="test_event_2")],
            ),
            (
                Server(
                    current_killfeed=[
                        Event(name="test_event_1"),
                        Event(name="2nd_half_started"),
                        Event(name="test_event_2"),
                    ]
                ),
                [Event(name="2nd_half_started"), Event(name="test_event_2")],
            ),
            (
                Server(
                    current_killfeed=[
                        Event(name="test_event_1"),
                        Event(name="match_resumed"),
                        Event(name="test_event_2"),
                    ]
                ),
                [Event(name="match_resumed"), Event(name="test_event_2")],
            ),
            (
                Server(
                    current_killfeed=[
                        Event(name="test_event_1"),
                        Event(name="round_start"),
                        Event(name="test_event_2"),
                    ]
                ),
                [Event(name="round_start"), Event(name="test_event_2")],
            ),
            (
                Server(
                    current_killfeed=[
                        Event(name="test_event_1"),
                        Event(name="knife_round"),
                        Event(name="test_event_2"),
                    ]
                ),
                [Event(name="knife_round"), Event(name="test_event_2")],
            ),
        ],
    )
    @pytest.mark.asyncio()
    async def test_refresh_server(self, monkeypatch, running_pug, server, expected):
        monkeypatch.setattr(at, "timeout", mock.MagicMock())
        monkeypatch.setattr(
            asyncio,
            "sleep",
            mock.MagicMock(),
        )
        monkeypatch.setattr(
            Server,
            "refresh",
            refresh := mock.MagicMock(),
        )
        monkeypatch.setattr(
            pug.RunningPUG,
            "_modify_scoreboard_message",
            _modify_scoreboard_message := mock.MagicMock(),
        )
        monkeypatch.setattr(
            pug.RunningPUG,
            "_modify_killfeed_message",
            _modify_killfeed_message := mock.MagicMock(),
        )
        monkeypatch.setattr(
            pug.RunningPUG,
            "_modify_match_message",
            _modify_match_message := mock.MagicMock(),
        )
        running_pug.killfeed = []
        running_pug.server = server

        await running_pug.refresh_server()
        if server is not None:
            refresh.assert_called_once()
            _modify_match_message.assert_called_once()
            if server.players is not None or server.teams is not None:
                _modify_scoreboard_message.assert_called_once()
            else:
                _modify_scoreboard_message.assert_not_called()
            if server.current_killfeed:
                assert _modify_killfeed_message.call_count == len(
                    server.current_killfeed
                )
                assert running_pug.killfeed == expected
            else:
                _modify_killfeed_message.assert_not_called()
        else:
            refresh.assert_not_called()
            _modify_match_message.assert_not_called()

    @pytest.mark.parametrize(
        "status,captain,reaction,add_message_to_pug,expected",
        [
            # 1. case: loss reported
            (
                "running",
                Player(nick="test_nick_1"),
                Reaction(
                    message=Message(id=6, author=Player(nick="test_bot")),
                    emoji="\u26d4",
                ),
                True,
                "loss",
            ),
            # 2. case: draw reported
            (
                "running",
                Player(nick="test_nick_1"),
                Reaction(
                    message=Message(id=6, author=Player(nick="test_bot")),
                    emoji="\U0001F538",
                ),
                True,
                "draw",
            ),
            # 3. case: wrong status
            (
                "open",
                Player(nick="test_nick_1"),
                Reaction(
                    message=Message(id=6, author=Player(nick="test_bot")),
                    emoji="\u26d4",
                ),
                True,
                "draw",
            ),
            # 4. case: captain not in captains
            (
                "running",
                Player(nick="test_nick_3"),
                Reaction(
                    message=Message(id=6, author=Player(nick="test_bot")),
                    emoji="\u26d4",
                ),
                True,
                "draw",
            ),
            # 5. case: reaction.message not in pugs.messages
            (
                "running",
                Player(nick="test_nick_1"),
                Reaction(
                    message=Message(id=6, author=Player(nick="test_bot")),
                    emoji="\u26d4",
                ),
                False,
                "draw",
            ),
            # 6. case: wrong message id
            (
                "running",
                Player(nick="test_nick_1"),
                Reaction(
                    message=Message(id=7, author=Player(nick="test_bot")),
                    emoji="\u26d4",
                ),
                True,
                "draw",
            ),
            # 7. case: message author not bot
            (
                "running",
                Player(nick="test_nick_1"),
                Reaction(
                    message=Message(id=6, author=Player(nick="test_player")),
                    emoji="\u26d4",
                ),
                True,
                "draw",
            ),
            # 8. case: wrong emoji
            (
                "running",
                Player(nick="test_nick_1"),
                Reaction(
                    message=Message(id=6, author=Player(nick="test_bot")),
                    emoji="\u2705",
                ),
                True,
                "draw",
            ),
        ],
    )
    @pytest.mark.asyncio()
    async def test_report_score(
        self,
        monkeypatch,
        running_pug,
        message,
        status,
        captain,
        reaction,
        add_message_to_pug,
        expected,
    ):
        monkeypatch.setattr(
            Reaction,
            "remove",
            remove := mock.MagicMock(),
        )
        monkeypatch.setattr(
            pug.RunningPUG,
            "end_pug",
            end_pug := mock.MagicMock(),
        )
        attack = {
            "captain": Player(nick="test_nick_1"),
            "players": [Player(nick="test_nick_1")],
            "result": expected,
        }
        defence = {
            "captain": Player(nick="test_nick_2"),
            "players": [Player(nick="test_nick_2")],
            "result": expected,
        }
        running_pug.attack = attack.copy()
        running_pug.defence = defence.copy()
        running_pug.status = status
        if add_message_to_pug:
            message.author = Player(nick="test_bot")
            running_pug.messages["score report"] = message

        await running_pug.report_score(captain=captain, reaction=reaction)

        if (
            (
                captain == running_pug.attack["captain"]
                or captain == running_pug.defence["captain"]
            )
            and status == "running"
            and add_message_to_pug
            and message.id == reaction.message.id
            and message.author == reaction.message.author
            and (reaction.emoji == "\u26d4" or reaction.emoji == "\U0001F538")
        ):
            remove.assert_not_called()
            end_pug.assert_called_once_with(
                captain=captain,
                team=attack if captain == running_pug.attack["captain"] else defence,
            )
        else:
            remove.assert_called_once()
            end_pug.assert_not_called()
