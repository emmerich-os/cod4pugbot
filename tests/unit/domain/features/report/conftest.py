import dataclasses

import discord
import pytest
from discord.ext import commands

import cod4pugbot.domain.cogs


class Message(discord.Message):
    def __init__(self, content=None, embed=None, channel=None, *args, **kwargs):
        self.id = 6
        self.channel = channel
        self.embed = embed
        self.embeds = [embed] if embed is not None else []
        self.content = content
        self.args = [*args]
        self.kwargs = {**kwargs}
        self.emojis = []

    def __repr__(self):
        """Name when printing."""
        return (
            f"<{self.__class__.__name__} {self.id=} {self.embed=} {self.embeds=} {self.content=} "
            f"{self.args=} {self.kwargs=}>"
        )

    def __str__(self):
        """Name when formatted to string."""
        return (
            f"<{self.__class__.__name__} {self.id=} {self.embed=} {self.embeds=} {self.content=} "
            f"{self.args=} {self.kwargs=}>"
        )

    async def add_reaction(self, emoji):
        self.emojis.append(emoji)
        return emoji

    async def remove_reaction(self, emoji, member):
        self.emojis.remove(emoji)
        return emoji, member

    async def pin(self):
        return

    async def delete(self, *args, **kwargs):
        return

    async def edit(self, content=None, embed=None, *args, **kwargs):
        self.embed = embed
        self.embeds = [embed]
        self.content = content
        self.args = [*self.args, *args]
        self.kwargs = self.kwargs | kwargs


class History:
    async def __aiter__(self):
        """Return a message for `async for ... in discord.Channel.history()`."""
        yield Message(embed=None)


@dataclasses.dataclass(eq=False)
class Emoji(discord.Emoji):
    id: int = None
    name = "smile"
    animated = "smile"


@dataclasses.dataclass(eq=False)
class Guild(discord.Guild):
    id = "test_guild_id"

    def get_role(self, role_id):
        return role_id

    async def fetch_emoji(self, *args, **kwargs):
        return Emoji(*args, **kwargs)


@dataclasses.dataclass(eq=False)
class Channel(discord.TextChannel):
    id = 5
    args: list = None
    kwargs: dict = None
    history = History
    sent_messages: list = None
    guild: Guild = Guild()

    async def send(self, *args, **kwargs):
        message = Message(*args, **kwargs)
        if self.sent_messages is None:
            self.sent_messages = [message]
        else:
            self.sent_messages.append(message)
        return message

    async def delete(self, *args, **kwargs):
        return

    async def set_permissions(self, *args, **kwargs):
        return


@dataclasses.dataclass(eq=False)
class Context(commands.Context):
    message = Message()
    channel = Channel()


@pytest.fixture()
def message():
    return Message()


@pytest.fixture()
def channel():
    return Channel([], {})


@pytest.fixture()
def ctx():
    return Context()


@pytest.fixture()
def reporter(bot):
    return cod4pugbot.domain.cogs.ReportSystem(bot=bot)


@pytest.fixture()
def updater(bot):
    return cod4pugbot.domain.cogs.Updater(bot=bot)
