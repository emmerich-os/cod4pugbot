import dataclasses
from unittest import mock

import discord
import freezegun
import pytest


TIME = "2020-01-01 00:00"


class Message(discord.Message):
    def __init__(
        self, id=None, content=None, embed=None, channel=None, *args, **kwargs
    ):
        self.id = id
        self.channel = channel
        self.embed = embed
        self.embeds = [embed] if embed is not None else []
        self.content = content
        self.args = [*args]
        self.kwargs = {**kwargs}
        self.emojis = []

    def __repr__(self):
        """Name when printing."""
        return (
            f"<{self.__class__.__name__} {self.id=} {self.embed=} {self.embeds=} {self.content=} "
            f"{self.args=} {self.kwargs=}>"
        )

    def __str__(self):
        """Name when formatted to string."""
        return (
            f"<{self.__class__.__name__} {self.id=} {self.embed=} {self.embeds=} {self.content=} "
            f"{self.args=} {self.kwargs=}>"
        )

    async def add_reaction(self, emoji):
        self.emojis.append(emoji)
        return emoji

    async def remove_reaction(self, emoji, member):
        self.emojis.remove(emoji)
        return emoji, member

    async def pin(self):
        return

    async def delete(self, *args, **kwargs):
        return

    async def edit(self, content=None, embed=None, *args, **kwargs):
        self.embed = embed
        self.embeds = [embed]
        self.content = content
        self.args = [*self.args, *args]
        self.kwargs = self.kwargs | kwargs


@dataclasses.dataclass(eq=False)
class User(discord.User):
    id: int = None
    name: str = None
    discriminator: str = None

    def __eq__(self, other):
        """Compare name to other name or other."""
        if isinstance(other, discord.User):
            return self.name == other.name
        return self.name == other


@dataclasses.dataclass(eq=False)
class Attachment:
    url: str = None


@dataclasses.dataclass(eq=False)
class Emoji(discord.Emoji):
    id: int = 0
    name = id
    animated = id

    def __eq__(self, other):
        """Compare id to other."""
        return self.id == other


@dataclasses.dataclass(eq=False)
class RawReaction(discord.RawReactionActionEvent):
    channel_id: str = None
    message_id: int = None
    user_id: int = None
    emoji: Emoji = Emoji()
    count: int = None
    member: User = User()


class TestReportSystem:
    @pytest.mark.parametrize(
        "channel_id,author,attachments,user,content,expected",
        [
            # 1. case: wrong channel, don't invoke command
            (
                "test_channel",
                User(name="test_author"),
                [],
                User(name="test_reported_user"),
                "",
                None,
            ),
            # 2. case: message by bot, don't invoke command
            (
                "test_support_report_channel_id",
                User(name="test_bot"),
                [],
                User(name="test_reported_user"),
                "",
                None,
            ),
            # 3. case: no attachments, don't invoke command
            (
                "test_support_report_channel_id",
                User(name="test_author"),
                [],
                User(name="test_reported_user"),
                "",
                None,
            ),
            # 4. case: no reason given, don't invoke command
            (
                "test_support_report_channel_id",
                User(name="test_author"),
                [Attachment("test_url")],
                User(name="test_reported_user"),
                "",
                None,
            ),
            # 5. case: no reason given, don't invoke command
            (
                "test_support_report_channel_id",
                User(name="test_author"),
                [Attachment("test_url")],
                User(name="test_reported_user"),
                "asd",
                None,
            ),
            # 6. case: everything correct, single attachment, invoke command
            (
                "test_support_report_channel_id",
                User(name="test_author"),
                [Attachment("test_url")],
                User(name="test_reported_user"),
                'reason="test_reason"',
                {
                    "color": 7500402,
                    "fields": [
                        {"inline": True, "name": "Reported member", "value": "<@None>"},
                        {"inline": True, "name": "Reported by", "value": "<@None>"},
                        {
                            "inline": False,
                            "name": "Reason",
                            "value": "```\ntest_reason\n```",
                        },
                        {
                            "inline": False,
                            "name": "Status",
                            "value": "```brainfuck\nPENDING\n```",
                        },
                    ],
                    "thumbnail": {"url": "test_url"},
                    "timestamp": "2020-01-01T00:00:00+00:00",
                    "title": "MEMBER REPORTED",
                    "type": "rich",
                },
            ),
            # 7. case: everything correct, multiple attachments, invoke command
            (
                "test_support_report_channel_id",
                User(name="test_author"),
                [
                    Attachment("test_url"),
                    Attachment("test_url_2"),
                    Attachment("test_url_3"),
                ],
                User(name="test_reported_user"),
                'reason="test_reason"',
                {
                    "color": 7500402,
                    "fields": [
                        {"inline": True, "name": "Reported member", "value": "<@None>"},
                        {"inline": True, "name": "Reported by", "value": "<@None>"},
                        {
                            "inline": False,
                            "name": "Reason",
                            "value": "```\ntest_reason\n```",
                        },
                        {
                            "inline": False,
                            "name": "Further attachments",
                            "value": "test_url_2\ntest_url_3",
                        },
                        {
                            "inline": False,
                            "name": "Status",
                            "value": "```brainfuck\nPENDING\n```",
                        },
                    ],
                    "thumbnail": {"url": "test_url"},
                    "timestamp": "2020-01-01T00:00:00+00:00",
                    "title": "MEMBER REPORTED",
                    "type": "rich",
                },
            ),
        ],
    )
    @freezegun.freeze_time(TIME)
    @pytest.mark.asyncio()
    async def test_report_member(
        self,
        monkeypatch,
        reporter,
        ctx,
        message,
        channel,
        channel_id,
        author,
        attachments,
        user,
        content,
        expected,
    ):
        channel.id = channel_id
        message.channel = channel
        message.author = author
        message.mentions = [user]
        message.attachments = attachments
        monkeypatch.setattr(
            message,
            "delete",
            delete := mock.AsyncMock(),
        )
        ctx.message = message
        message_ = Message()
        monkeypatch.setattr(
            channel,
            "send",
            send := mock.AsyncMock(return_value=message_),
        )
        monkeypatch.setattr(
            reporter.bot,
            "fetch_channel",
            mock.AsyncMock(return_value=channel),
        )

        await reporter.report_member.callback(reporter, ctx, user, content=content)

        if expected:
            result = send.call_args_list[0].kwargs["embed"].to_dict()
            assert result == expected
            delete.assert_called_once()
        else:
            delete.assert_not_called()

    @pytest.mark.parametrize(
        (
            "channel_id,reaction,message_author,message_embeds,emoji,user,reactions,expect_removal,"
            "expected_embed"
        ),
        [
            # 1. case: wrong channel
            (
                "test_channel",
                RawReaction(),
                User(name="test_bot"),
                [],
                Emoji(),
                User(name="test_user"),
                [],
                False,
                None,
            ),
            # 2. case: bot not message's author
            (
                "test_admin_report_channel_id",
                RawReaction(),
                User(name="test_user"),
                ["test_embed"],
                Emoji(),
                User(name="test_user"),
                [],
                False,
                None,
            ),
            # 3. case: reaction added by bot
            (
                "test_admin_report_channel_id",
                RawReaction(),
                User(name="test_bot"),
                ["test_embed"],
                Emoji(),
                User(name="test_bot"),
                [],
                False,
                None,
            ),
            # 4. case: message has no embeds
            (
                "test_admin_report_channel_id",
                RawReaction(),
                User(name="test_bot"),
                [],
                Emoji(),
                User(name="test_bot"),
                [],
                False,
                None,
            ),
            # 5. case: wrong emoji, remove reaction
            (
                "test_admin_report_channel_id",
                RawReaction(),
                User(name="test_bot"),
                ["test_embed"],
                Emoji(),
                User(name="test_user"),
                [],
                True,
                None,
            ),
            # 6. case: emoji coount greater than 2, remove reaction
            (
                "test_admin_report_channel_id",
                RawReaction(count=3),
                User(name="test_bot"),
                ["test_embed"],
                Emoji(),
                User(name="test_user"),
                [],
                True,
                None,
            ),
            # 7. case: everything correct
            (
                "test_admin_report_channel_id",
                RawReaction(),
                User(name="test_bot"),
                [
                    discord.Embed.from_dict(
                        {
                            "color": 7500402,
                            "fields": [
                                {
                                    "inline": True,
                                    "name": "Reported member",
                                    "value": "<@None>",
                                },
                                {
                                    "inline": True,
                                    "name": "Reported by",
                                    "value": "<@None>",
                                },
                                {
                                    "inline": False,
                                    "name": "Reason",
                                    "value": "```\ntest_reason\n```",
                                },
                                {
                                    "inline": False,
                                    "name": "Status",
                                    "value": "```brainfuck\nPENDING\n```",
                                },
                            ],
                            "thumbnail": {"url": "test_url"},
                            "timestamp": "2020-01-01T00:00:00+00:00",
                            "title": "MEMBER REPORTED",
                            "type": "rich",
                        }
                    )
                ],
                Emoji(id="\u2705"),
                User(name="test_user"),
                [],
                False,
                {
                    "color": 65280,
                    "fields": [
                        {"inline": True, "name": "Reported member", "value": "<@None>"},
                        {"inline": True, "name": "Reported by", "value": "<@None>"},
                        {
                            "inline": False,
                            "name": "Reason",
                            "value": "```\ntest_reason\n```",
                        },
                        {
                            "inline": False,
                            "name": "Status",
                            "value": "```css\nREVIEWED\n```",
                        },
                    ],
                    "thumbnail": {"url": "test_url"},
                    "timestamp": "2020-01-01T00:00:00+00:00",
                    "title": "MEMBER REPORTED",
                    "type": "rich",
                },
            ),
        ],
    )
    @pytest.mark.asyncio()
    async def test_on_raw_reaction_add(
        self,
        monkeypatch,
        reporter,
        channel,
        channel_id,
        reaction,
        message_author,
        message_embeds,
        emoji,
        user,
        reactions,
        expect_removal,
        expected_embed,
    ):
        message = Message(id="test_message_id")
        channel.id = channel_id
        message.channel = channel
        message.author = message_author
        message.reactions = reactions.copy()
        message.embeds = message_embeds.copy()
        monkeypatch.setattr(
            message,
            "edit",
            edit := mock.AsyncMock(),
        )
        monkeypatch.setattr(
            channel,
            "fetch_message",
            mock.AsyncMock(return_value=message),
        )
        monkeypatch.setattr(
            channel.guild,
            "fetch_member",
            mock.AsyncMock(return_value=user),
        )
        monkeypatch.setattr(
            channel.guild,
            "fetch_emoji",
            mock.AsyncMock(return_value=emoji),
        )
        monkeypatch.setattr(
            reporter.bot,
            "get_channel",
            mock.MagicMock(return_value=channel),
        )
        monkeypatch.setattr(
            discord.Reaction,
            "remove",
            remove := mock.AsyncMock(),
        )

        await reporter.on_raw_reaction_add(reaction)

        if expect_removal:
            remove.assert_called_once_with(user)
        else:
            remove.assert_not_called()
        if expected_embed:
            assert len(edit.call_args_list) == 1
            result = edit.call_args_list[0].kwargs["embed"].to_dict()
            assert result == expected_embed
        else:
            edit.assert_not_called()
