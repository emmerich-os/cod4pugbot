import dataclasses

import discord
import pytest

from cod4pugbot import testing
from cod4pugbot.domain.features.updater import cog as updater


@dataclasses.dataclass
class User(discord.User):
    id: int = 1
    name: str = "test_name"
    discriminator: str = "1111"


@dataclasses.dataclass
class Member(discord.Member):
    id: int = 1
    name: str = "test_name"
    nick: str = "test_nick"
    discriminator: str = "1111"


@pytest.fixture()
def cog(bot):
    return updater.Updater(bot=bot)


class TestUpdater:
    @pytest.mark.parametrize(
        ("before", "after", "expected"),
        [
            (Member(), Member(), {}),
            (Member(), Member(name="changed"), {"name": "changed"}),
            (Member(), Member(nick="changed"), {"nick": "changed"}),
            (Member(), Member(discriminator="2222"), {"discriminator": "2222"}),
            (User(), User(), {}),
            (User(), User(name="changed"), {"name": "changed"}),
            (User(), User(discriminator="2222"), {"discriminator": "2222"}),
        ],
    )
    @pytest.mark.asyncio()
    async def test_on_member_update(self, cog, before, after, expected):
        cog.bot.database_uow.existing_members = [
            testing.test_database_member().to_dict()
        ]

        await cog.on_member_update(before, after)

        if expected:
            [result] = cog.bot.database_uow.members.updated

            for key, value in expected.items():
                assert getattr(result, key) == value


@pytest.mark.parametrize(
    ("before", "after", "expected"),
    [
        (Member(), Member(), False),
        (Member(), Member(name="changed"), True),
        (Member(), Member(nick="changed"), True),
        (Member(), Member(discriminator="2222"), True),
        (User(), User(), False),
        (User(), User(name="changed"), True),
        (User(), User(discriminator="2222"), True),
    ],
)
def test_details_have_changed(before, after, expected):
    result = updater._details_have_changed(before, after)

    assert result == expected


@pytest.mark.parametrize(
    ("before", "after", "expected"),
    [
        (User(), User(), {}),
        (User(), User(name="changed"), {"name": "changed"}),
        (User(), User(discriminator="2222"), {"discriminator": "2222"}),
        (Member(), Member(), {}),
        (Member(), Member(name="changed"), {"name": "changed"}),
        (Member(), Member(nick="changed"), {"nick": "changed"}),
        (Member(), Member(discriminator="2222"), {"discriminator": "2222"}),
    ],
)
def test_get_changed_details(before, after, expected):
    result = updater._get_changed_details(before, after)

    assert result == expected
