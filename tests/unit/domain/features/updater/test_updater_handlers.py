from cod4pugbot import testing
from cod4pugbot.domain.features.updater import handlers


def test_create_updated_member():
    member = testing.test_member()
    details = {"name": "test_name_changed"}
    expected = testing.test_member(name="test_name_changed")

    result = handlers._create_updated_member(member, details)

    assert id(member) != id(expected)
    assert result == expected
