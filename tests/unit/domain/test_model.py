import dataclasses

import pytest

from cod4pugbot.domain.model import AbstractModel


@dataclasses.dataclass(eq=False)
class Fake(AbstractModel):
    test: str
    discord_id: int = 1

    @property
    def _key(self) -> str:
        return self.test


class TestAbstractModel:
    @pytest.mark.parametrize(
        ("first", "second", "expected"),
        [
            (Fake(test="test_1"), Fake(test="test_1"), True),
            (Fake(test="test_1"), Fake(test="test_2"), False),
        ],
    )
    def test_eq(self, first, second, expected):
        result = first == second

        assert result == expected

    @pytest.mark.parametrize(
        ("instance", "test_dict", "expected"),
        [
            (Fake(test="test_1"), {Fake(test="test_1"): None}, True),
            (Fake(test="test_1"), {Fake(test="test_2"): None}, False),
        ],
    )
    def test_hash(self, instance, test_dict, expected):
        result = instance in test_dict

        assert result == expected

    @pytest.mark.parametrize(
        ("instance", "rename_keys", "expected"),
        [
            (Fake(test="test_1"), False, {"test": "test_1", "discord_id": 1}),
            (Fake(test="test_1"), True, {"test": "test_1", "Discord ID": 1}),
        ],
    )
    def test_to_dict(self, instance, rename_keys, expected):
        result = instance.to_dict(rename_keys=rename_keys)

        assert result == expected
