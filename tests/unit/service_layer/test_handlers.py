import discord
import pytest

from cod4pugbot import testing
from cod4pugbot.domain import events
from cod4pugbot.service_layer import handlers


@pytest.mark.parametrize(
    "event",
    [
        events.CommandSuccessful(
            channel=testing.Channel(),
            member=testing.test_member(),
            message="test message",
            embed=None,
            file=None,
            files=None,
        ),
        events.CommandSuccessful(
            channel=testing.Channel(),
            member=testing.test_member(),
            message="test message",
            embed=discord.Embed(description="test_message"),
            file=None,
            files=None,
        ),
        events.CommandFailed(
            channel=testing.Channel(),
            member=testing.test_member(),
            message="test message",
            embed=None,
            file=None,
            files=None,
        ),
        events.CommandFailed(
            channel=testing.Channel(),
            member=testing.test_member(),
            message="test message",
            embed=discord.Embed(description="test_message"),
            file=None,
            files=None,
        ),
    ],
)
@pytest.mark.asyncio()
async def test_respond_to_channel_for_invoked_command(event):
    await handlers.respond_to_channel_for_invoked_command(event)

    messages_received = event.channel.messages_received
    assert messages_received
    assert len(messages_received) == 1

    [message] = messages_received
    assert message.kwargs.embed
