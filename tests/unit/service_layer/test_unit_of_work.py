import dataclasses

import pytest

from cod4pugbot.domain.events import Event
from cod4pugbot.service_layer import unit_of_work


@dataclasses.dataclass(frozen=True, eq=False)
class AnyEvent(Event):
    id: int

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self.id == other.id
        return NotImplemented

    def __hash__(self):
        return hash(self.id)


@pytest.fixture()
def abstract_uow():
    return unit_of_work.AbstractUnitOfWork()


class TestAbstractUnitOfWork:
    @pytest.mark.parametrize("events", [[AnyEvent(id=1), AnyEvent(id=1)]])
    def test_add_new_events(self, abstract_uow, events):
        abstract_uow.add_new_events(events)

        for event in events:
            assert event in abstract_uow.events

    @pytest.mark.parametrize("events", [[AnyEvent(id=1), AnyEvent(id=1)]])
    def test_collect_new_events(self, abstract_uow, events):
        abstract_uow.add_new_events(events)

        collected_events = abstract_uow.collect_new_events()
        result = list(collected_events)

        assert result == events
