import pathlib
from unittest import mock

import pytest
import yaml

import cod4pugbot.config


@pytest.mark.parametrize(
    "config,expected",
    [
        (
            "config_dict",
            {
                "test1": 1,
                "test2": "two",
                "test3": "three",
                "test4": 4,
                "test5": {
                    "test5a": 5,
                    "test5b": "5b",
                    "test5c": {
                        "test5ca": 5,
                        "test5cb": "5cb",
                    },
                },
            },
        ),
    ],
)
def test_get_env_vars(request, config, expected):
    d = request.getfixturevalue(config)
    cod4pugbot.config._get_env_vars(config=d)
    assert d == expected


def test_save_to_config(config_dict, mock_set_value, mock_get_env_vars):
    path = pathlib.Path("tests/unit/utils/resources/test.yml")
    with mock.patch("cod4pugbot.config.set_value") as set_value, mock.patch(
        "cod4pugbot.config._get_env_vars"
    ) as _get_env_vars:
        set_value.side_effect = mock_set_value
        _get_env_vars.side_effect = mock_get_env_vars
        cod4pugbot.config.save_to_config(
            keys=("test",), value="test", path=path.absolute()
        )
    with open(path.absolute()) as config:
        config_dict_2 = yaml.safe_load(config)
    assert config_dict == config_dict_2


def test_read_config(config_dict, mock_get_env_vars):
    path = pathlib.Path("tests/unit/utils/resources/test.yml")
    with mock.patch("cod4pugbot.config._get_env_vars") as _get_env_vars:
        _get_env_vars.side_effect = mock_get_env_vars
        config = cod4pugbot.config.read_config(path=path.absolute())
    assert isinstance(config, dict)
    assert config == config_dict


def test_set_value(config_dict, config_dict_2):
    cod4pugbot.config.set_value(
        d=config_dict, keys=("test5", "test5c", "test5cb"), value="fivecb"
    )
    cod4pugbot.config.set_value(d=config_dict, keys=("test6", "test6a"), value="sixa")
    assert isinstance(config_dict, dict)
    assert config_dict == config_dict_2
